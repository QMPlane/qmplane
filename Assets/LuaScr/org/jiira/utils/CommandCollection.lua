require "org.jiira.protobuf.protobuf_pb"
CommandCollection = {}
CommandCollection.ProtoTypeEnum = {
	CLogin = 0, -- 
		CLoginStr = "CLogin.Lua",
	CRegister = 1, -- 
		CRegisterStr = "CRegister.Lua",
	SRegister = 2, -- 
		SRegisterStr = "SRegister.Lua",
	SUserData = 3, -- 
		SUserDataStr = "SUserData.Lua",
	CReNickName = 4, -- 
		CReNickNameStr = "CReNickName.Lua",
	SReNickName = 5, -- 
		SReNickNameStr = "SReNickName.Lua",
	CIdentity = 6, -- 
		CIdentityStr = "CIdentity.Lua",
	SIdentity = 7, -- 
		SIdentityStr = "SIdentity.Lua",
	CGetCharacterList = 8, -- 
		CGetCharacterListStr = "CGetCharacterList.Lua",
	SGetCharacterList = 9, -- 
		SGetCharacterListStr = "SGetCharacterList.Lua",
	CFeelReward = 10, -- 
		CFeelRewardStr = "CFeelReward.Lua",
	SFeelReward = 11, -- 
		SFeelRewardStr = "SFeelReward.Lua",
	CItems = 12, -- 
		CItemsStr = "CItems.Lua",
	SItems = 13, -- 
		SItemsStr = "SItems.Lua",
	CUseItem = 14, -- 
		CUseItemStr = "CUseItem.Lua",
	CBaseReward = 15, -- 
		CBaseRewardStr = "CBaseReward.Lua",
	SBaseReward = 16, -- 
		SBaseRewardStr = "SBaseReward.Lua",
	CShowAdv = 17, -- 
		CShowAdvStr = "CShowAdv.Lua",
	SSingleUpdate = 18, -- 
		SSingleUpdateStr = "SSingleUpdate.Lua",
	CLevels = 19, -- 
		CLevelsStr = "CLevels.Lua",
	SLevels = 20, -- 
		SLevelsStr = "SLevels.Lua",
	CInses = 21, -- 
		CInsesStr = "CInses.Lua",
	SInses = 22, -- 
		SInsesStr = "SInses.Lua",
	CInsComplete = 23, -- 
		CInsCompleteStr = "CInsComplete.Lua",
	CInsRecord = 24, -- 
		CInsRecordStr = "CInsRecord.Lua",
	SInsRecord = 25, -- 
		SInsRecordStr = "SInsRecord.Lua",
	CInsRecords = 26, -- 
		CInsRecordsStr = "CInsRecords.Lua",
	SInsRecords = 27, -- 
		SInsRecordsStr = "SInsRecords.Lua",
	CMusic = 28, -- 
		CMusicStr = "CMusic.Lua",
	SMusic = 29, -- 
		SMusicStr = "SMusic.Lua",
	CPhotos = 30, -- 
		CPhotosStr = "CPhotos.Lua",
	SPhotos = 31, -- 
		SPhotosStr = "SPhotos.Lua",
	CGetPhoto = 32, -- 
		CGetPhotoStr = "CGetPhoto.Lua",
	CRank = 33, -- 
		CRankStr = "CRank.Lua",
	SRank = 34, -- 
		SRankStr = "SRank.Lua",
	SError = 35, -- 
		SErrorStr = "SError.Lua",
	CHeart = 36, -- 
		CHeartStr = "CHeart.Lua",
}
CommandCollection.SkillTypeEnum = {
	Trigger = 0, -- 主动技能
		TriggerStr = "Trigger.Lua",
	Passive = 1, -- 被动技能
		PassiveStr = "Passive.Lua",
}
CommandCollection.SkillBehaviorEnum = {
	Unlock = 0, -- 解锁
		UnlockStr = "Unlock.Lua",
	Damage = 1, -- 造成伤害
		DamageStr = "Damage.Lua",
	Multiple = 2, -- 增加上下分身
		MultipleStr = "Multiple.Lua",
	RepairLife = 3, -- 恢复生命
		RepairLifeStr = "RepairLife.Lua",
	EMP = 4, -- 连锁攻击（迪妮莎-连锁EMP）
		EMPStr = "EMP.Lua",
	MaxLife = 5, -- 增加最大生命值
		MaxLifeStr = "MaxLife.Lua",
	addPart = 6, -- 增加结算齿轮奖励百分比
		addPartStr = "addPart.Lua",
	addScore = 7, -- 增加结算分数奖励百分比
		addScoreStr = "addScore.Lua",
	addBaseReward = 8, -- 增加结算转盘奖励次数
		addBaseRewardStr = "addBaseReward.Lua",
}
CommandCollection.BigTypeEnum = {
	None = 0, -- 没有
		NoneStr = "None.Lua",
	Ban = 1, -- 禁止
		BanStr = "Ban.Lua",
	Feel = 2, -- 好感度
		FeelStr = "Feel.Lua",
	Coin = 3, -- 游戏币
		CoinStr = "Coin.Lua",
	Exp = 4, -- 经验
		ExpStr = "Exp.Lua",
	Power = 5, -- 当前血量
		PowerStr = "Power.Lua",
	MaxPower = 6, -- 最大血量
		MaxPowerStr = "MaxPower.Lua",
	Part = 7, -- 齿轮
		PartStr = "Part.Lua",
	Key = 8, -- 钥匙
		KeyStr = "Key.Lua",
	Character = 9, -- 机师类
		CharacterStr = "Character.Lua",
	Robot = 10, -- 机体类
		RobotStr = "Robot.Lua",
	Enemy = 11, -- 怪物类
		EnemyStr = "Enemy.Lua",
	TriggerSkill = 12, -- 主动技能类
		TriggerSkillStr = "TriggerSkill.Lua",
	PassiveSkill = 13, -- 被动技能类
		PassiveSkillStr = "PassiveSkill.Lua",
	FeelItem = 14, -- 心之盒道具类型
		FeelItemStr = "FeelItem.Lua",
	Item = 15, -- 普通道具类型
		ItemStr = "Item.Lua",
	Level = 16, -- 关卡类型
		LevelStr = "Level.Lua",
	FeelCount = 17, -- 好感度奖励数量类型
		FeelCountStr = "FeelCount.Lua",
	Photo = 18, -- 海报类
		PhotoStr = "Photo.Lua",
	Music = 19, -- 音乐类
		MusicStr = "Music.Lua",
	Ins = 20, -- Ins类
		InsStr = "Ins.Lua",
}
CommandCollection.AccountTypeEnum = {
	Offline = 0, -- 离线
		OfflineStr = "Offline.Lua",
	Online = 1, -- 在线
		OnlineStr = "Online.Lua",
	Error = 2, -- 异常
		ErrorStr = "Error.Lua",
}
CommandCollection.InstagramTypeEnum = {
	AllGame = 0, -- 所有游戏执行次数
		AllGameStr = "AllGame.Lua",
	AllLevelGame = 1, -- 通过所有关卡游戏次数
		AllLevelGameStr = "AllLevelGame.Lua",
	TargetLevelGame = 2, -- 通过指定关卡
		TargetLevelGameStr = "TargetLevelGame.Lua",
	CharacterFeel = 3, -- 机师好感度达到某值
		CharacterFeelStr = "CharacterFeel.Lua",
	Rank = 4, -- 排行榜名次
		RankStr = "Rank.Lua",
	CharacterShootDown = 5, -- 机师击坠数
		CharacterShootDownStr = "CharacterShootDown.Lua",
	ShowAdv = 6, -- 观看广告
		ShowAdvStr = "ShowAdv.Lua",
	MaxOnLine = 7, -- 连续登陆天数
		MaxOnLineStr = "MaxOnLine.Lua",
	MaxOnOff = 8, -- 连续未登录天数
		MaxOnOffStr = "MaxOnOff.Lua",
	UnLockCharacter = 9, -- 解锁指定机师
		UnLockCharacterStr = "UnLockCharacter.Lua",
	One = 10, -- 选择1号奖励
		OneStr = "One.Lua",
	Two = 11, -- 选择2号奖励
		TwoStr = "Two.Lua",
}
CommandCollection.GameTypeEnum = {
	ALL = 0, -- 所有游戏
		ALLStr = "ALL.Lua",
	Level = 1, -- 关卡游戏
		LevelStr = "Level.Lua",
	Ins = 2, -- 剧情关卡游戏
		InsStr = "Ins.Lua",
	Box = 3, -- 开宝箱(奖励)
		BoxStr = "Box.Lua",
	Training = 4, -- 训练场游戏
		TrainingStr = "Training.Lua",
	Difficult = 5, -- 挑战关卡游戏
		DifficultStr = "Difficult.Lua",
	Restore = 6, -- 恢复生命
		RestoreStr = "Restore.Lua",
	Gas = 7, -- 躲避毒气游戏
		GasStr = "Gas.Lua",
	Bird = 8, -- 小鸟飞行游戏
		BirdStr = "Bird.Lua",
	End = 9, -- 关卡章节终点
		EndStr = "End.Lua",
}
CommandCollection.ErrorCodeEnum = {
	SocketDisconnectError = 0, -- 与服务器断开连接
		SocketDisconnectErrorStr = "SocketDisconnectError.Lua",
	UserNameOrPassWordError = 1, -- 用户名或密码错误
		UserNameOrPassWordErrorStr = "UserNameOrPassWordError.Lua",
	AccountNotFoundError = 2, -- 用户不存在
		AccountNotFoundErrorStr = "AccountNotFoundError.Lua",
	NickNameExistError = 3, -- 昵称已存在
		NickNameExistErrorStr = "NickNameExistError.Lua",
	AccountIsFoundError = 4, -- 用户已存在
		AccountIsFoundErrorStr = "AccountIsFoundError.Lua",
	AccountCreateError = 5, -- 用户创建失败
		AccountCreateErrorStr = "AccountCreateError.Lua",
	AccountError = 6, -- 帐号异常
		AccountErrorStr = "AccountError.Lua",
	RepeatLoginError = 7, -- 帐号已在登录状态
		RepeatLoginErrorStr = "RepeatLoginError.Lua",
	AccountOfflineError = 8, -- 目标用户是离线状态
		AccountOfflineErrorStr = "AccountOfflineError.Lua",
	BalanceIsNotEnoughError = 9, -- 余额不足
		BalanceIsNotEnoughErrorStr = "BalanceIsNotEnoughError.Lua",
	DataBaseError = 10, -- 数据库操作失败
		DataBaseErrorStr = "DataBaseError.Lua",
	DataTableError = 11, -- 数据表异常
		DataTableErrorStr = "DataTableError.Lua",
	OutOfIndexError = 12, -- 越界异常[和后台联系]
		OutOfIndexErrorStr = "OutOfIndexError.Lua",
	CharacterDataError = 13, -- 机师数据异常
		CharacterDataErrorStr = "CharacterDataError.Lua",
	LackOfPartError = 14, -- 齿轮数量不足
		LackOfPartErrorStr = "LackOfPartError.Lua",
	PartToMysqlError = 15, -- 游戏币更新数据库失败
		PartToMysqlErrorStr = "PartToMysqlError.Lua",
	LackOfKeyError = 16, -- 钥匙数量不足
		LackOfKeyErrorStr = "LackOfKeyError.Lua",
	BuyError = 17, -- 购买失败
		BuyErrorStr = "BuyError.Lua",
	ReceiveError = 18, -- 领取失败
		ReceiveErrorStr = "ReceiveError.Lua",
	LackOfItemError = 19, -- 道具数量不足
		LackOfItemErrorStr = "LackOfItemError.Lua",
	IdentityError = 20, -- 身份证号错误
		IdentityErrorStr = "IdentityError.Lua",
	OnlineTimeOut = 21, -- 在线超时
		OnlineTimeOutStr = "OnlineTimeOut.Lua",
	HookError = 22, -- 客户端有使用外挂嫌疑
		HookErrorStr = "HookError.Lua",
}
CommandCollection.SingleUpdateTypeEnum = {
	None = 0, -- 无
		NoneStr = "None.Lua",
	Feel = 1, -- 机师好感度
		FeelStr = "Feel.Lua",
	Part = 2, -- 齿轮
		PartStr = "Part.Lua",
	Key = 3, -- 钥匙
		KeyStr = "Key.Lua",
	Music = 4, -- 音乐
		MusicStr = "Music.Lua",
	Photo = 5, -- 海报
		PhotoStr = "Photo.Lua",
	GameBird = 6, -- 飞行小鸟游戏次数
		GameBirdStr = "GameBird.Lua",
	GameGas = 7, -- 躲避毒气游戏次数
		GameGasStr = "GameGas.Lua",
	GameTraining = 8, -- 训练关卡胜利次数
		GameTrainingStr = "GameTraining.Lua",
	GameWin = 9, -- 关卡游戏胜利次数
		GameWinStr = "GameWin.Lua",
	GameLose = 10, -- 所有游戏失败次数
		GameLoseStr = "GameLose.Lua",
	Ins = 11, -- ins剧情变更
		InsStr = "Ins.Lua",
	ShowAdv = 12, -- 广告浏览次数
		ShowAdvStr = "ShowAdv.Lua",
	GSMaxRank = 13, -- 小游戏排名变更
		GSMaxRankStr = "GSMaxRank.Lua",
	Level = 14, -- 关卡步数变更
		LevelStr = "Level.Lua",
	Power = 15, -- 当前生命值变更
		PowerStr = "Power.Lua",
	ShootDown = 16, -- 击坠数变更
		ShootDownStr = "ShootDown.Lua",
	Item = 17, -- 道具变更（貌似只是好感度道具）
		ItemStr = "Item.Lua",
	InsRecord = 18, -- ins完成数变更
		InsRecordStr = "InsRecord.Lua",
	MaxPower = 19, -- 最大生命值变更
		MaxPowerStr = "MaxPower.Lua",
}
CommandCollection.Sock = ".sock";
CommandCollection.DB_ID_BASETABLE = "gamedb";
CommandCollection.EnableAutocommit = true;
CommandCollection.FISH_ROOM_MAX = 1000;
CommandCollection.FISH_ROOM_USER_MAX = 4;
CommandCollection.SOCK_TYPE_LENGTH = 2;
CommandCollection.SOCK_CONTEXT_LENGTH = 2;
CommandCollection.SOCK_HEAD_LENGTH = 4;
CommandCollection.UPLEVEL = 1000;
CommandCollection.None = 0;
CommandCollection.Ban = 1;
CommandCollection.Feel = 6;
CommandCollection.Coin = 22;
CommandCollection.Exp = 33;
CommandCollection.Power = 55;
CommandCollection.MaxPower = 56;
CommandCollection.Part = 66;
CommandCollection.Key = 88;
CommandCollection.dataModel = {}
CommandCollection.getDataModel = function(body)
	local model = CommandCollection.dataModel[body.type]
	model:ParseFromString(body.context)
	return model
end
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CLogin] = protobuf_pb.CLogin()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CRegister] = protobuf_pb.CRegister()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.SRegister] = protobuf_pb.SRegister()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.SUserData] = protobuf_pb.SUserData()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CReNickName] = protobuf_pb.CReNickName()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.SReNickName] = protobuf_pb.SReNickName()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CIdentity] = protobuf_pb.CIdentity()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.SIdentity] = protobuf_pb.SIdentity()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CGetCharacterList] = protobuf_pb.CGetCharacterList()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.SGetCharacterList] = protobuf_pb.SGetCharacterList()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CFeelReward] = protobuf_pb.CFeelReward()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.SFeelReward] = protobuf_pb.SFeelReward()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CItems] = protobuf_pb.CItems()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.SItems] = protobuf_pb.SItems()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CUseItem] = protobuf_pb.CUseItem()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CBaseReward] = protobuf_pb.CBaseReward()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.SBaseReward] = protobuf_pb.SBaseReward()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CShowAdv] = protobuf_pb.CShowAdv()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.SSingleUpdate] = protobuf_pb.SSingleUpdate()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CLevels] = protobuf_pb.CLevels()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.SLevels] = protobuf_pb.SLevels()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CInses] = protobuf_pb.CInses()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.SInses] = protobuf_pb.SInses()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CInsComplete] = protobuf_pb.CInsComplete()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CInsRecord] = protobuf_pb.CInsRecord()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.SInsRecord] = protobuf_pb.SInsRecord()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CInsRecords] = protobuf_pb.CInsRecords()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.SInsRecords] = protobuf_pb.SInsRecords()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CMusic] = protobuf_pb.CMusic()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.SMusic] = protobuf_pb.SMusic()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CPhotos] = protobuf_pb.CPhotos()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.SPhotos] = protobuf_pb.SPhotos()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CGetPhoto] = protobuf_pb.CGetPhoto()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CRank] = protobuf_pb.CRank()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.SRank] = protobuf_pb.SRank()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.SError] = protobuf_pb.SError()
CommandCollection.dataModel[CommandCollection.ProtoTypeEnum.CHeart] = protobuf_pb.CHeart()
CommandCollection.getBigTypeEnum = function(itemId)
	if itemId >= 0 and itemId <= 0 then
		return CommandCollection.BigTypeEnum.None
	elseif itemId >= 1 and itemId <= 1 then
		return CommandCollection.BigTypeEnum.Ban
	elseif itemId >= 6 and itemId <= 6 then
		return CommandCollection.BigTypeEnum.Feel
	elseif itemId >= 22 and itemId <= 22 then
		return CommandCollection.BigTypeEnum.Coin
	elseif itemId >= 33 and itemId <= 33 then
		return CommandCollection.BigTypeEnum.Exp
	elseif itemId >= 55 and itemId <= 55 then
		return CommandCollection.BigTypeEnum.Power
	elseif itemId >= 56 and itemId <= 56 then
		return CommandCollection.BigTypeEnum.MaxPower
	elseif itemId >= 66 and itemId <= 66 then
		return CommandCollection.BigTypeEnum.Part
	elseif itemId >= 88 and itemId <= 88 then
		return CommandCollection.BigTypeEnum.Key
	elseif itemId >= 1000 and itemId <= 1999 then
		return CommandCollection.BigTypeEnum.Character
	elseif itemId >= 2000 and itemId <= 2999 then
		return CommandCollection.BigTypeEnum.Robot
	elseif itemId >= 3000 and itemId <= 4999 then
		return CommandCollection.BigTypeEnum.Enemy
	elseif itemId >= 5000 and itemId <= 5499 then
		return CommandCollection.BigTypeEnum.TriggerSkill
	elseif itemId >= 5500 and itemId <= 5999 then
		return CommandCollection.BigTypeEnum.PassiveSkill
	elseif itemId >= 6000 and itemId <= 6099 then
		return CommandCollection.BigTypeEnum.FeelItem
	elseif itemId >= 6100 and itemId <= 6199 then
		return CommandCollection.BigTypeEnum.Item
	elseif itemId >= 6201 and itemId <= 7000 then
		return CommandCollection.BigTypeEnum.Level
	elseif itemId >= 7001 and itemId <= 7000 then
		return CommandCollection.BigTypeEnum.FeelCount
	elseif itemId >= 10000 and itemId <= 10999 then
		return CommandCollection.BigTypeEnum.Photo
	elseif itemId >= 11000 and itemId <= 11999 then
		return CommandCollection.BigTypeEnum.Music
	elseif itemId >= 50000 and itemId <= 50999 then
		return CommandCollection.BigTypeEnum.Ins
	else
		return CommandCollection.BigTypeEnum.None
	end
end