local graph
local gameObject
local transform
local acc = CS.Sacu.Utils.ActionCollection
local pte = CS.Sacu.Utils.ProtoTypeEnum
local cc = CS.Sacu.Utils.CommandCollection
local page
local Bg
local Bg1
local iconAtlasBG
local FightIngBtn
local DailyBtn
local PilotBtn
local SetBtn
local iconAtlasCommon
local isBool =false
--预存工厂
local isFactory = nil
local alPha--图片的alpha通道
local Effect_FightingUI
function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
end

--页面加载出来   绑定UI组件
function onStart(obj)
    isFactory = CS.Sacu.Utils.SAManager.Instance:startFactory("FightUIFactory") 
    Bg = CS.UnityEngine.GameObject.Find("UIRoot/cn_MainUI(Clone)/BgIma")
    Bg1 = CS.UnityEngine.GameObject.Find("UIRoot/cn_MainUI(Clone)/BgIma1")

    iconAtlasBG = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/BGAtlas")
    FightIngBtn = graph:getGameObjectByTypeName("AttackBtn","Button").gameObject
    DailyBtn = graph:getGameObjectByTypeName("DailyUI","Button").gameObject
    PilotBtn = graph:getGameObjectByTypeName("PilotBtn","Button").gameObject
    SetBtn = graph:getGameObjectByTypeName("SetBtn","Button").gameObject
    iconAtlasCommon = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/CommonBtnAtlas")
    Effect_FightingUI = graph:getGameObjectForName("Effect_FightingUI")
    --musicIma:GetComponent(typeof(CS.UnityEngine.UI.Image)).material:SetFloat("_Speed",0)
end

--注册监听事件
function onRegister()
	graph:addListenerButtonClick("DailyUI", DailyEvent)
	graph:addListenerButtonClick("AttackBtn", FightingEvent)
	graph:addListenerButtonClick("PilotBtn", PilotEvent)
	graph:addListenerButtonClick("SetBtn", SetEvent)
end

--日常按钮的监听事件
function DailyEvent(evt)
    if isFactory ~= CS.Sacu.Utils.SAManager.Instance:startFactory("DailyUIFactory") 
    then
        Effect_FightingUI:SetActive(false)
        FightIngBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_Campaign")
        PilotBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_Pilot")
        SetBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_SETTINGS")
        DailyBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_DAILY_1")
        -- local iconAtlasBG = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/BGAtlas")
        -- Bg:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasBG:GetSprite("cn_FightingBG")
        isFactory:disposeFactory()
        isFactory = CS.Sacu.Utils.SAManager.Instance:startFactory("DailyUIFactory")
     end
end

--作战按钮的监听事件
function FightingEvent(evt)
    if isFactory ~= CS.Sacu.Utils.SAManager.Instance:startFactory("FightUIFactory") 
    then
        Effect_FightingUI:SetActive(true)
        DailyBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_DAILY")
        PilotBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_Pilot")
        SetBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_SETTINGS")
        FightIngBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_Campaign_1")
        --Bg:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasBG:GetSprite("cn_FightingBG")
        --Bg1:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasBG:GetSprite("cn_FightingBG")
        Bg:SetActive(true)
        Bg:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasBG:GetSprite("cn_FightingBG")
        CS.MoveTarget._instance.IsBool = true
        isFactory:disposeFactory()
        CS.Sacu.Utils.SAManager.Instance:disposeFactory("DailyUIFactory")
        isFactory = CS.Sacu.Utils.SAManager.Instance:startFactory("FightUIFactory")
    end
end

--机师按钮的监听事件
function PilotEvent(evt)
    if isFactory ~= CS.Sacu.Utils.SAManager.Instance:startFactory("PilotUIFactory")
    then
        Effect_FightingUI:SetActive(false)
        --Bg:GetComponent(typeof(CS.UnityEngine.UI.Image)).material.SetFloat("_Alpha", 0);
        FightIngBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_Campaign")
        DailyBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_DAILY")
        SetBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_SETTINGS")
        PilotBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_Pilot_1")
        --Bg1:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasBG:GetSprite("cn_PilotBG")
        Bg:SetActive(true)
        Bg:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasBG:GetSprite("cn_PilotBG")
        CS.MoveTarget._instance.IsBool = true
        isFactory:disposeFactory()
        isFactory = CS.Sacu.Utils.SAManager.Instance:startFactory("PilotUIFactory")
    end
end

--选项按钮的监听事件
function SetEvent(evt)
    if isFactory ~= CS.Sacu.Utils.SAManager.Instance:startFactory("SetUIFactory")
    then
        Effect_FightingUI:SetActive(false)
        FightIngBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_Campaign")
        DailyBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_DAILY")
        PilotBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_Pilot")
        SetBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_SETTINGS_1")
        -- local iconAtlasBG = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/BGAtlas")
        -- Bg:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasBG:GetSprite("cn_FightingBG")
        isFactory:disposeFactory()
        isFactory = CS.Sacu.Utils.SAManager.Instance:startFactory("SetUIFactory")
    end
end
