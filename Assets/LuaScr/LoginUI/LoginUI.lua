local graph;
local gameObject;
local transform;

local cc = CS.Sacu.Utils.CommandCollection;
local su = CS.Sacu.Utils.SAUtils;
local ac = CS.Sacu.Collection.SAACollection;
local acc = CS.Sacu.Utils.ActionCollection;
local pte = CS.Sacu.Utils.ProtoTypeEnum;
local proto = CS.org.jiira.protobuf;
local errorenum = CS.Sacu.Utils.ErrorCodeEnum
local infoLbl
local sock
function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
	graph:addListenerButtonClick("BGIma/Input/SingleBtn", singleHandler)
	graph:addListenerButtonClick("BGIma/Input/SingleBtn (1)", singleRigster)
end

function onStart(obj)
	InfoLbl = graph:getGameObjectByTypeName("BGIma/Input/InfoLbl","Text");
	--设置帐号密码
	local pp = CS.UnityEngine.PlayerPrefs
	if pp.HasKey("SAUserName") then
		local userName = graph:getGameObjectByTypeName("BGIma/Input/UserNameTxt","InputField");
		local passWord = graph:getGameObjectByTypeName("BGIma/Input/PassWordTxt","InputField");
		userName.text = pp.GetString("SAUserName")
		passWord.text = pp.GetString("SAPassWord")
	end
end

function onRegister()
	graph:addEventDispatcherWithHandle(cc.Sock .. pte.SGetCharacterList:ToString(), "getSGetCharacterListHandler")
    graph:addEventDispatcherWithHandle(cc.Sock .. pte.SLevels:ToString(), "getSLevelHandler")
    graph:addEventDispatcherWithHandle(cc.Sock .. pte.SInses:ToString(), "getSInsesHandler")
	graph:addEventDispatcherWithHandle(cc.Sock .. pte.SItems:ToString(), "getSItems")
    graph:addEventDispatcherWithHandle(cc.Sock .. pte.SOldReceives:ToString(), "getSOldReceives")
	
    graph:addEventDispatcherWithHandle(cc.Sock .. pte.SInsRecords:ToString(), "getSInsRecords")
	-- graph:addEventDispatcherWithHandle(cc.Sock ..  pte.SError:ToString(),"userNameOrPassWordError")--用户名或密码错误
	-- graph:addEventDispatcherWithHandle(cc.Sock .. pte.SError:ToString(),"accountNotFoundError")--用户不存在
	-- graph:addEventDispatcherWithHandle(cc.Sock .. pte.SError:ToString(),"accountIsFoundError")--用户已存在
	CS.UnityEngine.Debug.Log(pte.SError)
end
function onRemove()
	graph:removeEventDispatcher(cc.Sock .. pte.SGetCharacterList:ToString())
    graph:removeEventDispatcher(cc.Sock .. pte.SLevels:ToString())
	graph:removeEventDispatcher(cc.Sock .. pte.SInses:ToString())
	graph:removeEventDispatcher(cc.Sock .. pte.SOldReceives:ToString())
    graph:removeEventDispatcher(cc.Sock .. pte.SItems:ToString())
	graph:removeEventDispatcher(cc.Sock .. pte.SInsRecords:ToString())
	graph:removeEventDispatcher(cc.Sock .. errorenum.UserNameOrPassWordError:ToString())--用户名或密码错误
	graph:removeEventDispatcher(cc.Sock .. errorenum.AccountNotFoundError:ToString())--用户不存在
	graph:removeEventDispatcher(cc.Sock .. errorenum.AccountIsFoundError:ToString())--用户已存在
    graph:removeEventDispatcher(cc.Sock .. ac.COMPLETE)
    graph:removeEventDispatcher(cc.Sock .. ac.ERROR)
end
function singleRigster(evt)
	graph:removeEventDispatcher(cc.Sock .. pte.SGetCharacterList:ToString())
    graph:removeEventDispatcher(cc.Sock .. pte.SLevels:ToString())
	graph:removeEventDispatcher(cc.Sock .. pte.SInses:ToString())
	graph:removeEventDispatcher(cc.Sock .. pte.SOldReceives:ToString())
    graph:removeEventDispatcher(cc.Sock .. pte.SItems:ToString())
	graph:removeEventDispatcher(cc.Sock .. pte.SInsRecords:ToString())
	graph:removeEventDispatcher(cc.Sock .. errorenum.UserNameOrPassWordError:ToString())--用户名或密码错误
	graph:removeEventDispatcher(cc.Sock .. errorenum.AccountNotFoundError:ToString())--用户不存在
	graph:removeEventDispatcher(cc.Sock .. errorenum.AccountIsFoundError:ToString())--用户已存在
	graph:removeEventDispatcher(cc.Sock .. ac.COMPLETE)
    graph:removeEventDispatcher(cc.Sock .. ac.ERROR)
	graph.factory:disposeFactory()
	CS.Sacu.Utils.SAManager.Instance:startFactory("RegisterUIFactory")
	
end
function singleHandler(evt)
	local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
	sock:close();
	connectHandle()
end

function loginHandle()
	local userName = graph:getGameObjectByTypeName("BGIma/Input/UserNameTxt","InputField").text;
	local passWord = graph:getGameObjectByTypeName("BGIma/Input/PassWordTxt","InputField").text;
	if #userName > 0 and #passWord > 0 then
		local pp = CS.UnityEngine.PlayerPrefs
		pp.SetString("SAUserName", userName)
		pp.SetString("SAPassWord", passWord)
		local login = cc.getDataModel(pte.CLogin);
		login:SetUserName(userName);
		login:SetPassWord(passWord);
		local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
		sock:sendMessage(pte.CLogin, login:Build():ToByteArray())
		su.Console("登陆请求已发送")
	else
		InfoLbl.text = "请输入帐号和密码";
		su.Console("登陆请求未发送")
	end
end

--连接服务器
function connectHandle()
	InfoLbl.text = "连接服务器......"
	graph:addEventDispatcherWithHandle(cc.Sock .. ac.COMPLETE, "connectComplete")
	graph:addEventDispatcherWithHandle(cc.Sock .. ac.ERROR, "connectError")
	su.Console(cc.Sock .. ac.COMPLETE)

	local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
	local version = CS.Sacu.Utils.SAGameVersion.Instance
	su.Console(version.ip .. ":" .. version.port)
	sock:connect(version.ip, version.port)--服务器
end

--服务器连接成功
function connectComplete(evt)
    InfoLbl.text = "服务器连接成功"
    graph:removeEventDispatcher(cc.Sock .. ac.COMPLETE)
    graph:removeEventDispatcher(cc.Sock .. ac.ERROR)
    loginHandle()
end

--服务器连接失败
function connectError(evt)
    graph:removeEventDispatcher(cc.Sock .. ac.COMPLETE)
    graph:removeEventDispatcher(cc.Sock .. ac.ERROR)
	InfoLbl.text = "网络环境不好,服务器连接失败,请重新连接"
end

function onRemove()
    graph:removeEventDispatcher(cc.Sock .. ac.COMPLETE)
    graph:removeEventDispatcher(cc.Sock .. ac.ERROR)
	graph:removeEventDispatcher(cc.Sock .. pte.SUserData:ToString())
end

function getUserInfoHandler(evt)
	local dao = evt.Body.bytes
	local userData = cc.getDataModel(pte.SUserData, dao);
	CS.Sacu.Utils.SAManager.Instance:startFactory("HeartFactory")
	--CS.Sacu.Utils.SAManager.Instance:startFactory("LoadingUIFactory")

	local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
    local cCharacter = cc.getDataModel(pte.CGetCharacterList);
    sock:sendMessage(pte.CGetCharacterList, cCharacter:Build():ToByteArray())
	--acc:setUserData(userData);
	--su.Log("用户ID : " .. userData.Id)
	
end
--===================================================收发消息  机师  关卡   ins   item  
function getSGetCharacterListHandler(evt)
    local dao = evt.Body.bytes
    cc.getDataModel(pte.SGetCharacterList, dao);
	local cLevel = cc.getDataModel(pte.CLevels);
	local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
    sock:sendMessage(pte.CLevels, cLevel:Build():ToByteArray())
end
function getSLevelHandler(evt)
    local dao = evt.Body.bytes
    cc.getDataModel(pte.SLevels, dao);
	local cInses = cc.getDataModel(pte.CInses);
	local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
    sock:sendMessage(pte.CInses, cInses:Build():ToByteArray())
end

function getSInsesHandler(evt)
    local dao = evt.Body.bytes
    cc.getDataModel(pte.SInses, dao);
    --graph:CheckIns()
    
    local cItems = cc.getDataModel(pte.CItems);
    local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
    sock:sendMessage(pte.CItems, cItems:Build():ToByteArray())
end
function getSItems(evt)
    local dao = evt.Body.bytes
    cc.getDataModel(pte.SItems, dao);
    local cOldReceives = cc.getDataModel(pte.COldReceives);
    local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
    sock:sendMessage(pte.COldReceives, cOldReceives:Build():ToByteArray())
end
function getSOldReceives(evt)
    local dao = evt.Body.bytes
    cc.getDataModel(pte.SOldReceives, dao);
    local cInsRecords = cc.getDataModel(pte.CInsRecords);
    local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
    sock:sendMessage(pte.CInsRecords, cInsRecords:Build():ToByteArray())
end
--============================================================= ins点赞数  回调  收到后直接开始主界面
function getSInsRecords(evt)
    local dao = evt.Body.bytes
	cc.getDataModel(pte.SInsRecords, dao)
	graph.factory:disposeFactory()
	--开启加载的工厂  将后续需要开启的工厂的图片掩盖
	CS.Sacu.Utils.SAManager.Instance:startFactory("MainUIFactory")
	--CS.Sacu.Utils.SAManager.Instance:startFactory("QNSActiveUIFactory")
	CS.Sacu.Utils.SAManager.Instance:startFactory("DailyUIFactory")
	--启动心跳
end

-- function userNameOrPassWordError(evt)
-- 	local dao = evt.Body.bytes
-- 	local sError = cc.getDataModel(pte.SError, dao)
-- 	print(sError.Code)
-- 	InfoLbl.text = "账户名或密码错误"
-- end

-- function accountNotFoundError(evt)
-- 	InfoLbl.text = "该账户不存在"
-- end

-- function accountIsFoundError(evt)
-- 	InfoLbl.text = "账户已存在"
-- end