local graph;
local gameObject;
local transform;
local lifeSlider
local timer
local timeout 
local stop
local player = CS.MiniPlayer.instance
local failedPanel
local fieldScore

local select1
local select2
local select3
local select4
local score
local lifecount

function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
end

--页面加载出来   绑定UI组件
function onStart(obj)
    CS.UnityEngine.Debug.Log("小游戏界面启动");

    -- local move1 = graph:getGameObjectByTypeName("Bottom/posGrid/Move1","Button");
    -- local move2 = graph:getGameObjectByTypeName("Bottom/posGrid/Move2","Button");
    -- local move3 = graph:getGameObjectByTypeName("Bottom/posGrid/Move3","Button");
    -- local move4 = graph:getGameObjectByTypeName("Bottom/posGrid/Move4","Button");
   
    lifeSlider = graph:getGameObjectByTypeName("Top/Life","Slider");
    failedPanel = graph:getGameObjectByTypeName("FailedPanel","RectTransform");
    -- winPanel = graph:getGameObjectByTypeName("WinPanel","RectTransform");
    
    lifecount = graph:getGameObjectByTypeName("Top/Life/lifecount","Text");
    score = graph:getGameObjectByTypeName("Top/score/scoreValue","Text");
    fieldScore = graph:getGameObjectByTypeName("FailedPanel/score","Text");
    select1 = graph:getGameObjectByTypeName("Bottom/posGrid/Move1/Select","RectTransform")
    select2 = graph:getGameObjectByTypeName("Bottom/posGrid/Move2/Select","RectTransform")
    select3 = graph:getGameObjectByTypeName("Bottom/posGrid/Move3/Select","RectTransform")
    select4 = graph:getGameObjectByTypeName("Bottom/posGrid/Move4/Select","RectTransform")

    failedPanel.gameObject:SetActive(false);
    
  
  
end

function ChangeLife()
    lifeSlider.value = player.curHP/player.maxHP;
    lifecount.text = player.curHP.. "/" .. player.maxHP
end
function ChangeScore()
    score.text = CS.Graphs.LittleGameUIGraphWorker.instance.score
end
--注册监听事件
function onRegister()
	graph:addListenerButtonClick("Bottom/posGrid/Move1", Move1)
	graph:addListenerButtonClick("Bottom/posGrid/Move2", Move2)
    graph:addListenerButtonClick("Bottom/posGrid/Move3", Move3)
    graph:addListenerButtonClick("Bottom/posGrid/Move4", Move4)
    graph:addListenerButtonClick("FailedPanel/restart",Restart)
    graph:addListenerButtonClick("FailedPanel/return",FailedReturn)
    -- graph:addListenerButtonClick("LuckyRoll/return",WinReturn)
    -- graph:addListenerButtonClick("WinPanel/GetReward",GetReward)
    -- graph:addListenerButtonClick("Top/Stop",TimeOut)
    -- graph:addEventDispatcherWithHandle("isWin", "openWin")
    graph:addEventDispatcherWithHandle("isFailed", "openFailed")
	-- graph:addListenerButtonClick("DailyBtn", DailyBtn)
	-- graph:addListenerButtonClick("FightBtn", FightBtn)
	-- graph:addListenerButtonClick("PilotBtn", PilotBtn)
	-- graph:addListenerButtonClick("SetBtn", SetBtn)
end
function onRemove()
    -- graph:removeEventDispatcher("isWin")
    graph:removeEventDispatcher("isFailed")
    
end
--战役按钮的监听事件
function Move1(evt)
    CS.MiniPlayer.instance:Move1()
    select1.gameObject:SetActive(true)
    select2.gameObject:SetActive(false)
    select3.gameObject:SetActive(false)
    select4.gameObject:SetActive(false)
end

--mini活动按钮的监听事件
function Move2(evt)
    CS.MiniPlayer.instance:Move2()
    select1.gameObject:SetActive(false)
    select2.gameObject:SetActive(true)
    select3.gameObject:SetActive(false)
    select4.gameObject:SetActive(false)
end

--击杀荣誉榜按钮的监听事件
function Move3(evt)
    CS.MiniPlayer.instance:Move3()
    select1.gameObject:SetActive(false)
    select2.gameObject:SetActive(false)
    select3.gameObject:SetActive(true)
    select4.gameObject:SetActive(false)
end
function Move4(evt)
    CS.MiniPlayer.instance:Move4()
    select1.gameObject:SetActive(false)
    select2.gameObject:SetActive(false)
    select3.gameObject:SetActive(false)
    select4.gameObject:SetActive(true)
end

function openFailed(evt)
    failedPanel.gameObject:SetActive(true);
    CS.Graphs.LittleGameUIGraphWorker.instance:FailedAnima()
    fieldScore.text = CS.Graphs.LittleGameUIGraphWorker.instance.score
end

function Restart(evt)
    CS.Graphs.LittleGameUIGraphWorker.instance:ReStart()
    failedPanel.gameObject:SetActive(false);
end
function FailedReturn(evt)
    failedPanel.gameObject:SetActive(false);
    CS.Sacu.Utils.SAManager.Instance:disposeFactory("LittleGameFactory")
    -- 小游戏给true为了移动格子
    CS.Sacu.Utils.SAManager.Instance:startFactory("MapFactory",true)
    CS.Graphs.LittleGameUIGraphWorker.instance:ReturnMusic();
    -- graph.factory:disposeFactory()
end
function GetReward(evt)
    
end

