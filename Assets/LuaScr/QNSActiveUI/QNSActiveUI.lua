local acc = CS.Sacu.Utils.ActionCollection
local Vector3 = CS.UnityEngine.Vector3
local Vector2 = CS.UnityEngine.Vector2
local Quaternion = CS.UnityEngine.Quaternion
local pte = CS.Sacu.Utils.ProtoTypeEnum;
local cc = CS.Sacu.Utils.CommandCollection;
local proto = CS.org.jiira.protobuf;
local ite = CS.Sacu.Utils.InstagramTypeEnum;
local graph
local sock
local dicUnlock--解锁的ins字典
local insDic_On--完成的ins字典
local insDic_Off--未完成的ins字典
local dicIns
local gameObject
local transform
local cellParent
local QNSCell
local prefab
local Inses
local isbool = true
local staticQNS
local dicValue--点赞数的list
local dicValue1
local dicValue2
local Bg
local musicBtn
local QNSBtn
local PngBtn
local iconAtlasCommon
function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
end
function onStart(obj)
    -- Bg = CS.UnityEngine.GameObject.Find("UIRoot/cn_MainUI(Clone)/BgIma")
    -- Bg1 = CS.UnityEngine.GameObject.Find("UIRoot/cn_MainUI(Clone)/BgIma1")

    -- musicBtn = CS.UnityEngine.GameObject.Find("UIRoot/cn_DailyUI(Clone)/DailyMusicBtn")
    -- QNSBtn = CS.UnityEngine.GameObject.Find("UIRoot/cn_DailyUI(Clone)/DailyInsBtn")
    -- PngBtn = CS.UnityEngine.GameObject.Find("UIRoot/cn_DailyUI(Clone)/DailyPngBtn")

    iconAtlasCommon = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/CommonBtnAtlas")
    -- QNSCell = graph:getGameObjectByTypeName("Scroll View/Viewport/Content/cn_QNSCellUI", "Image").gameObject;
    -- CellParent = graph:getGameObjectByTypeName("Scroll View/Viewport/Content", "GridLayoutGroup").gameObject;
    -- sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
    staticQNS = proto.STInstagram.getList()
    -- local cCharacter = cc.getDataModel(pte.CGetCharacterList);
    -- sock:sendMessage(pte.CGetCharacterList, cCharacter:Build():ToByteArray())
end

--注册监听事件
function onRegister()
    graph:addEventDispatcherWithHandle(cc.Sock .. pte.SGetCharacterList:ToString(), "getSGetCharacterListHandler")
    graph:addEventDispatcherWithHandle(cc.Sock .. pte.SLevels:ToString(), "getSLevelHandler")
    graph:addEventDispatcherWithHandle(cc.Sock .. pte.SInses:ToString(), "getSInsesHandler")
    graph:addEventDispatcherWithHandle(cc.Sock .. pte.SItems:ToString(), "getSItems")
    graph:addEventDispatcherWithHandle(cc.Sock .. pte.SInsRecords:ToString(), "getSInsRecords")
	graph:addEventDispatcherWithHandle(acc.DailyQNS .. acc.Close, "closePage")
	graph:addEventDispatcherWithHandle(acc.DailyQNS .. acc.Open, "openPage")
end
function onRemove()
    graph:removeEventDispatcher(cc.Sock .. pte.SGetCharacterList:ToString())
    graph:removeEventDispatcher(cc.Sock .. pte.SLevels:ToString())
    graph:removeEventDispatcher(cc.Sock .. pte.SInses:ToString())
    graph:removeEventDispatcher(acc.DailyQNS .. acc.Close)
    graph:removeEventDispatcher(acc.DailyQNS .. acc.Open)
end
--根据QNSID填写数据的方法
function activeQNS(arg1)
    local ID  = arg1
    --local cell = graph:LoadPrefabs(QNSCell,CellParent)
    local Panel = transform:Find("Panel").gameObject
    local BGIma = transform:Find("BGIma").gameObject

    local rightTxt = transform:Find("Panel/cn_QNSCellUI/RightTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
    local nameTxt = transform:Find("Panel/cn_QNSCellUI/NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
    local textTxt = transform:Find("Panel/cn_QNSCellUI/TextTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
    local UpTxt = transform:Find("Panel/cn_QNSCellUI/Zan"):GetComponent(typeof(CS.UnityEngine.UI.Text))
    local DownTxt = transform:Find("Panel/cn_QNSCellUI/TongQing"):GetComponent(typeof(CS.UnityEngine.UI.Text))
    local UpTxt_value = transform:Find("Panel/cn_QNSCellUI/UPTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
    local DownTxt_value = transform:Find("Panel/cn_QNSCellUI/DownTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
    local RightIma = transform:Find("Panel/cn_QNSCellUI/RightIma").gameObject
    local int1 = CS.System.Convert.ToInt32(ite.One);
    local int2 = CS.System.Convert.ToInt32(ite.Two);
    RightIma:SetActive(false)
    --遍历静态表将静态表的数据显示出来
    for key1, value1 in pairs(staticQNS) do
        if value1.Id== ID then
            rightTxt.text = value1.MissionName
            UpTxt.text = value1.PrizeName1
            DownTxt.text = value1.PrizeName2
            local txtList = graph:GetList(value1.MissionContent)
            local name_Arry = graph:GetSplitStr(value1.MissionContent[0])
            nameTxt.text = name_Arry[0]
            textTxt.text = name_Arry[1]
            if txtList.Count==2 then
                local commentCell1 = transform:Find("Panel/cn_QNSCellUI/AllCom/Comment").gameObject--下面评论的预设    
                commentCell1:SetActive(true)
                local name_Arry1 = graph:GetSplitStr(value1.MissionContent[1])
                
                local nameTxt1 = commentCell1.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                local textTxt1 = commentCell1.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                nameTxt1.text = name_Arry1[0]
                textTxt1.text = name_Arry1[1]
            elseif txtList.Count==3 then
                local commentCell1 = transform:Find("Panel/cn_QNSCellUI/AllCom/Comment").gameObject--下面评论的预设    
                local commentCell2 = transform:Find("Panel/cn_QNSCellUI/AllCom/Comment (1)").gameObject--下面评论的预设
                commentCell1:SetActive(true)
                commentCell2:SetActive(true)
                local name_Arry1 = graph:GetSplitStr(value1.MissionContent[1])
                local name_Arry2 = graph:GetSplitStr(value1.MissionContent[2])
                local nameTxt1 = commentCell1.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                local textTxt1 = commentCell1.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                nameTxt1.text = name_Arry1[0]
                textTxt1.text = name_Arry1[1]
                local nameTxt2 = commentCell2.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                local textTxt2 = commentCell2.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                nameTxt2.text = name_Arry2[0]
                textTxt2.text = name_Arry2[1]
            elseif txtList.Count==4 then
                local commentCell1 = transform:Find("Panel/cn_QNSCellUI/AllCom/Comment").gameObject--下面评论的预设    
                local commentCell2 = transform:Find("Panel/cn_QNSCellUI/AllCom/Comment (1)").gameObject--下面评论的预设
                local commentCell3 = transform:Find("Panel/cn_QNSCellUI/AllCom/Comment (2)").gameObject--下面评论的预设 
                commentCell1:SetActive(true)
                commentCell2:SetActive(true)
                commentCell3:SetActive(true)
                local name_Arry1 = graph:GetSplitStr(value1.MissionContent[1])
                local name_Arry2 = graph:GetSplitStr(value1.MissionContent[2])
                local name_Arry3 = graph:GetSplitStr(value1.MissionContent[3])
                local nameTxt1 = commentCell1.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                local textTxt1 = commentCell1.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                nameTxt1.text = name_Arry1[0]
                textTxt1.text = name_Arry1[1]
                local nameTxt2 = commentCell2.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                local textTxt2 = commentCell2.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                nameTxt2.text = name_Arry2[0]
                textTxt2.text = name_Arry2[1]
                local nameTxt3 = commentCell3.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                local textTxt3 = commentCell3.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                nameTxt3.text = name_Arry3[0]
                textTxt3.text = name_Arry3[1]
            elseif txtList.Count==5 then
                local commentCell1 = transform:Find("Panel/cn_QNSCellUI/AllCom/Comment").gameObject--下面评论的预设    
                local commentCell2 = transform:Find("Panel/cn_QNSCellUI/AllCom/Comment (1)").gameObject--下面评论的预设
                local commentCell3 = transform:Find("Panel/cn_QNSCellUI/AllCom/Comment (2)").gameObject--下面评论的预设    
                local commentCell4 = transform:Find("Panel/cn_QNSCellUI/AllCom/Comment (3)").gameObject--下面评论的预设
                local commentCell5 = transform:Find("Panel/cn_QNSCellUI/AllCom/Comment (4)").gameObject--下面评论的预设
                commentCell1:SetActive(true)
                commentCell2:SetActive(true)
                commentCell3:SetActive(true)
                commentCell4:SetActive(true)
                commentCell5:SetActive(true)
                local name_Arry1 = graph:GetSplitStr(value1.MissionContent[1])
                local name_Arry2 = graph:GetSplitStr(value1.MissionContent[2])
                local name_Arry3 = graph:GetSplitStr(value1.MissionContent[3])
                local name_Arry4 = graph:GetSplitStr(value1.MissionContent[4])
                local nameTxt1 = commentCell1.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                local textTxt1 = commentCell1.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                nameTxt1.text = name_Arry1[0]
                textTxt1.text = name_Arry1[1]
                local nameTxt2 = commentCell2.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                local textTxt2 = commentCell2.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                nameTxt2.text = name_Arry2[0]
                textTxt2.text = name_Arry2[1]
                local nameTxt3 = commentCell3.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                local textTxt3 = commentCell3.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                nameTxt3.text = name_Arry3[0]
                textTxt3.text = name_Arry3[1]
                local nameTxt4 = commentCell4.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                local textTxt4 = commentCell4.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                nameTxt4.text = name_Arry4[0]
                textTxt4.text = name_Arry4[1]
            else
                local commentCell1 = transform:Find("Panel/cn_QNSCellUI/AllCom/Comment").gameObject--下面评论的预设    
                local commentCell2 = transform:Find("Panel/cn_QNSCellUI/AllCom/Comment (1)").gameObject--下面评论的预设
                local commentCell3 = transform:Find("Panel/cn_QNSCellUI/AllCom/Comment (2)").gameObject--下面评论的预设    
                local commentCell4 = transform:Find("Panel/cn_QNSCellUI/AllCom/Comment (3)").gameObject--下面评论的预设
                local commentCell5 = transform:Find("Panel/cn_QNSCellUI/AllCom/Comment (4)").gameObject--下面评论的预设
                commentCell1:SetActive(true)
                commentCell2:SetActive(true)
                commentCell3:SetActive(true)
                commentCell4:SetActive(true)
                commentCell5:SetActive(true)
                local name_Arry1 = graph:GetSplitStr(value1.MissionContent[1])
                local name_Arry2 = graph:GetSplitStr(value1.MissionContent[2])
                local name_Arry3 = graph:GetSplitStr(value1.MissionContent[3])
                local name_Arry4 = graph:GetSplitStr(value1.MissionContent[4])
                local name_Arry5 = graph:GetSplitStr(value1.MissionContent[5])

                local nameTxt1 = commentCell1.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                local textTxt1 = commentCell1.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                nameTxt1.text = name_Arry1[0]
                textTxt1.text = name_Arry1[1]
                local nameTxt2 = commentCell2.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                local textTxt2 = commentCell2.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                nameTxt2.text = name_Arry2[0]
                textTxt2.text = name_Arry2[1]
                local nameTxt3 = commentCell3.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                local textTxt3 = commentCell3.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                nameTxt3.text = name_Arry3[0]
                textTxt3.text = name_Arry3[1]
                local nameTxt4 = commentCell4.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                local textTxt4 = commentCell4.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                nameTxt4.text = name_Arry4[0]
                textTxt4.text = name_Arry4[1]
                local nameTxt5 = commentCell5.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                local textTxt5 = commentCell5.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                nameTxt5.text = name_Arry5[0]
                textTxt5.text = name_Arry5[1]
            end
        end
    end
    transform:Find("Panel/cn_QNSCellUI/UPBtn"):GetComponent(typeof(CS.UnityEngine.UI.Button)).onClick:AddListener(function ()
            local cInsComplete = cc.getDataModel(pte.CInsComplete);
            -- print(value.Id)
            cInsComplete:SetIns(ID);
            cInsComplete:SetType(int1);
            local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
            sock:sendMessage(pte.CInsComplete, cInsComplete:Build():ToByteArray())
            -- dicUnlock:Remove(ID)
            -- insDic_On:Add(ID)
            RightIma:SetActive(false)
            Panel:SetActive(false)
            BGIma:SetActive(false)
    end)
    transform:Find("Panel/cn_QNSCellUI/DownBtn"):GetComponent(typeof(CS.UnityEngine.UI.Button)).onClick:AddListener(function ()
            local cInsComplete = cc.getDataModel(pte.CInsComplete);
                -- print(value.Id)
                cInsComplete:SetIns(ID);
                cInsComplete:SetType(int2);
                local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
                sock:sendMessage(pte.CInsComplete, cInsComplete:Build():ToByteArray())
                -- dicUnlock:Remove(ID)
                -- insDic_On:Add(ID)
                RightIma:SetActive(false)
                Panel:SetActive(false)
                BGIma:SetActive(false)
    end)
end
