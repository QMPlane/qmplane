local acc = CS.Sacu.Utils.ActionCollection
local Vector3 = CS.UnityEngine.Vector3
local Vector2 = CS.UnityEngine.Vector2
local Quaternion = CS.UnityEngine.Quaternion
local pte = CS.Sacu.Utils.ProtoTypeEnum;
local cc = CS.Sacu.Utils.CommandCollection;
local proto = CS.org.jiira.protobuf;
local ite = CS.Sacu.Utils.InstagramTypeEnum;
local single = CS.Sacu.Utils.SingleUpdateTypeEnum

local graph
local sock
local dicUnlock--解锁的ins字典
local insDic_On--完成的ins字典
local insDic_Off--未完成的ins字典
local dicIns
local gameObject
local transform
local cellParent
local QNSCell
local prefab
local Inses
local isbool = true
local staticQNS
local dicValue--点赞数的list
local dicValue1
local dicValue2
local Bg
local musicBtn
local QNSBtn
local PngBtn
local iconAtlasCommon
local CellTable = {}--定义一个存储Cell的表
local ItemID --完成的Ins点赞时要发送的ID
local ItemCount--完成的Ins点赞是要发送的数量
local arry_S--一个存着ID和数量的数组
local UPList = {}--要传出去的表
local staticChar--机师的静态表
local HeadImaAtlas--机师头像的图集
local QNSPngAtlas--QNS图片的图集
function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
end
function onStart(obj)
    Bg = CS.UnityEngine.GameObject.Find("UIRoot/cn_MainUI(Clone)/BgIma")
    Bg1 = CS.UnityEngine.GameObject.Find("UIRoot/cn_MainUI(Clone)/BgIma1")

    musicBtn = CS.UnityEngine.GameObject.Find("UIRoot/cn_DailyUI(Clone)/DailyMusicBtn")
    QNSBtn = CS.UnityEngine.GameObject.Find("UIRoot/cn_DailyUI(Clone)/DailyInsBtn")
    PngBtn = CS.UnityEngine.GameObject.Find("UIRoot/cn_DailyUI(Clone)/DailyPngBtn")

    iconAtlasCommon = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/CommonBtnAtlas")
    HeadImaAtlas = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/HeadImaAtlas")
    QNSPngAtlas = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/QNSPngAtlas")

    QNSCell = graph:getGameObjectByTypeName("Scroll View/Viewport/Content/cn_QNSCellUI", "Image").gameObject;
    CellParent = graph:getGameObjectByTypeName("Scroll View/Viewport/Content", "GridLayoutGroup").gameObject;
    sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
    staticQNS = proto.STInstagram.getList()
    staticChar = proto.STCharacter.getList()
    -- local cCharacter = cc.getDataModel(pte.CGetCharacterList);
    -- sock:sendMessage(pte.CGetCharacterList, cCharacter:Build():ToByteArray())
    -- local  ins  = graph.InsDic_On
    -- for key, value in pairs(ins) do
    --     print(key,value)
    -- end
end

--注册监听事件
function onRegister()
    -- graph:addEventDispatcherWithHandle(cc.Sock .. pte.SGetCharacterList:ToString(), "getSGetCharacterListHandler")
    -- graph:addEventDispatcherWithHandle(cc.Sock .. pte.SLevels:ToString(), "getSLevelHandler")
    -- graph:addEventDispatcherWithHandle(cc.Sock .. pte.SInses:ToString(), "getSInsesHandler")
    -- graph:addEventDispatcherWithHandle(cc.Sock .. pte.SItems:ToString(), "getSItems")
    -- graph:addEventDispatcherWithHandle(cc.Sock .. pte.SInsRecords:ToString(), "getSInsRecords")
	graph:addEventDispatcherWithHandle(acc.DailyQNS .. acc.Close, "closePage")
	graph:addEventDispatcherWithHandle(acc.DailyQNS .. acc.Open, "openPage")
end
function onRemove()
    graph:removeEventDispatcher(cc.Sock .. pte.SGetCharacterList:ToString())
    graph:removeEventDispatcher(cc.Sock .. pte.SLevels:ToString())
    graph:removeEventDispatcher(cc.Sock .. pte.SInses:ToString())
    graph:removeEventDispatcher(acc.DailyQNS .. acc.Close)
    graph:removeEventDispatcher(acc.DailyQNS .. acc.Open)
end

function activeCell()
    local int1 = CS.System.Convert.ToInt32(ite.One);
    local int2 = CS.System.Convert.ToInt32(ite.Two);
    dicUnlock = graph.InsDic_UnLock
    insDic_On =graph.InsDic_On
    insDic_Off = graph.InsDic_Off
    QNSCell:SetActive(true)
    if CellTable ~= nil then
        for key, value in pairs(CellTable) do
            CS.UnityEngine.Object.Destroy(value)
        end
    end
    CellTable = {}
    for key, value in pairs(dicUnlock) do--已经解锁的表
        local cell = graph:LoadPrefabs(QNSCell,CellParent)
        table.insert(CellTable,cell)
        local rightTxt = cell.transform:Find("RightTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        local nameTxt = cell.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        local textTxt = cell.transform:Find("TextTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        local ResTxt = cell.transform:Find("ResTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        local UpTxt = cell.transform:Find("Zan"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        local DownTxt = cell.transform:Find("TongQing"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        local UpTxt_value = cell.transform:Find("UPTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        local DownTxt_value = cell.transform:Find("DownTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        local RightIma = cell.transform:Find("RightIma").gameObject
        local HeadIma = cell.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
        local QNSPng = cell.transform:Find("ImaPht"):GetComponent(typeof(CS.UnityEngine.UI.Image))
        for key1, value1 in pairs(staticQNS)
        do
            if value1.Id== key then
                QNSPng.sprite = QNSPngAtlas:GetSprite("cn_"..value1.Id)
                rightTxt.text =value1.MissionName
                UpTxt.text = value1.PrizeName1
                DownTxt.text = value1.PrizeName2
                local txtList = graph:GetList(value1.MissionContent)
                local name_Arry = graph:GetSplitStr(value1.MissionContent[0])
                nameTxt.text = name_Arry[0]
               for key_cha, value_cha in pairs(staticChar) do
                print(value_cha.Intro)

                if name_Arry[0] == value_cha.Name then
                    HeadIma.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                    local intro = graph:GetSplitStr(value_cha.Intro) 
                    ResTxt.text = intro[0]
                end
               end
                textTxt.text = name_Arry[1]
                --print(value1.MissionContent.Length)
                if txtList.Count==2 then
                    local commentCell1 = cell.transform:Find("AllCom/Comment").gameObject--下面评论的预设    
                    commentCell1:SetActive(true)
                    local name_Arry1 = graph:GetSplitStr(value1.MissionContent[1])
                    
                    local nameTxt1 = commentCell1.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt1 = commentCell1.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt1.text = name_Arry1[0]
                    textTxt1.text = "[ "..name_Arry1[1].." ]"
                    local HeadIma1 = commentCell1.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    for key_cha, value_cha in pairs(staticChar) do
                        if name_Arry1[0] == value_cha.Name then
                            HeadIma1.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        activePng(name_Arry1[0],HeadIma1)

                    end
                elseif txtList.Count==3 then
                    local commentCell1 = cell.transform:Find("AllCom/Comment").gameObject--下面评论的预设    
                    local commentCell2 = cell.transform:Find("AllCom/Comment (1)").gameObject--下面评论的预设
                    commentCell1:SetActive(true)
                    commentCell2:SetActive(true)
                    local name_Arry1 = graph:GetSplitStr(value1.MissionContent[1])
                    local name_Arry2 = graph:GetSplitStr(value1.MissionContent[2])
                    local nameTxt1 = commentCell1.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt1 = commentCell1.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt1.text = name_Arry1[0]
                    textTxt1.text = "[ "..name_Arry1[1].." ]"
                    local nameTxt2 = commentCell2.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt2 = commentCell2.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt2.text = name_Arry2[0]
                    textTxt2.text = "[ "..name_Arry2[1].." ]"
                    local HeadIma1 = commentCell1.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma2 = commentCell2.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    for key_cha, value_cha in pairs(staticChar) do
                        if name_Arry1[0] == value_cha.Name then
                            HeadIma1.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry2[0] == value_cha.Name then
                            HeadIma2.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        activePng(name_Arry1[0],HeadIma1)
                        activePng(name_Arry2[0],HeadIma2)
                    end
                elseif txtList.Count==4 then
                    local commentCell1 = cell.transform:Find("AllCom/Comment").gameObject--下面评论的预设    
                    local commentCell2 = cell.transform:Find("AllCom/Comment (1)").gameObject--下面评论的预设
                    local commentCell3 = cell.transform:Find("AllCom/Comment (2)").gameObject--下面评论的预设 
                    commentCell1:SetActive(true)
                    commentCell2:SetActive(true)
                    commentCell3:SetActive(true)
                    local name_Arry1 = graph:GetSplitStr(value1.MissionContent[1])
                    local name_Arry2 = graph:GetSplitStr(value1.MissionContent[2])
                    local name_Arry3 = graph:GetSplitStr(value1.MissionContent[3])
                    local nameTxt1 = commentCell1.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt1 = commentCell1.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt1.text = name_Arry1[0]
                    textTxt1.text = "[ "..name_Arry1[1].." ]"
                    local nameTxt2 = commentCell2.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt2 = commentCell2.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt2.text = name_Arry2[0]
                    textTxt2.text = "[ "..name_Arry2[1].." ]"
                    local nameTxt3 = commentCell3.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt3 = commentCell3.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt3.text = name_Arry3[0]
                    textTxt3.text = "[ "..name_Arry3[1].." ]"
                    local HeadIma1 = commentCell1.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma2 = commentCell2.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma3 = commentCell3.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    for key_cha, value_cha in pairs(staticChar) do
                        if name_Arry1[0] == value_cha.Name then
                            HeadIma1.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry2[0] == value_cha.Name then
                            HeadIma2.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry3[0] == value_cha.Name then
                            HeadIma3.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        activePng(name_Arry1[0],HeadIma1)
                        activePng(name_Arry2[0],HeadIma2)
                        activePng(name_Arry3[0],HeadIma3)

                    end
                elseif txtList.Count==5 then
                    local commentCell1 = cell.transform:Find("AllCom/Comment").gameObject--下面评论的预设    
                    local commentCell2 = cell.transform:Find("AllCom/Comment (1)").gameObject--下面评论的预设
                    local commentCell3 = cell.transform:Find("AllCom/Comment (2)").gameObject--下面评论的预设    
                    local commentCell4 = cell.transform:Find("AllCom/Comment (3)").gameObject--下面评论的预设
                    commentCell1:SetActive(true)
                    commentCell2:SetActive(true)
                    commentCell3:SetActive(true)
                    commentCell4:SetActive(true)
                    local name_Arry1 = graph:GetSplitStr(value1.MissionContent[1])
                    local name_Arry2 = graph:GetSplitStr(value1.MissionContent[2])
                    local name_Arry3 = graph:GetSplitStr(value1.MissionContent[3])
                    local name_Arry4 = graph:GetSplitStr(value1.MissionContent[4])
                    local nameTxt1 = commentCell1.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt1 = commentCell1.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt1.text = name_Arry1[0]
                    textTxt1.text = "[ "..name_Arry1[1].." ]"
                    local nameTxt2 = commentCell2.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt2 = commentCell2.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt2.text = name_Arry2[0]
                    textTxt2.text = "[ "..name_Arry2[1].." ]"
                    local nameTxt3 = commentCell3.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt3 = commentCell3.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt3.text = name_Arry3[0]
                    textTxt3.text = "[ "..name_Arry3[1].." ]"
                    local nameTxt4 = commentCell4.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt4 = commentCell4.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt4.text = name_Arry4[0]
                    textTxt4.text = "[ "..name_Arry4[1].." ]"
                    local HeadIma1 = commentCell1.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma2 = commentCell2.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma3 = commentCell3.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma4 = commentCell4.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    for key_cha, value_cha in pairs(staticChar) do
                        if name_Arry1[0] == value_cha.Name then
                            HeadIma1.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry2[0] == value_cha.Name then
                            HeadIma2.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry3[0] == value_cha.Name then
                            HeadIma3.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry4[0] == value_cha.Name then
                            HeadIma4.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        activePng(name_Arry1[0],HeadIma1)
                        activePng(name_Arry2[0],HeadIma2)
                        activePng(name_Arry3[0],HeadIma3)
                        activePng(name_Arry4[0],HeadIma4)
                    end
                else
                    local commentCell1 = cell.transform:Find("AllCom/Comment").gameObject--下面评论的预设    
                    local commentCell2 = cell.transform:Find("AllCom/Comment (1)").gameObject--下面评论的预设
                    local commentCell3 = cell.transform:Find("AllCom/Comment (2)").gameObject--下面评论的预设    
                    local commentCell4 = cell.transform:Find("AllCom/Comment (3)").gameObject--下面评论的预设
                    local commentCell5 = cell.transform:Find("AllCom/Comment (4)").gameObject--下面评论的预设
                    commentCell1:SetActive(true)
                    commentCell2:SetActive(true)
                    commentCell3:SetActive(true)
                    commentCell4:SetActive(true)
                    commentCell5:SetActive(true)
                    local name_Arry1 = graph:GetSplitStr(value1.MissionContent[1])
                    local name_Arry2 = graph:GetSplitStr(value1.MissionContent[2])
                    local name_Arry3 = graph:GetSplitStr(value1.MissionContent[3])
                    local name_Arry4 = graph:GetSplitStr(value1.MissionContent[4])
                    local name_Arry5 = graph:GetSplitStr(value1.MissionContent[5])

                    local nameTxt1 = commentCell1.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt1 = commentCell1.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt1.text = name_Arry1[0]
                    textTxt1.text = "[ "..name_Arry1[1].." ]"
                    local nameTxt2 = commentCell2.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt2 = commentCell2.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt2.text = name_Arry2[0]
                    textTxt2.text = "[ "..name_Arry2[1].." ]"
                    local nameTxt3 = commentCell3.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt3 = commentCell3.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt3.text = name_Arry3[0]
                    textTxt3.text = "[ "..name_Arry3[1].." ]"
                    local nameTxt4 = commentCell4.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt4 = commentCell4.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt4.text = name_Arry4[0]
                    textTxt4.text = "[ "..name_Arry4[1].." ]"
                    local nameTxt5 = commentCell5.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt5 = commentCell5.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt5.text = name_Arry5[0]
                    textTxt5.text = "[ "..name_Arry5[1].." ]"
                    local HeadIma1 = commentCell1.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma2 = commentCell2.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma3 = commentCell3.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma4 = commentCell4.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma5 = commentCell5.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))

                    for key_cha, value_cha in pairs(staticChar) do
                        if name_Arry1[0] == value_cha.Name then
                            HeadIma1.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry2[0] == value_cha.Name then
                            HeadIma2.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry3[0] == value_cha.Name then
                            HeadIma3.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry4[0] == value_cha.Name then
                            HeadIma4.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry5[0] == value_cha.Name then
                            HeadIma5.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        activePng(name_Arry1[0],HeadIma1)
                        activePng(name_Arry2[0],HeadIma2)
                        activePng(name_Arry3[0],HeadIma3)
                        activePng(name_Arry4[0],HeadIma4)
                        activePng(name_Arry5[0],HeadIma5)

                    end
                end
                end
        end
        dicValue1 = graph.InsDic_Value1
        dicValue2 = graph.InsDic_Value2
        for key_Value1, value_Value1 in pairs(dicValue1) do
            if key_Value1 == key then
                UpTxt_value.text = value_Value1.."次"

            end
        end
        for key_Value2, value_Value2 in pairs(dicValue2) do
            if key_Value2 == key then
                DownTxt_value.text = value_Value2.."次"
            end
        end
        cell.transform:Find("UPBtn"):GetComponent(typeof(CS.UnityEngine.UI.Button)).onClick:AddListener(function ()
            --先检测字典里面是否包含键  如果包含发送  不包含就弹出已经领取的框
            if dicUnlock:ContainsKey(value.Id) then
                local cInsComplete = cc.getDataModel(pte.CInsComplete);
                print(value.Id)
                cInsComplete:SetIns(value.Id);
                cInsComplete:SetType(int1);
                local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
                sock:sendMessage(pte.CInsComplete, cInsComplete:Build():ToByteArray())
                for key_S, value_S in pairs(staticQNS) do
                    if value_S.Id == value.Id then
                        arry_S = graph:GetSplitStr_(value_S.Prize1[0])
                        ItemID= tonumber(arry_S[0])
                        ItemCount = tonumber(arry_S[1])
                        graph:AddInt(ItemID,ItemCount)
                    end
                end
                -- dicUnlock:Remove(value.Id)
                -- insDic_On:Add(value.Id)
                RightIma:SetActive(false)
            else
                print("已经领取过")
            end
        end)
        cell.transform:Find("DownBtn"):GetComponent(typeof(CS.UnityEngine.UI.Button)).onClick:AddListener(function ()
            if dicUnlock:ContainsKey(value.Id) then
                local cInsComplete = cc.getDataModel(pte.CInsComplete);
                -- print(value.Id)
                cInsComplete:SetIns(value.Id);
                cInsComplete:SetType(int2);
                local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
                sock:sendMessage(pte.CInsComplete, cInsComplete:Build():ToByteArray())
                dicUnlock:Remove(value.Id)
                insDic_On:Add(value.Id)
                for key_S, value_S in pairs(staticQNS) do
                    if value_S.Id == value.Id then
                        arry_S = graph:GetSplitStr_(value_S.Prize1[0])
                        ItemID= tonumber(arry_S[0])
                        ItemCount = tonumber(arry_S[1])
                        graph:AddInt(ItemID,ItemCount)
                    end
                end
                RightIma:SetActive(false)
            else
                print("已经领取过")
            end
        end)
    end
    for key, value in pairs(insDic_On) do--已经完成的表
        local cell = graph:LoadPrefabs(QNSCell,CellParent)
        table.insert(CellTable,cell)
        local rightTxt = cell.transform:Find("RightTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        local nameTxt = cell.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        local textTxt = cell.transform:Find("TextTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        local ResTxt = cell.transform:Find("ResTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        local UpTxt = cell.transform:Find("Zan"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        local DownTxt = cell.transform:Find("TongQing"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        local UpTxt_value = cell.transform:Find("UPTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        local DownTxt_value = cell.transform:Find("DownTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        local RightIma = cell.transform:Find("RightIma").gameObject
        local HeadIma = cell.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
        local QNSPng = cell.transform:Find("ImaPht"):GetComponent(typeof(CS.UnityEngine.UI.Image))
        RightIma:SetActive(false)
        for key1, value1 in pairs(staticQNS) do
            if value1.Id== key then
                QNSPng.sprite = QNSPngAtlas:GetSprite("cn_"..value1.Id)
                rightTxt.text = value1.MissionName
                UpTxt.text = value1.PrizeName1
                DownTxt.text = value1.PrizeName2
                local txtList = graph:GetList(value1.MissionContent)
                local name_Arry = graph:GetSplitStr(value1.MissionContent[0])
                nameTxt.text = name_Arry[0]
                for key_cha, value_cha in pairs(staticChar) do
                    if name_Arry[0] == value_cha.Name then
                        HeadIma.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        local intro = graph:GetSplitStr(value_cha.Intro) 
                        ResTxt.text = intro[0]
                    end
                end
                textTxt.text = name_Arry[1]
                if txtList.Count==2 then
                    local commentCell1 = cell.transform:Find("AllCom/Comment").gameObject--下面评论的预设
                    commentCell1:SetActive(true)
                    local name_Arry1 = graph:GetSplitStr(value1.MissionContent[1])
                    local nameTxt1 = commentCell1.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt1 = commentCell1.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt1.text = name_Arry1[0]
                    local HeadIma1 = commentCell1.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    for key_cha, value_cha in pairs(staticChar) do
                        if name_Arry1[0] == value_cha.Name then
                            HeadIma1.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        activePng(name_Arry1[0],HeadIma1)
                    end
                    textTxt1.text = "[ "..name_Arry1[1].." ]"
                elseif txtList.Count==3 then
                    local commentCell1 = cell.transform:Find("AllCom/Comment").gameObject--下面评论的预设    
                    local commentCell2 = cell.transform:Find("AllCom/Comment (1)").gameObject--下面评论的预设
                    commentCell1:SetActive(true)
                    commentCell2:SetActive(true)
                    local name_Arry1 = graph:GetSplitStr(value1.MissionContent[1])
                    local name_Arry2 = graph:GetSplitStr(value1.MissionContent[2])
                    local nameTxt1 = commentCell1.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt1 = commentCell1.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt1.text = name_Arry1[0]
                    
                    textTxt1.text = "[ "..name_Arry1[1].." ]"
                    local nameTxt2 = commentCell2.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt2 = commentCell2.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt2.text = name_Arry2[0]
                    textTxt2.text = "[ "..name_Arry2[1].." ]"
                    local HeadIma1 = commentCell1.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma2 = commentCell2.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    for key_cha, value_cha in pairs(staticChar) do
                        if name_Arry1[0] == value_cha.Name then
                            HeadIma1.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry2[0] == value_cha.Name then
                            HeadIma2.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        activePng(name_Arry1[0],HeadIma1)
                        activePng(name_Arry2[0],HeadIma2)
                    end
                elseif txtList.Count==4 then
                    local commentCell1 = cell.transform:Find("AllCom/Comment").gameObject--下面评论的预设    
                    local commentCell2 = cell.transform:Find("AllCom/Comment (1)").gameObject--下面评论的预设
                    local commentCell3 = cell.transform:Find("AllCom/Comment (2)").gameObject--下面评论的预设 
                    commentCell1:SetActive(true)
                    commentCell2:SetActive(true)
                    commentCell3:SetActive(true)
                    local name_Arry1 = graph:GetSplitStr(value1.MissionContent[1])
                    local name_Arry2 = graph:GetSplitStr(value1.MissionContent[2])
                    local name_Arry3 = graph:GetSplitStr(value1.MissionContent[3])
                    local nameTxt1 = commentCell1.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt1 = commentCell1.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt1.text = name_Arry1[0]
                    textTxt1.text = "[ "..name_Arry1[1].." ]"
                    local nameTxt2 = commentCell2.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt2 = commentCell2.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt2.text = name_Arry2[0]
                    textTxt2.text = "[ "..name_Arry2[1].." ]"
                    local nameTxt3 = commentCell3.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt3 = commentCell3.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt3.text = name_Arry3[0]
                    textTxt3.text = "[ "..name_Arry3[1].." ]"
                    local HeadIma1 = commentCell1.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma2 = commentCell2.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma3 = commentCell3.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))

                    for key_cha, value_cha in pairs(staticChar) do
                        if name_Arry1[0] == value_cha.Name then
                            HeadIma1.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry2[0] == value_cha.Name then
                            HeadIma2.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry3[0] == value_cha.Name then
                            HeadIma3.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        activePng(name_Arry1[0],HeadIma1)
                        activePng(name_Arry2[0],HeadIma2)
                        activePng(name_Arry3[0],HeadIma3)

                    end
                elseif txtList.Count==5 then
                    local commentCell1 = cell.transform:Find("AllCom/Comment").gameObject--下面评论的预设    
                    local commentCell2 = cell.transform:Find("AllCom/Comment (1)").gameObject--下面评论的预设
                    local commentCell3 = cell.transform:Find("AllCom/Comment (2)").gameObject--下面评论的预设    
                    local commentCell4 = cell.transform:Find("AllCom/Comment (3)").gameObject--下面评论的预设
                    local commentCell5 = cell.transform:Find("AllCom/Comment (4)").gameObject--下面评论的预设
                    commentCell1:SetActive(true)
                    commentCell2:SetActive(true)
                    commentCell3:SetActive(true)
                    commentCell4:SetActive(true)
                    commentCell5:SetActive(true)
                    local name_Arry1 = graph:GetSplitStr(value1.MissionContent[1])
                    local name_Arry2 = graph:GetSplitStr(value1.MissionContent[2])
                    local name_Arry3 = graph:GetSplitStr(value1.MissionContent[3])
                    local name_Arry4 = graph:GetSplitStr(value1.MissionContent[4])
                    local nameTxt1 = commentCell1.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt1 = commentCell1.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt1.text = name_Arry1[0]
                    textTxt1.text = "[ "..name_Arry1[1].." ]"
                    local nameTxt2 = commentCell2.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt2 = commentCell2.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt2.text = name_Arry2[0]
                    textTxt2.text = "[ "..name_Arry2[1].." ]"
                    local nameTxt3 = commentCell3.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt3 = commentCell3.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt3.text = name_Arry3[0]
                    textTxt3.text = "[ "..name_Arry3[1].." ]"
                    local nameTxt4 = commentCell4.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt4 = commentCell4.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt4.text = name_Arry4[0]
                    textTxt4.text = "[ "..name_Arry4[1].." ]"
                    local HeadIma1 = commentCell1.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma2 = commentCell2.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma3 = commentCell3.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma4 = commentCell4.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))

                    for key_cha, value_cha in pairs(staticChar) do
                        if name_Arry1[0] == value_cha.Name then
                            HeadIma1.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry2[0] == value_cha.Name then
                            HeadIma2.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry3[0] == value_cha.Name then
                            HeadIma3.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry4[0] == value_cha.Name then
                            HeadIma4.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        activePng(name_Arry1[0],HeadIma1)
                        activePng(name_Arry2[0],HeadIma2)
                        activePng(name_Arry3[0],HeadIma3)
                        activePng(name_Arry4[0],HeadIma4)

                    end
                else
                    local commentCell1 = cell.transform:Find("AllCom/Comment").gameObject--下面评论的预设    
                    local commentCell2 = cell.transform:Find("AllCom/Comment (1)").gameObject--下面评论的预设
                    local commentCell3 = cell.transform:Find("AllCom/Comment (2)").gameObject--下面评论的预设
                    local commentCell4 = cell.transform:Find("AllCom/Comment (3)").gameObject--下面评论的预设
                    local commentCell5 = cell.transform:Find("AllCom/Comment (4)").gameObject--下面评论的预设
                    commentCell1:SetActive(true)
                    commentCell2:SetActive(true)
                    commentCell3:SetActive(true)
                    commentCell4:SetActive(true)
                    commentCell5:SetActive(true)
                    local name_Arry1 = graph:GetSplitStr(value1.MissionContent[1])
                    local name_Arry2 = graph:GetSplitStr(value1.MissionContent[2])
                    local name_Arry3 = graph:GetSplitStr(value1.MissionContent[3])
                    local name_Arry4 = graph:GetSplitStr(value1.MissionContent[4])
                    local name_Arry5 = graph:GetSplitStr(value1.MissionContent[5])

                    local nameTxt1 = commentCell1.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt1 = commentCell1.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt1.text = name_Arry1[0]
                    textTxt1.text = "[ "..name_Arry1[1].." ]"
                    local nameTxt2 = commentCell2.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt2 = commentCell2.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt2.text = name_Arry2[0]
                    textTxt2.text = "[ "..name_Arry2[1].." ]"
                    local nameTxt3 = commentCell3.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt3 = commentCell3.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt3.text = name_Arry3[0]
                    textTxt3.text = "[ "..name_Arry3[1].." ]"
                    local nameTxt4 = commentCell4.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt4 = commentCell4.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt4.text = name_Arry4[0]
                    textTxt4.text = "[ "..name_Arry4[1].." ]"
                    local nameTxt5 = commentCell5.transform:Find("NameTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    local textTxt5 = commentCell5.transform:Find("CommentTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                    nameTxt5.text = name_Arry5[0]
                    textTxt5.text = "[ "..name_Arry5[1].." ]"
                    local HeadIma1 = commentCell1.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma2 = commentCell2.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma3 = commentCell3.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma4 = commentCell4.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
                    local HeadIma5 = commentCell5.transform:Find("HeadIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))

                    for key_cha, value_cha in pairs(staticChar) do
                        if name_Arry1[0] == value_cha.Name then
                            HeadIma1.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry2[0] == value_cha.Name then
                            HeadIma2.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry3[0] == value_cha.Name then
                            HeadIma3.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry4[0] == value_cha.Name then
                            HeadIma4.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        if name_Arry5[0] == value_cha.Name then
                            HeadIma5.sprite = HeadImaAtlas:GetSprite("cn_"..value_cha.Id)
                        end
                        activePng(name_Arry1[0],HeadIma1)
                        activePng(name_Arry2[0],HeadIma2)
                        activePng(name_Arry3[0],HeadIma3)
                        activePng(name_Arry4[0],HeadIma4)
                        activePng(name_Arry5[0],HeadIma5)

                    end
                end
            end
        end
        dicValue1 = graph.InsDic_Value1
        dicValue2 = graph.InsDic_Value2
        for key_Value1, value_Value1 in pairs(dicValue1) do
            if key_Value1 == key then
                -- print(key_Value1,value_Value1)
                -- print("=====================")
                UpTxt_value.text = value_Value1.."次"
            end
        end
        for key_Value2, value_Value2 in pairs(dicValue2) do
            if key_Value2 == key then
                -- print(key_Value2,value_Value2)
                DownTxt_value.text = value_Value2.."次"
            end
        end
        cell.transform:Find("UPBtn"):GetComponent(typeof(CS.UnityEngine.UI.Button)).onClick:AddListener(function ()
            local str = "您已经领取过奖励了，不要贪心哦"
            CS.Sacu.Utils.SAManager.Instance:sendMessageToFactory("QNSActiveUIFactory", acc.QNSGets .. acc.Open,str);--向弹窗工厂发消息
        end)
        cell.transform:Find("DownBtn"):GetComponent(typeof(CS.UnityEngine.UI.Button)).onClick:AddListener(function ()
            local str = "您已经领取过奖励了，不要贪心哦"
            CS.Sacu.Utils.SAManager.Instance:sendMessageToFactory("QNSActiveUIFactory", acc.QNSGets .. acc.Open,str);--向弹窗工厂发消息
        end)
    end
    QNSCell:SetActive(false)
end
function openPage(evt)
   graph:setActive(true);
    local iconAtlasBG = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/BGAtlas")
    PngBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_album_0")
    musicBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_music_0")
    QNSBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_Qns_2")
    Bg:SetActive(true)
    Bg:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasBG:GetSprite("cn_QnsBG")
    CS.MoveTarget._instance.IsBool = true
    if isbool then
        graph:CheckIns()
        isbool =false
    end
    activeCell()
    QNSCell:SetActive(false)
end
function closePage(evt)
    graph:setActive(false);
    QNSCell:SetActive(true)
    for key, value in pairs(CellTable) do
        CS.UnityEngine.Object.Destroy(value)
    end
    CellTable={}
end

function getDicIns(arg1)
    dicIns = arg1;
end
function getDic(arg1)
    insDic_Off = arg1;
end
function getDicValue(arg1)
    dicValue = arg1;
end
function activePng(arg1, arg2)
    if arg1 == "骸B" then
        arg2.sprite = HeadImaAtlas:GetSprite("cn_kulou_1")
    end
    if arg1 == "骸C" then
        arg2.sprite = HeadImaAtlas:GetSprite("cn_kulou_2")
    end
    if arg1 == "骸D" then
        arg2.sprite = HeadImaAtlas:GetSprite("cn_kulou_3")
    end
    if arg1 == "匿名" then
        arg2.sprite = HeadImaAtlas:GetSprite("cn_null")
    end
end

