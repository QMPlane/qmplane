local graph
local gameObject
local transform
local acc = CS.Sacu.Utils.ActionCollection
local pte = CS.Sacu.Utils.ProtoTypeEnum
local cc = CS.Sacu.Utils.CommandCollection
local proto = CS.org.jiira.protobuf
local sin = CS.Sacu.Utils.SingleUpdateTypeEnum;
local sock
local PhotoData
local isbool = true --加载的开关
local staticPng
local pngCell
local cellParent
local bigIma
local bgIma
local userData
local PngTable = {}
local keyTxt
local iconAtlasPng
local musicBtn
local QNSBtn
local PngBtn
local iconAtlasCommon
local PngSmartAtlas
function new(gw)
	graph      = gw
    gameObject = gw.SAGameObject
    transform  = gw.SATransform
end
function onStart(obj)
    Bg              = CS.UnityEngine.GameObject.Find("UIRoot/cn_MainUI(Clone)/BgIma")
    Bg1             = CS.UnityEngine.GameObject.Find("UIRoot/cn_MainUI(Clone)/BgIma1")
    musicBtn        = CS.UnityEngine.GameObject.Find("UIRoot/cn_DailyUI(Clone)/DailyMusicBtn")
    QNSBtn          = CS.UnityEngine.GameObject.Find("UIRoot/cn_DailyUI(Clone)/DailyInsBtn")
    PngBtn          = CS.UnityEngine.GameObject.Find("UIRoot/cn_DailyUI(Clone)/DailyPngBtn")
    iconAtlasCommon = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/CommonBtnAtlas")
    PngSmartAtlas   = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/PngSmartAtlas")
    userData        = cc.getDataModel(pte.SUserData);
    staticPng       = proto.STIllustration.getList()
    keyTxt          = graph:getGameObjectByTypeName("CDAllIma/Image/KeyIma/Text", "Text")
    keyTxt.text     = userData.Key
    pngCell     = graph:getGameObjectByTypeName("CDAllIma/Scroll View/Viewport/Content/cn_PngCellUI","Image").gameObject
    cellParent  = graph:getGameObjectByTypeName("CDAllIma/Scroll View/Viewport/Content","GridLayoutGroup").gameObject
    bigIma      = graph:getGameObjectByTypeName("BigIma","Image").gameObject
    bgIma       = graph:getGameObjectByTypeName("BgIma","Image").gameObject
    graph:addEventDispatcherWithHandle(cc.Sock .. pte.SPhotos:ToString(), "getPhotosModel")
    graph:addEventDispatcherWithHandle(acc.Update .. sin.Photo:ToString(), "getPhotos")
    sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
    local cPhotos = cc.getDataModel(pte.CPhotos);
    sock:sendMessage(pte.CPhotos, cPhotos:Build():ToByteArray())
end
--注册监听事件
function onRegister()
	graph:addEventDispatcherWithHandle(acc.DailyPng .. acc.Close, "closePage")
	graph:addEventDispatcherWithHandle(acc.DailyPng .. acc.Open, "openPage")
end
function onRemove()
    graph:removeEventDispatcher(acc.DailyPng .. acc.Close)
    graph:removeEventDispatcher(acc.DailyPng .. acc.Open)
end

function openPage(evt)
    graph:setActive(true)
    local iconAtlasBG = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/BGAtlas")
    QNSBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_Qns_0")
    musicBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_music_0")
    PngBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_album_2")
    Bg:SetActive(true)
    Bg:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasBG:GetSprite("cn_PngBG")
    CS.MoveTarget._instance.IsBool = true
    if isbool then
        LoadCell()
        isbool =false
    end
    pngCell:SetActive(false)
end
function closePage(evt)
    graph:setActive(false)
end
function LoadCell()
    local modelPngdata = PhotoData.IdList
    for key1, value1 in pairs(staticPng) do
        local cell = graph:LoadPrefabs(pngCell,cellParent)
        local cellTf = cell.transform
        local MaskBtn = cellTf:Find("Mask/Head_SmartIma"):GetComponent(typeof(CS.UnityEngine.UI.Button))
        local maskBtnImage = cellTf:Find("Mask/Head_SmartIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
        maskBtnImage.sprite = PngSmartAtlas:GetSprite("cn_pic_"..value1.Id)
        MaskBtn.enabled = false--先将所有缩略图的按钮取消掉
        local KeyBtn = cellTf:Find("KeyBtn"):GetComponent(typeof(CS.UnityEngine.UI.Button))
        local Mask = cellTf:Find("Mask"):GetComponent(typeof(CS.UnityEngine.UI.Mask))
        --循环动态表将解锁的图片显示出来
        table.insert(PngTable,{ID = value1.Id,Btn = KeyBtn,MaskSc = Mask ,maskBtn = MaskBtn})
        for key, value in pairs(modelPngdata) do
            if value == value1.Id then
                Mask.enabled = false
                local keyBtn = cellTf:Find("KeyBtn"):GetComponent(typeof(CS.UnityEngine.UI.Button))
                keyBtn.gameObject:SetActive(false)
                MaskBtn.enabled = true
                iconAtlasPng = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/PngBigAtlas")
                cellTf:Find("Mask/Head_SmartIma"):GetComponent(typeof(CS.UnityEngine.UI.Button)).onClick:AddListener(function ()
                    bigIma.gameObject:SetActive(true)
                    local bigIma_Sp = bigIma:GetComponent(typeof(CS.UnityEngine.UI.Image))
                    bigIma_Sp.sprite = iconAtlasPng:GetSprite("cn_"..value1.Id)
                    local bigIma_Btn = bigIma:GetComponent(typeof(CS.UnityEngine.UI.Button))
                    bgIma.gameObject:SetActive(true)
                    bigIma_Btn.onClick:AddListener(function ()
                        bigIma.gameObject:SetActive(false)
                        bgIma.gameObject:SetActive(false)
                    end)
                end)
            end
        end
        KeyBtn.onClick:AddListener(function ()
            --消息传递到弹窗脚本  显示弹窗  点击确定后传递消息回来  然后向服务器发消息
            --后续要修改 先留着等改版出来再添加
           if userData.Key > 0 then
            local cGetPhoto = cc.getDataModel(pte.CGetPhoto);
            cGetPhoto:SetId(value1.Id)
            sock:sendMessage(pte.CGetPhoto, cGetPhoto:Build():ToByteArray())
           end
        end)
    end
end
--获取服务器的海报列表动态表
function getPhotosModel(evt)
    local dao = evt.Body.bytes
    PhotoData = cc.getDataModel(pte.SPhotos, dao);
end
--获取数据，修改本地数据，添加监听事件
function getPhotos_C(arg1)
    local up = arg1
    --删除解锁按钮 钥匙本地减1
    userData = cc.getDataModel(pte.SUserData)
    for key, value in pairs(PngTable) do
        if up.ID == value.ID then
            value.Btn.gameObject:SetActive(false)
            value.MaskSc.enabled = false
            value.maskBtn.enabled = true
            value.maskBtn.onClick:AddListener(function ()
                bigIma.gameObject:SetActive(true)
                local bigIma_Sp = bigIma:GetComponent(typeof(CS.UnityEngine.UI.Image))
                bigIma_Sp.sprite = iconAtlasPng:GetSprite("cn_"..up.ID)
                local bigIma_Btn = bigIma:GetComponent(typeof(CS.UnityEngine.UI.Button))
                bgIma.gameObject:SetActive(true)
                bigIma_Btn.onClick:AddListener(function ()
                    bigIma.gameObject:SetActive(false)
                    bgIma.gameObject:SetActive(false)
                end)
            end)
        end
    end
    --keyTxt.text = userData.Key
end
function updatePng(evt)
     local userData = cc.getDataModel(pte.SUserData)
     keyTxt.text = userData.Key
end
