local acc = CS.Sacu.Utils.ActionCollection
local Vector3 = CS.UnityEngine.Vector3
local Vector2 = CS.UnityEngine.Vector2
local Quaternion = CS.UnityEngine.Quaternion
local pte = CS.Sacu.Utils.ProtoTypeEnum;

local cc = CS.Sacu.Utils.CommandCollection;
local ac = CS.Sacu.Collection.SAACollection;
local proto = CS.org.jiira.protobuf;
local graph
local gameObject
local transform
local cellParent
local musicCell
local prefab
local isbool = true --加载的开关
local staticMusic
local musicData
local musicCamera--播放音乐的相机物体
local musicIma --CD图片
local bool RawIma =false--控制图片轮播的布尔值
local  RawImage--轮播的图片的物体
local audioSouce
local RawImageX
local stop1 --播放键切换的图的物体
local stop2 --播放键切换的图的物体
local timer
local timerTxt2
local isBool_Play--定义一个记录按钮的值
local Bg 
local musicBtn
local QNSBtn
local PngBtn
local iconAtlasCommon
local MusicPlayBtn = {}
local UpdataID
-- local un = CS.UnityEngine
function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
end
function onStart(obj)
    Bg = CS.UnityEngine.GameObject.Find("UIRoot/cn_MainUI(Clone)/BgIma")
    Bg1 = CS.UnityEngine.GameObject.Find("UIRoot/cn_MainUI(Clone)/BgIma1")
    musicBtn = CS.UnityEngine.GameObject.Find("UIRoot/cn_DailyUI(Clone)/DailyMusicBtn")
    QNSBtn = CS.UnityEngine.GameObject.Find("UIRoot/cn_DailyUI(Clone)/DailyInsBtn")
    PngBtn = CS.UnityEngine.GameObject.Find("UIRoot/cn_DailyUI(Clone)/DailyPngBtn")

    iconAtlasCommon = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/CommonBtnAtlas")
    
    graph:addEventDispatcherWithHandle(cc.Sock .. pte.SMusic:ToString(), "getMusicModel")
    local cMusic = cc.getDataModel(pte.CMusic);
    local sock   = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
    sock:sendMessage(pte.CMusic, cMusic:Build():ToByteArray())
    musicCell   = graph:getGameObjectByTypeName("CDAllIma/Scroll View/Viewport/Content/cn_MusicCellUI","Image").gameObject
    cellParent  = graph:getGameObjectByTypeName("CDAllIma/Scroll View/Viewport/Content","GridLayoutGroup").gameObject
    staticMusic = proto.STBgm.getList()
    musicCamera =  CS.UnityEngine.GameObject.Find("Main3DCamera").gameObject;
    musicIma    =  graph:getGameObjectByTypeName("CDAllIma/Image/MusicIma","Image").gameObject
    stop1       = graph:getGameObjectByTypeName("CDAllIma/Image/ViewIma","Image").gameObject
    stop2       = graph:getGameObjectByTypeName("CDAllIma/Image/ViewsIma","Image").gameObject
    audioSouce = musicCamera:GetComponent(typeof(CS.UnityEngine.AudioSource));
    musicIma:GetComponent(typeof(CS.UnityEngine.UI.Image)).material:SetFloat("_Speed",0)
    timerTxt2 =  graph:getGameObjectByTypeName("CDAllIma/Image/TimerIma/Text","Text").gameObject:GetComponent(typeof(CS.UnityEngine.UI.Text))--当前的播放时间
    timer = 167/audioSouce.clip.length
end
function Update()
   if RawIma then
        RawImageX = RawImageX +0.001
        RawImage:GetComponent(typeof(CS.UnityEngine.UI.RawImage)).uvRect =CS.UnityEngine.Rect(RawImageX,0,1,1)
        local miao = audioSouce.time/timer
        local fen = miao/60--歌曲的分钟数
        local miaos = miao%60--歌曲的秒数
        local miaos_ = graph:GetStr(miaos)
        local fen_ = string.sub(fen,1,1)
        if tonumber(miaos_) < 10 then
            timerTxt2.text = "0"..fen_..":".."0"..miaos_
        else
            timerTxt2.text = "0"..fen_..":"..miaos_
        end
   end
end

--注册监听事件
function onRegister()
	graph:addEventDispatcherWithHandle(acc.DailyMusic .. acc.Close, "closePage")
    graph:addEventDispatcherWithHandle(acc.DailyMusic .. acc.Open, "openPage")
end

function onRemove()
    graph:removeEventDispatcher(acc.DailyMusic .. acc.Close)
    graph:removeEventDispatcher(acc.DailyMusic .. acc.Open)
end
function openPage(evt)
    graph:setActive(true);
    local iconAtlasBG = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/BGAtlas")
    Bg:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasBG:GetSprite("cn_MusicBG")
    QNSBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_Qns_0")
    PngBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_album_0")
    musicBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasCommon:GetSprite("cn_music_2")
    --Bg1:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasBG:GetSprite("cn_MusicBG")
    Bg:SetActive(true)
    Bg:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasBG:GetSprite("cn_MusicBG")
    CS.MoveTarget._instance.IsBool = true
    --通过静态表加载预制体出来
    if isbool then
        LoadCell()
        isbool =false
    end
    musicCell:SetActive(false)
end
--加载预制体的方法  并改变音乐名字及其ID
function LoadCell()
    local modelMusicdata = musicData.IdList;
    for key, value in pairs(staticMusic) 
    do
        local cell = graph:LoadPrefabs(musicCell,cellParent)
        local cellTf = cell.transform
        IdTxt = cellTf:Find("MusicTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        local num = key+1
        if num<10 then
            IdTxt.text ="0".. num.."-"..value.Name
        else
            IdTxt.text = num.."-"..value.Name
        end
        for key_model, value_model in pairs(modelMusicdata) 
        do
            if tonumber(value_model) == tonumber(value.Id) then
                --加载图集的方法
                local iconAtlasMusic = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/MusicAtlas")
                local  imaBigBg      = iconAtlasMusic:GetSprite("cn_title_01")
                cellTf:Find("Image"):GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = imaBigBg
                local  imaBigPlayOff = iconAtlasMusic:GetSprite("cn_play")
                local  imaBigPlayOn  = iconAtlasMusic:GetSprite("cn_pause_on")
                local imaPlay1       = iconAtlasMusic:GetSprite("cn_fon_PLAY")
                local imaPlay2       = iconAtlasMusic:GetSprite("cn_paly_icon")
                local imaStop1       = iconAtlasMusic:GetSprite("cn_fon_STOP")
                local imaStop2       = iconAtlasMusic:GetSprite("cn_stop_icon")
                local playBtn        = cellTf:Find("PlayBtn")
                table.insert(MusicPlayBtn,playBtn)
                local timerTxt       = cellTf:Find("Text"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                local timers  = timer*CS.Sacu.Utils.SACache.getAudioClipWithName("Sound/Bgm/"..value.Id).length
                local fen = timers/60--歌曲的分钟数
                local miaos = timers%60--歌曲的秒数
                local miaos_ = graph:GetStr(miaos)
                local fen_ = string.sub(fen,1,1)
                if tonumber(miaos_) < 10 then
                    timerTxt.text = "0"..fen_..":".."0"..miaos_
                else
                    timerTxt.text = "0"..fen_..":"..miaos_
                end
                playBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = imaBigPlayOff
                --播放按钮加监听事件
                playBtn:GetComponent(typeof(CS.UnityEngine.UI.Button)).onClick:AddListener(function ()
                    
                    --关闭所有波形图
                    if RawImage ~= nil then
                        RawImage:SetActive(false)
                    end
                    --根据名字去判断播放还是暂停
                    if playBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite.name ==imaBigPlayOn.name then
                        --暂停和播放的歌曲名字都按照静态表里的歌曲名字去取
                        --切换到暂停，暂停歌曲，材质球
                        for key_, value_ in pairs(MusicPlayBtn) do
                            value_:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite =imaBigPlayOff
                        end
                        musicIma:GetComponent(typeof(CS.UnityEngine.UI.Image)).material:SetFloat("_Speed",0)
                        playBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite =imaBigPlayOff
                        stop1:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite =imaStop1
                        stop2:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite =imaStop2
                        audioSouce:Stop();
                        RawImage:SetActive(false)
                        RawIma = false
                    else
                        --切换到播放键，播放歌曲，材质球
                        for key_, value_ in pairs(MusicPlayBtn) do
                            value_:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite =imaBigPlayOff
                        end
                        musicIma:GetComponent(typeof(CS.UnityEngine.UI.Image)).material:SetFloat("_Speed",0.3)
                        playBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite =imaBigPlayOn
                        stop1:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite =imaPlay1
                        stop2:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite =imaPlay2
                        audioSouce.clip =CS.Sacu.Utils.SACache.getAudioClipWithName("Sound/Bgm/"..value.Id);
                        audioSouce:Play();
                        RawImage = cellTf:Find("RawImage").gameObject
                        RawImageX = RawImage:GetComponent(typeof(CS.UnityEngine.UI.RawImage)).uvRect.position.x
                        RawImage:SetActive(true)
                        RawIma = true
                        isBool_Play = playBtn
                    end
                end)
            end
        end
    end
end
function closePage(evt)
    graph:setActive(false);
end

--获取服务器的音乐列表动态表
function getMusicModel(evt)
    local dao = evt.Body.bytes
    musicData = cc.getDataModel(pte.SMusic, dao);
end
--获取服务器的音乐列表动态表
function updateMusic(evt)
    local up = evt.Body;
    print("解锁音乐")
    --table.insert(modelMusicdata,up.ID)
end
--解锁后需要做的操作 （两种方法  一个是只改变要添加的ID的CEll另一种是全部刷新）
function LoadCells()
    
    for key, value in pairs(staticMusic) 
    do
        
        -- for key_model, value_model in pairs(modelMusicdata) 
        -- do
            if tonumber(UpdataID) == tonumber(value.Id) then
                --加载图集的方法
                local cell = graph:LoadPrefabs(musicCell,cellParent)
                local cellTf = cell.transform
                IdTxt = cellTf:Find("MusicTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                local num = key+1
                if num<10 then
                    IdTxt.text ="0".. num.."-"..value.Name
                else
                    IdTxt.text = num.."-"..value.Name
                end
                local iconAtlasMusic = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/MusicAtlas")
                local  imaBigBg      = iconAtlasMusic:GetSprite("cn_title_01")
                cellTf:Find("Image"):GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = imaBigBg
                local  imaBigPlayOff = iconAtlasMusic:GetSprite("cn_play")
                local  imaBigPlayOn  = iconAtlasMusic:GetSprite("cn_pause_on")
                local imaPlay1       = iconAtlasMusic:GetSprite("cn_fon_PLAY")
                local imaPlay2       = iconAtlasMusic:GetSprite("cn_paly_icon")
                local imaStop1       = iconAtlasMusic:GetSprite("cn_fon_STOP")
                local imaStop2       = iconAtlasMusic:GetSprite("cn_stop_icon")
                local playBtn        = cellTf:Find("PlayBtn")
                table.insert(MusicPlayBtn,playBtn)
                local timerTxt       = cellTf:Find("Text"):GetComponent(typeof(CS.UnityEngine.UI.Text))
                local timers  = timer*CS.Sacu.Utils.SACache.getAudioClipWithName("Sound/Bgm/"..value.Id).length
                local fen = timers/60--歌曲的分钟数
                local miaos = timers%60--歌曲的秒数
                local miaos_ = graph:GetStr(miaos)
                local fen_ = string.sub(fen,1,1)
                if tonumber(miaos_) < 10 then
                    timerTxt.text = "0"..fen_..":".."0"..miaos_
                else
                    timerTxt.text = "0"..fen_..":"..miaos_
                end
                playBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = imaBigPlayOff
                --播放按钮加监听事件
                playBtn:GetComponent(typeof(CS.UnityEngine.UI.Button)).onClick:AddListener(function ()
                    
                    --关闭所有波形图
                    if RawImage ~= nil then
                        RawImage:SetActive(false)
                    end
                    --根据名字去判断播放还是暂停
                    if playBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite.name ==imaBigPlayOn.name then
                        --暂停和播放的歌曲名字都按照静态表里的歌曲名字去取
                        --切换到暂停，暂停歌曲，材质球
                        for key_, value_ in pairs(MusicPlayBtn) do
                            value_:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite =imaBigPlayOff
                        end
                        musicIma:GetComponent(typeof(CS.UnityEngine.UI.Image)).material:SetFloat("_Speed",0)
                        playBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite =imaBigPlayOff
                        stop1:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite =imaStop1
                        stop2:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite =imaStop2
                        audioSouce:Stop();
                        RawImage:SetActive(false)
                        RawIma = false
                    else
                        --切换到播放键，播放歌曲，材质球
                        for key_, value_ in pairs(MusicPlayBtn) do
                            value_:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite =imaBigPlayOff
                        end
                        musicIma:GetComponent(typeof(CS.UnityEngine.UI.Image)).material:SetFloat("_Speed",0.3)
                        playBtn:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite =imaBigPlayOn
                        stop1:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite =imaPlay1
                        stop2:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite =imaPlay2
                        audioSouce.clip =CS.Sacu.Utils.SACache.getAudioClipWithName("Sound/Bgm/"..value.Id);
                        audioSouce:Play();
                        RawImage = cellTf:Find("RawImage").gameObject
                        RawImageX = RawImage:GetComponent(typeof(CS.UnityEngine.UI.RawImage)).uvRect.position.x
                        RawImage:SetActive(true)
                        RawIma = true
                        isBool_Play = playBtn
                    end
                end)
            end
        --end
    end
end
function updateMusic(evt)
    -- local up = evt.Body
    -- print(up.Count)
    -- local userData = cc.getDataModel(pte.SUserData)
    -- part.text = userData.Part
end
