local graph;
local gameObject;
local transform;

function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
end

--页面加载出来   绑定UI组件
function onStart(obj)
    transform:SetParent(CS.UnityEngine.GameObject.Find("UIRoot/cn_DailyUI(Clone)/cn_MusicUI(Clone)/Scroll View/Viewport/Content").transform)
    --解析表，加载Cell
    for i = 1,5
    do
        local Cell = CS.UnityEngine.GameObject.Instantiate(gameObject)
        Cell.transform:SetParent(CS.UnityEngine.GameObject.Find("UIRoot/cn_DailyUI(Clone)/cn_MusicUI(Clone)/Scroll View/Viewport/Content").transform)
        Cell.transform.localScale = CS.UnityEngine.Vector3.one
    end
    gameObject:SetActive(false)
    CS.UnityEngine.Debug.Log("Cell加载成功");
end

--注册监听事件
function onRegister()
	-- graph:addListenerButtonClick("DailyBtn", DailyBtn)
	-- graph:addListenerButtonClick("FightBtn", FightBtn)
	-- graph:addListenerButtonClick("PilotBtn", PilotBtn)
    -- graph:addListenerButtonClick("SetBtn", SetBtn)
	--graph:addListenerButtonClick("Scroll View/Viewport/Content/", DailyBtn)
end

--音乐按钮的监听事件
function MusicEvent(evt)
    if isFactory ~= CS.Sacu.Utils.SAManager.Instance:startFactory("MusicUIFactory")
    then
        isFactory:disposeFactory()
        isFactory = CS.Sacu.Utils.SAManager.Instance:startFactory("MusicUIFactory")
    end
    print("音乐按钮的监听事件")
end

--QNS按钮的监听事件
function QNSEvent(evt)
    if isFactory ~= CS.Sacu.Utils.SAManager.Instance:startFactory("QNSUIFactory")
    then
        isFactory:disposeFactory()
        isFactory = CS.Sacu.Utils.SAManager.Instance:startFactory("QNSUIFactory")
    end
    print("QNS按钮的监听事件")
end

--相册按钮的监听事件
function PngEvent(evt)
    isFactory = CS.Sacu.Utils.SAManager.Instance:startFactory("PngUIFactory")
    if isFactory ~= CS.Sacu.Utils.SAManager.Instance:startFactory("PngUIFactory")
    then
        isFactory:disposeFactory()
        isFactory = CS.Sacu.Utils.SAManager.Instance:startFactory("PngUIFactory")
    end
    print("相册按钮的监听事件")
end