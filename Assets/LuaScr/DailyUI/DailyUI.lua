local acc = CS.Sacu.Utils.ActionCollection;
local graph;
local gameObject;
local transform;
local isbool=true
--当前打开的页面
local page =nil 
function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
end

--页面加载出来   绑定UI组件
function onStart(obj)
    
end

--注册监听事件
function onRegister()
    page = acc.DailyMusic
    graph:addEventDispatcherWithHandle(acc.DailyUI .. acc.Open, "openPage")
    graph:addEventDispatcherWithHandle(acc.DailyUI .. acc.Close, "closePage")
	graph:addListenerButtonClick(acc.DailyQNS, flipPagehandler)
	graph:addListenerButtonClick(acc.DailyMusic, flipPagehandler)
    graph:addListenerButtonClick(acc.DailyPng, flipPagehandler)
end

--按钮的监听事件
function flipPagehandler(evt)
    if evt.name ~= page then
        graph:dispatchEvent(page .. acc.Close)-- 关闭之前的界面
        page = evt.name --切换新界面标识
        graph:dispatchEvent(page .. acc.Open)--打开新界面
    end
end
function onRemove()
    graph:removeEventDispatcher(acc.DailyMusic .. acc.Close)
    graph:removeEventDispatcher(acc.DailyMusic .. acc.Open)
end
function openPage(evt)
    graph:setActive(true);
    graph:dispatchEvent(page .. acc.Open)--打开新界面
end
function closePage(evt)
    graph:setActive(false);
end

