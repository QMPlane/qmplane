local acc = CS.Sacu.Utils.ActionCollection;
local proto = CS.org.jiira.protobuf;
local Vector3 = CS.UnityEngine.Vector3
local Vector2 = CS.UnityEngine.Vector2
local Quaternion = CS.UnityEngine.Quaternion
local pte = CS.Sacu.Utils.ProtoTypeEnum;
local cc = CS.Sacu.Utils.CommandCollection;
local graph;
local gameObject;
local transform;
function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
end

--页面加载出来   绑定UI组件
function onStart(obj)
end

--注册监听事件
function onRegister()
    graph:addListenerButtonClick("CloseBtn", flipPagehandler)
    graph:addListenerButtonClick("ClickBtn", flipPagehandlers)
    
end

--按钮的监听事件
function flipPagehandler(evt)
    graph.factory:disposeFactory()
end
function flipPagehandlers(evt)
    local cFeelReward = cc.getDataModel(pte.CFeelReward);
    local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
    sock:sendMessage(pte.CFeelReward, cFeelReward:Build():ToByteArray())
end
function onRemove()

end
