local graph;
local gameObject;
local transform;
local lifeSlider
local timer
local timeout 
local stop
local player = CS.Player.instance
local failedPanel
local winPanel
local luckyRoll
local select1
local select2
local select3
local select4
local posLight1
local posLight2
local posLight3
local posLight4
local lifecount
local atkValue
local shootDownValue
local skillObj
local failedReStartBtn
local failedReturnBtn
function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
end

--页面加载出来   绑定UI组件
function onStart(obj)
    CS.UnityEngine.Debug.Log("战斗界面启动");

    -- local move1 = graph:getGameObjectByTypeName("Bottom/posGrid/Move1","Button");
    -- local move2 = graph:getGameObjectByTypeName("Bottom/posGrid/Move2","Button");
    -- local move3 = graph:getGameObjectByTypeName("Bottom/posGrid/Move3","Button");
    -- local move4 = graph:getGameObjectByTypeName("Bottom/posGrid/Move4","Button");
    timer = graph:getGameObjectByTypeName("Timer","Text")
    lifeSlider = graph:getGameObjectByTypeName("Top/Life","Slider");
    failedPanel = graph:getGameObjectByTypeName("FailedPanel","RectTransform");
    failedReStartBtn = graph:getGameObjectByTypeName("FailedPanel/restart","RectTransform");
    failedReturnBtn = graph:getGameObjectByTypeName("FailedPanel/return","RectTransform");
    winPanel = graph:getGameObjectByTypeName("WinPanel","RectTransform");
    luckyRoll = graph:getGameObjectByTypeName("LuckyRoll","RectTransform");
    lifecount = graph:getGameObjectByTypeName("Top/Life/lifecount","Text");
    atkValue = graph:getGameObjectByTypeName("Top/atk/atkValue","Text");
    shootDownValue = graph:getGameObjectByTypeName("Top/shootDown/shootDownValue","Text");
    select1 = graph:getGameObjectByTypeName("Bottom/posGrid/Move1/Select","RectTransform")
    select2 = graph:getGameObjectByTypeName("Bottom/posGrid/Move2/Select","RectTransform")
    select3 = graph:getGameObjectByTypeName("Bottom/posGrid/Move3/Select","RectTransform")
    select4 = graph:getGameObjectByTypeName("Bottom/posGrid/Move4/Select","RectTransform")
   
    posLight1 = graph:getGameObjectByTypeName("posList/pos1/on","RectTransform")
    posLight2 = graph:getGameObjectByTypeName("posList/pos2/on","RectTransform")
    posLight3 = graph:getGameObjectByTypeName("posList/pos3/on","RectTransform")
    posLight4 = graph:getGameObjectByTypeName("posList/pos4/on","RectTransform")
    skillObj = graph:getGameObjectByTypeName("Bottom/Skill","RectTransform")
    failedPanel.gameObject:SetActive(false);
    failedReStartBtn.gameObject:SetActive(false)
    failedReturnBtn.gameObject:SetActive(false)
    winPanel.gameObject:SetActive(false);
    luckyRoll.gameObject:SetActive(false);
    skillObj.gameObject:SetActive(true)
    atkValue.text = player.atk
  
end
-- function Update()
--     timer.text = math.floor(CS.Graphs.BattleSceneGraphWorker.timer * 1000);

-- end
function ShowFailedBtn()
    failedReStartBtn.gameObject:SetActive(true)
    failedReturnBtn.gameObject:SetActive(true)
end
function ChangeLife()
    lifeSlider.value = player.curHP/player.maxHP;
    lifecount.text = player.curHP.. "/" .. player.maxHP
end
function ChangeShootDown()
    shootDownValue.text = CS.Graphs.BattleSceneGraphWorker.instance.shootDown
end
--注册监听事件
function onRegister()
	graph:addListenerButtonClick("Bottom/posGrid/Move1", Move1)
	graph:addListenerButtonClick("Bottom/posGrid/Move2", Move2)
    graph:addListenerButtonClick("Bottom/posGrid/Move3", Move3)
    graph:addListenerButtonClick("Bottom/posGrid/Move4", Move4)
    graph:addListenerButtonClick("Bottom/Skill", UseSkill)
    graph:addListenerButtonClick("FailedPanel/restart",Restart)
    graph:addListenerButtonClick("FailedPanel/return",FailedReturn)
    graph:addListenerButtonClick("LuckyRoll/return",WinReturn)
    graph:addListenerButtonClick("WinPanel/GetReward",GetReward)
    graph:addListenerButtonClick("Top/Stop",TimeOut)
    graph:addEventDispatcherWithHandle("isWin", "openWin")
    graph:addEventDispatcherWithHandle("isFailed", "openFailed")
	-- graph:addListenerButtonClick("DailyBtn", DailyBtn)
	-- graph:addListenerButtonClick("FightBtn", FightBtn)
	-- graph:addListenerButtonClick("PilotBtn", PilotBtn)
	-- graph:addListenerButtonClick("SetBtn", SetBtn)
end
function onRemove()
    graph:removeEventDispatcher("isWin")
    graph:removeEventDispatcher("isFailed")
    
end
function UseSkill(evt)
    CS.Player.instance:UseSkill()
end
--战役按钮的监听事件
function Move1(evt)
    if CS.Graphs.BattleUIGraphWorker.instance.isCloseUp == false then
    CS.Player.instance:Move1()
    select1.gameObject:SetActive(true)
    select2.gameObject:SetActive(false)
    select3.gameObject:SetActive(false)
    select4.gameObject:SetActive(false)
    end
end

--mini活动按钮的监听事件
function Move2(evt)
    if CS.Graphs.BattleUIGraphWorker.instance.isCloseUp == false then
    CS.Player.instance:Move2()
    select1.gameObject:SetActive(false)
    select2.gameObject:SetActive(true)
    select3.gameObject:SetActive(false)
    select4.gameObject:SetActive(false)
    end
end

--击杀荣誉榜按钮的监听事件
function Move3(evt)
    if CS.Graphs.BattleUIGraphWorker.instance.isCloseUp == false then
    CS.Player.instance:Move3()
    select1.gameObject:SetActive(false)
    select2.gameObject:SetActive(false)
    select3.gameObject:SetActive(true)
    select4.gameObject:SetActive(false)
    end
end
function Move4(evt)
    if CS.Graphs.BattleUIGraphWorker.instance.isCloseUp == false then
    CS.Player.instance:Move4()
    select1.gameObject:SetActive(false)
    select2.gameObject:SetActive(false)
    select3.gameObject:SetActive(false)
    select4.gameObject:SetActive(true)
    end
end
function TimeOut(evt)
    if stop == true then
        CS.UnityEngine.Time.timeScale = 1;
        CS.Graphs.BattleSceneGraphWorker.instance:ContinueMusic()
        stop = false
    else
        stop = true
        CS.Graphs.BattleSceneGraphWorker.instance:PauseMusic()
        CS.UnityEngine.Time.timeScale = 0;
    end
end
function openFailed(evt)
    skillObj.gameObject:SetActive(false)
    failedPanel.gameObject:SetActive(true);
    CS.Graphs.BattleUIGraphWorker.instance:FailedAnima()
end
function openWin(evt)
    skillObj.gameObject:SetActive(false)
    winPanel.gameObject:SetActive(true);
    CS.Graphs.BattleUIGraphWorker.instance:WinAnima()
end
function Restart(evt)
    skillObj.gameObject:SetActive(true)
    CS.Graphs.BattleSceneGraphWorker.instance:ReStart()
    failedPanel.gameObject:SetActive(false);
    winPanel.gameObject:SetActive(false);
end
function FailedReturn(evt)
    failedPanel.gameObject:SetActive(false);
    CS.Sacu.Utils.SAManager.Instance:disposeFactory("BattleFactory")
    CS.Sacu.Utils.SAManager.Instance:startFactory("MapFactory",false)
    CS.Graphs.BattleSceneGraphWorker.instance:ReturnMusic();
    -- graph.factory:disposeFactory()
end
function WinReturn(evt)
    winPanel.gameObject:SetActive(false);
    luckyRoll.gameObject:SetActive(false);
    CS.Sacu.Utils.SAManager.Instance:disposeFactory("BattleFactory")
    CS.Sacu.Utils.SAManager.Instance:startFactory("MapFactory",true)
    CS.Graphs.BattleSceneGraphWorker.instance:ReturnMusic();
    -- graph.factory:disposeFactory()
end
function GetReward(evt)
    luckyRoll.gameObject:SetActive(true);
end
function ShowPosHighLight(arg1)
    -- CS.UnityEngine.Debug.Log(arg1 .."   Showlua");
    if arg1 == 1 then
        posLight1.gameObject:SetActive(true);
    elseif arg1 == 2 then
        posLight2.gameObject:SetActive(true);
    elseif arg1 == 3 then
        posLight3.gameObject:SetActive(true);
    elseif arg1 == 4 then
        posLight4.gameObject:SetActive(true);
    end
end
function ClosePosHighLight(arg1)
    -- CS.UnityEngine.Debug.Log(arg1 .."   Closelua");
    if arg1 == 1 then
        posLight1.gameObject:SetActive(false);
    elseif arg1 == 2 then
        posLight2.gameObject:SetActive(false);
    elseif arg1 == 3 then
        posLight3.gameObject:SetActive(false);
    elseif arg1 == 4 then
        posLight4.gameObject:SetActive(false);
    end
end