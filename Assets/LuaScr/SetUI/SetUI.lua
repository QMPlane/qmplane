local graph;
local gameObject;
local transform;

function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
end

--页面加载出来   绑定UI组件
function onStart(obj)
    CS.UnityEngine.Debug.Log("SetUI界面启动");

    -- local BattleBtn = graph:getGameObjectByTypeName("BattleBtn","Button");
    -- local ActivityBtn = graph:getGameObjectByTypeName("ActivityBtn","Button");
    -- local HonorBtn = graph:getGameObjectByTypeName("HonorBtn","Button");

    -- local DailyBtn = graph:getGameObjectByTypeName("DailyBtn","Button");
    -- local FightBtn = graph:getGameObjectByTypeName("FightBtn","Button");
    -- local PilotBtn = graph:getGameObjectByTypeName("PilotBtn","Button");
    -- local SetBtn = graph:getGameObjectByTypeName("SetBtn","Button");

end

--注册监听事件
function onRegister()
	graph:addListenerButtonClick("LanguageBtn", LanguageEvent)
	-- graph:addListenerButtonClick("ActivityBtn", ActivityEvent)
	-- graph:addListenerButtonClick("HonorBtn", HonorEvent)
	-- graph:addListenerButtonClick("DailyBtn", DailyBtn)
	-- graph:addListenerButtonClick("FightBtn", FightBtn)
	-- graph:addListenerButtonClick("PilotBtn", PilotBtn)
	-- graph:addListenerButtonClick("SetBtn", SetBtn)
end

--语言按钮的监听事件
function LanguageEvent(evt)
    graph.factory:disposeFactory()
    CS.Sacu.Utils.SAManager.Instance:startFactory("LanguageUIFactory")
    print("语言按钮的监听事件")
end

--mini活动按钮的监听事件
function ActivityEvent(evt)
    CS.Sacu.Utils.SAManager.Instance:startFactory("ActivityUIFactory")
    print("mini活动按钮的监听事件")
end

--击杀荣誉榜按钮的监听事件
function HonorEvent(evt)
    print("击杀荣誉榜按钮的监听事件")
end
