local graph
local gameObject
local transform
local acc = CS.Sacu.Utils.ActionCollection
local pte = CS.Sacu.Utils.ProtoTypeEnum
local cc = CS.Sacu.Utils.CommandCollection
local proto = CS.org.jiira.protobuf
local sin = CS.Sacu.Utils.SingleUpdateTypeEnum;
local staticList--机师的静态数据表
local PilotAllT = {}--记录每个总榜的物体的表
local Effect_FightingUI
local Bg
local sOldReceivesDic--存有服务器取到的已经领取过的击杀数礼物的字典
local GiftAllT = {}--所有可以显示出来的奖励物品的物体的表
local PilotAllt_--去掉已经领取过的击杀数的字典
local giftBtnS --记录点击的按钮  返回消息后直接换掉图片并且删除监听
local GiftIDs --记录的应该显示的道具的ID
local GiftCounts --记录的应该显示的道具的数量
local GiftGets = {}--记录已经领取奖励的按钮
local GiftID_Gets--记录已经领取过的ID
local GiftShootDown_Gets--记录已经领取过的击杀数

function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
	graph:addListenerButtonClick("Panel/BackBtn", closePage)
end
function onStart(obj)
    Bg = CS.UnityEngine.GameObject.Find("UIRoot/cn_MainUI(Clone)/BgIma")
    Effect_FightingUI = CS.UnityEngine.GameObject.Find("UIRoot/cn_MainUI(Clone)/Effect_FightingUI")
    staticList = proto.STCharacter.getList()
    GiftAtlas = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/Gift_BAtlas")
    --现将按钮加到表里面  按照ID   math.floor(8%3)  取余
    for i = 1, 6 do
        local allIma = graph:getGameObjectByTypeName("Panel/All/PilotIma_100"..i,"Image")
        local slider = graph:getGameObjectByTypeName("Panel/All/PilotIma_100"..i.."/Image/Slider","Slider")
        table.insert(PilotAllT,{ID = 1000+i,AllIma = allIma,Slider = slider})--存到表里
    end
    --将所有可以显示出来的奖励物品的物体都存到表里  ID和击杀数两个做键  物体做值
    --遍历静态表将ID对应的击杀数
    for i = 0, 2 do
        for key1, value1 in pairs(staticList) do
            local arry = graph:GetSplitStr(value1.ShootDownReward[i])
            local allIma = graph:getGameObjectByTypeName("Panel/All/PilotIma_100"..(key1+1).."/GiftIma"..i,"Image")
            local ID_ = value1.Id
            local shoodDown_ = arry[0]
            local GiftID_ = arry[1]
            local GiftCount_ =arry[2]
            table.insert(GiftAllT,{ID = ID_,shootDown = shoodDown_,GiftIma = allIma,GiftID = GiftID_,GiftCount= GiftCount_})--存到表里
        end
    end
    local allIma_ = graph:getGameObjectByTypeName("Panel/All/PilotIma_1001/GiftIma3","Image")
    table.insert(GiftAllT,{ID = 1001,shootDown = 1000,GiftIma = allIma_,GiftID = 6010,GiftCount= 3})--得到的是所有有可能激活的奖励物品 
end
--注册监听事件
function onRegister()
    graph:addEventDispatcherWithHandle(acc.ShootDownUI .. acc.Open, "openPage")
    graph:addEventDispatcherWithHandle(cc.Sock .. pte.SSDReward:ToString(), "getSSDRewardHandler")
end
function onRemove()
    graph:removeEventDispatcher(acc.ShootDownUI .. acc.Close)
    graph:removeEventDispatcher(acc.ShootDownUI .. acc.Open)
    graph:removeEventDispatcher(cc.Sock .. pte.SSDReward:ToString())
end

function openPage(evt)
    graph:setActive(true)
    Effect_FightingUI:SetActive(false)
    Bg:SetActive(false)
    Load()
end
function closePage(evt)
    graph:setActive(false)
    Bg:SetActive(true)
end
--遍历顺序:应该馅遍历静态表获得每个ID对应的击杀数  然后遍历动态表  比较两个值  如果大于 那就找到预设中对应的物体将它的图标换掉  并且添加事件
function Load()
    local ModelFeelT = graph.ShootDownList
    local sOldReceivesDic = graph.SOldReceivesDic--遍历?服务器记录到的已经领取过的礼物  两种思路一种是直接在存的物体表里面删掉物体  
    local str = "您已经领取过奖励了，不要贪心哦"                                    --另一种是在最后将可领取的物体变为不可领取状态
    --因为可能会连续删除表中数据  采用倒序遍历
    for i = #GiftAllT, 1, -1 do
        for key_old, value_old in pairs(sOldReceivesDic) do
            if GiftAllT[i].ID == key_old and value_old:Contains(tonumber(GiftAllT[i].shootDown)) then
                local giftIma = GiftAllT[i].GiftIma
                giftIma.sprite = GiftAtlas:GetSprite("gift_2_1")
                --将已经领取过的道具的按钮加监听
                giftIma.gameObject:GetComponent(typeof(CS.UnityEngine.UI.Button)).onClick:AddListener(function ()
                    CS.Sacu.Utils.SAManager.Instance:sendMessageToFactory("QNSActiveUIFactory", acc.QNSGets .. acc.Open,str);--向弹窗工厂发消息
                end)
                table.remove(GiftAllT, i)
                break--必须跳出内层循环  已经删除了
            end
        end
    end
    --先将每个机师的能量条显示出来
    for key_model, value_model in pairs(ModelFeelT) do
        for key1, value1 in pairs(PilotAllT) do--遍历存的物体的总表  
            if key_model == value1.ID then
                value1.Slider.value = value_model/1000--先将机师的击杀数以能量条的方式显示出来
            end
        end
    end
    for key_model, value_model in pairs(ModelFeelT) do
        for key, value in pairs(GiftAllT) do
            local giftBtn = value.GiftIma.gameObject:GetComponent(typeof(CS.UnityEngine.UI.Button))
            if key_model == value.ID and tonumber(value_model) >= tonumber(value.shootDown) then--这是已经满足条件的按钮
                local giftIma = value.GiftIma
                giftIma.sprite = GiftAtlas:GetSprite("gift_3_1")
                giftBtn.onClick:AddListener(function ()
                    giftBtnS = giftBtn
                    GiftID_Gets = value.ID
                    GiftShootDown_Gets = value.shootDown
                    local cSDReward = cc.getDataModel(pte.CSDReward);
                    cSDReward:SetId(key_model);
                    cSDReward:SetShootDown(value.shootDown)
                    local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
                    sock:sendMessage(pte.CSDReward, cSDReward:Build():ToByteArray())
                    GiftIDs = value.GiftID
                    GiftCounts = value.GiftCount
                end)
            else--不满足条件的就显示未满足条件
                local str0 = "您还未满足领取条件"
                giftBtn.onClick:AddListener(function ()
                    CS.Sacu.Utils.SAManager.Instance:sendMessageToFactory("QNSActiveUIFactory", acc.ShootDownDont .. acc.Open,str0);--向弹窗工厂发消息
                end)
            end
        end
    end
end
function getSSDRewardHandler(evt)--完成击杀数领奖的回调 收到后去遍历击杀数领奖的静态表获取到领取的物品的ID和数量
    local str = "您已经领取过奖励了，不要贪心哦"
    print(GiftIDs)
    print(GiftCounts)
    graph:AddInt(GiftIDs,GiftCounts)
    CS.Sacu.Utils.SAManager.Instance:sendMessageToFactory("QNSActiveUIFactory", acc.ItemActive .. acc.Open, graph.UPList);--向弹窗工厂发消息
    for i = #GiftAllT, 1, -1 do
            if tonumber(GiftAllT[i].ID) == tonumber(GiftID_Gets) and tonumber(GiftShootDown_Gets) == tonumber(GiftAllT[i].shootDown) then
                table.remove(GiftAllT, i)
                break--必须跳出内层循环  已经删除了
            end
    end
    giftBtnS.gameObject:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = GiftAtlas:GetSprite("gift_2_1")
    giftBtnS.onClick:RemoveAllListeners()
    giftBtnS.onClick:AddListener(function ()
        CS.Sacu.Utils.SAManager.Instance:sendMessageToFactory("QNSActiveUIFactory", acc.QNSGets .. acc.Open,str);--向弹窗工厂发消息
    end)--这里应该将监听事件切换为已经领取过
end