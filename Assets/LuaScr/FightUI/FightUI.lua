local graph
local gameObject
local transform
local acc = CS.Sacu.Utils.ActionCollection
local pte = CS.Sacu.Utils.ProtoTypeEnum
local cc = CS.Sacu.Utils.CommandCollection
local single = CS.Sacu.Utils.SingleUpdateTypeEnum
local part
function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
end

--页面加载出来   绑定UI组件
function onStart(obj)
    local userData = cc.getDataModel(pte.SUserData);
    part = graph:getGameObjectByTypeName("Items/ModelTxt","Text")
    part.text = userData.Part.."/100"
end

--注册监听事件
function onRegister()
	graph:addListenerButtonClick("BattleBtn", BattleEvent)
	graph:addListenerButtonClick("ActivityBtn", ActivityEvent)
    graph:addListenerButtonClick("HonorBtn", HonorEvent)
end

--战役按钮的监听事件
function BattleEvent(evt)
    graph.factory:disposeFactory()
    CS.Sacu.Utils.SAManager.Instance:startFactory("MapFactory")
    CS.Sacu.Utils.SAManager.Instance:disposeFactory("MainUIFactory")
end

--mini活动按钮的监听事件
function ActivityEvent(evt)
    --CS.Sacu.Utils.SAManager.Instance:startFactory("ActivityUIFactory")
    graph:dispatchEvent(acc.ActivityUI .. acc.Open)--打开新界面
    --CS.Sacu.Utils.SAManager.Instance:startFactory("SABattleFactory", 6202)
    -- CS.Sacu.Utils.SAManager.Instance:disposeFactory("FightUIFactory")
    -- CS.Sacu.Utils.SAManager.Instance:disposeFactory("MainUIFactory")
end

--击杀荣誉榜按钮的监听事件
function HonorEvent(evt)
    graph:dispatchEvent(acc.ShootDownUI .. acc.Open)--打开新界面
end
--只要齿轮改变，完成齿轮的刷新
function updatePart(evt)
    local userData = cc.getDataModel(pte.SUserData)
    part.text = userData.Part.."/100"
end
