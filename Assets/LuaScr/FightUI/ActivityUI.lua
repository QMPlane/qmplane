local graph
local gameObject
local transform
local acc = CS.Sacu.Utils.ActionCollection
local pte = CS.Sacu.Utils.ProtoTypeEnum
local cc = CS.Sacu.Utils.CommandCollection
local proto = CS.org.jiira.protobuf
local sin = CS.Sacu.Utils.SingleUpdateTypeEnum
local Effect_FightingUI
local Bg
local Coin --游戏币
local miniGameBtn1
local miniGameBtn2
local miniGameBtn3
local miniGameBtn4
local miniGameBtn5
local miniGameBtn6

function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
end
function onStart(obj)
    miniGameBtn1 = graph:getGameObjectByTypeName("Panel/Scroll View/Viewport/Content/MiNiGameBtn","Button")
    miniGameBtn2 = graph:getGameObjectByTypeName("Panel/Scroll View/Viewport/Content/MiNiGameBtn (1)","Button")
    miniGameBtn3 = graph:getGameObjectByTypeName("Panel/Scroll View/Viewport/Content/MiNiGameBtn (2)","Button")
    miniGameBtn4 = graph:getGameObjectByTypeName("Panel/Scroll View/Viewport/Content/MiNiGameBtn (3)","Button")
    miniGameBtn5 = graph:getGameObjectByTypeName("Panel/Scroll View/Viewport/Content/MiNiGameBtn (4)","Button")
    miniGameBtn6 = graph:getGameObjectByTypeName("Panel/Scroll View/Viewport/Content/MiNiGameBtn (5)","Button")

    Bg = CS.UnityEngine.GameObject.Find("UIRoot/cn_MainUI(Clone)/BgIma")
    Effect_FightingUI = CS.UnityEngine.GameObject.Find("UIRoot/cn_MainUI(Clone)/Effect_FightingUI")
    Coin =  graph:getGameObjectByTypeName("Panel/RightIma/Image/Text","Text")
    local userData = cc.getDataModel(pte.SUserData);
    Coin.text = userData.Coin
end
--注册监听事件
function onRegister()
    graph:addListenerButtonClick("Panel/CloseBtn", closePage)
    graph:addEventDispatcherWithHandle(acc.ActivityUI .. acc.Open, "openPage")
end
function onRemove()
    graph:removeEventDispatcher(acc.ActivityUI .. acc.Open)
end

function openPage(evt)
    graph:setActive(true)
    Effect_FightingUI:SetActive(false)
    Bg:SetActive(false)
    addListener()
end
function closePage(evt)
    graph:setActive(false)
    Bg:SetActive(true)
end
function addListener()--给界面的按钮添加监听事件
    local str = "该游戏还未解锁或游戏币不足"
    if tonumber(Coin.text) > 0 then
        miniGameBtn1.onClick:AddListener(function ()
            CS.Sacu.Utils.SAManager.Instance:startFactory("SABattleFactory", 6205)
            CS.Sacu.Utils.SAManager.Instance:disposeFactory("FightUIFactory")
            CS.Sacu.Utils.SAManager.Instance:disposeFactory("MainUIFactory")
        end)
        miniGameBtn2.onClick:AddListener(function ()
            CS.Sacu.Utils.SAManager.Instance:sendMessageToFactory("QNSActiveUIFactory", acc.QNSGets .. acc.Open,str);--向弹窗工厂发消息
        end)
    else
        miniGameBtn1.onClick:AddListener(function ()
            CS.Sacu.Utils.SAManager.Instance:sendMessageToFactory("QNSActiveUIFactory", acc.QNSGets .. acc.Open,0);--向弹窗工厂发消息
        end)
        miniGameBtn2.onClick:AddListener(function ()
            CS.Sacu.Utils.SAManager.Instance:sendMessageToFactory("QNSActiveUIFactory", acc.QNSGets .. acc.Open,0);--向弹窗工厂发消息
        end)
        miniGameBtn3.onClick:AddListener(function ()
            CS.Sacu.Utils.SAManager.Instance:sendMessageToFactory("QNSActiveUIFactory", acc.QNSGets .. acc.Open,0);--向弹窗工厂发消息
        end)
    end
    
end