local graph;
local gameObject;
local transform;
local tips;
local map3;
function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
end

--页面加载出来   绑定UI组件
function onStart(obj)
    CS.UnityEngine.Debug.Log("章节界面启动");
    tips = graph:getGameObjectByTypeName("Tips","RectTransform")

end

--注册监听事件
function onRegister()
	graph:addListenerButtonClick("Tips/YES", NextStage)
    graph:addListenerButtonClick("Tips/NO", CloseTips)
    graph:addListenerButtonClick("return", ReturnMain)
    graph:addListenerButtonClick("GO", GoNext)
    graph:addListenerButtonClick("RewardTips/kuang/CancleBtn", CloseRewardTips)
    graph:addListenerButtonClick("RewardTips/kuang/CloseBtn", CloseRewardTips)
    
end
function CloseRewardTips()
    CS.Graphs.MapUIGraphWorker.Instance:CloseRewardTips()
end
function ShowTips()
    tips.gameObject:SetActive(true)
end

function CloseTips()
    tips.gameObject:SetActive(false)
end
function NextStage()

end
function GoNext()
    CS.Graphs.MapUIGraphWorker.Instance:MapMoveTo(CS.Graphs.MapUIGraphWorker.Instance.curCell + 1)
end
function ReturnMain()
    CS.Graphs.MapUIGraphWorker.Instance:DestroyMap();
    -- CS.UnityEngine.Transform.FindObjectOfType("UIRoot/cn_MainUI(Clone)").gameObject:SetActive(true)
    CS.Sacu.Utils.SAManager.Instance:startFactory("MainUIFactory")
    CS.Sacu.Utils.SAManager.Instance:startFactory("FightUIFactory")

    CS.Sacu.Utils.SAManager.Instance:disposeFactory("MapFactory")
    
    
end

