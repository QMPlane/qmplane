local graph;
local gameObject;
local transform;

function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
end

function onStart()
	--CS.Sacu.Utils.SAUtils.Log("AssetBundleRemote:onStart ... ")
end

function onRegister()
	--CS.Sacu.Utils.SAUtils.Log("register")
end

function onRegisterComplete()
	--静态表解析
	--SAProtoDecode.parsing(CS.Sacu.Utils.SADataTable.Instance.iostring)
	graph.factory:disposeFactory()
	CS.Sacu.Utils.SAManager.Instance:startFactory("LoginUIFactory")
	CS.Sacu.Utils.SAManager.Instance:startFactory("ErrorCodeFactory")
	CS.Sacu.Utils.SAManager.Instance:startFactory("UpdateCodeFactory")
end

function onRemove()
end

function onDispose()
end
