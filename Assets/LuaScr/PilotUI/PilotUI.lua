local acc = CS.Sacu.Utils.ActionCollection
local proto = CS.org.jiira.protobuf
local pte = CS.Sacu.Utils.ProtoTypeEnum
local cc = CS.Sacu.Utils.CommandCollection
local single = CS.Sacu.Utils.SingleUpdateTypeEnum
local graph
local gameObject
local transform
--当前打开的页面
local page =nil 
local iD = nil
local staticIDList
local userData
local PartTxt
function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
    graph:addListenerButtonClick(acc.PassiveSkillUI, flipPagehandler)
    graph:addListenerButtonClick(acc.HeartBtn, StartHeartUI)
	graph:addListenerButtonClick(acc.PilotBtn, flipPagehandler)
	graph:addListenerButtonClick(acc.PilotBtn1, flipPagehandler)
	graph:addListenerButtonClick(acc.PilotBtn2, flipPagehandler)
	graph:addListenerButtonClick(acc.PilotBtn3, flipPagehandler)
	graph:addListenerButtonClick(acc.PilotBtn4, flipPagehandler)
	graph:addListenerButtonClick(acc.PilotBtn5, flipPagehandler)
end

--页面加载出来   绑定UI组件
function onStart(obj)
    staticIDList = proto.STCharacter.getList()
    userData = cc.getDataModel(pte.SUserData)
    PartTxt = graph:getGameObjectByTypeName("PartTxtIma/Text","Text")
    -- HeartSpine = graph:getGameObjectForName("HeartSpine")
    -- HeartSpine_Btn = graph:getGameObjectByTypeName("HeatBtn","Button")
    -- HeartSpine_SK = graph:GetSpine(HeartSpine)
    local num = graph:GetSplitStr(userData.Part/100)
    PartTxt.text = num[0]
    
end

--注册监听事件
function onRegister()
    
end

--按钮的监听事件
function flipPagehandler(evt)
    local a =evt.name
    if evt.name == acc.PilotBtn then
        Id = staticIDList[0].Id
    elseif evt.name == acc.PilotBtn1 then
        Id = staticIDList[1].Id
    elseif evt.name == acc.PilotBtn2 then
        Id = staticIDList[2].Id
    elseif evt.name == acc.PilotBtn3 then
        Id = staticIDList[3].Id
    elseif evt.name == acc.PilotBtn4 then
        Id = staticIDList[4].Id
    elseif evt.name == acc.PilotBtn5 then
        Id = staticIDList[5].Id
    end
    graph:dispatchEvent(evt.name .. acc.Open,Id)--打开新界面
end
function onRemove()
    graph:removeEventDispatcher(acc.PassiveSkillUI .. acc.Close)
    graph:removeEventDispatcher(acc.PassiveSkillUI .. acc.Open)
    graph:removeListenerButtonClick(acc.HeartBtn)
	graph:removeListenerButtonClick(acc.PilotBtn)
	graph:removeListenerButtonClick(acc.PilotBtn1)
	graph:removeListenerButtonClick(acc.PilotBtn2)
	graph:removeListenerButtonClick(acc.PilotBtn3)
	graph:removeListenerButtonClick(acc.PilotBtn4)
	graph:removeListenerButtonClick(acc.PilotBtn5)
end
function StartHeartUI()
    graph:StartHeartUIS()
    -- HeartSpine_Btn.gameObject:SetActive(true)
    -- HeartSpine_Btn.enabled = false
    -- HeartSpine:SetActive(true)
    -- --HeartSpine_SK.AnimationState:SetAnimation(0, "START", false)
    -- HeartSpine_SK.AnimationState:AddAnimation(0,"START",false,0)
    -- HeartSpine_SK.AnimationState:AddAnimation(0,"continu",true,1.667)
    -- graph:InvokeLua()
    -- HeartSpine_Btn.onClick:AddListener(function ()
    --     HeartSpine_SK.AnimationState:SetAnimation(0, "END", false)
    --    if userData.Part>=100 then
    --     local cFeelReward = cc.getDataModel(pte.CFeelReward);
    --     local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
    --     sock:sendMessage(pte.CFeelReward, cFeelReward:Build():ToByteArray())
    --     else
    --         print("齿轮不足")
    --     end
    -- end)
end
function updatePart(evt)
    userData = cc.getDataModel(pte.SUserData)
    local num = graph:GetSplitStr(userData.Part/100)
    PartTxt.text = num[0]
end
