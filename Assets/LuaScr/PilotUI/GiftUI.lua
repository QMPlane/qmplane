local acc = CS.Sacu.Utils.ActionCollection
local Vector3 = CS.UnityEngine.Vector3
local Vector2 = CS.UnityEngine.Vector2
local Quaternion = CS.UnityEngine.Quaternion
local pte = CS.Sacu.Utils.ProtoTypeEnum;
local sut = CS.Sacu.Utils.SingleUpdateTypeEnum;

local cc = CS.Sacu.Utils.CommandCollection;
local proto = CS.org.jiira.protobuf;
local graph
local closeBtn
local itemDic
local characterList
local feelDic
local ShootDownDic
local GiftCell
local CellParent
local FeelTxt
local IdTxt
function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
end
function onStart(obj)
    FeelTxt = graph:getGameObjectByTypeName("FeelTxt", "Text");
    GiftCell = graph:getGameObjectByTypeName("Scroll View/Viewport/Content/cn_GiftCellUI", "Image").gameObject;
    CellParent = graph:getGameObjectByTypeName("Scroll View/Viewport/Content", "GridLayoutGroup").gameObject;
end

--注册监听事件
function onRegister()
    graph:addListenerButtonClick("CloseBtn", closePage)
    graph:addEventDispatcherWithHandle(acc.GiftBtn .. acc.Open, "openPage")
    --graph:addEventDispatcherWithHandle(acc.Update .. sut.Feel:ToString(), "sendGift")
end

function onRemove()
    graph:removeEventDispatcher(acc.GiftBtn .. acc.Open)
    --graph:removeEventDispatcher(acc.Update .. sut.Feel)
    --ActionCollection.Update + SingleUpdateTypeEnum.Feel
end
function flipPagehandler(evt)
    graph:dispatchEvent(evt.name .. acc.Open)--打开新界面
end
function openPage(evt)
    graph:setActive(true);
    Id =evt.Body--取得上个界面所存的ID
    loadSkill()
end
function closePage(evt)
    graph:setActive(false);
    FeelTxt.text = 0
end
--得到静态表
function loadSkill(evt)
    local StaticCharacterList = proto.STCharacter.getList();
    for key, value in pairs(StaticCharacterList) 
    do
        if value.Id==Id then
            print(value.Id)
        end
    end
    getCharacterLists()
    getItemsLists()
end
--将C#的机师的list传过来
function getCharacterList(arg1)
    characterList =arg1
end
--将C#的Items的list传过来
function getItemsDic(arg1)
    itemDic =arg1
end
--将传过来的机师值分成两个字典一个好感度 一个击杀数  动态表
function getCharacterLists(arg1)
    feelDic = characterList[0]
    ShootDownDic = characterList[1]
    for key, value in pairs(feelDic) do
        if key == Id then
            FeelTxt.text = value
        end
    end
    for key, value in pairs(ShootDownDic) do
    end
end
--遍历道具的ID和数量的字典 动态表 加载道具的 ID数量  发送消息
function getItemsLists(arg1)
    for key, value in pairs(itemDic) do
        print(key,value)
        local cell = graph:LoadPrefabs(GiftCell,CellParent)
        IdTxt = cell.transform:Find("Text"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        IdTxt.text = key
        cell:GetComponent(typeof(CS.UnityEngine.UI.Button)).onClick:AddListener(function ()
            local cUseItem = cc.getDataModel(pte.CUseItem);
            cUseItem:SetCharacterID(Id);
            cUseItem:SetItemID(key);
            cUseItem:SetCount(1)
            local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
            sock:sendMessage(pte.CUseItem, cUseItem:Build():ToByteArray())
            --IdTxt.text= feelDic[Id]
        end)
    end
    GiftCell:SetActive(false)
end
function changeTxt(arg1)
    local up = arg1
    FeelTxt.text= up.Count
end
-- function sendGift(evt)
--     local up = evt.Body;
--     print("??????????????")
--     for key, value in pairs(feelDic) do
--         if key == up.ID then
--             print("================")
--             print(up.Count)
--             print(value)
--             value = up.Count;
--             FeelTxt.text= up.Count
--         end
--     end
-- end