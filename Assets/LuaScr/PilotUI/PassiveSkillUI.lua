local acc = CS.Sacu.Utils.ActionCollection
local Vector3 = CS.UnityEngine.Vector3
local Vector2 = CS.UnityEngine.Vector2
local Quaternion = CS.UnityEngine.Quaternion
local pte = CS.Sacu.Utils.ProtoTypeEnum;
local cc = CS.Sacu.Utils.CommandCollection;
local graph
local closeBtn
local SkillBtn1
local SkillBtn2
local SkillBtn3
local SkillBtn4
local SkillBtn5
local SkillBtn6
local SkillBtn7
local SkillBtn8
local SkillBtn9
local SkillBtn10
local SkillBtn11
local SkillBtn12
local SkillBtnT = {}--定义一个按钮的表
local SkillBtnT1 = {}--定义一个等级大于5小于10的按钮的表
local SkillBtnT2 = {}--定义一个等级大于10的按钮的表
local SkillTxt--技能描述
local Skill_Ima--技能的黄条图片
local SkillAtlas
local ClickBtnIma1_T = {}--点击的按钮
local ClickBtnIma2_T = {}--点击的按钮

function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
    graph:addListenerButtonClick("BgIma/CloseBtn", closePage)
    SkillTxt = graph:getGameObjectByTypeName("BgIma/Skill/Image","Image")
    Skill_Ima = graph:getGameObjectByTypeName("BgIma/Skill/Image/Image","Image")

end
function onStart(obj)
    SkillAtlas = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/SkillAtlas")
    --现将按钮加到表里面  按照ID   math.floor(8%3)  取余
    for i = 1, 12 do
        local btn = graph:getGameObjectByTypeName("BgIma/Skill/SkillBtn"..i,"Button")
        table.insert(SkillBtnT,{ID = 5500+i,Btn = btn})
    end
    --将现有的表根据机师的ID存到两个表里 
    for key, value in pairs(SkillBtnT) do
        value.Btn.onClick:AddListener(function ()
            print("技能"..value.ID.."还没有解锁")
        end)
        if math.floor(value.ID%2) == 0 then
            local id = 1000+(key/2)
            local ids = string.sub(id,1,4)
            table.insert(SkillBtnT1,{ID = ids,SkillID = value.ID,Btn = value.Btn})
        else
            local id_ = 1000+(key/2)+1
            local ids_ = string.sub(id_,1,4)
            table.insert(SkillBtnT2,{ID = ids_,SkillID = value.ID,Btn = value.Btn})
        end
    end
end

--注册监听事件
function onRegister()
	--graph:addEventDispatcherWithHandle(acc.PassiveSkillUI .. acc.Close, "closePage")
    graph:addEventDispatcherWithHandle(acc.PassiveSkillUI .. acc.Open, "openPage")
end

function onRemove()
    --graph:removeEventDispatcher(acc.PassiveSkillUI .. acc.Close)
    graph:removeEventDispatcher(acc.PassiveSkillUI .. acc.Open)
end
function openPage(evt)
    graph:setActive(true)
    LoadSkill()
end
function closePage(evt)
    graph:setActive(false)
end
--遍历存的两个表
function LoadSkill()
    --将方法分为两个  一个将所有的表
    local ModelFeelT = graph.FeelList
    for key1, value1 in pairs(ModelFeelT) do
        local feels = value1
        local levels = graph:GetLevelFeel(feels)
        --遍历技能一的表  技能一的表是指在5级之上的
        if levels >= 5  then
            for key, value in pairs(SkillBtnT2) do
                --先将所有图片的回复
                if tonumber(value.ID) == tonumber(key1) then
                    --将图片换成及解锁并且添加监听事件
                    print("等级在5到10".."机师ID"..value.ID.."技能ID"..value.SkillID)
                   local ClickBtnIma = value.Btn.gameObject:GetComponent(typeof(CS.UnityEngine.UI.Image))
                   ClickBtnIma.sprite = SkillAtlas:GetSprite("cn_"..value.SkillID.."_1")
                   value.Btn.onClick:RemoveAllListeners()
                   --添加按钮的监听事件
                   value.Btn.onClick:AddListener(function ()
                    --定义一个表记录每次点击的按钮  在每次点击之前先遍历这个表  将表中的图片换成已解锁的图
                        for key_T1, value_T1 in pairs(ClickBtnIma1_T) do
                            print(key_T1)
                            value_T1.Btn.sprite = SkillAtlas:GetSprite("cn_"..value_T1.ID.."_1")
                        end
                        for key_T2, value_T2 in pairs(ClickBtnIma2_T) do
                            print(key_T2)
                            value_T2.Btn.sprite = SkillAtlas:GetSprite("cn_"..value_T2.ID.."_1")
                        end
                        table.insert(ClickBtnIma1_T,{ID = value.SkillID,Btn = ClickBtnIma}) --将ID和物体存到表里 遍历回复
                        --这块会重复添加数据到表里面  
                        print("技能"..value.SkillID.."的技能描述")
                        ClickBtnIma.sprite = SkillAtlas:GetSprite("cn_"..value.SkillID.."_2")
                        SkillTxt.sprite = SkillAtlas:GetSprite("cn_Description_"..value.SkillID)
                        Skill_Ima.gameObject:SetActive(false)
                   end)
                end
            end
        end
        --遍历技能二的表   技能二是指在十级以上的
        if levels >= 10  then
            for key, value in pairs(SkillBtnT1) do
                if tonumber(value.ID) == tonumber(key1) then
                    --将图片换成及解锁并且添加监听事件
                    print("等级在10以上".."机师ID"..value.ID.."技能ID"..value.SkillID)
                   local ClickBtnIma = value.Btn.gameObject:GetComponent(typeof(CS.UnityEngine.UI.Image))
                   ClickBtnIma.sprite = SkillAtlas:GetSprite("cn_"..value.SkillID.."_1")
                   value.Btn.onClick:RemoveAllListeners()
                   value.Btn.onClick:AddListener(function ()
                    for key_T1, value_T1 in pairs(ClickBtnIma1_T) do
                        print(key_T1)
                        value_T1.Btn.sprite = SkillAtlas:GetSprite("cn_"..value_T1.ID.."_1")
                    end
                    for key_T2, value_T2 in pairs(ClickBtnIma2_T) do
                        print(key_T2)
                        value_T2.Btn.sprite = SkillAtlas:GetSprite("cn_"..value_T2.ID.."_1")
                    end
                    table.insert(ClickBtnIma2_T,{ID = value.SkillID,Btn = ClickBtnIma}) --将ID和物体存到表里 遍历回复
                    print("技能"..value.SkillID.."的技能描述")
                    ClickBtnIma.sprite = SkillAtlas:GetSprite("cn_"..value.SkillID.."_2")
                    local num = tonumber(value.SkillID) - 1
                    SkillTxt.sprite = SkillAtlas:GetSprite("cn_Description_"..num)
                    Skill_Ima.gameObject:SetActive(false)
                    end)
                end
            end   
        end
    end
end
