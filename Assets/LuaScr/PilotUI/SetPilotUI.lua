local acc = CS.Sacu.Utils.ActionCollection
local Vector3 = CS.UnityEngine.Vector3
local Vector2 = CS.UnityEngine.Vector2
local Quaternion = CS.UnityEngine.Quaternion
local pte = CS.Sacu.Utils.ProtoTypeEnum;
local cc = CS.Sacu.Utils.CommandCollection;
local proto = CS.org.jiira.protobuf;
local single = CS.Sacu.Utils.SingleUpdateTypeEnum
local isbool = true
local graph
local Id
local staticIDList
local characterList
local feelList
local ShootDownList
local content--列表中content的物体
local sco
local itemDic
local feelDic
local LevelFeelT= {}
local PilotCell
local CellParent
local GiftTxt1
local GiftTxt2
local GiftTxt3
local GiftTxt4
local GiftTxt5
local GiftTxt6
local GiftTxt7
local GiftTxt8
local GiftBtn1
local GiftBtn2
local GiftBtn3
local GiftBtn4
local GiftBtn5
local GiftBtn6
local GiftBtn7
local GiftBtn8
local Plate1
local Plate2
local Plate3
local Plate4
local Plate5
local Plate6
local Plate7
local Plate8
local TxtIma1
local TxtIma2
local TxtIma3
local TxtIma4
local TxtIma5
local TxtIma6
local TxtIma7
local TxtIma8
local FeelT = {}--记录循环生成的好感的TExt的表
local CellT = {}--记录Cell的表
local iconAtlasPilotIma
local iconAtlasPilotAIma
local iconAtlasPilotBIma
local ChoseImaT = {}--记录图标出战的表
local  staticList
local refreshT = {}--刷新数据要用到的表  一般去遍历这个表刷新  在第一次生成页面的时候讲需要刷新的物体添加进去
function new(gw)
	graph = gw
    gameObject = gw.SAGameObject
    transform = gw.SATransform
end
function onStart(obj)
    iconAtlasPilotIma = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/PilotImaAtlas")
    iconAtlasPilotAIma = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/Pilot_AAtlas")
    iconAtlasPilotBIma = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/Pilot_BAtlas")

    
    staticList = proto.STCharacter.getList()
    content = graph:getGameObjectByTypeName("Scroll View/Viewport/Content","GridLayoutGroup").gameObject
    sco = graph:getGameObjectByTypeName("Scroll View","Image").gameObject
    PilotCell   = graph:getGameObjectByTypeName("Scroll View/Viewport/Content/Pilot","Image").gameObject
    CellParent  = graph:getGameObjectByTypeName("Scroll View/Viewport/Content","GridLayoutGroup").gameObject
    GiftTxt1 = graph:getGameObjectByTypeName("GiftBtn/FrameIma1/6002Txt","Text")
    GiftTxt2 = graph:getGameObjectByTypeName("GiftBtn/FrameIma2/6003Txt","Text")
    GiftTxt3 = graph:getGameObjectByTypeName("GiftBtn/FrameIma3/6004Txt","Text")
    GiftTxt4 = graph:getGameObjectByTypeName("GiftBtn/FrameIma4/6001Txt","Text")
    GiftTxt5 = graph:getGameObjectByTypeName("GiftBtn/FrameIma5/6007Txt","Text")
    GiftTxt6 = graph:getGameObjectByTypeName("GiftBtn/FrameIma6/6005Txt","Text")
    GiftTxt7 = graph:getGameObjectByTypeName("GiftBtn/FrameIma7/6006Txt","Text")
    GiftTxt8 = graph:getGameObjectByTypeName("GiftBtn/FrameIma8/6008Txt","Text")
    GiftBtn1 = graph:getGameObjectByTypeName("GiftBtn/FrameIma1/Button","Button")
    GiftBtn2 = graph:getGameObjectByTypeName("GiftBtn/FrameIma2/Button","Button")
    GiftBtn3 = graph:getGameObjectByTypeName("GiftBtn/FrameIma3/Button","Button")
    GiftBtn4 = graph:getGameObjectByTypeName("GiftBtn/FrameIma4/Button","Button")
    GiftBtn5 = graph:getGameObjectByTypeName("GiftBtn/FrameIma5/Button","Button")
    GiftBtn6 = graph:getGameObjectByTypeName("GiftBtn/FrameIma6/Button","Button")
    GiftBtn7 = graph:getGameObjectByTypeName("GiftBtn/FrameIma7/Button","Button")
    GiftBtn8 = graph:getGameObjectByTypeName("GiftBtn/FrameIma8/Button","Button")
    Plate1 = graph:getGameObjectByTypeName("Gift/GiftBg/Plate/Plate1","Image")
    Plate2 = graph:getGameObjectByTypeName("Gift/GiftBg/Plate/Plate2","Image")
    Plate3 = graph:getGameObjectByTypeName("Gift/GiftBg/Plate/Plate3","Image")
    Plate4 = graph:getGameObjectByTypeName("Gift/GiftBg/Plate/Plate4","Image")
    Plate5 = graph:getGameObjectByTypeName("Gift/GiftBg/Plate/Plate5","Image")
    Plate6 = graph:getGameObjectByTypeName("Gift/GiftBg/Plate/Plate6","Image")
    Plate7 = graph:getGameObjectByTypeName("Gift/GiftBg/Plate/Plate7","Image")
    Plate8 = graph:getGameObjectByTypeName("Gift/GiftBg/Plate/Plate8","Image")
    TxtIma1 = graph:getGameObjectByTypeName("GiftBtn/FrameIma1/TxtIma","Image")
    TxtIma2 = graph:getGameObjectByTypeName("GiftBtn/FrameIma2/TxtIma","Image")
    TxtIma3 = graph:getGameObjectByTypeName("GiftBtn/FrameIma3/TxtIma","Image")
    TxtIma4 = graph:getGameObjectByTypeName("GiftBtn/FrameIma4/TxtIma","Image")
    TxtIma5 = graph:getGameObjectByTypeName("GiftBtn/FrameIma5/TxtIma","Image")
    TxtIma6 = graph:getGameObjectByTypeName("GiftBtn/FrameIma6/TxtIma","Image")
    TxtIma7 = graph:getGameObjectByTypeName("GiftBtn/FrameIma7/TxtIma","Image")
    TxtIma8 = graph:getGameObjectByTypeName("GiftBtn/FrameIma8/TxtIma","Image")
    GiftIma1 = graph:getGameObjectByTypeName("GiftBtn/FrameIma1/Button","Image")
    GiftIma2 = graph:getGameObjectByTypeName("GiftBtn/FrameIma2/Button","Image")
    GiftIma3 = graph:getGameObjectByTypeName("GiftBtn/FrameIma3/Button","Image")
    GiftIma4 = graph:getGameObjectByTypeName("GiftBtn/FrameIma4/Button","Image")
    GiftIma5 = graph:getGameObjectByTypeName("GiftBtn/FrameIma5/Button","Image")
    GiftIma6 = graph:getGameObjectByTypeName("GiftBtn/FrameIma6/Button","Image")
    GiftIma7 = graph:getGameObjectByTypeName("GiftBtn/FrameIma7/Button","Image")
    GiftIma8 = graph:getGameObjectByTypeName("GiftBtn/FrameIma8/Button","Image")
    
end

--注册监听事件
function onRegister()
    graph:addListenerButtonClick("BackBtn", closePage)
	-- --graph:addEventDispatcherWithHandle(acc.PassiveSkillUI .. acc.Close, "closePage")
    graph:addEventDispatcherWithHandle(acc.PilotBtn .. acc.Open, "openPage")
    graph:addEventDispatcherWithHandle(acc.PilotBtn1 .. acc.Open, "openPage")
    graph:addEventDispatcherWithHandle(acc.PilotBtn2 .. acc.Open, "openPage")
    graph:addEventDispatcherWithHandle(acc.PilotBtn3 .. acc.Open, "openPage")
    graph:addEventDispatcherWithHandle(acc.PilotBtn4 .. acc.Open, "openPage")
    graph:addEventDispatcherWithHandle(acc.PilotBtn5 .. acc.Open, "openPage")
    --graph:addEventDispatcherWithHandle(acc.Update .. single.Model, "updateModel")
    -- graph:addListenerButtonClick(acc.GiftBtn, flipPagehandler)
end
function Update()
    GetTargetID()
end
function onRemove()
    --graph:removeEventDispatcher(acc.PassiveSkillUI .. acc.Close)
    graph:removeEventDispatcher(acc.PilotBtn .. acc.Open)
    graph:removeEventDispatcher(acc.PilotBtn1 .. acc.Open)
    graph:removeEventDispatcher(acc.PilotBtn2 .. acc.Open)
    graph:removeEventDispatcher(acc.PilotBtn3 .. acc.Open)
    graph:removeEventDispatcher(acc.PilotBtn4 .. acc.Open)
    graph:removeEventDispatcher(acc.PilotBtn5 .. acc.Open)
    graph:removeEventDispatcher(acc.PilotBtn5 .. acc.Open)
end
function flipPagehandler(evt)
    graph:dispatchEvent(evt.name .. acc.Open,Id)--打开新界面
end
function openPage(evt)
    graph:setActive(true)

    graph:AddSC(sco)
    Id =evt.Body--取得上个界面所存的ID
    if isbool then
        LoadCell()
        isbool = false
    end
    
    moveTarget()
    LoadGift()
    PilotCell:SetActive(false)
    -- graph:SendDic()
    -- getCharacterLists()
    -- loadSkill()
end
function closePage(evt)
    graph:CloseClick()
    graph:DeleteSC(sco)
    graph:setActive(false);
    Id = nil

    --CS.Sacu.Utils.SAManager.Instance:sendMessageToFactory("PilotUIFactory", acc.ISItem .. acc.Open);--在关闭这个页面的时候将监听道具的监听事件加到这个工人上
end
--将列表中的某个Cell作为第一个要展示的界面
function moveTarget(evt)
    if Id == 1001 then
        content.transform.localPosition = Vector3(8, 54, 0)
    elseif Id == 1002 then
        content.transform.localPosition = Vector3(-1079.85, 54, 0)
    elseif Id == 1003 then
        content.transform.localPosition = Vector3(-2160.12, 54, 0)
    elseif Id == 1004 then
        content.transform.localPosition = Vector3(-3240, 54, 0)
    elseif Id == 1005 then
        content.transform.localPosition = Vector3(-4320, 54, 0)
    elseif Id == 1006 then
        content.transform.localPosition = Vector3(-5400, 54, 0)
    end
    
end
function GetTargetID(evt)
    --print(content.transform.localPosition.x)
    if content.transform.localPosition.x >= -500 and content.transform.localPosition.x <= 500 then
        Id = 1001
    elseif content.transform.localPosition.x <= -500 and content.transform.localPosition.x >= -1500 then
        Id = 1002
    elseif content.transform.localPosition.x <= -1500 and content.transform.localPosition.x >= -2500 then
        Id = 1003
    elseif content.transform.localPosition.x <= -2500 and content.transform.localPosition.x >= -3500 then
        Id = 1004
    elseif content.transform.localPosition.x <= -3500 and content.transform.localPosition.x >= -4700 then
        Id = 1005
    elseif content.transform.localPosition.x <= -4700 and content.transform.localPosition.x >= -5700 then
        Id = 1006
    end
end
--由于是写死的   所以根据名字去找对应的预设   
--1===6002  2===6003   3===6004   4===6001   5===6007   6===6005   7===6006   8===6010
function LoadGift()
    graph:GetItems()
    graph:CloseClick()
    local iconAtlasGiftPlate = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/PlateAtlas")
    local iconAtlasGiftFrame = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/Pilot_BAtlas")
    local iconAtlasGift = CS.Sacu.Utils.SACache.getSpriteAtlasWithName("Atlas/GiftIconAtlas")
   

    local itemDic = graph.ItemDic
    if itemDic:ContainsKey(6002) then
        GiftTxt1.text = itemDic[6002]
        if itemDic[6002] == 0 then
            Plate1.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_1_1")
            TxtIma1.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_2_bar")
            GiftIma1.sprite = iconAtlasGift:GetSprite("cn_6002_1")
        GiftBtn1.onClick:AddListener(function ()
            print("礼物数量为0")
        end)
        else
            Plate1.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_1")
            TxtIma1.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_1_bar")
            GiftIma1.sprite = iconAtlasGift:GetSprite("cn_6002")

            GiftBtn1.onClick:AddListener(function ()
                local cUseItem = cc.getDataModel(pte.CUseItem);
                cUseItem:SetCharacterID(Id);
                cUseItem:SetItemID(6002);
                cUseItem:SetCount(1)
                local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
                sock:sendMessage(pte.CUseItem, cUseItem:Build():ToByteArray())
            end)
        end
    else
        Plate1.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_1_1")
        TxtIma1.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_2_bar")
        GiftIma1.sprite = iconAtlasGift:GetSprite("cn_6002_1")

        GiftBtn1.onClick:AddListener(function ()
            print("礼物数量为0")
        end)
    end
    if itemDic:ContainsKey(6003) then
        GiftTxt2.text = itemDic[6003] 
        if itemDic[6003] == 0 then
            Plate2.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_2_1")
            TxtIma2.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_2_bar")
            GiftIma2.sprite = iconAtlasGift:GetSprite("cn_6003_1")

        GiftBtn2.onClick:AddListener(function ()
            print("礼物数量为0")
        end)
        else
            Plate2.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_2")
            TxtIma2.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_1_bar")
            GiftIma2.sprite = iconAtlasGift:GetSprite("cn_6003")

            GiftBtn2.onClick:AddListener(function ()
                local cUseItem = cc.getDataModel(pte.CUseItem);
                cUseItem:SetCharacterID(Id);
                cUseItem:SetItemID(6003);
                cUseItem:SetCount(1)
                local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
                sock:sendMessage(pte.CUseItem, cUseItem:Build():ToByteArray())
            end)
        end
    else
        Plate2.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_2_1")
        TxtIma2.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_2_bar")
        GiftIma2.sprite = iconAtlasGift:GetSprite("cn_6003_1")

        GiftBtn2.onClick:AddListener(function ()
            print("礼物数量为0")
        end)
    end
    if itemDic:ContainsKey(6004) then
        GiftTxt3.text = itemDic[6004]
        if itemDic[6004] == 0 then
            Plate3.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_3_1")
            TxtIma3.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_2_bar")
            GiftIma3.sprite = iconAtlasGift:GetSprite("cn_6004_1")

        GiftBtn3.onClick:AddListener(function ()
            print("礼物数量为0")
        end)
        else
            Plate3.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_3")
            TxtIma3.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_1_bar")
            GiftIma3.sprite = iconAtlasGift:GetSprite("cn_6004")

        GiftBtn3.onClick:AddListener(function ()
            local cUseItem = cc.getDataModel(pte.CUseItem);
            cUseItem:SetCharacterID(Id);
            cUseItem:SetItemID(6004);
            cUseItem:SetCount(1)
            local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
            sock:sendMessage(pte.CUseItem, cUseItem:Build():ToByteArray())
        end)
        end
    else
        Plate3.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_3_1")
        TxtIma3.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_2_bar")
        GiftIma3.sprite = iconAtlasGift:GetSprite("cn_6004_1")

        GiftBtn3.onClick:AddListener(function ()
            print("礼物数量为0")
        end)
    end
    if itemDic:ContainsKey(6001) then
        GiftTxt4.text = itemDic[6001]
        if itemDic[6001] == 0 then
            Plate4.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_4_1")
            TxtIma4.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_2_bar")
            GiftIma4.sprite = iconAtlasGift:GetSprite("cn_6001_1")

        GiftBtn4.onClick:AddListener(function ()
            print("礼物数量为0")
        end)
        else
            Plate4.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_4")
            TxtIma4.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_1_bar")
            GiftIma4.sprite = iconAtlasGift:GetSprite("cn_6001")

            GiftBtn4.onClick:AddListener(function ()
                local cUseItem = cc.getDataModel(pte.CUseItem);
                cUseItem:SetCharacterID(Id);
                cUseItem:SetItemID(6001);
                cUseItem:SetCount(1)
                local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
                sock:sendMessage(pte.CUseItem, cUseItem:Build():ToByteArray())
            end)
        end
    else
        Plate4.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_4_1")
        TxtIma4.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_2_bar")
        GiftIma4.sprite = iconAtlasGift:GetSprite("cn_6001_1")

        GiftBtn4.onClick:AddListener(function ()
            print("礼物数量为0")
        end)
    end
    if itemDic:ContainsKey(6007) then
        GiftTxt5.text = itemDic[6007]
        if itemDic[6007] == 0 then
            Plate5.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_5_1")
        TxtIma5.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_2_bar")
        GiftIma5.sprite = iconAtlasGift:GetSprite("cn_6007_1")

        GiftBtn5.onClick:AddListener(function ()
            print("礼物数量为0")
        end)
        else
            Plate5.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_5")
            TxtIma5.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_1_bar")
            GiftIma5.sprite = iconAtlasGift:GetSprite("cn_6007")

        GiftBtn5.onClick:AddListener(function ()
            local cUseItem = cc.getDataModel(pte.CUseItem);
            cUseItem:SetCharacterID(Id);
            cUseItem:SetItemID(6007);
            cUseItem:SetCount(1)
            local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
            sock:sendMessage(pte.CUseItem, cUseItem:Build():ToByteArray())
        end)
        end
    else
        Plate5.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_5_1")
        TxtIma5.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_2_bar")
        GiftIma5.sprite = iconAtlasGift:GetSprite("cn_6007_1")

        GiftBtn5.onClick:AddListener(function ()
            print("礼物数量为0")
        end)
    end
    if itemDic:ContainsKey(6005) then
        GiftTxt6.text = itemDic[6005]
        if itemDic[6005] == 0 then
            Plate6.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_6_1")
            TxtIma6.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_2_bar")
            GiftIma6.sprite = iconAtlasGift:GetSprite("cn_6005_1")

            GiftBtn6.onClick:AddListener(function ()
                print("礼物数量为0")
            end)
        else
            Plate6.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_6")
            TxtIma6.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_1_bar")
            GiftIma6.sprite = iconAtlasGift:GetSprite("cn_6005")

        GiftBtn6.onClick:AddListener(function ()
            local cUseItem = cc.getDataModel(pte.CUseItem);
            cUseItem:SetCharacterID(Id);
            cUseItem:SetItemID(6005);
            cUseItem:SetCount(1)
            local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
            sock:sendMessage(pte.CUseItem, cUseItem:Build():ToByteArray())
        end)
        end
    else
        Plate6.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_6_1")
        TxtIma6.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_2_bar")
        GiftIma6.sprite = iconAtlasGift:GetSprite("cn_6005_1")

        GiftBtn6.onClick:AddListener(function ()
            print("礼物数量为0")
        end)
    end
    if itemDic:ContainsKey(6006) then
        GiftTxt7.text = itemDic[6006]
        if itemDic[6006] == 0 then
            Plate7.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_7_1")
        TxtIma7.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_2_bar")
        GiftIma7.sprite = iconAtlasGift:GetSprite("cn_6006_1")

        GiftBtn7.onClick:AddListener(function ()
            print("礼物数量为0")
        end)
        else
            Plate7.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_7")
            TxtIma7.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_1_bar")
            GiftIma7.sprite = iconAtlasGift:GetSprite("cn_6006")

        GiftBtn7.onClick:AddListener(function ()
            local cUseItem = cc.getDataModel(pte.CUseItem);
            cUseItem:SetCharacterID(Id);
            cUseItem:SetItemID(6006);
            cUseItem:SetCount(1)
            local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
            sock:sendMessage(pte.CUseItem, cUseItem:Build():ToByteArray())
        end)
        end
    else
        Plate7.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_7_1")
        TxtIma7.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_2_bar")
        GiftIma7.sprite = iconAtlasGift:GetSprite("cn_6006_1")

        GiftBtn7.onClick:AddListener(function ()
            print("礼物数量为0")
        end)
    end
    if itemDic:ContainsKey(6010) then
        GiftTxt8.text = itemDic[6010]
        if itemDic[6010] == 0 then
            Plate8.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_8_1")
        TxtIma8.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_2_bar")
        GiftIma8.sprite = iconAtlasGift:GetSprite("cn_6010_1")

        GiftBtn8.onClick:AddListener(function ()
            print("礼物数量为0")
        end)
        else
            Plate8.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_8")
            TxtIma8.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_1_bar")
            GiftIma8.sprite = iconAtlasGift:GetSprite("cn_6010")

        GiftBtn8.onClick:AddListener(function ()
            local cUseItem = cc.getDataModel(pte.CUseItem);
            cUseItem:SetCharacterID(Id);
            cUseItem:SetItemID(6010);
            cUseItem:SetCount(1)
            local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
            sock:sendMessage(pte.CUseItem, cUseItem:Build():ToByteArray())
        end)
        end
    else
        Plate8.sprite = iconAtlasGiftPlate:GetSprite("cn_plate_8_1")
        TxtIma8.sprite = iconAtlasGiftFrame:GetSprite("cn_frame_2_bar")
        GiftIma8.sprite = iconAtlasGift:GetSprite("cn_6010_1")

        GiftBtn8.onClick:AddListener(function ()
            print("礼物数量为0")
        end)
    end

    -- print(itemDic[6001])
    -- for key, value in pairs(itemDic) do
    --     print(key,value)
    --     print(GiftTxt1.gameObject.name)
        
    --     -- IdTxt.text = key
    --     -- cell:GetComponent(typeof(CS.UnityEngine.UI.Button)).onClick:AddListener(function ()
    --     --     local cUseItem = cc.getDataModel(pte.CUseItem);
    --     --     cUseItem:SetCharacterID(Id);
    --     --     cUseItem:SetItemID(key);
    --     --     cUseItem:SetCount(1)
    --     --     local sock = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
    --     --     sock:sendMessage(pte.CUseItem, cUseItem:Build():ToByteArray())
    --     --     --IdTxt.text= feelDic[Id]
    --     -- end)
    -- end
end
function LoadCell()
    --========================================将记录下来的cell删除
    PilotCell:SetActive(true)
    if CellT ~= nil then
        for key, value in pairs(CellT) do
            CS.UnityEngine.Object.Destroy(value)
        end
        
    end
    CellT = {}
    --==========================================
    local PrefabDic = graph.PrefabDic
    local userData = cc.getDataModel(pte.SUserData)
    feelDic = graph.FeelList
    for key1, value1 in pairs(staticList) do
        local cell = graph:LoadPrefabs(PilotCell,CellParent)
        
        table.insert(CellT,cell)
        local cellTF = cell.transform
        local HeroIma = cellTF:Find("LeftIma/HeroIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))
        local FeelTxt = cellTF:Find("LeftIma/FeelTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        local LevelTxt = cellTF:Find("LeftIma/LvTxt"):GetComponent(typeof(CS.UnityEngine.UI.Text))
        LevelTxt.text = 0
        FeelTxt.text = 0
        
        HeroIma.sprite = iconAtlasPilotIma:GetSprite("cn_"..value1.Id)
        local ChoseGo = cellTF:Find("LeftIma/ChoseIma").gameObject
        table.insert(ChoseImaT,{ID = value1.Id,Ima = ChoseGo})--将物体存到表里  id坐键 物体坐值
        local ChoseIma = cellTF:Find("RightIma/CloseBtn"):GetComponent(typeof(CS.UnityEngine.UI.Image))
        local ReIma = cellTF:Find("RightIma/ReIma"):GetComponent(typeof(CS.UnityEngine.UI.Image))--机师的详情面板的描述  每个要更换图片

        local PrefabParent = cellTF:Find("LeftIma/HeroIma").gameObject
        local choseBtn = cellTF:Find("RightIma/CloseBtn"):GetComponent(typeof(CS.UnityEngine.UI.Button))
        table.insert(refreshT,{ID = value1.Id,Feel = FeelTxt,Level = LevelTxt,choseIma = ChoseIma,choseBtn_ = choseBtn})--将要修改的物体存进表里返回消息的时候遍历  修改值
        for key, value in pairs(PrefabDic) do
            if key == value1.Id then
                local prefabCell = graph:LoadPrefabs_(value,PrefabParent)
                ReIma.sprite = iconAtlasPilotIma:GetSprite("cn_Profile_"..key)
            end
        end
        if userData.Model == value1.Id then
            ChoseGo:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasPilotBIma:GetSprite("cn_rectangulard_font")
        end
        -- cellTF:Find("RightIma/CloseBtn"):GetComponent(typeof(CS.UnityEngine.UI.Button)).onClick:AddListener(function ()
        --     local cSetModel = cc.getDataModel(pte.CSetModel);
        --     cSetModel:SetModel(value1.Id)
        --     local sock   = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
        --     sock:sendMessage(pte.CSetModel, cSetModel:Build():ToByteArray())
        -- end)
        for key, value in pairs(feelDic) do
            if key == value1.Id then
                local feels = value
                local levels = graph:GetLevelFeel(feels)
                LevelTxt.text = levels
                FeelTxt.text = feels
                if feels < 100 then
                    ChoseIma.sprite = iconAtlasPilotAIma:GetSprite("btn_choose​​​_off")
                else
                    ChoseIma.sprite = iconAtlasPilotAIma:GetSprite("cn_btn_choose​​​")
                    cellTF:Find("RightIma/CloseBtn"):GetComponent(typeof(CS.UnityEngine.UI.Button)).onClick:AddListener(function ()
                        local cSetModel = cc.getDataModel(pte.CSetModel);
                        cSetModel:SetModel(key)
                        local sock   = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
                        sock:sendMessage(pte.CSetModel, cSetModel:Build():ToByteArray())
                    end)
                end
            end
        end
    end    
    PilotCell:SetActive(false)
end

function updateModel(evt)
    local userData = cc.getDataModel(pte.SUserData)
    for key, value in pairs(ChoseImaT) do
        value.Ima:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasPilotBIma:GetSprite("cn_rectangulard_font_01")
        --value.Ima:SetActive(false)
    end
    for key, value in pairs(ChoseImaT) do
        if userData.Model == value.ID then
        value.Ima:GetComponent(typeof(CS.UnityEngine.UI.Image)).sprite = iconAtlasPilotBIma:GetSprite("cn_rectangulard_font")
           -- value.Ima:SetActive(true)
        end
    end
end
--写一个根据好感度判断等级的方法

--不按递增数列升级  先留着 不要删除
-- function getLevelFell(arg1)
--     --
--     -- local StaticCharacterLevelList = proto.STCharacterLevel.getList();
--     -- --首先计算每个等级对应的好感度存到表里面  将每次的好感度都加上  
--     -- local Feels = 0
--     -- local TFeelStatic = {}
--     -- for key, value in pairs(StaticCharacterLevelList) do
--     --     Feels = Feels + value.Feel
--     --     table.insert(LevelFeelT,{Level = value.Id ,Feel = Feels})--等级对应的好感度的值
--     -- end
-- end
--加载技能
function loadSkill(evt)
    local StaticCharacterList = proto.STCharacter.getList();
    for key, value in pairs(StaticCharacterList) 
    do
        if value.Id==Id then
            print(value.Id)
        end
    end
end
function updateFeel(evt)
    local up = evt.Body;
    graph:GetCharacterIns()
    feelDic = graph.FeelList
    for key, value in pairs(feelDic) do
        for key1, value1 in pairs(refreshT) do
            if key == value1.ID then
                local feels = value
                local levels = graph:GetLevelFeel(feels)
                if tonumber(value1.Level.text) ~= levels then
                    value1.Level.text = levels
                    --一旦等级变化  发消息到弹窗模块将得到的道具显示出来  如果用item的监听因为送礼物也会收到这个消息  所以pass
                    --遍历静态表将得到的ID和等级传进去  得到要显示的道具ID和数量
                    for i = 0, 9 do
                        for key_static, value_static in pairs(staticList) do
                            local arry = graph:GetSplitStr(value_static.LevelReward[i])
                            local Level_ = arry[0]
                            local GiftID_ = arry[1]
                            local GiftCount_ =arry[2]
                            if tonumber(value_static.Id) == tonumber(value1.ID) and tonumber(levels) == tonumber(Level_) then
                                graph:AddInt(GiftID_,GiftCount_)
                                CS.Sacu.Utils.SAManager.Instance:sendMessageToFactory("QNSActiveUIFactory", acc.ItemActive .. acc.Open, graph.UPList);--向弹窗工厂发消息
                            end
                        end
                    end
                end
                value1.Feel.text = feels
                if feels < 100 then
                    value1.choseIma.sprite = iconAtlasPilotAIma:GetSprite("btn_choose​​​_off")
                else
                    value1.choseIma.sprite = iconAtlasPilotAIma:GetSprite("cn_btn_choose​​​")
                    value1.choseBtn_.onClick:AddListener(function ()
                        local cSetModel = cc.getDataModel(pte.CSetModel);
                        cSetModel:SetModel(key)
                        local sock   = CS.Sacu.Utils.IOCManager.Instance:getIOCDataWorker("Datas.SocketDataWorker")
                        sock:sendMessage(pte.CSetModel, cSetModel:Build():ToByteArray())
                    end)
                end
            end
        end
    end
end
-- function updateItem(evt)
--     local up = evt.Body;
--     print("Item"..up.Count)
-- end

