function Location(transform)
	local localPosition = transform.localPosition;
	localPosition.x = 0.789;
	localPosition.y = 0;
	localPosition.z = 0;
	transform.localPosition = localPosition;
end

function Collision(cv3, ev3)
	local offset = 0.075;
	return cv3.y + offset >= ev3.y and cv3.y - offset <= ev3.y;
end