function Location(transform)
	local localPosition = transform.localPosition;
	localPosition.x = 0;
	localPosition.y = -18;
	localPosition.z = 0.1;
	transform.localPosition = localPosition;
end

function Collision(cv3, ev3)
	--屏幕左侧全部纵向
	--只判断X轴是否在范围内即可
	return ev3.x < -0.18;
end