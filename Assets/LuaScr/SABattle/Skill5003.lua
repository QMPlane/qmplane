function Location(transform)
	local localPosition = transform.localPosition;
	localPosition.x = 0.6;--1.36;
	localPosition.y = 0;
	localPosition.z = 0;
	transform.localPosition = localPosition;
end

function Collision(cv3, ev3)
	local r = 0.27;--技能半径
	local gap = 0.25;--技能偏移
	return CS.System.Math.Sqrt((ev3.x - (cv3.x + gap)) * (ev3.x - (cv3.x + gap)) + (ev3.y - cv3.y) * (ev3.y - cv3.y)) <= r;
end