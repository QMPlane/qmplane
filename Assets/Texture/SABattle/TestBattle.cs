﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sacu.Utils;

public class TestBattle : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Camera MainCamera = GameObject.Find("Main3DCamera").GetComponent<Camera>();
        //摄像机必须是正交的
        float height = MainCamera.orthographicSize * 2.0f;
        float width = height * MainCamera.aspect;
        transform.localScale = new Vector3(width, height, 0.1f);
        SAUtils.Console("tb : " + transform.localScale.ToString());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
