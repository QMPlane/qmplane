﻿using System.Collections;
using System;
using System.IO;
using UnityEngine;
using System.Xml;
using UnityEngine.Networking;
using Google.Producer.Events;
namespace Sacu.Utils
{
    public class SAGameVersion
    {
        public string ip;//地址
        public int port;//端口
        public string asset_ip;//资源地址
        public int asset_port;//资源端口
        public int ioc_version;//ioc版本
        public int data_version;//datatable版本
        public int resources_version;//资源版本
        public string language;//国际化
        public static string asset_path;//资源路径

        private static SAGameVersion instance;
        public static SAGameVersion Instance
        {
            get
            {
                if (null == instance)
                {
                    instance = new SAGameVersion();
                }
                return instance;
            }
        }
        public SAGameVersion()
        {

        }
        public IEnumerator loadVersion(EventDispatcher eventDispatcher)
        {
            string random = SAAppConfig.ConfigRelease ? "?" + DateTime.Now.ToString("yyyymmddhhmmss") : "";

			string versionFile = (SAAppConfig.ConfigRelease ? SAAppConfig.VersionPath : SAAppConfig.DevResDir + SAAppConfig.DataDir) + SAAppConfig.versionXML + random;
            //SAUtils.Console(versionFile);
            UnityWebRequest www = UnityWebRequest.Get(versionFile);
            yield return www.SendWebRequest();

            if (www.isHttpError || www.isNetworkError)
            {
                SAUtils.Console("loadVersion error : " + versionFile);
                yield break;
            }
            XmlDocument convertXML = SALang.stringConvertXML(www.downloadHandler.text);
            ip = Convert.ToString(convertXML.GetElementsByTagName("ip").Item(0).InnerText);
            //socket端口先强制写死，以后正式上线再改
            port = 8888;// Convert.ToInt32(convertXML.GetElementsByTagName("port").Item(0).InnerText);
            asset_ip = Convert.ToString(convertXML.GetElementsByTagName("asset_ip").Item(0).InnerText);
#if UNITY_IPHONE
            asset_port = Convert.ToInt32(convertXML.GetElementsByTagName("asset_ios_port").Item(0).InnerText);
            asset_path = SAAppConfig.HTTP;
#elif UNITY_ANDROID
            asset_port = Convert.ToInt32(convertXML.GetElementsByTagName("asset_android_port").Item(0).InnerText);
            asset_path = SAAppConfig.HTTP;
#else
            asset_port = Convert.ToInt32(convertXML.GetElementsByTagName("asset_android_port").Item(0).InnerText);
            asset_path = SAAppConfig.HTTP;
#endif
            ioc_version = Convert.ToInt32(convertXML.GetElementsByTagName("ioc_version").Item(0).InnerText);
            data_version = Convert.ToInt32(convertXML.GetElementsByTagName("data_version").Item(0).InnerText);
            resources_version = Convert.ToInt32(convertXML.GetElementsByTagName("resources_version").Item(0).InnerText);
            language = Convert.ToString(convertXML.GetElementsByTagName("language").Item(0).InnerText);

            asset_path += asset_ip + ":" + asset_port + "/" + SAAppConfig.GameName + "/";
            if (!Directory.Exists(Path.Combine(SAAppConfig.RemotePath, SAAppConfig.versionXML)))
            {
                PlayerPrefs.DeleteKey("ioc_version");
                PlayerPrefs.DeleteKey("data_version");
                PlayerPrefs.DeleteKey("resources_version");
                PlayerPrefs.DeleteKey("language");
            }
            if (SALang.MakDirValid(SAAppConfig.RemotePath))
            {
                SALang.writeLocalByteWithName(System.Text.Encoding.UTF8.GetBytes(www.downloadHandler.text), SAAppConfig.versionXML);
            }
            else
            {
                SAUtils.Console("写入路径创建错误");
            }
            SAUtils.Console("版本信息加载结束");

            eventDispatcher.dispatchEvent(new GEvent(ActionCollection.LOAD + "Version"));
        }
    }
}
