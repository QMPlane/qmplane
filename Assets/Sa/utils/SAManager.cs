﻿using UnityEngine;
using Sacu.Collection;
using Sacu.Factory;
using System.Collections.Generic;
using Google.Producer.Events;

namespace Sacu.Utils
{
    public class SAManager
    {

        private static SAManager _instance;
        private SALaunch launch;
        protected EventDispatcher eventDispatcher;//事件域
        public static SAManager Instance
        {
            get
            {
                if (null == _instance)
                {
                    _instance = new SAManager();
                }
                return _instance;
            }
        }
        protected bool _init = false;

        public int UGUILayer;
        //########### factory
        public Vector3 HideVec3;
        public Camera UIRootCamera;
        public Transform UIRoot;

        public Camera MainCamera;

        private SAManager()
        {
            eventDispatcher = new EventDispatcher();
        }

        public void Init(SALaunch launch)
        {
            if (_init)
            {
                //return;
            }
            //GameObject.DontDestroyOnLoad(launch);
            //GameObject.DontDestroyOnLoad(launch.gameObject);
            this.launch = launch;
            UGUILayer = LayerMask.NameToLayer("UGUI");
            UIRoot = GameObject.Find("UIRoot").transform;
            if (null == UIRoot)
            {
                UIRoot = ((GameObject)GameObject.Instantiate(Resources.Load("UIRoot"))).transform;
            }
            MainCamera = GameObject.Find("Main3DCamera").GetComponent<Camera>();
            UIRootCamera = UIRoot.Find("Main2DCamera").GetComponent<Camera>();
            HideVec3 = launch.transform.position - (launch.transform.forward * 1000);
            //SAManager.Instance.UIRootCamera.Find("ConsoleUI").gameObject.SetActive(true);
            _init = true;
            Revert();
        }

        public void Revert()
        {
            killAllFactory();

            eventDispatcher.addEventListener(ActionCollection.LOAD + "Version", this, "loadVersionHandler");
            eventDispatcher.addEventListener(ActionCollection.LOAD + "DataTable", this, "loadDataTableHandler");
            eventDispatcher.addEventListener(ActionCollection.LOAD + "IOC", this, "loadIOCHandler");

            launch.StartCoroutine(SAGameVersion.Instance.loadVersion(eventDispatcher));//版本信息加载
        }
        private void loadVersionHandler(GEvent e)
        {
            launch.StartCoroutine(SADataTable.Instance.loadDataTable(eventDispatcher));//DataTable信息加载
        }
        private void loadDataTableHandler(GEvent e)
        {
            launch.StartCoroutine(IOCManager.Instance.loadIOC(eventDispatcher));//ioc信息加载
        }
        private void loadIOCHandler(GEvent e)
        {
            SAUtils.Console("配置文件加载完毕……");
        }

        //##################### factory controller #################################
        public SAFactory startFactory(string factoryName)
        {
            if (IOCManager.Instance.factorys.ContainsKey(factoryName))
            {
                return startFactory(factoryName, null);
            }
            return null;
        }
        public SAFactory startFactory(string factoryName, object args)
        {
            if (IOCManager.Instance.factorys.ContainsKey(factoryName))
            {
                SAFactory factory = IOCManager.Instance.factorys[factoryName];
                factory.startFactory(args);
                return factory;
            }
            return null;
        }
        public void disposeFactory(string factoryName)
        {
            if (IOCManager.Instance.factorys.ContainsKey(factoryName))
            {
                IOCManager.Instance.factorys[factoryName].disposeFactory();
                Debug.Log(factoryName);
            }
        }
        /**
         * 关闭所有工厂
         */
        public void killAllFactory()
        {
            Dictionary<string, SAFactory> factorys = IOCManager.Instance.factorys;
            Dictionary<string, SAFactory>.Enumerator en = factorys.GetEnumerator();
            KeyValuePair<string, SAFactory> pair;
            while (en.MoveNext())
            {
                pair = en.Current;
                SAUtils.Log("factory name : " + pair.Key);
                pair.Value.KillFactory();
            }
        }

        public SAFactory getFactory(string factoryName)
        {
            if (IOCManager.Instance.factorys.ContainsKey(factoryName))
            {
                return IOCManager.Instance.factorys[factoryName];
            }
            return null;
        }
        /**
         * 向所有工厂发送消息
         */
        public void sendMessageToAllFactory(string type, object body)
        {
            Dictionary<string, SAFactory> factorys = IOCManager.Instance.factorys;
            Dictionary<string, SAFactory>.Enumerator en = factorys.GetEnumerator();
            KeyValuePair<string, SAFactory> pair;
            while (en.MoveNext())
            {
                pair = en.Current;
                pair.Value.dispatchEvent(type, body);
            }
        }

        public void sendMessageToAllFactory(string type)
        {
            sendMessageToAllFactory(type, null);
        }
        public void sendMessageToFactory(string factoryName, string type, object body)
        {
            getFactory(factoryName).dispatchEvent(type, body);
        }
    }
}