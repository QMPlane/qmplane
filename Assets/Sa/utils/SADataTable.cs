﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using org.jiira.protobuf;
using Sacu.Collection;
using System.Text;
using System.IO;
using System;
using UnityEngine.Networking;
using Google.Producer.Events;

namespace Sacu.Utils
{
    public class SADataTable
    {

        public string iostring;

        private static SADataTable instance;
        public static SADataTable Instance
        {
            get
            {
                if (null == instance)
                {
                    instance = new SADataTable();
                }
                return instance;
            }
        }
        public IEnumerator loadDataTable(EventDispatcher eventDispatcher)
        {
            if (!PlayerPrefs.HasKey("data_version") || PlayerPrefs.GetInt("data_version") != SAGameVersion.Instance.data_version)
            {
                string random = SAAppConfig.ConfigRelease ? "?" + DateTime.Now.ToString("yyyymmddhhmmss") : "";
                string versionFile = (SAAppConfig.ConfigRelease ? SAAppConfig.VersionPath : SAAppConfig.DevResDir + SAAppConfig.DataDir) + SAAppConfig.dataTable + random;
                
                UnityWebRequest www = UnityWebRequest.Get(versionFile);
                //SAUtils.Console(versionFile);
                yield return www.SendWebRequest();
				if (www.isHttpError || www.isNetworkError)
                {
                    SAUtils.Console("loadDataTable error : " + www.error);
                    yield break;
                }
                iostring = www.downloadHandler.text;
                //SAUtils.Console("本地写入：" + SAAppConfig.RemotePath);
                if (SALang.MakDirValid(SAAppConfig.RemotePath))
                {
                    SALang.writeLocalByteWithName(System.Text.Encoding.UTF8.GetBytes(iostring), SAAppConfig.dataTable);
                    PlayerPrefs.SetInt("data_version", SAGameVersion.Instance.data_version);
                } else
                {
                    SAUtils.Console("写入路径创建错误");
                }
            }
            else
            {
                iostring = System.Text.Encoding.UTF8.GetString(SALang.readLocalByteWithName(SAAppConfig.dataTable));
            }
            SAProtoDecode.parsing(iostring);
            SAUtils.Console("数据表加载结束");
            
            eventDispatcher.dispatchEvent(new GEvent(ActionCollection.LOAD + "DataTable"));

        }
    }
}
   
