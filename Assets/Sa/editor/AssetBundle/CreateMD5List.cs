﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;

using Sacu.Utils;
public class CreateMD5List
{

	private static bool checkSuffix(string filePath)
	{
		//取后缀
		int i = filePath.LastIndexOf(".");
		if (i >= 0)
		{
			string suffix = filePath.Substring(filePath.LastIndexOf("."));
			if (suffix.Contains(".meta") || suffix.Contains(".u") || suffix.Contains("VersionMD5") || suffix.Contains(".xml") || suffix.Contains(".DS_Store"))
				return true;
		}
		return false;
	}


	public static bool Execute(BuildTarget target)
	{
        string platform = SAAppConfig.Language + BuildPipelinePanel.GetPlatformName(target);
        Dictionary<string, string> DicFileMD5 = new Dictionary<string, string>();
		MD5CryptoServiceProvider md5Generator = new MD5CryptoServiceProvider();
		
		string dir = Path.Combine(SAAppConfig.ReleasePath, platform);

		foreach (string filePath in Directory.GetFiles(dir))
		{
			Debug.Log("##############################");
			Debug.Log(filePath);
			if (checkSuffix(filePath))
				continue;
			Debug.Log(filePath);
			setVersionToDictionary(dir, platform + "/", filePath, DicFileMD5, md5Generator);
		}

		
        AssetDatabase.Refresh();

        return true;
    }
	private static void setVersionToDictionary(string dir, string ext, string filePath, Dictionary<string, string> DicFileMD5, MD5CryptoServiceProvider md5Generator){
		FileStream file = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
		byte[] hash = md5Generator.ComputeHash(file);
		string strMD5 = System.BitConverter.ToString(hash);
		file.Close();

		Debug.Log (filePath);
		string key = ext + filePath.Substring(dir.Length + 1, filePath.Length - dir.Length - 1);
		
		if (DicFileMD5.ContainsKey(key) == false)
			DicFileMD5.Add(key, strMD5);
		else
			Debug.LogWarning("<Two File has the same name> name = " + filePath);
	}
	
}