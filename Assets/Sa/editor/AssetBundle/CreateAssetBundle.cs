﻿using UnityEditor;
using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using Sacu.Utils;
public class CreateAssetBundle
{
    public static bool Execute(UnityEditor.BuildTarget target, BuildAssetBundleOptions bbo, string ext, string[] dirs, string[] extNames)
    {
        return Execute(target, bbo, ext, dirs, extNames, false, "");
    }
    public static bool Execute(UnityEditor.BuildTarget target, BuildAssetBundleOptions bbo, string ext, string[] dirs, string[] extNames, bool fixedAssetName, string assetName)
    {
        AssetDatabase.Refresh();
        return true;
    }
    static void DoSetAssetBundleName(string path, string name)
    {
        StreamReader fs = new StreamReader(path);
        List<string> ret = new List<string>();
        string line;
        while ((line = fs.ReadLine()) != null)
        {
            line = line.Replace("\n", "");
            if (line.IndexOf("assetBundleName:") != -1)
            {
                line = "  assetBundleName: " + name;

            }
            ret.Add(line);
        }
        fs.Close();

        File.Delete(path);

        StreamWriter writer = new StreamWriter(path + ".tmp");
        foreach (var each in ret)
        {
            writer.WriteLine(each);
        }
        writer.Close();

        File.Copy(path + ".tmp", path);
        File.Delete(path + ".tmp");
    }
    private static void CopyFile(DirectoryInfo path, string desPath)
    {
        string sourcePath = path.FullName;
        System.IO.FileInfo[] files = path.GetFiles();
        foreach (System.IO.FileInfo file in files)
        {
            string sourceFileFullName = file.FullName;
            string destFileFullName = sourceFileFullName.Replace(sourcePath, desPath);
            file.CopyTo(destFileFullName, true);
        }
    }
	static string ConvertToAssetBundleName(string ResName)
	{
		return ResName.Replace('/', '.');
	}
}