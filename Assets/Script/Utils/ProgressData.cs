﻿using UnityEngine;
using System.Collections;

namespace Utils
{
    public class ProgressData
    {
        public string name;//名称
        public float total;//总
        public float current;//当前
        public string info;
        //小进度条
        public float sc;
        public ProgressData(float total = 0, float current = 0)
        {
            revert(total, current);
        }
        public void revert(float total = 0, float current = 0)
        {
            this.total = total;
            this.current = current;
            this.info = "";
        }
    }

}
