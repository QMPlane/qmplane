﻿using UnityEngine;
using System.Collections;
using Utils;
using Datas;
using org.jiira.protobuf;

namespace Sacu.Utils
{
    public class ActionCollection
    {
        //战斗相关
        public const int RDlen = 4;//四个路线标记
        public const string RDLight = ".rd.light";//线路怪物消息

        public const string BattleMoveRoad = ".move.road";//按照行移动（±1）
        public const string BattleMoveTarget = ".move.target";//移动到指定行
        public const string BattleMovePixel = ".move.pixel";//按照像素移动(±移动速度)
        public const string BattleSkill = ".battle.skill";//释放技能

        public const string EnyHit = "eny.hit";//发送敌人的攻击
        public const string ChrHit = "chr.hit";//发送机师的攻击
        public const string HitChrAnim = "hit.chr.anim";//机师被击动画
        public const string GameOver = "game.over";//游戏结束
        public const string ChrLifeChange = "chr.life.change";//机师生命值变更

        public const string CreEnyTxt = "cre.eny.txt";//创建敌人文本
        public const string EnyTxtMove = "eny.txt.move";//敌人文本移动
        public const string EnyTxtChange = "eny.txt.change";//敌人文本变化
        public const string DelEnyTxt = "del.eny.txt";//删除敌人文本

        public const string SkillName = "Skill";//skill 文件名称
        public const string SkillCollision = "Collision";//检测skill碰撞
        public const string SkillLocation = "Location";//设置skill位置
        


        //预设后缀
        public const string Suffix = "prefab";
        public const string SuffixSDA = "asset";

        public const string OnRayHit = "on.ray.hit";//射线碰撞通知
        public const string ShareSDKRequest = "share.request";//share 请求
        public const string ShareSDKResponse = "share.response";//shaer 返回
        public const string LocalFile = ".local.file";
        public const string LocalFileComplement = ".local.file.Complement";
        public const string AnimationComplete = "AnimationComplete.";   //动画完成
        public const string missionRefreshTime = "RefreshTime";//刷新时间
        public const string receiveExp = "receiveExp";//接受等级经验值
        public const string LastExp = "LastExp";
        public const string CurrentExp = "CurrentExp";
        public const string LastLevel = "LastLevel";
        public const string CurrentLevel = "CurrentLevel";
        public const string GooglePayPrice = "GooglePayPrice";
        public const string GooglePayIsSuccess = "GooglePayIsSuccess";
        public const string DailyQNS = "DailyInsBtn";
        public const string DailyMusic = "DailyMusicBtn";
        public const string DailyPng = "DailyPngBtn";

        public const string PassiveSkillUI = "SkillBtn";

        public const string PilotUI = "PilotUI";
        public const string PilotBtn = "PilotBtn";
        public const string PilotBtn1 = "PilotBtn (1)";
        public const string PilotBtn2 = "PilotBtn (2)";
        public const string PilotBtn3 = "PilotBtn (3)";
        public const string PilotBtn4 = "PilotBtn (4)";
        public const string PilotBtn5 = "PilotBtn (5)";
        public const string GiftBtn = "GiftBtn";
        public const string HeartBtn = "HeartBtn";


        public const string Update = ".update.";
        public const string SingleUpdate = ".SingleUpdate.";
        public const string QNSActive = "QNSActive";

        public const string Open = ".open";
        public const string Close = ".close";
        public const string ShootDownUI = "ShootDownUI";
        public const string ActivityUI = "ActivityUI";
        public const string ItemActive = "ItemActive";
        public const string ISItem = "ISItem";
        public const string FeelTxt = "FeelTxt";
        public const string QNSGets = "QNSGets";
        public const string ShootDownDont = "ShootDownDont";



        public const string DailyUI = "DailyUI";
        public const string FightingUI = "FightingUI";
        public const string SetUI = "SetUI";

        public const string openPage = "openPage";
        public const string CN = "cn_";
        public  const int InsStart = 50000;//ins开始ID
        public  const int LevelStart = 6201;//关卡开始ID
        public  const int BitLen = 60;//位运算每条基数

        public const string LOAD = "load.";//加载

        public const string SError = "Serror";//错误码

        public const int Percentage = 100;
        public readonly static Vector2 PosTiling = new Vector2(-1f, -1f);
        public readonly static Vector2[] PosOffset = new Vector2[] {
            new Vector2(0f, 0f),//无牌
            new Vector2(0f, 0f),//红框
            new Vector2(0f, 0f),//黄框
            new Vector2(0f, 0f),//有牌
        };
        public static long getSystemTime()
        {
            return System.DateTime.Now.Ticks;
        }
        public readonly static Vector2 Tiling = new Vector2(-0.09f, -0.5f);
        public readonly static Vector2[] Offset = new Vector2[] {
            new Vector2(0.09f, 0.5f),//蓝色0
            new Vector2(0.18f, 0.5f),//蓝色1
            new Vector2(0.27f, 0.5f),//蓝色2
            new Vector2(0.36f, 0.5f),//蓝色3
            new Vector2(0.45f, 0.5f),//蓝色4
            new Vector2(0.54f, 0.5f),//蓝色5
            new Vector2(0.64f, 0.5f),//蓝色6
            new Vector2(0.73f, 0.5f),//蓝色7
            new Vector2(0.82f, 0.5f),//蓝色8
            new Vector2(0.91f, 0.5f),//蓝色9
            new Vector2(0f, 0.5f),//蓝色S
            new Vector2(0.09f, 0.5f),//红色0
            new Vector2(0.18f, 0f),//红色1
            new Vector2(0.27f, 0f),//红色2
            new Vector2(0.36f, 0f),//红色3
            new Vector2(0.45f, 0f),//红色4
            new Vector2(0.54f, 0f),//红色5
            new Vector2(0.64f, 0f),//红色6
            new Vector2(0.73f, 0f),//红色7
            new Vector2(0.82f, 0f),//红色8
            new Vector2(0.91f, 0f),//红色9
            new Vector2(0f, 0f),//红色S
        };
    }
}

