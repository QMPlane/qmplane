﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBlood : MonoBehaviour
{
    private int time;
    // Start is called before the first frame update
    void Start()
    {
        time = 0;
        rv = 2f;
        num = 1f;
    }
    // Update is called once per frame
    void Update()
    {
        if (--time > 0)
        {
            num += Time.deltaTime * 4;
            rv = Mathf.Sin(num) / 16.0f + 1.45f;
        } else
        {
            enabled = false;
        }
        
    }
    public void open()
    {
        //gameObject.SetActive(true);
        time = 10;
        num = 1f;
        rv = 2f;
        enabled = true;
    }
    private float num;
    private float rv;
    public Material mat;
    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        //Debug.Log("testaasdasd");
        mat.SetFloat("_Value", rv);
        Graphics.Blit(source, destination, mat);
    }
}
