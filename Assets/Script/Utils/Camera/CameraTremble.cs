﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTremble : MonoBehaviour
{
    public Material mat;
    private int time;
    void Start()
    {
        time = 0;
    }
    void Update()
    {
        if (--time < 0)
        {
            enabled = false;
        }

    }
    public void open()
    {
        //gameObject.SetActive(true);
        time = 10;
        enabled = true;
    }
    public void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, mat);
    }
}
