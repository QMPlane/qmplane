using Sacu.Factory.Worker;
using Sacu.Utils;
using Google.Producer.Events;
using org.jiira.protobuf;
using Sacu.Collection;

using UnityEngine.UI;
using UnityEngine;


using Datas;
//Graphs1  未启动
namespace Graphs
{
    public class RegisterUIGraphWorker : SAGraphWorker
    {
        protected GameObject sureButton;
        protected GameObject userNameTxtGo;
        protected GameObject PassWordTxtGo;
        //protected GameObject PassWordTxtGo2;
        protected Text InfoLbl;
        protected Button BackBtn;
        protected Button ConfirmBtn;

        public string userNameTxt;
        public string passWordTxt1;
        public string passWordTxt2;
        override protected void init()
        {
            base.init();
            sureButton = GameObject.Find("UIRoot/cn_RegisterUI(Clone)/BGIma/SingleBtn");
            userNameTxtGo = GameObject.Find("UIRoot/cn_RegisterUI(Clone)/BGIma/UserNameTxt/Text");
            PassWordTxtGo = GameObject.Find("UIRoot/cn_RegisterUI(Clone)/BGIma/PassWordTxt/Text");
            BackBtn = getComponentForGameObjectName<Button>("BGIma/BackBtn");
            ConfirmBtn = getComponentForGameObjectName<Button>("BGIma/ConfirmBtn");
            //PassWordTxtGo2 = GameObject.Find("UIRoot/cn_RegisterUI(Clone)/PassWordTxt (1)/Text");
            InfoLbl = (Text)getGameObjectByTypeName("BGIma/InfoLbl", "Text");
            Button sureButton1 = sureButton.GetComponent<Button>();
            sureButton1.onClick.AddListener(SingleSure);
            BackBtn.onClick.AddListener(DeleteThis);
            ConfirmBtn.onClick.AddListener(DeleteThis);
        }
        override protected void onRegister()
        {
            base.onRegister();
            addEventDispatcherWithHandle(CommandCollection.Sock + ProtoTypeEnum.SRegister, getRegisterHandler);
            addEventDispatcherWithHandle(CommandCollection.Sock + ErrorCodeEnum.AccountIsFoundError, accountIsFoundError);//用户已存在

        }

        override protected void onRemove()
        {
            base.onRemove();
            removeEventDispatcher(CommandCollection.Sock + ProtoTypeEnum.SUserData);
            removeEventDispatcher(CommandCollection.Sock + ProtoTypeEnum.SRegister);
        }
        public void SingleSure()
        {
            SocketDataWorker sock =  (SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            Debug.Log(sock.getConnection());
            if(sock.getConnection())
            {
               Sure();
            }
            else
            {
                connectHandle();
            }
        }
        public void Sure()
        {
            userNameTxt =  userNameTxtGo.GetComponent<Text>().text;
            passWordTxt1 = PassWordTxtGo.GetComponent<Text>().text;
            //passWordTxt2 = PassWordTxtGo2.GetComponent<Text>().text;
            CRegister.Builder cRegister = (CRegister.Builder)CommandCollection.getDataModel(ProtoTypeEnum.CRegister);
            cRegister.SetUserName(userNameTxt);
            cRegister.SetPassWord(passWordTxt1);
            cRegister.SetNickName("Plane");
            cRegister.SetTourist(false);
            Datas.SocketDataWorker wokerRegister = (Datas.SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            wokerRegister.sendMessage(ProtoTypeEnum.CRegister, cRegister.Build().ToByteArray());
        }
        public void connectHandle()
        {
            InfoLbl.text = "连接服务器......";
            addEventDispatcherWithHandle(CommandCollection.Sock+SAACollection.COMPLETE, ConnectComplete);
            addEventDispatcherWithHandle(CommandCollection.Sock+SAACollection.ERROR, ConnectError);

            SAUtils.Console(CommandCollection.Sock + SAACollection.COMPLETE);
            SocketDataWorker sock =  (SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            SAGameVersion version = Sacu.Utils.SAGameVersion.Instance;
            Sacu.Utils.SAUtils.Console(version.ip + ":"+version.port);
            sock.connect(version.ip, version.port);
        }
        public void ConnectComplete(FactoryEvent action)
        {
            Sure();
            InfoLbl.text = "服务器连接成功";
            removeEventDispatcher(CommandCollection.Sock + SAACollection.COMPLETE);
            removeEventDispatcher(CommandCollection.Sock + SAACollection.ERROR);
        }
        public void  ConnectError(FactoryEvent action)
        {
            InfoLbl.text = "服务器连接成功";
            removeEventDispatcher(CommandCollection.Sock + SAACollection.COMPLETE);
            removeEventDispatcher(CommandCollection.Sock + SAACollection.ERROR);
        }
        private void getRegisterHandler(FactoryEvent action)
        {
            SASocketDataDAO sdd = (SASocketDataDAO)action.Body;
            SRegister.Builder levleData = (SRegister.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SRegister, sdd.bytes);
            SocketDataWorker sock = (SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            sock.close();
            factory.disposeFactory();
            SAManager.Instance.startFactory("LoginUIFactory");
        }
        private void DeleteThis()
        {
            factory.disposeFactory();
            SAManager.Instance.startFactory("LoginUIFactory");
        }
        public void accountIsFoundError(FactoryEvent action)
        {
            InfoLbl.text = "用户已存在";
        }
    }
}
