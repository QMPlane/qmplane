﻿using Sacu.Factory.Worker;
using Sacu.Utils;
using Google.Producer.Events;
using org.jiira.protobuf;
using Datas;
using UnityEngine;
using UnityEngine.UI;

using System.Collections.Generic;
using System;

namespace Graphs
{
    public class UpdateCodeGraphWorker : SAGraphWorker
    {
        //在这个脚本里生成 //            1.升级之后的弹窗  显示属性
        //            2.音乐的弹窗
        //            3.是否进入？
        //4.道具的弹窗
        private SUserData.Builder userData;
        private SAManager sm;
        public GameObject QNS;
        public GameObject Frame;
        public int QNSId;
        override protected void init()
        {
            base.init();
            sm = SAManager.Instance;
        }
        override protected void onRegister()
        {
            base.onRegister();
            addEventDispatcherWithHandle(CommandCollection.Sock + ProtoTypeEnum.SSingleUpdate, updateCodeHandler);
        }

        override protected void onRemove()
        {
            base.onRemove();
            removeEventDispatcher(CommandCollection.Sock + ProtoTypeEnum.SSingleUpdate);
        }
        private void updateCodeHandler(FactoryEvent action)
        {
            SASocketDataDAO sdd =(SASocketDataDAO) action.Body;
            userData = (SUserData.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SUserData);
            SSingleUpdate.Builder update = (SSingleUpdate.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SSingleUpdate, sdd.bytes);
            Debug.Log(update.IDsList.Count);
            Debug.Log(update.KeysList.Count);
            for (int i = 0; i < update.IDsList.Count; i++)
            {
                Debug.Log("进来了"+i);
                Updates Updatesd = new Updates(update.IDsList[i], update.KeysList[i], update.CountsList[i]);
                SingleUpdateTypeEnum Value1 = (SingleUpdateTypeEnum)Enum.ToObject(typeof(SingleUpdateTypeEnum), update.KeysList[i]);
                Debug.Log(Value1);
                //Debug.Log(Value1);//心之盒没触发
                switch (Value1)
                {
                    case SingleUpdateTypeEnum.None:
                        break;
                    case SingleUpdateTypeEnum.Feel:
                        Debug.Log("ID" + update.IDsList[i] + "Key" + update.KeysList[i] + "Count" + update.CountsList[i]);
                        int FeelId = update.IDsList[i];
                        SGetCharacterList.Builder sFeels = (SGetCharacterList.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SGetCharacterList);
                        IList<int> sFeelslist = sFeels.FeelsList;
                        IList<int> sIDslist = sFeels.IdsList;
                        IList<int> sShotList = sFeels.ShootDownsList;
                        if (sIDslist.Contains(update.IDsList[i]))
                        {
                            for (int j = 0; j < sIDslist.Count; j++)
                            {
                                if (FeelId == sIDslist[j])
                                {
                                    sFeelslist[j] = update.CountsList[i];
                                    break;
                                }
                            }
                        }
                        else
                        {
                            sIDslist.Add(update.IDsList[i]);
                            sFeelslist.Add(update.CountsList[i]);
                            sShotList.Add(0);
                        }
                        break;
                    case SingleUpdateTypeEnum.Part://ok
                        Debug.Log("Part" + userData.Part);
                        userData.Part = update.CountsList[i];
                        //PartDic.Add(0, update.CountsList[i]);//向字典里面传数量  一般只会进来一次
                        Debug.Log("ID" + update.IDsList[i] + "Key" + update.KeysList[i] + "Count" + update.CountsList[i]+ "Part" + userData.Part);
                        break;
                    case SingleUpdateTypeEnum.Key://ok
                        Debug.Log("Key" + userData.Key);
                        userData.Key = update.CountsList[i];
                        Debug.Log("ID" + update.IDsList[i] + "Key" + update.KeysList[i] + "Count" + update.CountsList[i] + "Key" + userData.Key);
                        break;
                    case SingleUpdateTypeEnum.Music:
                        //userData.Key = update.CountsList[i];
                        Debug.Log("ID" + update.IDsList[i] + "Key" + update.KeysList[i] + "Count" + update.CountsList[i] + "Key" + userData.Key);
                        SMusic.Builder Musics = (SMusic.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SMusic);
                        IList<int> MusicIDList = Musics.IdList;
                        if (!MusicIDList.Contains(update.IDsList[i]))
                        {
                            MusicIDList.Add(update.IDsList[i]);
                        }
                        break;
                    case SingleUpdateTypeEnum.Photo:
                        break;
                    case SingleUpdateTypeEnum.GameBird:
                        Debug.Log("GameBird" + userData.Bird);
                        userData.Bird = update.CountsList[i];
                        Debug.Log("ID" + update.IDsList[i] + "Key" + update.KeysList[i] + "Count" + update.CountsList[i] + "GameBird" + userData.Bird);
                        break;
                    case SingleUpdateTypeEnum.GameGas:
                        Debug.Log("GameGas" + userData.Gas);
                        userData.Gas = update.CountsList[i];
                        Debug.Log("ID" + update.IDsList[i] + "Key" + update.KeysList[i] + "Count" + update.CountsList[i] + "GameGas" + userData.Gas);
                        break;
                    case SingleUpdateTypeEnum.GameLose:
                        Debug.Log("GameLose" + userData.Lose);
                        userData.Lose = update.CountsList[i];
                        Debug.Log("ID" + update.IDsList[i] + "Key" + update.KeysList[i] + "Count" + update.CountsList[i] + "GameLose" + userData.Lose);
                        break;
                    case SingleUpdateTypeEnum.GameTraining:
                        Debug.Log("GameTraining" + userData.Training);
                        userData.Training = update.CountsList[i];
                        Debug.Log("ID" + update.IDsList[i] + "Key" + update.KeysList[i] + "Count" + update.CountsList[i] + "GameTraining" + userData.Training);
                        break;
                    case SingleUpdateTypeEnum.GameWin:
                        Debug.Log("GameWin" + userData.Win);
                        userData.Win = update.CountsList[i];
                        Debug.Log("ID" + update.IDsList[i] + "Key" + update.KeysList[i] + "Count" + update.CountsList[i] + "GameWin" + userData.Win);
                        break;
                    case SingleUpdateTypeEnum.Ins:
                        Debug.Log("收到一条ins");
                        int id = update.IDsList[i];
                        SInses.Builder sInses = (SInses.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SInses);
                        IList<long> Sin = sInses.InsesList;
                        int AllIndex = id - ActionCollection.InsStart;
                        int index = (int)Math.Floor((decimal)AllIndex / ActionCollection.BitLen);
                        long inses = Sin[index];
                        int index3 = AllIndex % ActionCollection.BitLen;
                        long bit = 1L << index3;
                        inses = inses | bit;
                        Sin[index] = inses;
                        break;
                    case SingleUpdateTypeEnum.ShowAdv:
                        Debug.Log("ShowAdv" + userData.ShowAdv);
                        userData.ShowAdv = update.CountsList[i];
                        Debug.Log("ID" + update.IDsList[i] + "Key" + update.KeysList[i] + "Count" + update.CountsList[i] + "ShowAdv" + userData.ShowAdv);
                        break;
                    case SingleUpdateTypeEnum.GSMaxRank:
                        Debug.Log("GSMaxRank" + userData.GasMaxRank);
                        userData.GasMaxRank = update.CountsList[i];
                        Debug.Log("ID" + update.IDsList[i] + "Key" + update.KeysList[i] + "Count" + update.CountsList[i] + "GSMaxRank" + userData.GasMaxRank);
                        break;
                    case SingleUpdateTypeEnum.Level:
                        int levelId = update.CountsList[i];
                        SLevels.Builder sLevels = (SLevels.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SLevels);
                        IList<long> level = sLevels.LevelsList;
                        int AllIndex_level = levelId - ActionCollection.LevelStart;
                        int index_level = (int)Math.Floor((decimal)AllIndex_level / ActionCollection.BitLen);
                        long levels = level[index_level];
                        int index3_level = AllIndex_level % ActionCollection.BitLen;
                        long bit_level = 1L << index3_level;
                        levels = levels | bit_level;
                        level[index_level] = levels;
                        Debug.Log(level[index_level]);
                        break;
                    case SingleUpdateTypeEnum.Power://ok
                        Debug.Log("Power" + userData.Power);
                        userData.Power = update.CountsList[i];
                        Debug.Log("ID" + update.IDsList[i] + "Key" + update.KeysList[i] + "Count" + update.CountsList[i] + "Power" + userData.Power);
                        break;
                    case SingleUpdateTypeEnum.ShootDown:
                        int ShootDownId = update.IDsList[i];
                        SGetCharacterList.Builder sShootDowns = (SGetCharacterList.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SGetCharacterList);
                        IList<int> sShootDownslist = sShootDowns.FeelsList;
                        IList<int> sShootDownIDslist = sShootDowns.IdsList;
                        for (int j = 0; j < sShootDownIDslist.Count; j++)
                        {
                            if (ShootDownId == sShootDownIDslist[j])
                            {
                                sShootDownIDslist[j] = update.CountsList[j];
                                break;
                            }
                        }
                        break;
                    //Item, //道具变更（貌似只是好感度道具）
                    //InsRecord, //ins完成数变更
                    //MaxPower, //最大生命值变更
                    case SingleUpdateTypeEnum.InsRecord:
                        Debug.Log("ID" + update.IDsList[i] + "Key" + update.KeysList[i] + "Count" + update.CountsList[i]);
                        int InsRecordId = update.IDsList[i];
                        break;
                    case SingleUpdateTypeEnum.Item:
                        Debug.Log("ID" + update.IDsList[i] + "Key" + update.KeysList[i] + "Count" + update.CountsList[i]);
                        SItems.Builder Items = (SItems.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SItems);
                        IList<int> ItemIDList = Items.ItemIDList;
                        IList<int> ItemFeelList = Items.ItemCountList;
                        if (ItemIDList.Contains(update.IDsList[i]))
                        {
                            for (int j = 0; j < ItemIDList.Count; j++)
                            {
                                if (update.IDsList[i] == ItemIDList[j])
                                {
                                    ItemFeelList[j] = update.CountsList[i];
                                    break;
                                }
                            }
                        }
                        else
                        {
                            ItemIDList.Add(update.IDsList[i]);
                            ItemFeelList.Add(update.CountsList[i]);
                        }
                        //ItemDic.Add(update.IDsList[i], update.CountsList[i]);//向字典里面传数量  一般只会进来一次
                        break;
                    case SingleUpdateTypeEnum.Model:
                        Debug.Log("ID" + update.IDsList[i] + "Key" + update.KeysList[i] + "Count" + update.CountsList[i]);
                        userData.Model = update.IDsList[i];
                        Debug.Log(userData.Model);
                        break;
                    case SingleUpdateTypeEnum.MaxPower:
                        Debug.Log("ID" + update.IDsList[i] + "Key" + update.KeysList[i] + "Count" + update.CountsList[i]);
                        userData.MaxPower = update.CountsList[i];
                        break;
                    default:
                        break;
                }
                /**
                 * 下边会向所有工厂发送当前的更新消息，以SingleUpdateTypeEnum的枚举类型做区分
                 * 接收消息的每个工厂（模块）根据自己的需求，监听不同的 ActionCollection.Update + SingleUpdateTypeEnum.XXX 来获取更新消息
                 * 例如：
                 *  ins模块，在上方已经更新了ins和后台的缓存数据SInses.Builder,这里发出消息后
                 *  在ins模块做一个监听，每次收到 ActionCollection.Update+SingleUpdateTypeEnum.Ins的时候刷新ins界面列表的显示
                 * 例如：
                 *  齿轮更新，主界面需要监听 ActionCollection.Update+SingleUpdateTypeEnum.Part，修改主界面显示的齿轮数量
                 * 再例如：
                 *  收到 SingleUpdateTypeEnum.Level 消息，更新完userData的齿轮数据后，发送ActionCollection.Update + SingleUpdateTypeEnum.Level
                 *  例如这个更新会影响 战斗地图、ins 数据，这两个地方就都需要监听
                 *  战斗地图可能需要进行新关卡开启运算
                 *  ins模块需要根据关卡通关情况计算是否完成指定关卡检测（将未完成字典移动到可完成字典），实际就是调用之前在ins写的GetLevelIns();函数
                 * 
                 */
                sm.sendMessageToAllFactory(ActionCollection.Update + Value1, Updatesd);//可以选择放到case发消息  提升性能
            }
            
        }
       
    }
}
