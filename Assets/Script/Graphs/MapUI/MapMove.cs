﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sacu.Utils;
using org.jiira.protobuf;
public class MapMove : MonoBehaviour
{
    public static GameObject mapPlayer;
    public List<GameObject> cellObj;
    GameObject target;
    bool startMove;

    SUserData.Builder userData;
    Dictionary<int, STLevels> levels;
    Camera mapCamera;
    // Start is called before the first frame update
    void Start()
    {
        mapCamera = GameObject.Find("Camera").GetComponent<Camera>();
        mapPlayer = transform.Find("mapPlayer").gameObject;
        levels = STLevels.getMap();
        userData = (SUserData.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SUserData);
        for (int i = 0; i < cellObj.Count; i++)
        {
            //解锁图标显示和隐藏
            if (MapModel.Instance.stageData[int.Parse(cellObj[i].name)].isLock == true)
            {
                cellObj[i].transform.GetChild(1).gameObject.SetActive(true);
            }
            else
            {
                cellObj[i].transform.GetChild(1).gameObject.SetActive(false);
            }
        }
        
        
    }

    // Update is called once per frame
    void Update()
    {

        //入场放大效果
        if (mapCamera.fieldOfView > 60)
        {
            mapCamera.fieldOfView -= Time.deltaTime * 15; 
        }
        //走格子的逻辑
        if (startMove)
        {
            mapPlayer.transform.position = Vector2.MoveTowards(mapPlayer.transform.position, target.transform.position, Time.deltaTime * 400);
            if (Vector2.Distance(target.transform.position, mapPlayer.transform.position) <= 0)
            {
                startMove = false;
                //switch
                if (levels.ContainsKey(int.Parse(target.name)))
                {
                    if (levels[int.Parse(target.name)].Type == 1 || levels[int.Parse(target.name)].Type == 4 || levels[int.Parse(target.name)].Type == 5)
                    {

                        SGetCharacterList.Builder CharacterData = (SGetCharacterList.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SGetCharacterList);
                        BattleNeedInfo info = new BattleNeedInfo();
                        info.stageID = Graphs.MapUIGraphWorker.Instance.nextCell;
                        info.charaterID = userData.Model;
                        info.charaterFeel = STCharacter.getMap()[userData.Model].Skill1;
                        info.maxHP = 200;
                        // info.curHP = userData.Power;
                        info.curHP = 200;
                        if (info.curHP > 0)
                        {
                            SAManager.Instance.disposeFactory("MapFactory");
                            SAManager.Instance.startFactory("BattleFactory", info);
                        }

                    }
                    else if (levels[int.Parse(target.name)].Type == 2)
                    {
                        //QNS
                        Debug.LogError("剧情格子");
                        CBaseReward.Builder cBaseReward = (CBaseReward.Builder)CommandCollection.getDataModel(ProtoTypeEnum.CBaseReward);
                        cBaseReward.Level = Graphs.MapUIGraphWorker.Instance.nextCell;
                        cBaseReward.Score = 0;
                        cBaseReward.IsWin = true;
                        cBaseReward.Type = 2;
                        cBaseReward.ShootDown = 0;
                        cBaseReward.Power = userData.Power;
                        Graphs.MapUIGraphWorker.Instance.SendMapMessage(cBaseReward);
                    }
                    else if (levels[int.Parse(target.name)].Type == 3)
                    {
                        Debug.LogError("空白格子");
                        CBaseReward.Builder cBaseReward = (CBaseReward.Builder)CommandCollection.getDataModel(ProtoTypeEnum.CBaseReward);
                        cBaseReward.Level = Graphs.MapUIGraphWorker.Instance.nextCell;
                        cBaseReward.Score = 0;
                        cBaseReward.IsWin = true;
                        cBaseReward.Type = 3;
                        cBaseReward.ShootDown = 0;
                        cBaseReward.Power = userData.Power;
                        Graphs.MapUIGraphWorker.Instance.SendMapMessage(cBaseReward);
                    }
                    else if (levels[int.Parse(target.name)].Type == 6)
                    {
                        CBaseReward.Builder cBaseReward = (CBaseReward.Builder)CommandCollection.getDataModel(ProtoTypeEnum.CBaseReward);
                        cBaseReward.Level = Graphs.MapUIGraphWorker.Instance.nextCell;
                        cBaseReward.Score = 0;
                        cBaseReward.IsWin = true;
                        cBaseReward.Type = 6;
                        cBaseReward.ShootDown = 0;
                        cBaseReward.Power = userData.Power + int.Parse(levels[Graphs.MapUIGraphWorker.Instance.nextCell].SOF[0]) > 200 ? 200 : userData.Power + int.Parse(levels[Graphs.MapUIGraphWorker.Instance.nextCell].SOF[0]);
                        Graphs.MapUIGraphWorker.Instance.SendMapMessage(cBaseReward);
                    }
                    else if (levels[int.Parse(target.name)].Type == 7)
                    {
                        BattleNeedInfo info = new BattleNeedInfo();
                        info.stageID = Graphs.MapUIGraphWorker.Instance.nextCell;
                        info.charaterID = userData.Model;
                        info.charaterFeel = STCharacter.getMap()[userData.Model].Skill1;
                        info.maxHP = 200;
                       
                        info.curHP = 200;
                        if (info.curHP > 0)
                        {
                            SAManager.Instance.disposeFactory("MapFactory");
                            SAManager.Instance.startFactory("LittleGameFactory", info);
                        }
                        //小鸟
                       
                    }
                    else if (levels[int.Parse(target.name)].Type == 8)
                    {
                        //躲导弹
                    }

                }
            }
        }
    }
    //大地图光标移动
    public void MoveTo(string cellID)
    {
        target = GameObject.Find(cellID);

        if (MapModel.Instance.stageData.ContainsKey(int.Parse(cellID)))
        {
            // Debug.LogError(cellID);
            if (MapModel.Instance.stageData[int.Parse(cellID)].isLock == false)
            {
                // Debug.LogError("not lock");
                if (levels[int.Parse(cellID)].Type == 7 || levels[int.Parse(cellID)].Type == 8 || levels[int.Parse(cellID)].Type == 4)
                {

                    Graphs.MapUIGraphWorker.Instance.nextCell = int.Parse(cellID);
                    startMove = true;
                }
                else
                //  if (MapModel.Instance.stageData[int.Parse(cellID)].clear == false)
                {
                    //      Debug.LogError("not clear");
                    //     if (userData.Power > 0)
                    //     {
                    Graphs.MapUIGraphWorker.Instance.nextCell = int.Parse(cellID);
                    startMove = true;
                    // Debug.LogError("move" + Graphs.MapUIGraphWorker.Instance.nextCell);
                    // }
                }
            }
        }
    }
    public void SetPostion(int cell)
    {
        mapPlayer.transform.position = GameObject.Find(cell.ToString()).transform.position;
    }

}
