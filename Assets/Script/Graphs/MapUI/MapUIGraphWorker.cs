﻿using Sacu.Factory.Worker;
using Sacu.Utils;
using Google.Producer.Events;
using org.jiira.protobuf;

using System.Collections;
using System.Collections.Generic;

using UnityEngine.UI;
using UnityEngine;
using Spine.Unity;
using System;
using Datas;
using DG.Tweening;
namespace Graphs
{
    public class MapUIGraphWorker : SAGraphWorker
    {
        public static MapUIGraphWorker Instance;
        public int curCell;
        public int nextCell;
        SkeletonGraphic openingAnima;
        Camera mapCamera;
        MapMove curMap;
        private GameObject rewardTips;
        private Text rewardText;
        private Text titleText;
        private GameObject itemGrid;

        private Image itemIcon1;
        private Text itemCount1;
        private Image itemIcon2;
        private Text itemCount2;
        private GameObject rewardBG;
        List<string[]> itemSplitList = new List<string[]>();

        override protected void init()
        {
            base.init();
            Instance = this;
            // try
            // {
            MapModel.Instance.Init();
            // }
            // catch (Exception ex)
            // {

            // }
            curCell = 6240;

            Debug.Log("大地图初始化完成");
        }
        override protected void onStart(System.Object args)
        {
            base.onStart(args);
            mapCamera = GameObject.Find("Camera").GetComponent<Camera>();
            mapCamera.fieldOfView = 70;
            rewardTips = transform.Find("RewardTips").gameObject;
            itemGrid = transform.Find("RewardTips/kuang/itemGrid").gameObject;
            rewardText = transform.Find("RewardTips/kuang/RewardText").GetComponent<Text>();
            titleText = transform.Find("RewardTips/kuang/titleText").GetComponent<Text>();
            rewardBG = transform.Find("rewardBG").gameObject;
            itemIcon1 = transform.Find("RewardTips/kuang/itemGrid/item/Image").GetComponent<Image>();
            itemCount1 = transform.Find("RewardTips/kuang/itemGrid/item/Text").GetComponent<Text>();
            itemIcon2 = transform.Find("RewardTips/kuang/itemGrid/item2/Image").GetComponent<Image>();
            itemCount2 = transform.Find("RewardTips/kuang/itemGrid/item2/Text").GetComponent<Text>();
            if (GameObject.Find("cn_MapPanel3(Clone)") == null)
            {
                GameObject go = Instantiate(SACache.getResWithName<GameObject>("UI/MapUI/MapPanel3", "prefab"));
                go.transform.SetParent(GameObject.Find("MapCanvas").transform);
                go.transform.localPosition = Vector3.zero;
                go.transform.localScale = Vector3.one;
                curMap = go.GetComponent<MapMove>();

            }
            openingAnima = transform.Find("openingAnima").GetComponent<SkeletonGraphic>();
            openingAnima.AnimationState.SetAnimation(0, "screen_FX_1", false);
            if (args != null)
            {
                bool win = (bool)args;
                if (win)
                {
                    curCell = nextCell;
                    // Debug.LogError("winMove");
                    MapMove.mapPlayer.transform.position = GameObject.Find(nextCell.ToString()).transform.position;
                }
                else
                {
                    // Debug.LogError("loseMove");
                    MapMove.mapPlayer.transform.position = GameObject.Find(curCell.ToString()).transform.position;
                }
            }
        }
        public void ShowRewardTips()
        {

        }
        public void MapMoveTo(int cellID)
        {
            if (cellID == 6250)
            {

            }
            else if (cellID == 6255)
            {

            }
            else if (cellID == 6265)
            {

            }
            else
            {
                curMap.MoveTo(cellID.ToString());
            }
        }
        override protected void onRegister()
        {
            base.onRegister();

            addEventDispatcherWithHandle(CommandCollection.Sock + ProtoTypeEnum.SBaseReward, getRewardHandler);
        }

        override protected void onRemove()
        {
            base.onRemove();
            removeEventDispatcher(CommandCollection.Sock + ProtoTypeEnum.SBaseReward);
        }
        //切换章节
        public void ChangeStagePage()
        {

        }
        public void SendMapMessage(CBaseReward.Builder cBaseReward)
        {
            Debug.LogError("发送消息" + cBaseReward.Level);
            Datas.SocketDataWorker wokerStageReward = (Datas.SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            wokerStageReward.sendMessage(ProtoTypeEnum.CBaseReward, cBaseReward.Build().ToByteArray());
        }
        private void getRewardHandler(FactoryEvent action)
        {
            itemSplitList.Clear();
            Debug.LogError("获得奖励");
            SASocketDataDAO sdd = (SASocketDataDAO)action.Body;
            SBaseReward.Builder reward = (SBaseReward.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SBaseReward, sdd.bytes);
            Debug.LogError("道具" + reward.RewardID);
            if (reward.IsWin == false)
            {
                Debug.LogError("失败" + curCell);
                //dispatchEvent("isFailed");
                MapMove.mapPlayer.transform.position = GameObject.Find(curCell.ToString()).transform.position;
            }
            else
            {
                MapModel.Instance.stageData[nextCell].clear = true;
                for (int i = 0; i < MapModel.Instance.stageData[nextCell].openStage.Count; i++)
                {
                    MapModel.Instance.stageData[MapModel.Instance.stageData[nextCell].openStage[i]].isLock = false;
                    GameObject.Find(MapModel.Instance.stageData[nextCell].openStage[i].ToString()).transform.GetChild(1).gameObject.SetActive(false);
                }
                curCell = nextCell;
                STLevels level = STLevels.getMap()[curCell];
                if (level.Type == 3)
                {
                    //道具奖励
                    if (level.SOF[0] != "0" && level.SOF.Length > 1)
                    {
                        for (int i = 0; i < level.SOF.Length; i++)
                        {
                            string a = level.SOF[i];
                            itemSplitList.Add(a.Split(':'));
                            // Debug.LogError(itemSplitList[i][0]);
                        }
                        if (itemSplitList[0].Length == 1 && int.Parse(itemSplitList[0][0]) > 11000)
                        {
                            //是音乐
                            rewardTips.SetActive(true);
                            rewardBG.gameObject.SetActive(true);
                            titleText.gameObject.SetActive(true);
                            rewardText.gameObject.SetActive(true);
                            itemGrid.SetActive(false);
                            rewardTips.transform.DOScale(Vector3.one, 0.5f);
                            rewardText.text = "";
                            for (int i = 0; i < level.SOF.Length; i++)
                            {
                                // rewardText.text += level.SOF[i] + "\n";
                                rewardText.text += "音乐" + STBgm.getMap()[int.Parse(level.SOF[i])].Name + "\n";
                            }
                            // Debug.LogError(rewardText.text);
                        }
                        else
                        {
                            rewardTips.SetActive(true);
                            rewardBG.gameObject.SetActive(true);
                            titleText.gameObject.SetActive(true);
                            rewardText.gameObject.SetActive(false);
                            itemGrid.SetActive(true);
                            rewardTips.transform.DOScale(Vector3.one, 0.5f);

                            itemIcon1.sprite = SACache.getSpriteAtlasWithName("Atlas/ItemAtlas").GetSprite(itemSplitList[0][0]);
                            itemCount1.text = "x " + itemSplitList[0][1];

                            itemIcon2.sprite = SACache.getSpriteAtlasWithName("Atlas/ItemAtlas").GetSprite(itemSplitList[1][0]);
                            itemCount2.text = "x " + itemSplitList[1][1];
                        }
                    }
                }
                else if (STLevels.getMap()[curCell].Type == 6)
                {
                    //恢复生命
                    rewardTips.SetActive(true);
                    rewardBG.gameObject.SetActive(true);
                    titleText.gameObject.SetActive(true);
                    rewardText.gameObject.SetActive(true);
                    rewardTips.transform.DOScale(Vector3.one, 0.5f);
                    rewardText.text = "恢复生命值" + level.SOF[0];
                }

                Debug.LogError("胜利" + curCell);
                //dispatchEvent("isWin");
            }

        }
        public void CloseRewardTips()
        {
            rewardTips.transform.DOScale(Vector3.zero,0.5f);
            rewardTips.SetActive(false);
            
            itemGrid.SetActive(false);
            rewardBG.gameObject.SetActive(false);
            titleText.gameObject.SetActive(false);
            rewardText.gameObject.SetActive(false);
        }
        public void ShowItemTips()
        {

        }
        public void DestroyMap()
        {
            Destroy(GameObject.Find("cn_MapPanel3(Clone)"));
            // GameObject.Find("UIRoot").transform.Find("cn_MainUI(Clone)").gameObject.SetActive(true);
        }
        // IEnumerator Wait(){
        //     yield return new WaitForSeconds(1);
        // }
    }
}
