﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using org.jiira.protobuf;
using Sacu.Utils;
using System;
public class MapModel
{
    static MapModel s_Instance = null;
    public static MapModel Instance
    {
        get
        {
            if (s_Instance == null)
            {
                s_Instance = new MapModel();
            }
            return s_Instance;
        }
    }
    public class MapData
    {
        public int id;
        public int preId;
        public bool clear = false;
        public bool isLock = true;
        public List<int> openStage = new List<int>();
    }
    //玩家数据
    SUserData.Builder userData;
    List<STLevels> LevelList = new List<STLevels>();
    public Dictionary<int, MapData> stage1Data = new Dictionary<int, MapData>();
    public Dictionary<int, MapData> stage2Data = new Dictionary<int, MapData>();
    public Dictionary<int, MapData> stage3Data = new Dictionary<int, MapData>();
    public Dictionary<int, MapData> stage4Data = new Dictionary<int, MapData>();
    public Dictionary<int, MapData> stage5Data = new Dictionary<int, MapData>();
    public Dictionary<int, MapData> stageData = new Dictionary<int, MapData>();


    /// <summary>
    /// 获取服务器关卡信息表并且初始化存储类
    /// </summary>
    /// <param name="data"></param>
    public void Init()
    {
        userData = (SUserData.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SUserData);

        // stage1Data.Clear();
        // stage2Data.Clear();
        // stage3Data.Clear();
        // stage4Data.Clear();
        // stage5Data.Clear();
        LevelList.Clear();
             //服务器取消息
        SLevels.Builder levelData = (SLevels.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SLevels);
        
        IList<long> Level = levelData.LevelsList;
        LevelList = STLevels.getList();
        // Debug.LogError(Level.Count);
        Dictionary<int, STLevels> LevelList2 = STLevels.getMap();
        STLevels Stlevel;
        for (int i = 0; i < LevelList.Count; i++)
        {
            Stlevel = LevelList[i];
            //位运算总下标
            int AllIndex = Stlevel.Id - ActionCollection.LevelStart;
            // Debug.LogError(Stlevel.Id +"  ~~~  " + ActionCollection.LevelStart);
            //位列表的下标
            int index = (int)Math.Floor((decimal)AllIndex / ActionCollection.BitLen);
            // Debug.LogError((decimal)AllIndex+"           "+ActionCollection.BitLen);
            // Debug.LogError( (int)Math.Floor((decimal)AllIndex / ActionCollection.BitLen) );
            //当前位下标
            int index2 = index % ActionCollection.BitLen;
            //服务器取到的
            long levels = Level[index];
            //位列表long的当前下标
            int index3 = AllIndex % ActionCollection.BitLen;
            long bit = 1L << index3;
            //与运算比较得达到布尔值  若都是0则未完成
            bool value = ((levels & bit) == 0);//true 是未完成 
            MapData data = new MapData();
            data.id = Stlevel.Id;
            data.clear = !value;
            data.preId = Stlevel.PreId;
            // if (data.clear == true)
            // {
            //     data.isLock = false;
            // }
            // else
            // {
            //     data.isLock = true;
            // }

            // if (Stlevel.Id == 6201)
            // {
            //     data.clear = true;
            //     data.isLock = false;
            //     stage1Data.Add(Stlevel.Id, data);
            // }
            // else if (Stlevel.Id > 6201 && Stlevel.Id <= 6214)
            // {
            //     stage1Data.Add(Stlevel.Id, data);
            // }
            // else if (Stlevel.Id >= 6215 && Stlevel.Id <= 6239)
            // {
            //     stage2Data.Add(Stlevel.Id, data);
            // }
            // else if (Stlevel.Id >= 6240 && Stlevel.Id <= 6265)
            // {
            //     stage3Data.Add(Stlevel.Id, data);
            // }
            // else if (Stlevel.Id >= 6266 && Stlevel.Id <= 6290)
            // {
            //     stage4Data.Add(Stlevel.Id, data);
            // }
            // else if (Stlevel.Id >= 6291 && Stlevel.Id <= 6316)
            // {
            //     stage5Data.Add(Stlevel.Id, data);
            // }
            // Debug.Log(Stlevel.Id +"    +++++++    "+ data.clear + "   +++++++   " + data.isLock);
            //----------------------------------------------
            if (Stlevel.Id == 6240)
            {
                data.clear = true;
                data.isLock = false;
                stageData.Add(Stlevel.Id, data);
            }else{
                stageData.Add(Stlevel.Id, data);
            }
        }
        //添加解锁关卡关系
        foreach (MapData a in stageData.Values)
        {
            if (stageData.ContainsKey(a.preId))
            {
                stageData[a.preId].openStage.Add(a.id);
            }
        }
        foreach(MapData b in stageData.Values){
            for(int i = 0; i < b.openStage.Count;i++){
                if(stageData.ContainsKey(b.openStage[i]) &&  stageData[b.id].clear == true){
                    stageData[b.openStage[i]].isLock = false;
                }
            }
        }
        // foreach (MapData a in stageData.Values)
        // {
        //     for(int i = 0; i < a.openStage.Count ;i++){
        //         Debug.LogError(a.id+"---------"+a.openStage[i]);
        //     }
        // }
    }
    //---------------------------------------------------------------------------------------------------------------
    //以下逻辑目前均没有用到，以后可根据需求使用
    
    /// <summary>
    /// 刷新第一章解锁关卡
    /// </summary>
    public void RefreshData1()
    {

        //LevelList = STLevels.getList();
        foreach (MapData a in stage1Data.Values)
        {

            if (stage1Data.ContainsKey(a.preId))
            {
                // Debug.LogError(a.preId);
                if (stage1Data[a.preId].clear == true)
                {
                    stage1Data[a.preId].isLock = false;
                    if (stage1Data.ContainsKey(a.id))
                    {
                        stage1Data[a.id].isLock = false;

                    }
                }

            }

            Debug.LogError(stage1Data[a.id].id + "  ~~~~~~~~~~~~~~~~   " + stage1Data[a.id].clear.ToString() + "  ~~~~~~~~~~~~~~~~  " + stage1Data[a.id].isLock.ToString());
        }
    }
    /// <summary>
    /// 刷新第二章解锁关卡
    /// </summary>
    public void RefreshData2()
    {

        //LevelList = STLevels.getList();
        foreach (MapData a in stage2Data.Values)
        {

            if (stage2Data.ContainsKey(a.preId))
            {
                // Debug.LogError(a.preId);
                if (stage2Data[a.preId].clear == true)
                {
                    stage2Data[a.preId].isLock = false;
                    if (stage2Data.ContainsKey(a.id))
                    {
                        stage2Data[a.id].isLock = false;

                    }
                }

            }

            // Debug.LogError(stage1Data[a.id].id + "  ~~~~~~~~~~~~~~~~   " + stage1Data[a.id].clear.ToString() + "  ~~~~~~~~~~~~~~~~  " + stage1Data[a.id].isLock.ToString());
        }
    }
    /// <summary>
    /// 刷新第三章解锁关卡
    /// </summary>
    public void RefreshData3()
    {

        //LevelList = STLevels.getList();
        foreach (MapData a in stage3Data.Values)
        {

            if (stage3Data.ContainsKey(a.preId))
            {
                // Debug.LogError(a.preId);
                if (stage3Data[a.preId].clear == true)
                {
                    stage3Data[a.preId].isLock = false;
                    if (stage3Data.ContainsKey(a.id))
                    {
                        stage3Data[a.id].isLock = false;

                    }
                }

            }

            // Debug.LogError(stage1Data[a.id].id + "  ~~~~~~~~~~~~~~~~   " + stage1Data[a.id].clear.ToString() + "  ~~~~~~~~~~~~~~~~  " + stage1Data[a.id].isLock.ToString());
        }
    }
    /// <summary>
    /// 刷新第四章解锁关卡
    /// </summary>
    public void RefreshData4()
    {

        //LevelList = STLevels.getList();
        foreach (MapData a in stage4Data.Values)
        {

            if (stage4Data.ContainsKey(a.preId))
            {
                // Debug.LogError(a.preId);
                if (stage4Data[a.preId].clear == true)
                {
                    stage4Data[a.preId].isLock = false;
                    if (stage4Data.ContainsKey(a.id))
                    {
                        stage4Data[a.id].isLock = false;

                    }
                }

            }

            // Debug.LogError(stage1Data[a.id].id + "  ~~~~~~~~~~~~~~~~   " + stage1Data[a.id].clear.ToString() + "  ~~~~~~~~~~~~~~~~  " + stage1Data[a.id].isLock.ToString());
        }
    }
    /// <summary>
    /// 刷新第五章解锁关卡
    /// </summary>
    public void RefreshData5()
    {

        //LevelList = STLevels.getList();
        foreach (MapData a in stage5Data.Values)
        {

            if (stage5Data.ContainsKey(a.preId))
            {
                // Debug.LogError(a.preId);
                if (stage5Data[a.preId].clear == true)
                {
                    stage5Data[a.preId].isLock = false;
                    if (stage5Data.ContainsKey(a.id))
                    {
                        stage5Data[a.id].isLock = false;

                    }
                }

            }

            // Debug.LogError(stage1Data[a.id].id + "  ~~~~~~~~~~~~~~~~   " + stage1Data[a.id].clear.ToString() + "  ~~~~~~~~~~~~~~~~  " + stage1Data[a.id].isLock.ToString());
        }
    }
}
