using Sacu.Factory.Worker;
using Sacu.Utils;
using Google.Producer.Events;
using org.jiira.protobuf;
using Sacu.Collection;

using UnityEngine.UI;
using UnityEngine;


using Datas;
using System.Collections.Generic;
using System;
//Graphs1  未启动
namespace Graphs
{
    public class QNSUIGraphWorker : SAGraphWorker
    {
        //ins的弹窗的类别：钥匙：88  齿轮：66  游戏币：22  好感度 ：1001-1006  道具：6000-6099 考虑写到点赞之后的回调里  
        public List<Dictionary<int, STInstagram>> DicIns = new List<Dictionary<int, STInstagram>>();
        public List<Dictionary<int, int>> DicIns_SInsRecords = new List<Dictionary<int, int>>();

        public Dictionary<int, STInstagram> InsDic_On = new Dictionary<int, STInstagram>();//INS已完成的
        public Dictionary<InstagramTypeEnum, List<STInstagram>> InsDic_Off = new Dictionary<InstagramTypeEnum, List<STInstagram>>();//INS未完成的
        public Dictionary<int, STInstagram> InsDic_UnLock = new Dictionary<int, STInstagram>();//INS可完成的
        public List<int> UPList = new List<int>();

        private SUserData.Builder userData;

        protected GameObject QNSCell;
        protected GameObject CellParent;
        public SAManager sm;
        public STInstagram Stinsta;

        public Dictionary<int, int> InsDic_Value1 = new Dictionary<int, int>();//INS点赞数的字典
        public Dictionary<int, int> InsDic_Value2 = new Dictionary<int, int>();//INS踩数的字典
        override protected void init()
        {
            base.init();
            sm = SAManager.Instance;
        }
        override protected void onRegister()
        {
            base.onRegister();
            addEventDispatcherWithHandle(ActionCollection.Update + SingleUpdateTypeEnum.Ins, updateIns);//ins本身
            //addEventDispatcherWithHandle(ActionCollection.Update + SingleUpdateTypeEnum.Feel, updateIns);//机师
            //addEventDispatcherWithHandle(ActionCollection.Update + SingleUpdateTypeEnum.ShootDown, updateIns);//机师
            addEventDispatcherWithHandle(ActionCollection.Update + SingleUpdateTypeEnum.Level, updateLevel);//关卡
            //addEventDispatcherWithHandle(ActionCollection.ISItem + ActionCollection.Open, ISItem);//方便别的地方可以开启道具的消息

            //addEventDispatcherWithHandle(ActionCollection.Update + SingleUpdateTypeEnum.Item, AvtiveItem);
            addEventDispatcherWithHandle(ActionCollection.Update + SingleUpdateTypeEnum.InsRecord, updateInsRecord);//点赞完成的监听给奖励的弹窗

        }
        protected override void onRemove()
        {
            base.onRemove();
            //removeEventDispatcher(ActionCollection.Update + SingleUpdateTypeEnum.Item);
        }
        private void ISItem()
        {
            addEventDispatcherWithHandle(ActionCollection.Update + SingleUpdateTypeEnum.Item, AvtiveItem);
        }
        private void updateInsRecord(FactoryEvent action)
        {
            Sacu.Utils.SAManager.Instance.sendMessageToFactory("QNSActiveUIFactory", ActionCollection.ItemActive+ActionCollection.Open, UPList);
        }
        private void AvtiveItem(FactoryEvent action)
        {
            Updates up = (Updates)action.Body;
            Sacu.Utils.SAManager.Instance.sendMessageToFactory("QNSActiveUIFactory", ActionCollection.ItemActive + ActionCollection.Open, up);
        }
        private void updateIns(FactoryEvent action)
        {
            Updates up =(Updates)action.Body;
            for (int i = InsDic_UnLock.Count-1; i >=0; i--)
            {
                if (InsDic_UnLock.ContainsKey(up.ID))
                {
                    InsDic_On.Add(up.ID, InsDic_UnLock[up.ID]);
                    InsDic_UnLock.Remove(up.ID);
                }
            }
            callLuaFun("activeCell");
        }
        /// <summary>
        /// 关卡的更新
        /// </summary>
        /// <param name="action"></param>
        private void updateLevel(FactoryEvent action)
        {
            Updates up = (Updates)action.Body;
            Distinguish(InstagramTypeEnum.TargetLevelGame, up.Count, 0, true);
        }
        public void CheckIns()
        {
            userData = (SUserData.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SUserData);
            GetSInses();
            GetUserdataIns();
            GetLevelIns();
            GetCharacterIns();
            getSInsRecords_();
        }
        /// <summary>
        /// 处理userdata类的数据
        /// </summary>
        public void GetUserdataIns()
        {
            int len = Enum.GetNames(typeof(GameTypeEnum)).GetLength(0);
            for (int i = 0; i < len; i++)
            {
                Distinguish(InstagramTypeEnum.AllGame, i, 0);
                Distinguish(InstagramTypeEnum.AllLevelGame, i, 0);
            }
            int Value_Gas = (int)Enum.ToObject(typeof(GameTypeEnum), GameTypeEnum.Gas);
            int Value_Bird = (int)Enum.ToObject(typeof(GameTypeEnum), GameTypeEnum.Bird);
            Distinguish(InstagramTypeEnum.Rank, Value_Gas, 0);
            Distinguish(InstagramTypeEnum.Rank, Value_Bird, 0);
            Distinguish(InstagramTypeEnum.ShowAdv, 0, 0);
            Distinguish(InstagramTypeEnum.MaxOnLine, 0, 0);
            Distinguish(InstagramTypeEnum.MaxOnOff, 0, 0);
        }
        /// <summary>
        /// 获取服务器QNS信息表
        /// </summary>
        /// <param name="data"></param>
        public void GetSInses()
        {
            SInses.Builder sInses = (SInses.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SInses);
            IList<long> Sin = sInses.InsesList;
            List<STInstagram> InsList = STInstagram.getList();
            STInstagram Stins;
            List<STInstagram> offList;
            for (int i = 0; i < InsList.Count; i++)
            {
                Stins = InsList[i];
                //位运算总下标
                int AllIndex = Stins.Id - ActionCollection.InsStart;
                //位列表的下标
                int index = (int)Math.Floor((decimal)AllIndex / ActionCollection.BitLen);
                //当前位下标
                int index2 = index % ActionCollection.BitLen;
                //服务器取到的
                long inses = Sin[index];
                //位列表long的当前下标
                int index3 = AllIndex % ActionCollection.BitLen;
                long bit = 1L << index3;
                //与运算比较得达到布尔值  若都是0则未完成
                bool value = ((inses & bit) == 0);//true 是未完成 
                if (!value)
                {
                    InsDic_On.Add(Stins.Id, Stins);
                }
                else
                {
                    InstagramTypeEnum insType = (InstagramTypeEnum)Stins.Condition;
                    if (!InsDic_Off.ContainsKey(insType))
                    {
                        InsDic_Off.Add(insType, new List<STInstagram>());
                    }
                    offList = InsDic_Off[insType];
                    offList.Add(Stins);
                }
            }
        }
        /// <summary>
        /// 获取服务器关卡信息表
        /// </summary>
        /// <param name="data"></param>
        public void GetLevelIns()
        {
            //服务器取消息
            SLevels.Builder levelData = (SLevels.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SLevels);
            IList<long> Level = levelData.LevelsList;
            List<STLevels> LevelList = STLevels.getList();
            STLevels Stlevel;
            for (int i = 0; i < LevelList.Count; i++)
            {
                Stlevel = LevelList[i];
                //位运算总下标
                int AllIndex = Stlevel.Id - ActionCollection.LevelStart;
                //位列表的下标
                int index = (int)Math.Floor((decimal)AllIndex / ActionCollection.BitLen);
                //当前位下标
                int index2 = index % ActionCollection.BitLen;
                //服务器取到的
                long levels = Level[index];
                //位列表long的当前下标
                int index3 = AllIndex % ActionCollection.BitLen;
                long bit = 1L << index3;
                //与运算比较得达到布尔值  若都是0则未完成
                bool value = ((levels & bit) == 0);//true 是未完成 
                if (!value)
                {
                    Distinguish(InstagramTypeEnum.TargetLevelGame, Stlevel.Id, 0);
                }
            }
        }
        /// <summary>
        /// 获取服务器机师信息表
        /// </summary>
        /// <param name="data"></param>
        public void GetCharacterIns()
        {
            //服务器取消息
            SGetCharacterList.Builder CharacterData = (SGetCharacterList.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SGetCharacterList);
            IList<int> CharacterIDList = CharacterData.IdsList;
            IList<int> CharacterFeelList = CharacterData.FeelsList;
            IList<int> CharacterShootDownsList = CharacterData.ShootDownsList;
            for (int i = 0; i < CharacterIDList.Count; i++)
            {
                Distinguish(InstagramTypeEnum.CharacterFeel, CharacterIDList[i], CharacterFeelList[i]);//验证机师好感度
                Distinguish(InstagramTypeEnum.CharacterShootDown, CharacterIDList[i], CharacterShootDownsList[i]);//验证机师击杀数
            }
        }
        private void a2b(List<STInstagram> insList, STInstagram Stinsta, bool openSingle)
        {
            insList.Remove(Stinsta);
            InsDic_UnLock.Add(Stinsta.Id, Stinsta);
            Debug.Log(openSingle);
            if (openSingle)
            {
                sm.sendMessageToFactory("QNSActiveUIFactory", ActionCollection.QNSActive + ActionCollection.Open, Stinsta);//可以选择放到case发消息  提升性能
            }
        }
        public void Distinguish(InstagramTypeEnum insType, int value1, int value2)
        {
            Distinguish(insType, value1, value2, false);
        }
        /// <summary>
        /// 从未完成的ins字典内拆分出可完成和不可完成的字典
        /// 
        /// openSingle 表示 是否发送打开独立ins面板通知
        /// </summary>
        public void Distinguish(InstagramTypeEnum insType, int value1, int value2, bool openSingle)
        {
            if (!InsDic_Off.ContainsKey(insType)) return;
            List<STInstagram> insList = InsDic_Off[insType];
            bool flag = false;
            switch (insType)
            {
                case InstagramTypeEnum.AllGame:
                    GameTypeEnum Value1 = (GameTypeEnum)Enum.ToObject(typeof(GameTypeEnum), value1);
                    for (int i = insList.Count - 1; i >= 0; i--)
                    {
                        Stinsta = insList[i];
                        int staticValue2 = Stinsta.Value2;
                        switch (Value1)
                        {
                            case GameTypeEnum.ALL: //全部游戏次数就是：关卡胜利次数+关卡失败次数+训练场次数+躲避毒气次数+小鸟飞行次数
                                flag = userData.Win + userData.Training + userData.Gas + userData.Bird >= staticValue2;
                                break;
                            case GameTypeEnum.Level:
                                flag = userData.Win>= staticValue2;
                                break;
                            case GameTypeEnum.Training:
                                flag = userData.Training >= staticValue2;
                                break;
                            case GameTypeEnum.Gas:
                                flag = userData.Gas >= staticValue2;
                                break;
                            case GameTypeEnum.Bird:
                                flag = userData.Bird >= staticValue2;
                                break;
                            default:
                                break;
                        }
                        if (flag) a2b(insList, Stinsta, openSingle);
                    }
                    break;
                case InstagramTypeEnum.AllLevelGame:
                    GameTypeEnum Value1_AllLevelGame = (GameTypeEnum)Enum.ToObject(typeof(GameTypeEnum), value1);
                    for (int i = insList.Count - 1; i >= 0; i--)
                    {
                        Stinsta = insList[i];
                        int staticValue2 = Stinsta.Value2;
                        switch (Value1_AllLevelGame)
                        {
                            case GameTypeEnum.ALL:
                                //全部游戏次数就是：关卡胜利次数+关卡失败次数+训练场次数+躲避毒气次数+小鸟飞行次数
                                flag = userData.Win + userData.Training >= staticValue2;
                                break;
                            case GameTypeEnum.Level:
                                flag = userData.Win >= staticValue2;
                                break;
                            case GameTypeEnum.Training:
                                flag = userData.Training >= staticValue2;
                                break;
                            default:
                                break;
                        }
                        if (flag) a2b(insList, Stinsta, openSingle);
                    }
                    break;
                case InstagramTypeEnum.TargetLevelGame:
                    for (int i = insList.Count - 1; i >= 0; i--)
                    {
                        Stinsta = insList[i];
                        flag = Stinsta.Value1 == value1;
                        if (flag) a2b(insList, Stinsta, openSingle);
                    }
                    break;
                case InstagramTypeEnum.Rank:
                    GameTypeEnum Value1_Rank = (GameTypeEnum)Enum.ToObject(typeof(GameTypeEnum), value1);
                    for (int i = insList.Count - 1; i >= 0; i--)
                    {
                        Stinsta = insList[i];
                        switch (Value1_Rank)
                        {
                            case GameTypeEnum.Gas:
                                {
                                    flag = userData.GasMaxRank >= Stinsta.Value2;
                                    break;
                                }
                            case GameTypeEnum.Bird:
                                {
                                    flag = userData.BirdMaxRank >= Stinsta.Value2;
                                    break;
                                }
                        }
                        if (flag) a2b(insList, Stinsta, openSingle);
                    }
                    break;
                case InstagramTypeEnum.CharacterFeel:
                case InstagramTypeEnum.CharacterShootDown:
                    for (int i = insList.Count - 1; i >= 0; i--)
                    {
                        Stinsta = insList[i];
                        flag = Stinsta.Value1 == value1 && Stinsta.Value2 <= value2;
                        if (flag) a2b(insList, Stinsta, openSingle);
                    }
                    break;
                case InstagramTypeEnum.ShowAdv:
                    for (int i = insList.Count - 1; i >= 0; i--)
                    {
                        Stinsta = insList[i];
                        flag = userData.ShowAdv >= Stinsta.Value1;
                        if (flag)
                        {
                            a2b(insList, Stinsta, openSingle);
                        }
                    }
                    break;
                case InstagramTypeEnum.MaxOnLine:
                    for (int i = insList.Count - 1; i >= 0; i--)
                    {
                        Stinsta = insList[i];
                        flag = userData.MaxPersist >= Stinsta.Value1;
                        if (flag) a2b(insList, Stinsta, openSingle);
                    }
                    break;
                case InstagramTypeEnum.MaxOnOff:
                    for (int i = insList.Count - 1; i >= 0; i--)
                    {
                        Stinsta = insList[i];
                        flag = userData.MaxOffline >= Stinsta.Value1;
                        if (flag) a2b(insList, Stinsta, openSingle);
                    }
                    break;
                default:
                    break;
            }
        }
        public void getSInsRecords_()
        {
            
            SInsRecords.Builder SInsRecordsData = (SInsRecords.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SInsRecords);
            IList<int> SInsRecordsIDList = SInsRecordsData.IdList;
            IList<int> SInsRecordsValue1List = SInsRecordsData.Value1List;
            IList<int> SInsRecordsValue2List = SInsRecordsData.Value2List;
            for (int i = 0; i < SInsRecordsIDList.Count; i++)
            {
                InsDic_Value1.Add(SInsRecordsIDList[i], SInsRecordsValue1List[i]);
                InsDic_Value2.Add(SInsRecordsIDList[i], SInsRecordsValue2List[i]);
            }
        }
        /// <summary>
        /// 加载预制体到某个物体下面
        /// </summary>
        /// <param name="prafabs"></param>
        /// <param name="parents"></param>
        public GameObject LoadPrefabs(GameObject prafabs, GameObject parents)
        {
            //加载Cell出来
            GameObject prafab = Instantiate(prafabs);
            prafab.name = prafabs.name;
            Transform Tf = prafab.transform;
            Tf.SetParent(parents.gameObject.transform);
            //坐标四元数大小归零
            Tf.localPosition = Vector3.zero;
            Tf.localRotation = Quaternion.identity;
            Tf.localScale = Vector3.one;
            //设置为最后一个子物体
            Tf.SetAsLastSibling();
            return prafab;
        }
        public List<string> GetList(string[] s)
        {
            List<string> TxtList = new List<string>();//将静态表里的数据分割用到的表
            for (int i = 0; i < s.Length; i++)
            {
                TxtList.Add(s[i]);
            }
            return TxtList;
        }
        public string[] GetSplitStr(string s)
        {

            string[] sArray = s.Split('&');
            
            return sArray;
        }
        public string[] GetSplitStr_(string s)
        {

            string[] sArray = s.Split(':');

            return sArray;
        }
        public List<int> AddInt(int a,int b)
        {
            UPList = new List<int>();
            UPList.Add(a);
            UPList.Add(b);
            return UPList;
        }
    }
}
