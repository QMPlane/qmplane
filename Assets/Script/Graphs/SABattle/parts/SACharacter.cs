﻿using org.jiira.protobuf;
using Sacu.Utils;
using UnityEngine;
using Spine;
using Spine.Unity;
using Sacu.Factory.Worker;
using Google.Producer.Events;
using System.Collections.Generic;
using XLua;
using Graphs;

public class SACharacter : SACell
{
    private static SACharacter _instance;
    
    public static SACharacter getInstance()
    {
        if (null == _instance)
        {
            _instance = new SACharacter();
        }
        return _instance;
    }

    private GameObject model;
    private MeshRenderer modelMR;
    private GameObject skillModel;
    private ParticleSystem skillps;
    private STCharacter stc;
    private STSkill stk;
    private SkeletonAnimation sa;
    private const string alley = "Model/SABattle/SACell/Character";
    private const string sdaAlley = "SpineAnime/Player/Chr";

    private Vector3 show;//进场位置
    private Transform parent;

    private Vector3 chrpos;//角色位置
    private float moveX;//X移动长度
    private float moveY;//Y移动长度

    private const float speedX = 0.01f;//X移动速度
    private const float speedY = 0.01f;//Y移动速度

    private int road;//当前路线

    private Rect rect;
    private Rect skillRect;
    private const float re = 0.15f;//普通攻击范围
    private const float reh = 0.075f;//普通攻击范围偏移量

    private const float skillScale = 0.4f;//技能缩放
    private Vector3 v3ss = new Vector3(skillScale, skillScale, skillScale);

    //动画帧名
    private string[] animAtt = new string[] { "Attack_ear", "Attack_punch_L", "Attack_punch_R" };//攻击
    private string animFly = "Fly";//飞行
    private string animMove = "Move_1";//上下移动
    private string animSkill = "Attack_eat";//技能
    private string animHit = "Attacked";//被击

    //状态
    private bool attack;//攻击
    private bool skill;//技能
    private bool skillPrepare;//技能前摇
    private bool hit;//被击
    private bool fly;//飞行
    private bool move;//移动

    private float skillTime;//技能持续时间
    private float skillPrepareTime;//技能前摇时间

    //消息
    private bool die;//死亡状态
    private int damage;//攻击力
    private SABattleFieldGraphWorker graph;

    private List<SAEnemy> enys;//被击列表


    private SACharacter()
    {
        enys = new List<SAEnemy>();
        moveY = moveX = 0;
        model = SACache.getResWithName<GameObject>(alley, ActionCollection.Suffix);
        model = GameObject.Instantiate(model);
    }

    public void init(Transform parent, Vector3 show, SABattleFieldGraphWorker graph)
    {
        this.graph = graph;
        this.show = show;
        this.parent = parent;
        model.transform.parent = parent;
        
    }

    
    public void reset(int cid)
    {
        if (null == stc || stc.Id != cid)
        {
            if (sa != null)
            {
                sa.AnimationState.Complete -= HandleEvent;
            }
            int skillID = 5001;//方便测试

            sa = model.GetComponent<SkeletonAnimation>();
            stc = STCharacter.getMap()[cid];
            stk = STSkill.getMap()[skillID];//[stc.Skill1];//获取技能ID

            //initLua(stc.Skill1, graph.OriginName);//初始化lua
            initLua(stk.Id, graph.OriginName);//初始化lua
            if (null != skillModel)
            {
                skillps.Stop(true);
                GameObject.Destroy(skillModel);
            }
            skillModel = SACache.getResWithName<GameObject>(SAAppConfig.ParticlesDir + "Skill/" + stk.Id, ActionCollection.Suffix);
            //加入机师
            skillModel = GameObject.Instantiate(skillModel);
            if (stk.Location == 0)//随机师移动
            {
                skillModel.transform.parent = model.transform;
            } else//固定位置
            {
                skillModel.transform.parent = parent.Find("cpos");
            }
            LuaLocation(skillModel.transform);

            Vector3 temp = skillModel.transform.localPosition;
            temp.z = 0.1f;
            skillModel.transform.localPosition = temp;
            //skillModel.transform.localScale = v3ss;
            
            foreach (var item in skillModel.GetComponentsInChildren<ParticleSystem>())
            {
                var main = item.main;
                main.scalingMode = ParticleSystemScalingMode.Local;
                item.transform.localScale = v3ss;
            }
            //var main = skillps.main;
            //main.scalingMode = ParticleSystemScalingMode.Local;
            skillps = skillModel.GetComponent<ParticleSystem>();
            //skillModel.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);

            sa.skeletonDataAsset = SACache.getResWithName<SkeletonDataAsset>(sdaAlley + cid, ActionCollection.SuffixSDA);
            sa.skeletonDataAsset.GetSkeletonData(true);
            sa.Initialize(true);
            sa.AnimationState.Complete += HandleEvent;
            modelMR = model.GetComponent<MeshRenderer>();
            modelMR.sortingOrder = 10;
            model.transform.SetParent(parent);
            damage = 1;//机师表没有？

        }
        resetState();
        chrpos = model.transform.localPosition = show;
        moveY = show.y;

        rect = new Rect(show.x - reh, show.y - reh, re, re);
    }

    public void battleSkillHandler()
    {
        if (!die && !skill)
        {
            if (stk.Hide)
            {
                modelMR.enabled = false;
            }

            SkillBehaviorEnum type = (SkillBehaviorEnum)stk.Behavior;
            switch (type)
            {
                case SkillBehaviorEnum.RepairLife:
                    {
                        graph.dispatchEvent(ActionCollection.ChrLifeChange, stk.Value);
                        break;
                    }
                
            }
            clearHitEnys();//清除当前被击列表，重新计算（放置某些奇怪技能，比如 跳格打怪，或者不打当前行）
            skillModel.SetActive(true);
            skillps.Play(true);
            skill = true;
            skillPrepare = true;
            attack = false;//强制中断普通攻击
            skillTime = stk.Time;//随便加点持续时间方便测试
            skillPrepareTime = stk.Prepare;
            sa.AnimationState.SetAnimation(0, animSkill, true);
        }
    }

    public void hitChrAnimHandler(bool flag)
    {
        die = flag;
        play(animHit);
    }


    public void checkMove(float deltaTime)
    {
        if (skill)
        {
            SAUtils.Console("skillTime : " + skillTime);
            if (skillPrepare)
            {
                if ((skillPrepareTime -= deltaTime) <= 0)
                {
                    skillPrepare = false;
                }
            } else if ((skillTime -= deltaTime) <= 0)//技能释放完毕判断
            {
                resetState();
            }
            if (!stk.HideMove)
            {//隐藏期间角色不移动
                return;
            }
        }
        bool flag = false;
        if (moveX != chrpos.x)
        {
            if (chrpos.x > moveX)
            {
                chrpos.x -= Mathf.Min(chrpos.x - moveX, speedX);
            } else
            {
                chrpos.x += Mathf.Min(moveX - chrpos.x, speedX);
            }
            rect.x = chrpos.x - reh;
            flag = true;
        }
        if (moveY != chrpos.y)
        {
            if (chrpos.y > moveY)
            {
                chrpos.y -= Mathf.Min(chrpos.y - moveY, speedY);
            }
            else
            {
                chrpos.y += Mathf.Min(moveY - chrpos.y, speedY);
            }
            rect.y = chrpos.y - reh;
            play(animMove);
            flag = true;
        }
        if (flag)
        {
            model.transform.localPosition = chrpos;
        }
    }

    public void checkCollision(SAEnemy eny)
    {
        if (!eny.Hit)
        {
            //检测范围，随后取Lua
            bool flag = skill ? LuaCollision(model.transform.localPosition, eny.Position) : rect.Contains(eny.Position);
            if (flag)
            {
                if (skill)
                {
                    if (!skillPrepare)
                    {
                        setHitStatus(eny, 999, animSkill);
                    }
                } else
                {
                    setHitStatus(eny, 1, animAtt[Random.Range(0, 3)]);
                    attack = true;
                }

            }
        }
    }
    private void setHitStatus(SAEnemy eny, int _damage, string animName)
    {
        enys.Add(eny);
        eny.Hit = true;
        eny.HitEny(_damage);
        graph.dispatchEvent(ActionCollection.ChrHit);
        play(animName);
    }

    private void play(string animName)
    {
        if (!attack && !skill)//这两个不可以打断
        {
            sa.AnimationState.SetAnimation(0, animName, false);
        }
    }
    private void HandleEvent(TrackEntry trackEntry)
    {
        if (!die)
        {
            if (!skill)
            {
                if (attack)
                {
                    attack = false;
                    resetState();
                }
                sa.AnimationState.SetAnimation(0, animFly, true);
            }
        }
    }

    private void clearHitEnys()
    {
        for (int i = 0; i < enys.Count; ++i)
        {
            enys[i].Hit = false;//还原被击状态
        }
        enys.Clear();
    }

    private void resetState()
    {
        if (stk.Hide)
        {
            modelMR.enabled = true;
        }
        skillPrepare = false;
        attack = false;//攻击
        skill = false;//技能
        skillps.Stop(true);
        skillModel.SetActive(false);
        clearHitEnys();
        play(animFly);
    }

    public void gameOver()
    {
        resetState();
    }

    public void setMoveX(float value)
    {
        moveX = value;
    }
    public void setMoveY(float value)
    {
        moveY = value;
    }

    public void setRoad(int road)
    {
        this.road = road;
    }
    public int getRoad()
    {
        return road;
    }

    public int Damage
    {
        get
        {
            return damage;
        }
    }
}
