﻿using org.jiira.protobuf;
using UnityEngine;
using Spine.Unity;
using Sacu.Utils;
using Sacu.Factory.Worker;
using Spine;
using Google.Producer.Events;

using System;
using System.Runtime.InteropServices;


public class SAEnemy : SACell
{
    private int idx;
    private int id;
    private string enyName;
    private int life;
    private float _speed;//速度备份
    private float speed;//当前速度
    private STEnemy eny;

    private bool super;//超级士兵
    private bool superCamera;//超级相机

    private bool die;//死亡状态

    private bool hit;//被击状态

    private long delay;
    private Vector3 pos3;//位置 1-4
    private int road;
    private float end;

    private GameObject model;

    private ParticleSystem behit;
    
    private STEnemy ste;
    private SkeletonAnimation sa;
    private Transform parent;
    private const string alley = "Model/SABattle/SACell/Enemy";
    private const string sdaAlley = "SpineAnime/Enemy/Eny";

    private const string animHit1 = "Attacked_1";
    private const string animHit2 = "Attacked_2";
    private const string animFly = "Fly";
    private SACharacter chr;
    private SAGraphWorker graph;

    private const float skillScale = 0.4f;//技能缩放
    private Vector3 v3ss = new Vector3(skillScale, skillScale, skillScale);

    private Transform lifeBox;

    //被击抖动
    private bool rock;
    private const float rockRange = 5;
    private float crrs = 2.5f;
    private float crr;


    public SAEnemy(int idx, int eid, long delay, Vector3 pos3, int road, float end, Transform parent, SACharacter chr, SAGraphWorker graph)
    {
        this.idx = idx;
        id = eid;
        this.delay = delay;
        this.pos3 = pos3;
        this.road = road;
        this.end = end;
        this.parent = parent;
        this.chr = chr;
        this.graph = graph;
    }

    public void init()
    {
        model = SACache.getResWithName<GameObject>(alley, ActionCollection.Suffix);
        model = GameObject.Instantiate(model);
        model.transform.parent = parent;
        model.transform.localPosition = pos3;
        sa = model.GetComponent<SkeletonAnimation>();
        ste = STEnemy.getMap()[id];
        enyName = ste.Name;
        //life = ste.Life;//血量需要通过攻击计算
        _speed = speed = ste.Speed;
        hit = false;
        //int ten = id / 10 % 10 * 10;//获取10位，用来计算皮肤
        int modelid = id / 1 % 10 + id / 100 % 10 * 100 + id / 1000 % 10 * 1000;//得到id
        sa.skeletonDataAsset = SACache.getResWithName<SkeletonDataAsset>(sdaAlley + modelid, ActionCollection.SuffixSDA);
        try
        {
            sa.skeletonDataAsset.GetSkeletonData(false);
        }
        catch (System.Exception e)
        {
            SAUtils.Console(sdaAlley + id);
        }
        super = ste.Life > chr.Damage;
        int ten = super ? 10 : 0;
        if (!super)
        {
            graph.getGameObjectChild(model, "Level").SetActive(false);
        } else
        {
            lifeBox = graph.getGameObjectChild(model, "Level").transform;
            life = (int)Mathf.Ceil(ste.Life / chr.Damage);
            graph.dispatchEvent(ActionCollection.CreEnyTxt, this);
        }

        foreach (var item in model.GetComponentsInChildren<ParticleSystem>())
        {
            var main = item.main;
            main.scalingMode = ParticleSystemScalingMode.Local;
            item.transform.localScale = v3ss;
        }

        behit = model.transform.Find("behit").GetComponent<ParticleSystem>();

        superCamera = life > 2;//抗击打数大于等于3才会生效

        sa.initialSkinName = "enemy_" + (modelid + ten);
        sa.Initialize(true);
        sa.AnimationState.Complete += HandleEvent;
        model.GetComponent<MeshRenderer>().sortingOrder = 1;
        graph.dispatchEvent(ActionCollection.RDLight, SARoadLight.N(road, 1));
    }
    public void HitEny(int damage)
    {
        die = (life -= damage) <= 0;
        graph.dispatchEvent(ActionCollection.EnyTxtChange, this);
        behit.Play(true);
        if (die)//被击杀
        {
            model.transform.localEulerAngles = new Vector3(0, 0, 0);
            model.GetComponent<MeshRenderer>().sortingOrder = 11;
            graph.getGameObjectChild(model, "Level").SetActive(false);
            play(super? animHit2:animHit1);
        }
    }
    private void play(string animName)
    {
        sa.AnimationState.SetAnimation(0, animName, false);
    }
    private void HandleEvent(TrackEntry trackEntry)
    {
        if (!die)
        {
            sa.AnimationState.SetAnimation(0, animFly, true);
        } else
        {
            Destroy();
        }
    }
    public long Delay
    {
        get
        {
            return delay;
        }
    }

    public bool checkShow(long time)
    {
        return (delay -= time) <= 0;
    }

    public bool checkStop(float time)
    {
        if (!die)
        {
            chr.checkCollision(this);
            if (!hit)
            {
                Vector3 pos = model.transform.localPosition;
                pos.x -= speed * time / 2;
                model.transform.localPosition = pos;
                bool flag = pos.x < end;
                if (flag)
                {
                    graph.dispatchEvent(ActionCollection.DelEnyTxt, idx);//移除文本
                    graph.dispatchEvent(ActionCollection.EnyHit, ste.Damage);
                    Destroy();
                } else if (super)
                {
                    graph.dispatchEvent(ActionCollection.EnyTxtMove, this);//发送世界坐标
                }
                return flag;
            } else if(super)//被击抖动
            {
                Vector3 ea3 = model.transform.localEulerAngles;
                if (rock)
                {
                    if((crr -= crrs) <= -rockRange)
                    {
                        rock = false;
                    }
                } else
                {
                    if ((crr += crrs) >= rockRange)
                    {
                        rock = true;
                    }
                }
                ea3.z = crr;
                model.transform.localEulerAngles = ea3;
                SAUtils.Console("############################");
                SAUtils.Console("欧拉 : " + model.transform.eulerAngles);
                SAUtils.Console("普通 : " + model.transform.localEulerAngles);
            }
        }
        return die;
    }

    public Vector3 Position
    {
        get
        {
            return model.transform.localPosition;
        }
    }

    public bool Hit
    {
        get
        {
            return hit;
        }
        set
        {
            hit = value;
            if (hit)
            {
                crr = rockRange;
            } else
            {
                model.transform.localEulerAngles = new Vector3(0, 0, 0);
            }
        }
    }

    public bool SuperCamera
    {
        get
        {
            return superCamera;
        }
    }

    public Vector3 LifeBoxOffset
    {
        get
        {
            return lifeBox.localPosition;
        }
    }
    public int Idx
    {
        get
        {
            return idx;
        }
    }
    public int Life
    {
        get
        {
            return life;
        }
    }
    private void Destroy()
    {
        //移除时 关灯
        graph.dispatchEvent(ActionCollection.DelEnyTxt, idx);//移除文本
        graph.dispatchEvent(ActionCollection.RDLight, SARoadLight.N(road, -1));
        GameObject.Destroy(model);
    }
}
