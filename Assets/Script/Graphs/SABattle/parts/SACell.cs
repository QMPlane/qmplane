﻿using Sacu.Utils;
using System.IO;
using XLua;
using UnityEngine;

public class SACell
{

    private LuaEnv luaEnv;//技能
    private CallCollision coli;
    private CallLocation local;
    protected void initLua(int skillID, string originName)
    {
        if (null != luaEnv)
        {
            luaEnv.Dispose();
            luaEnv = null;
            coli = null;
        }
        string luaFile = originName.Substring(0, originName.LastIndexOf("/") + 1) + ActionCollection.SkillName + skillID + ".lua";
        SAUtils.Console("originName : " + luaFile);
        
        string localFilePath = Path.Combine(SAAppConfig.LuaRelease ? SAAppConfig.RemoteLuaDir : SAAppConfig.DevLuaDir, luaFile);
        bool isExist = File.Exists(localFilePath);//判断本地文件
        
        if (isExist)
        {
            
            luaEnv = new LuaEnv();
            luaEnv.AddLoader(LuaFile);
            luaEnv.DoString("require '" + localFilePath + "'");
            coli = luaEnv.Global.Get<CallCollision>(ActionCollection.SkillCollision);
            local = luaEnv.Global.Get<CallLocation>(ActionCollection.SkillLocation);
        }
    }
    protected byte[] LuaFile(ref string filepath)
    {
        return File.ReadAllBytes(filepath);
    }
    protected bool LuaCollision(Vector3 cv3, Vector3 ev3)
    {
        if (null != luaEnv)
        {
            if (null != coli)//没有对应的lua类
            {
                if (null != cv3 && null != ev3)
                {
                    return coli(cv3, ev3);
                }
            }
        }
        return false;
    }

    protected void LuaLocation(Transform value)
    {
        if (null != luaEnv)
        {
            if (null != local)//没有对应的lua类
            {
                if (null != value)
                {
                    local(value);
                }
            }
        }
    }


    [CSharpCallLua]
    //机师坐标，敌人坐标，技能范围(在lua中写死)
    public delegate bool CallCollision(Vector3 cv3, Vector3 ev3);

    [CSharpCallLua]
    public delegate void CallLocation(Transform tf);
}
