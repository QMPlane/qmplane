﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SARoadLight
{
    private int road;
    private int num;

    private static SARoadLight instance;

    public static SARoadLight N(int road, int num)
    {
        if (null == instance)
        {
            instance = new SARoadLight();
        }
        instance.road = road;
        instance.num = num;
        return instance;
    }
    private SARoadLight()
    {

    }

    public int Road
    {
        get
        {
            return road;
        }
    }
    public int Num
    {
        get
        {
            return num;
        }
    }
}
