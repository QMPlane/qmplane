﻿using Sacu.Factory.Worker;
using UnityEngine.UI;
using UnityEngine.Events;
using Sacu.Utils;


/**
 * 
 * AB按钮
 * 上下两个键
 * 
 */
public class ABWheel
{
    private static ABWheel _ab;
    
    public static ABWheel getInstance()
    {
        if (null == _ab)
        {
            _ab = new ABWheel();
        }
        return _ab;
    }

    private SAGraphWorker graph;

    private ABWheel() { }

    public void init (SAGraphWorker graph)
    {
        this.graph = graph;
        initButton("UpBtn", upButtonHandler);
        initButton("DownBtn", downButtonHandler);
        initButton("SkillBtn", skillButtonHandler);
    }

    private void initButton(string buttonName, UnityAction func)
    {
        Button temp = graph.getComponentForGameObjectName<Button>(buttonName);
        temp.onClick.AddListener(func);
    }

    private void upButtonHandler()
    {
        graph.dispatchEvent(ActionCollection.BattleMoveRoad, 1);
    }
    private void downButtonHandler()
    {
        graph.dispatchEvent(ActionCollection.BattleMoveRoad, -1);
    }
    private void skillButtonHandler()
    {
        graph.dispatchEvent(ActionCollection.BattleSkill);
    }
}
