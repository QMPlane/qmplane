﻿using Sacu.Factory.Worker;
using org.jiira.protobuf;
using System.Collections.Generic;
using Sacu.Utils;
using UnityEngine;
using Google.Producer.Events;

/*
 * 战斗
 */

namespace Graphs
{
    public class SABattleFieldGraphWorker : SAGraphWorker
    {
        private const string r = "road/r";
        // Start is called before the first frame update
        private bool run;//false 暂停
        private bool on;//是进行中
        private bool war;//继续出征
        private SACharacter chr;//机师
        private List<SAEnemy> enys;//敌人
        private List<SAEnemy> hidenys;//未出现敌人
        private long lostTime;//经过的时间
        private int enyIdx;//当前未出现敌人下标
        private SAEnemy ceny;//当前敌人

        private float showEnd;//进场位置

        private SAEnemy tempEny;//临时敌人
        private int enyCount;

        private Vector3[] pos3;//四条路坐标系
        private float end;//终点
        private Vector3 scale;


        protected override void init()
        {
            base.init();

            Camera MainCamera = SAManager.Instance.MainCamera;
            float height = MainCamera.orthographicSize * 2.0f;
            float width = height * MainCamera.aspect;
            scale = new Vector3(width, height, 0.1f);
            transform.localScale = scale;
            
            chr = SACharacter.getInstance();
            enys = new List<SAEnemy>();

            pos3 = new Vector3[4];
            GameObject temp;
            for (int i = 0; i < ActionCollection.RDlen; ++i)
            {
                temp = getGameObjectForName(r + i);
                pos3[i] = temp.transform.localPosition;
                pos3[i].z = 1;
            }
            temp = getGameObjectForName("rend");
            end = temp.transform.localPosition.x;
            Vector3 showPos = temp.transform.localPosition;
            temp = getGameObjectForName("cpos");
            showEnd = temp.transform.localPosition.x;
            chr.init(transform, showPos, this);
        }

        public string OriginName
        {
            get
            {
                return originName;
            }
        }
        override protected void onRegister()
        {
            base.onRegister();
            addEventDispatcherWithHandle(ActionCollection.HitChrAnim, hitChrAnimHandler);
            //因为技能不用做移动范围运算，因此，技能释放直接做到机师里
            addEventDispatcherWithHandle(ActionCollection.BattleSkill, battleSkillHandler);
            addEventDispatcherWithHandle(ActionCollection.BattleMoveRoad, moveHandler);
            addEventDispatcherWithHandle(ActionCollection.GameOver, gameOverHandler);
        }
        override protected void onRemove()
        {
            base.onRemove();
            removeEventDispatcher(ActionCollection.HitChrAnim);
            removeEventDispatcher(ActionCollection.BattleSkill);
            removeEventDispatcher(ActionCollection.BattleMoveRoad);
            removeEventDispatcher(ActionCollection.GameOver);
        }

        private void battleSkillHandler(FactoryEvent e)
        {
            chr.battleSkillHandler();
        }

        private void hitChrAnimHandler(FactoryEvent e)
        {
            chr.hitChrAnimHandler(System.Convert.ToBoolean(e.Body));
        }

        private void moveHandler(FactoryEvent e)
        {
            int road = chr.getRoad() - System.Convert.ToInt32(e.Body);
            if (road >= 0 && road < 4)
            {
                chr.setRoad(road);
                chr.setMoveY(pos3[road].y);
            }
        }

        private void gameOverHandler(FactoryEvent e)
        {
            run = false;
            chr.gameOver();
        }
        private STLevels stl;//当前关卡数据
        protected override void onStart(object args)
        {
            run = false;
            base.onStart(args);
            //此处随后需要做个关卡数据加载（怪物列表以后加入局部热更）
            int levelID = System.Convert.ToInt32(args);
            //获取关卡数据
            stl = STLevels.getMap()[levelID];
            SUserData.Builder ud = (SUserData.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SUserData);
            //读取机师信息
            //chr.reset(ud.Model);
            chr.reset(1001);//先写死，现在就一个机师
            chr.setMoveX(showEnd);//设置进场位置
            chr.setRoad(1);//设置路线
            string[] sof = stl.SOF;
            enyCount = sof.Length;
            if (null != hidenys)
            {
                hidenys.Clear();
            }
            enys.Clear();
            hidenys = new List<SAEnemy>();
            int eid;
            long delay;
            int pos;
            string[] enyArr;
            bool no;
            for (int i = 0; i < enyCount; ++i)
            {
                enyArr = sof[i].Split(':');
                eid = int.Parse(enyArr[0]);
                delay = long.Parse(enyArr[1]);
                pos = int.Parse(enyArr[2]) - 1;//策划表是从1开始……所以-1
                ceny = new SAEnemy(i, eid, delay, pos3[pos], pos, end, transform, chr, this);
                no = true;
                for (int j = 0; j < hidenys.Count; ++j)
                {
                    tempEny = hidenys[j];
                    if (ceny.Delay < tempEny.Delay)
                    {//插入到当前位置
                        no = false;
                        hidenys.Insert(j, ceny);
                        break;
                    }
                }
                if (no)
                {
                    hidenys.Add(ceny);
                }
            }
            tempEny = ceny = null;
            enyIdx = 0;//初始化下标
            lostTime = 5000;//初始化经过时间
            getEny();//初始化敌人
            war = on = run = true;//开始
        }

        private void getEny()
        {
            if (++enyIdx < enyCount)
            {
                ceny = hidenys[enyIdx];
                ceny.checkShow(lostTime);
            }
            else
            {
                war = false;
            }
        }
        void Update()
        {
            if (run)
            {
                if (war)
                {
                    int time = (int)(Time.deltaTime * 1000);
                    lostTime += time;
                    if (ceny.checkShow(time))
                    {
                        enys.Add(ceny);
                        ceny.init();
                        getEny();
                    }
                }
                if (on)
                {
                    if (enys.Count > 0)
                    {
                        for (int i = 0; i < enys.Count;)
                        {
                            tempEny = enys[i];
                            if (tempEny.checkStop(Time.deltaTime))
                            {
                                enys.RemoveAt(i);
                            } else
                            {
                                ++i;
                            }
                        }
                    } else if(!war)
                    {
                        on = false;
                    }
                } else//战斗结束
                {
                    SAUtils.Console("战斗结束^");
                }
                chr.checkMove(Time.deltaTime);//角色移动
            }
        }
    }
}
