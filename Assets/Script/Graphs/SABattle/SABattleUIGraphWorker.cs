﻿using Sacu.Factory.Worker;
using org.jiira.protobuf;
using Sacu.Utils;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;
using Google.Producer.Events;
using System;
using System.Collections.Generic;

namespace Graphs
{
    public class SABattleUIGraphWorker : SAGraphWorker
    {
        private Image[] roadNums;
        private int[] lights;
        private SpriteAtlas ba;//battle atlas
        private const string n = "num";
        //
        private Slider lifeSlider;
        private Text lifeTxt;

        private GameObject EnyLifeTxtTemp;
        private Dictionary<int, GameObject> EnyLifeTxts;
        //相机控制
        private CameraBlood blood;
        private CameraTremble tremble;
        //比例参数
        private float C2DWidth;
        private float C2DHeight;
        private float C3DSize;//相机大小
        private const float C3DScale = 0.5f;//相机比例

        //敌人头顶字体
        private Dictionary<Text, bool> textEffect;
        private const int TextMax = 200;
        private const int TextMin = 80;
        private const int TextStep = 20;

        //相机
        private Camera Camera2D;
        private Camera Camera3D;

        //相机位置&缩放       5次完成变焦
        private bool openScale;
        private bool closeScale;
        private const float CamMax = 5;
        private const float CamMin = 3;
        private const float CamOffet = -1.1f;
        private const float CamSizeStep = 0.4f;
        private const float CamOffsetStep = 0.22f;

        private const int closeTimeMax = 50;
        private int closeTime;

        protected override void init()
        {
            base.init();
            textEffect = new Dictionary<Text, bool>();
            EnyLifeTxtTemp = getGameObjectForName("EnyLifeTxt");
            EnyLifeTxts = new Dictionary<int, GameObject>();
            roadNums = new Image[ActionCollection.RDlen];
            lights = new int[ActionCollection.RDlen];
            GameObject temp;
            for (int i = 0; i < ActionCollection.RDlen; ++i)
            {
                temp = getGameObjectForName(n + i);
                roadNums[i] = temp.GetComponent<Image>();
                lights[i] = 0;
            }
            ba = SACache.getSpriteAtlasWithName(SAAppConfig.AtlasDir + "BattleAtlas");
            //设置屏幕参数
            C2DWidth = Screen.width;
            C2DHeight = Screen.height;
            //获取2D相机
            Camera2D = SAManager.Instance.UIRootCamera;
            //获取3D相机
            Camera3D = SAManager.Instance.MainCamera;//3d相机
            C3DSize = Camera3D.orthographicSize;
            //被击掉血
            Material mat = SACache.getResWithName<Material>("Material/Blood", "mat");
            blood = Camera3D.gameObject.AddComponent<CameraBlood>();
            blood.mat = mat;
            //击打震动
            mat = SACache.getResWithName<Material>("Material/TrembleX", "mat");
            tremble = Camera3D.gameObject.AddComponent<CameraTremble>();
            tremble.mat = mat;
            //生命值
            SUserData.Builder ud = (SUserData.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SUserData);
            lifeSlider = getComponentForGameObjectName<Slider>("Life");
            lifeTxt = getComponentForGameObjectName<Text>("lifeLbl");
            lifeSlider.minValue = 0;
            lifeSlider.maxValue = ud.Power;
            lifeSlider.value = ud.Power;
            lifeTxt.text = lifeSlider.value + "/" + lifeSlider.maxValue;

            openScale = closeScale = false;
            //摇杆
            ABWheel.getInstance().init(this);
        }

        protected override void onStart(object args)
        {
            base.onStart(args);
            EnyLifeTxts.Clear();
            //初始化UI路线标记
            Image temp;
            for (int i = 0; i < ActionCollection.RDlen; ++i)
            {
                temp = roadNums[i];
                temp.sprite = ba.GetSprite(n + i + "0");
            }
        }
        override protected void onRegister()
        {
            base.onRegister();
            addEventDispatcherWithHandle(ActionCollection.RDLight, lightHandler);
            addEventDispatcherWithHandle(ActionCollection.EnyHit, enyHitHandler);
            addEventDispatcherWithHandle(ActionCollection.ChrHit, chrHitHandler);
            addEventDispatcherWithHandle(ActionCollection.CreEnyTxt, creEnyTxtHandler);
            addEventDispatcherWithHandle(ActionCollection.EnyTxtMove, enyTxtMoveHandler);
            addEventDispatcherWithHandle(ActionCollection.EnyTxtChange, enyTxtChangeHandler);
            addEventDispatcherWithHandle(ActionCollection.DelEnyTxt, delEnyTxtHandler);

            addEventDispatcherWithHandle(ActionCollection.ChrLifeChange, chrLifeChangeHandler);
            

        }
        override protected void onRemove()
        {
            base.onRemove();
            removeEventDispatcher(ActionCollection.RDLight);
            removeEventDispatcher(ActionCollection.EnyHit);
            removeEventDispatcher(ActionCollection.ChrHit);
            removeEventDispatcher(ActionCollection.CreEnyTxt);
            removeEventDispatcher(ActionCollection.EnyTxtMove);
            removeEventDispatcher(ActionCollection.EnyTxtChange);
            removeEventDispatcher(ActionCollection.DelEnyTxt);
            removeEventDispatcher(ActionCollection.ChrLifeChange);
        }

        private void creEnyTxtHandler(FactoryEvent e)
        {
            SAEnemy eny = (SAEnemy)e.Body;
            GameObject temp = Instantiate(EnyLifeTxtTemp);
            temp.GetComponent<Text>().text = eny.Life.ToString();
            temp.transform.parent = SATransform;
            EnyLifeTxts.Add(eny.Idx, temp);
        }
        private void enyTxtMoveHandler(FactoryEvent e)
        {
            try
            {
                SAEnemy eny = (SAEnemy)e.Body;
                if (EnyLifeTxts.ContainsKey(eny.Idx))
                {
                    Vector3 p = eny.Position;
                    //p.x = C2DWidth * (p.x + Camera3D.orthographicSize / 10);//如果有屏幕翻转设定，则不能提前取屏幕值
                    //p.y = C2DHeight * (p.y + Camera3D.orthographicSize / 10 + eny.LifeBoxOffset.y / (Camera3D.orthographicSize));

                    p.x = Screen.width * (p.x + C3DScale);//如果有屏幕翻转设定，则不能提前取屏幕值(/10是缩放比例)
                    p.y = -Screen.height * (C3DScale - p.y - eny.LifeBoxOffset.y / 10);

                    //p.x = Screen.width * (p.x + C3DScale);//如果有屏幕翻转设定，则不能提前取屏幕值
                    //p.y = Screen.height * (p.y + C3DScale + eny.LifeBoxOffset.y / C3DSize);
                    if (openScale)
                    {
                        p.x += 175;
                        p.y += 65;
                    }
                    GameObject temp = EnyLifeTxts[eny.Idx];
                    temp.GetComponent<RectTransform>().anchoredPosition3D = p;
                }
            }
            catch (Exception ex)
            {
                SAUtils.Console(ex.Message);
            }
            
        }
        private void enyTxtChangeHandler(FactoryEvent e)
        {
            SAEnemy eny = (SAEnemy)e.Body;
            if (EnyLifeTxts.ContainsKey(eny.Idx))
            {
                GameObject temp = EnyLifeTxts[eny.Idx];
                Text t = temp.GetComponent<Text>();
                if (eny.Life > 0)
                {
                    t.text = eny.Life.ToString();
                } else
                {
                    t.text = "KO";
                }
                textEffect[t] = true;
                //只要有文字变化，就说明是特殊怪，就需要相机变化
                if (eny.SuperCamera)
                {
                    t.fontSize = 80;
                    Vector3 p = eny.Position;
                    p.x = Screen.width * (p.x + C3DScale) + 175;
                    p.y = -Screen.height * (C3DScale - p.y - eny.LifeBoxOffset.y / 10) + 65;
                    temp.GetComponent<RectTransform>().anchoredPosition3D = p;
                    closeTime = closeTimeMax;
                    openScale = true;
                    closeScale = false;
                }
            }
        }
        
        private void delEnyTxtHandler(FactoryEvent e)
        {
            int idx = Convert.ToInt32(e.Body);
            if (EnyLifeTxts.ContainsKey(idx))
            {
                Destroy(EnyLifeTxts[idx]);
                EnyLifeTxts.Remove(idx);
            }
        }

        private void chrHitHandler(FactoryEvent e)
        {
            tremble.open();
        }
        private void enyHitHandler(FactoryEvent e)
        {
            int hit = System.Convert.ToInt32(e.Body);
            bool flag = (lifeSlider.value -= hit) <= 0;
            blood.open();
            lifeTxt.text = lifeSlider.value + "/" + lifeSlider.maxValue;
            if (flag)//被击杀
            {
                SAUtils.Console("Game Over");
                dispatchEvent(ActionCollection.GameOver);//注释后死亡不结束
            }
            dispatchEvent(ActionCollection.HitChrAnim, flag);
        }

        private void chrLifeChangeHandler(FactoryEvent e)
        {
            lifeSlider.value += Convert.ToInt32(e.Body);
            lifeTxt.text = lifeSlider.value + "/" + lifeSlider.maxValue;
        }

        private void lightHandler(FactoryEvent e)
        {
            SARoadLight rl = (SARoadLight)e.Body;
            int r = rl.Road;
            lights[r] += rl.Num;
            roadNums[r].sprite = ba.GetSprite(n + r + (lights[r] > 0 ? "1" : "0"));
        }


        private void Update()
        {
            if (openScale)
            {
                Vector3 cv3;
                cv3 = Camera3D.transform.localPosition;
                if (closeScale)//开始还原
                {
                    if (closeTime > 0)
                    {
                        --closeTime;
                    } else
                    {
                        cv3.x += CamOffsetStep;
                        if ((Camera3D.orthographicSize += CamSizeStep) >= CamMax)
                        {
                            Camera3D.orthographicSize = CamMax;
                            cv3.x = 0;
                            openScale = false;
                        }
                    }
                }
                else//开始放大
                {
                    cv3.x -= CamOffsetStep;
                    if ((Camera3D.orthographicSize -= CamSizeStep) <= CamMin)
                    {
                        Camera3D.orthographicSize = CamMin;
                        cv3.x = CamOffet;
                        closeScale = true;
                    }
                }
                Camera3D.transform.localPosition = cv3;
            }
            if (textEffect.Count > 0)
            {
                Text t;
                List<Text> ts = new List<Text>(textEffect.Keys);
                for (int i = 0; i < ts.Count; ++i)
                {
                    t = ts[i];
                    if (textEffect[t])
                    {
                        if ((t.fontSize += TextStep) >= TextMax)
                        {
                            textEffect[t] = false;
                        }
                    }
                    else
                    {
                        if ((t.fontSize -= TextStep) <= TextMin)
                        {
                            textEffect.Remove(t);
                        }
                    }
                }
            }
        }
    }
}