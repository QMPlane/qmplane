﻿using Sacu.Factory.Worker;
using Sacu.Utils;
using Google.Producer.Events;
using org.jiira.protobuf;
using Sacu.Collection;

using UnityEngine.UI;
using UnityEngine;


using Datas;

namespace Graphs
{
    public class MusicUIGraphWorker : SAGraphWorker
    {
        override protected void init()
        {
            base.init();
        }
        override protected void onRegister()
        {
            base.onRegister();
            addEventDispatcherWithHandle(ActionCollection.Update + SingleUpdateTypeEnum.Music, updateMusic);//music本身
        }
        private void updateMusic(FactoryEvent action)
        {

        }

        public string GetStr(string s)
        {

            string[] sArray = s.Split('.');
            string str = sArray[0];
            return str;
        }
        

        /// <summary>
        /// 加载预制体到某个物体下面
        /// </summary>
        /// <param name="prafabs"></param>
        /// <param name="parents"></param>
        public GameObject LoadPrefabs(GameObject prafabs, GameObject parents)
        {
            //加载Cell出来
            GameObject prafab = Instantiate(prafabs);
            prafab.name = prafabs.name;
            Transform Tf = prafab.transform;
            Tf.SetParent(parents.gameObject.transform);
            //坐标四元数大小归零
            Tf.localPosition = Vector3.zero;
            Tf.localRotation = Quaternion.identity;
            Tf.localScale = Vector3.one;
            //设置为最后一个子物体
            Tf.SetAsLastSibling();
            return prafab;
        }
       

        
    }
}



