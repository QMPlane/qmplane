﻿using Sacu.Factory.Worker;
using Sacu.Utils;
using Google.Producer.Events;
using org.jiira.protobuf;
using Sacu.Collection;

using UnityEngine.UI;
using UnityEngine;


using Datas;
using System.Collections.Generic;
using System;
using Spine.Unity;
using System.Linq;
//Graphs1  未启动
namespace Graphs
{
    public class PilotUIGraphWorker : SAGraphWorker
    {
        public List<int> IDList = new List<int>();
        public Dictionary<int, int> FeelList = new Dictionary<int, int>();
        public Dictionary<int, int> ShootDownList = new Dictionary<int, int>();
        public List<Dictionary<int, int>> CharacterList = new List<Dictionary<int, int>>();
        private SAManager sm;
        private GameObject HeartSpine;
        private Button HeartSpine_Btn;
        private SkeletonGraphic HeartSpine_SK;
        //private Button HeartBtn;
        override protected void init()
        {
            
            base.init();
            HeartSpine = getGameObjectForName("HeartSpine");
            HeartSpine_Btn = (Button)getGameObjectByTypeName("HeatBtn", "Button");
            //HeartBtn = (Button)getGameObjectByTypeName("HeatBtn", "Button");
            HeartSpine_SK = GetSpine(HeartSpine);
            GetCharacterIns();
            callLuaFun("getCharacterList", CharacterList);

        }
        override protected void onRegister()
        {
            base.onRegister();
            addEventDispatcherWithHandle(ActionCollection.Update + SingleUpdateTypeEnum.Part, updatePart);
            //addEventDispatcherWithHandle(ActionCollection.ISItem + ActionCollection.Open, ISItem);//方便别的地方可以开启道具的消息
            //addEventDispatcherWithHandle(ActionCollection.Update + SingleUpdateTypeEnum.Item, AvtiveItem);
            addEventDispatcherWithHandle(CommandCollection.Sock + ProtoTypeEnum.SFeelReward, AvtiveItem);
            
        }
        protected override void onRemove()
        {
            base.onRemove();
            removeEventDispatcher(ActionCollection.Update + SingleUpdateTypeEnum.Part);
            removeEventDispatcher(CommandCollection.Sock + ProtoTypeEnum.SFeelReward);
            
        }
        //写一个开启监听事件的方法
       /* private void ISItem(FactoryEvent action)
        {
            //addEventDispatcherWithHandle(ActionCollection.Update + SingleUpdateTypeEnum.Item, AvtiveItem);
            addEventDispatcherWithHandle(CommandCollection.Sock + ProtoTypeEnum.SFeelReward, AvtiveItem);

        }*/
        private void updatePart(FactoryEvent action)
        {
            
        }
        private void AvtiveItem(FactoryEvent action)
        {
            List<int> ItemList_ = new List<int>();
            SASocketDataDAO sdd = (SASocketDataDAO)action.Body;
            SFeelReward.Builder FeelReward = (SFeelReward.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SFeelReward, sdd.bytes);
            int ItemID = FeelReward.FeelItemID;//道具的ID
            ItemList_.Add(ItemID);
            int ItemID_ = FeelReward.FeelItemNumID;//抽奖的ID，可以取到数量和概率
            List<STFeelItemNum> STFeelList = STFeelItemNum.getList();
            for (int i = 0; i < STFeelList.Count; i++)
            {
                if (ItemID_ == STFeelList[i].Id)
                {
                    int Count = STFeelList[i].Amount;
                    ItemList_.Add( Count);
                    break;
                }
            }
            Sacu.Utils.SAManager.Instance.sendMessageToFactory("QNSActiveUIFactory", ActionCollection.ItemActive + ActionCollection.Open, ItemList_);
        }
        /// <summary>
        /// 获取机师静态表  把ID存进表里
        /// </summary>
        /// <returns></returns>
        public List<int> GetStaticList()
        {
            List<STCharacter> PilotStaticList = STCharacter.getList();
            for (int i = 0; i < PilotStaticList.Count; i++)
            {
                IDList.Add(PilotStaticList[i].Id);
            }
            return IDList;
        }
        /// <summary>
        /// 获取机师动态表存成两个字典备用
        /// </summary>
        public void GetCharacterIns()
        {
            //服务器取消息
            SGetCharacterList.Builder CharacterData = (SGetCharacterList.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SGetCharacterList);
            IList<int> CharacterIDList = CharacterData.IdsList;
            IList<int> CharacterFeelList = CharacterData.FeelsList;
            IList<int> CharacterShootDownsList = CharacterData.ShootDownsList;
            for (int i = 0; i < CharacterIDList.Count; i++)
            {
                FeelList.Add(CharacterIDList[i], CharacterFeelList[i]);
                ShootDownList.Add(CharacterIDList[i], CharacterShootDownsList[i]);
            }
            CharacterList.Add(FeelList);
            CharacterList.Add(ShootDownList);
        }
        /// <summary>
        /// 加载预制体到某个物体下面
        /// </summary>
        /// <param name="prafabs"></param>
        /// <param name="parents"></param>
        public GameObject LoadPrefabs(GameObject prafabs, GameObject parents)
        {
            //加载Cell出来
            GameObject prafab = Instantiate(prafabs);
            prafab.name = prafabs.name;
            Transform Tf = prafab.transform;
            Tf.SetParent(parents.gameObject.transform);
            //坐标四元数大小归零
            Tf.localPosition = Vector3.zero;
            Tf.localRotation = Quaternion.identity;
            Tf.localScale = Vector3.one;
            //设置为最后一个子物体
            Tf.SetAsLastSibling();
            return prafab;
        }
        public string[] GetSplitStr(string s)
        {

            string[] sArray = s.Split('.');

            return sArray;
        }
        public SkeletonGraphic GetSpine(GameObject go)
        {
            /*SkeletonDataAsset a = SACache.getResWithName<SkeletonDataAsset>("SpineAnime/heart/Heart", ActionCollection.SuffixSDA);
            Debug.Log(a.a);*/
            SkeletonGraphic sk = go.GetComponent<SkeletonGraphic>();
            return sk;
            /* SkeletonDataAsset playerData = ScriptableObject.CreateInstance<SkeletonDataAsset>();
             playerData = Resources.Load<SkeletonDataAsset>("ren_1/ren1_SkeletonData");
             var spineAnimation = playerData.GetSkeletonData(false).FindAnimation("1_1");
             var sa = SkeletonGraphic.NewSkeletonGraphicGameObject(playerData, transform);
             if (spineAnimation != null)
             {
                 sa.Initialize(false);
                 sa.AnimationState.SetAnimation(0, spineAnimation, true);
             }
 */
        }
        public void StartHeartUIS()
        {
            SUserData.Builder userdates = (SUserData.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SUserData);
            if (userdates.Part >= 100)
            {
                // 先将动画播放切换到待机状态
                HeartSpine_Btn.gameObject.SetActive(true);
                HeartSpine_Btn.enabled = false;
                HeartSpine.SetActive(true);
                //HeartSpine_SK.AnimationState:SetAnimation(0, "START", false)
                HeartSpine_SK.AnimationState.SetAnimation(0, "START", false);
                HeartSpine_SK.AnimationState.Complete += delegate
                {
                    if (HeartSpine_SK.AnimationState.GetCurrent(0).Animation.Name == "START")
                    {
                        HeartSpine_SK.AnimationState.SetAnimation(0, "continu", true);
                        //将按钮激活  
                        HeartSpine_Btn.enabled = true;
                        HeartSpine_Btn.onClick.AddListener(HeartBtnClick);
                    }
                };
                
            }
            else
            {
                Debug.Log("Part不足");
                Sacu.Utils.SAManager.Instance.sendMessageToFactory("QNSActiveUIFactory", ActionCollection.FeelTxt + ActionCollection.Open,0);
            }
        }
        public void HeartBtnClick()
        {
            HeartSpine_Btn.onClick.RemoveAllListeners();

            HeartSpine_Btn.enabled = false;
            bool isClick = true;//定义一个发送消息的开关防止多次发送消息
            //点击按钮的回调 先将动画换到end然后监测动画播放完毕  发送消息
            HeartSpine_SK.AnimationState.SetAnimation(0, "END", false);
            HeartSpine_SK.AnimationState.Complete += delegate
            {
                if (HeartSpine_SK.AnimationState.GetCurrent(0).Animation.Name == "END"&&isClick)
                {
                    CFeelReward.Builder cFeelReward = (CFeelReward.Builder)CommandCollection.getDataModel(ProtoTypeEnum.CFeelReward);
                    Datas.SocketDataWorker sock = (Datas.SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
                    sock.sendMessage(ProtoTypeEnum.CFeelReward, cFeelReward.Build().ToByteArray());
                    HeartSpine_Btn.enabled = false;
                    HeartSpine_Btn.gameObject.SetActive(false);
                    HeartSpine.SetActive(false);
                    isClick = false;
                }
            };
        }
    }
}
