﻿using Sacu.Factory.Worker;
using Sacu.Utils;
using Google.Producer.Events;
using org.jiira.protobuf;
using Sacu.Collection;

using UnityEngine.UI;
using UnityEngine;


using Datas;
using System.Collections.Generic;
using System;
using System.Linq;
using Spine.Unity;
//Graphs1  未启动
namespace Graphs
{
    public class ActivityUIGraphWorker : SAGraphWorker
    {
        //public List<int> IDList = new List<int>();
        /*public Dictionary<int, int> FeelList = new Dictionary<int, int>();
        public Dictionary<int, int> FeelList_old = new Dictionary<int, int>();

        public Dictionary<int, int> ShootDownList = new Dictionary<int, int>();
        public Dictionary<int, int> ShootDownList_old = new Dictionary<int, int>();

        public List<Dictionary<int, int>> CharacterList = new List<Dictionary<int, int>>();
        public Dictionary<int, IList<int>> SOldReceivesDic = new Dictionary<int, IList<int>>();//已经领取过的奖励的字典
        public Dictionary<int, GameObject> PrefabDic = new Dictionary<int, GameObject>();
        public List<STCharacterLevel> PilotLevelStaticList;
        public List<int> IDShootDownList = new List<int>();*/
        override protected void init()
        {
            base.init();
            /*PilotLevelStaticList = STCharacterLevel.getList();
            *//* GiftBtn1 = getComponentForGameObjectName<Button>("GiftBtn/FrameIma1/Button");
             GiftBtn2 = getComponentForGameObjectName<Button>("GiftBtn/FrameIma2/Button");
             GiftBtn3 = getComponentForGameObjectName<Button>("GiftBtn/FrameIma3/Button");
             GiftBtn4 = getComponentForGameObjectName<Button>("GiftBtn/FrameIma4/Button");
             GiftBtn5 = getComponentForGameObjectName<Button>("GiftBtn/FrameIma5/Button");
             GiftBtn6 = getComponentForGameObjectName<Button>("GiftBtn/FrameIma6/Button");
             GiftBtn7 = getComponentForGameObjectName<Button>("GiftBtn/FrameIma7/Button");
             GiftBtn8 = getComponentForGameObjectName<Button>("GiftBtn/FrameIma8/Button");
             GetCharacterIns();
             GetItems();
             GetPrefabCell();*//*
            GetCharacterIns();
            GetSOldReceives();*/
        }
        override protected void onRegister()
        {
            base.onRegister();
            //addEventDispatcherWithHandle(ActionCollection.Update + SingleUpdateTypeEnum.Feel, updateFeel);//ins本身
            /* addEventDispatcherWithHandle(ActionCollection.Update + SingleUpdateTypeEnum.Item, updateItem);//ins本身
             addEventDispatcherWithHandle(ActionCollection.Update + SingleUpdateTypeEnum.Feel, updateFeel);//ins本身
             addEventDispatcherWithHandle(ActionCollection.Update + SingleUpdateTypeEnum.Model, updateModel);//ins本身*/
        }
        override protected void onRemove()
        {
            base.onRemove();
            //removeEventDispatcher(ActionCollection.Update + SingleUpdateTypeEnum.Feel);//ins本身
            /*removeEventDispatcher(ActionCollection.Update + SingleUpdateTypeEnum.Item);//ins本身

            removeEventDispatcher(ActionCollection.Update + SingleUpdateTypeEnum.Feel);//ins本身
            removeEventDispatcher(ActionCollection.Update + SingleUpdateTypeEnum.Model);//ins本身*/


        }
       /* private void updateFeel(FactoryEvent action)
        {
            GetCharacterIns();
        }*/
        /* private void updateItem(FactoryEvent action)
         {
             Updates up = (Updates)action.Body;
             GetItems();
             callLuaFun("LoadGift");
         }*/
        /* private void updateModel(FactoryEvent action)
         {

         }*/
       /* public void GetCharacterIns()
        {
            FeelList = new Dictionary<int, int>();
            FeelList_old = new Dictionary<int, int>();
            ShootDownList_old = new Dictionary<int, int>();
            ShootDownList = new Dictionary<int, int>();
            List<STCharacter> InsList = STCharacter.getList();
            //服务器取消息
            SGetCharacterList.Builder CharacterData = (SGetCharacterList.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SGetCharacterList);
            IList<int> CharacterIDList = CharacterData.IdsList;
            IList<int> CharacterFeelList = CharacterData.FeelsList;
            IList<int> CharacterShootDownsList = CharacterData.ShootDownsList;
            for (int i = 0; i < CharacterIDList.Count; i++)
            {
                ShootDownList_old.Add(CharacterIDList[i], CharacterShootDownsList[i]);
            }
            ShootDownList = ShootDownList_old.OrderBy(o => o.Key).ToDictionary(o => o.Key, p => p.Value);
        }
        public void GetSOldReceives()
        {
            //cc.getDataModel(pte.SOldReceives, dao);
            SOldReceivesDic = new Dictionary<int, IList<int>>();
            //服务器取消息
            SOldReceives.Builder SOldReceives_ = (SOldReceives.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SOldReceives);
            IList<int> SOldReceivesIDList = SOldReceives_.IdsList;
            IList<int> SOldReceivesList = SOldReceives_.ShootDownsList;
            *//*for (int i = 0; i < SOldReceivesList.Count; i++)
            {

            }*//*
            for (int i = 0; i < SOldReceivesIDList.Count; i++)
            {


                if (SOldReceivesDic.ContainsKey(SOldReceivesIDList[i]))
                {

                    //Debug.Log(SOldReceivesIDList[i]);
                    IDShootDownList.Add(SOldReceivesList[i]);
                    SOldReceivesDic[SOldReceivesIDList[i]] = IDShootDownList;
                }
                else
                {
                    IDShootDownList = new List<int>();
                    //Debug.Log(SOldReceivesIDList[i]);
                    IDShootDownList.Add(SOldReceivesList[i]);
                    SOldReceivesDic.Add(SOldReceivesIDList[i], IDShootDownList);
                }
            }
            *//*foreach (var item in SOldReceivesDic.Keys)
            {
                Debug.Log("key" + item);
            }*//*

        }
        public void SendDic()
        {
            callLuaFun("getCharacterList", CharacterList);
        }
        *//* public void SendItemsDic()
         {
             callLuaFun("getItemsDic", ItemDic);
         }*//*
        public void AddSC(GameObject go)
        {
            if (go.GetComponent<PageView>() == null)
            {
                go.AddComponent<PageView>();
            }
        }
        public void DeleteSC(GameObject go)
        {
            if (go.GetComponent<PageView>() == null)
            {
                return;
            }
            UnityEngine.Object ob = (UnityEngine.Object)go.GetComponent<PageView>();
            Destroy(ob);
        }
        public int GetLevelFeel(int feels)
        {
            int level = 0;
            List<STCharacterLevel> PilotLevelStaticList = STCharacterLevel.getList();
            for (int i = 0; i < PilotLevelStaticList.Count; i++)
            {
                if (feels >= PilotLevelStaticList[i].Feel)
                {
                    level = PilotLevelStaticList[i].Id;
                }
            }
            return level;
        }

        /// <summary>
        /// 获取机师动态表存成两个字典备用
        /// </summary>

        /// <summary>
        /// 加载预制体到某个物体下面
        /// </summary>
        /// <param name="prafabs"></param>
        /// <param name="parents"></param>
        public GameObject LoadPrefabs(GameObject prafabs, GameObject parents)
        {
            //加载Cell出来
            GameObject prafab = Instantiate(prafabs);
            prafab.name = prafabs.name;
            Transform Tf = prafab.transform;
            Tf.SetParent(parents.gameObject.transform);
            //坐标四元数大小归零
            Tf.localPosition = Vector3.zero;
            Tf.localRotation = Quaternion.identity;
            Tf.localScale = Vector3.one;
            //设置为最后一个子物体
            Tf.SetAsLastSibling();
            return prafab;
        }
        public GameObject LoadPrefabs_(GameObject prafabs, GameObject parents)
        {
            //加载Cell出来
            GameObject prafab = Instantiate(prafabs);
            prafab.name = prafabs.name;
            Transform Tf = prafab.transform;
            Tf.SetParent(parents.gameObject.transform);
            //坐标四元数大小归零
            Tf.localPosition = Vector3.zero;
            Tf.localRotation = Quaternion.identity;
            Tf.localScale = Vector3.one;
            //设置为最后一个子物体
            Tf.SetAsLastSibling();
            return prafab;
        }
        public string[] GetSplitStr(string s)
        {

            string[] sArray = s.Split(':');

            return sArray;
        }*/
    }
}



