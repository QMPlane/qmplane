using Sacu.Factory.Worker;
using Sacu.Utils;
using Google.Producer.Events;
using org.jiira.protobuf;
using Sacu.Collection;

using UnityEngine.UI;
using UnityEngine;


using Datas;

namespace Graphs
{
    public class PngUIGraphWorker : SAGraphWorker
    {
        override protected void init()
        {
            base.init(); 
        }
        override protected void onRegister()
        {
            base.onRegister();
            addEventDispatcherWithHandle(ActionCollection.Update + SingleUpdateTypeEnum.Key, updatePng);//music本身
        }
        override protected void onRemove()
        {
            base.onRemove();
            removeEventDispatcher(ActionCollection.Update + SingleUpdateTypeEnum.Key);//ins本身
        }
        private void updatePng(FactoryEvent action)
        {

        }
        public string GetStr(string s)
        {

            string[] sArray = s.Split('.');
            string str = sArray[0];
            return str;
        }
        private void getPhotos(FactoryEvent action)
        {
            Updates up = (Updates)action.Body;
            callLuaFun("getPhotos_C", up);
        }
        //getPhotos
        /// <summary>
        /// 加载预制体到某个物体下面
        /// </summary>
        /// <param name="prafabs"></param>
        /// <param name="parents"></param>
        public GameObject LoadPrefabs(GameObject prafabs, GameObject parents)
        {
            //加载Cell出来
            GameObject prafab = Instantiate(prafabs);
            prafab.name = prafabs.name;
            Transform Tf = prafab.transform;
            Tf.SetParent(parents.gameObject.transform);
            //坐标四元数大小归零
            Tf.localPosition = Vector3.zero;
            Tf.localRotation = Quaternion.identity;
            Tf.localScale = Vector3.one;
            //设置为最后一个子物体
            Tf.SetAsLastSibling();
            return prafab;
        }
       
    }
}



