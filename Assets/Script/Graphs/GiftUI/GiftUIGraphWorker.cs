﻿using Sacu.Factory.Worker;
using Sacu.Utils;
using Google.Producer.Events;
using org.jiira.protobuf;
using Sacu.Collection;

using UnityEngine.UI;
using UnityEngine;


using Datas;
using System.Collections.Generic;
using System;
//Graphs1  未启动
namespace Graphs
{
    public class GiftUIGraphWorker : SAGraphWorker
    {
        //public List<int> IDList = new List<int>();
        public Dictionary<int, int> FeelDic = new Dictionary<int, int>();//机师好感度字典
        public Dictionary<int, int> ShootDownDic = new Dictionary<int, int>();//机师击杀数字典
        public List<Dictionary<int, int>> CharacterList = new List<Dictionary<int, int>>();//合成一个list传到lua

        public Dictionary<int, int> ItemDic = new Dictionary<int, int>();//机师好感度字典

        override protected void init()
        {
            base.init();
            /*GetCharacterIns();
            GetItems();
            SendCharacterDic();
            SendItemsDic();*/
        }
        override protected void onRegister()
        {
            base.onRegister();
            addEventDispatcherWithHandle(ActionCollection.Update + SingleUpdateTypeEnum.Feel, updateFeel);//机师
        }
        override protected void onRemove()
        {
            base.onRemove();
            removeEventDispatcher(ActionCollection.Update + SingleUpdateTypeEnum.Feel);
        }
        private void updateFeel(FactoryEvent action)
        {
            Updates up = (Updates)action.Body;
            for (int i = FeelDic.Count - 1; i >= 0; i--)
            {
                if (FeelDic.ContainsKey(up.ID))
                {
                    FeelDic[up.ID] = up.Count;
                }
            }
            callLuaFun("changeTxt",up);
        }
        /* /// <summary>
         /// 获取机师静态表  把ID存进表里
         /// </summary>
         /// <returns></returns>
         public List<int> GetStaticList()
         {
             List<STCharacter> PilotStaticList = STCharacter.getList();
             for (int i = 0; i < PilotStaticList.Count; i++)
             {
                 IDList.Add(PilotStaticList[i].Id);
             }
             return IDList;
         }*/
        public void GetCharacterIns()
        {
            //服务器取消息
            SGetCharacterList.Builder CharacterData = (SGetCharacterList.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SGetCharacterList);
            IList<int> CharacterIDList = CharacterData.IdsList;
            IList<int> CharacterFeelList = CharacterData.FeelsList;
            IList<int> CharacterShootDownsList = CharacterData.ShootDownsList;
            for (int i = 0; i < CharacterIDList.Count; i++)
            {
                FeelDic.Add(CharacterIDList[i], CharacterFeelList[i]);
                ShootDownDic.Add(CharacterIDList[i], CharacterShootDownsList[i]);
            }
            CharacterList.Add(FeelDic);
            CharacterList.Add(ShootDownDic);
            
        }
        public void GetItems()
        {
            //服务器取消息
            SItems.Builder Items = (SItems.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SItems);
            IList<int> ItemIDList = Items.ItemIDList;
            IList<int> ItemFeelList = Items.ItemCountList;
            for (int i = 0; i < ItemIDList.Count; i++)
            {
                ItemDic.Add(ItemIDList[i], ItemFeelList[i]);
            }
        }
        public void SendCharacterDic()
        {
            callLuaFun("getCharacterList", CharacterList);
        }
        public void SendItemsDic()
        {
            callLuaFun("getItemsDic", ItemDic);
        }
        /// <summary>
        /// 获取机师动态表存成两个字典备用
        /// </summary>

        /// <summary>
        /// 加载预制体到某个物体下面
        /// </summary>
        /// <param name="prafabs"></param>
        /// <param name="parents"></param>
        public GameObject LoadPrefabs(GameObject prafabs, GameObject parents)
        {
            //加载Cell出来
            GameObject prafab = Instantiate(prafabs);
            prafab.name = prafabs.name;
            Transform Tf = prafab.transform;
            Tf.SetParent(parents.gameObject.transform);
            //坐标四元数大小归零
            Tf.localPosition = Vector3.zero;
            Tf.localRotation = Quaternion.identity;
            Tf.localScale = Vector3.one;
            //设置为最后一个子物体
            Tf.SetAsLastSibling();
            return prafab;
        }
    }
}


