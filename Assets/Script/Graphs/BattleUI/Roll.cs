﻿using UnityEngine;
using System.Collections;

/// <summary>
/// 通过目标点的角度，计算目标点的位置
/// 计算目标点的位置，计算目标点的方向向量
/// 旋转 转动对象
/// 
/// 速度控制
/// 减速
/// 
/// 判断当前y方向与目标向量的夹角
/// 如果夹角小于某一值，认为达到终点，停止旋转
/// 
/// </summary>
public class Roll : MonoBehaviour {

    public float endAngle=100;//旋转停止位置，相对y坐标方向的角度

    public Vector3 targetDir;//目标点的方向向量
    bool isMoving=false;//是否在旋转
    public float speed=0;//当前的旋转速度
    public float maxSpeed=10;//最大旋转速度
    public float minSpeed=0.8f;//最小旋转速度
    float rotateTimer=2;//旋转计时器
    public int moveState=0;//旋转状态，旋转，减速
    public int keepTime = 3;//旋转减速前消耗的时间


    //按照顺时针方向递增
    string[] rewards = {"8等奖","7等奖","6等奖","5等奖","4等奖","3等奖","2等奖","1等奖","10等奖","9等奖"};

    void Start () {
        targetDir = new Vector3 (0, 1, 0);
//      Vector3.Angle (targetDir,transform.up);
    }


    void Update () {
        if(Input.GetKeyDown(KeyCode.A)){
            StartMove ();
        }

        if(Input.GetKey(KeyCode.S)){

            transform.Rotate (new Vector3(0,0,maxSpeed));
        }

        if(isMoving){
            if(moveState==1 && (rotateTimer>0 || getAngle()<270)){//如果旋转时间小于旋转保持时间，或者大于旋转保持时间但是与停止方向角度小于270，继续保持旋转
                rotateTimer -= Time.deltaTime;
                if(speed<maxSpeed) speed +=  1;
                transform.Rotate (new Vector3(0,0,speed));
            }else{//减速旋转，知道停止在目标位置
                moveState = 2;
                if (speed > minSpeed)
                        speed -= 7*speed / 10;
                if (getAngle () > 10)
                        transform.Rotate (new Vector3 (0, 0, speed));
                else {//stop
                    endMove();
                }
            }



        }
    }

    #region 计算当前对象y方向与目标方向的夹角

    float getAngle ()
    {
        return calAngle (targetDir, transform.up);//计算y轴方向的旋转角度
    }
    //计算从dir1旋转到dir2的角度

    float calAngle (Vector3 dir1, Vector3 dir2)
    {
        float angle = Vector3.Angle (dir1, dir2);
        Vector3 normal = Vector3.Cross (dir1, dir2);
        //      Debug.Log ("normal="+normal);
        //      angle = normal.z > 0 ? angle : (180+(180-angle));
        angle = normal.z > 0 ? angle : (360 - angle);
        return angle;
    }

    #endregion

    /// <summary>
    /// 计算目标位置的向量
    /// Calculates the dir.
    /// </summary>
    /// <param name="endAngle">End angle.</param>
    Vector3 calculateDir(float endAngle){
        float radiansX =  Mathf.Cos( Mathf.PI *(endAngle + 90) / 180);
        float radiansY = Mathf.Sin( Mathf.PI *(endAngle + 90) / 180);
        return new Vector3 (radiansX, radiansY, 0);

    }

    void endMove(){

        speed = 0;
        isMoving = false;
        moveState = 0;
    }

    void StartMove(){
        if (isMoving)
            return;
        int index=Random.Range (0, 3);
        Debug.Log(index);
        Debug.Log ("恭喜你获得"+rewards[index]);
        endAngle = index *  360/3;//获得目标位置相对y坐标方向的角度
        targetDir = calculateDir (endAngle);//获得目标位置方向向量
        rotateTimer = keepTime;
        isMoving = true;
        moveState = 1;
    }
}