﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using UnityEngine.UI;
using DG.Tweening;
public class Player : MonoBehaviour
{
    // private static Player instance;
    // public static Player Instance{
    //     get{
    //         if(instance == null){
    //             instance = new Player();
    //         }
    //         return instance;
    //     }
    // }
    public static Player instance;
    public Transform[] posList;
    [HideInInspector]
    public int maxHP;
    [HideInInspector]
    public int curHP;
    // public ParticleSystem atkEffect;
    public ParticleSystem stopMoveEffect;
    public ParticleSystem skill;
    bool startMove1 = false;
    bool startMove2 = false;
    bool startMove3 = false;
    bool startMove4 = false;
    float dis = 0;
    float speed = 0.1f;
    [HideInInspector]
    public int atk = 1;
    [HideInInspector]
    public bool isDead = false;
    string[] atkAniList = new string[] { "Attack_ear", "Attack_punch_L", "Attack_punch_R" };
    SkeletonAnimation skeletonAnime;
    public Collider2D box;
    bool damaged = false;
    public SpriteRenderer damage_Image;
    private Image skill_Image;
    public bool useSkill;
    private Image atkBoss_Image;
    Color beAtkColor;
    Color atkBossColor;
    public float flash_Speed = 5;
    [HideInInspector]
    public bool skillIsCD;
    bool reStart = false;
    void Awake()
    {
        instance = this;
        skeletonAnime = GetComponentInChildren<SkeletonAnimation>();
        box = GetComponent<Collider2D>();

    }
    // Start is called before the first frame update
    void Start()
    {
        
        damage_Image = GameObject.Find("beAtkMask").GetComponent<SpriteRenderer>();
        skill_Image = GameObject.Find("skillMask").GetComponent<Image>();
        atkBoss_Image = GameObject.Find("bossMask").GetComponent<Image>();
        beAtkColor = new Color(1, 0, 0, 0.7f);
        atkBossColor = new Color(235f / 255f, 0f / 255f, 255f / 255f, 168f / 255f);
        // maxHP = Graphs.BattleSceneGraphWorker.instance.maxHP;
        // curHP = Graphs.BattleSceneGraphWorker.instance.curHP;
    }
    /// <summary>
    /// 角色受伤后的屏幕效果
    /// </summary>
    void PlayDamagedEffect()
    {
        if (damaged)
        {
            damage_Image.color = beAtkColor;
        }
        else
        {
            damage_Image.color = Color.Lerp(damage_Image.color, Color.clear, flash_Speed * Time.deltaTime);

        }
        damaged = false;
    }
    //使用技能时候的屏幕效果
    void PlaySkillEffect()
    {
        if (useSkill)
        {
            skill_Image.color = Color.Lerp(skill_Image.color, Color.white, 10 * Time.deltaTime);
        }
        else
        {
            skill_Image.color = Color.Lerp(skill_Image.color, Color.clear, flash_Speed * Time.deltaTime);
        }

    }
    //攻击BOSS时候的屏幕效果
    void AtkBossEffect()
    {
        if (Graphs.BattleUIGraphWorker.instance.isCloseUp == true)
        {
            atkBoss_Image.color = atkBossColor;
        }
        else
        {
            atkBoss_Image.color = Color.Lerp(atkBoss_Image.color, Color.clear, 10 * Time.deltaTime);
        }
    }
    //重置主角坐标
    public void RePos()
    {
        transform.position = new Vector3(-8f, 2f, 0);
        reStart = true;

    }
    // Update is called once per frame
    void FixedUpdate()
    {
        //胜利时候主角飞出屏幕右侧
        if(Graphs.BattleSceneGraphWorker.instance.isWin == true){
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(10,transform.position.y), 8 * Time.deltaTime);
        }
        if (reStart)
        {
            transform.transform.position = Vector2.MoveTowards(transform.position, Player.instance.posList[1].position, 5 * Time.deltaTime);
        }
        if( reStart && Vector2.Distance(transform.position, posList[1].position) <= 0){
            reStart = false;
        }
        PlayDamagedEffect();
        PlaySkillEffect();
        AtkBossEffect();
        //主角移动逻辑
        if (startMove1)
        {
            // transform.position = Vector2.MoveTowards(transform.position, posList[0].position, dis / speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, posList[0].position) <= 0.01f)
            {
                Graphs.BattleSceneGraphWorker.instance.playerPos = 0;
                stopMoveEffect.gameObject.SetActive(true);
                skeletonAnime.skeleton.SetToSetupPose();
                skeletonAnime.state.ClearTracks();
                startMove1 = false;
                dis = 0;
                box.enabled = true;
                skeletonAnime.AnimationState.SetAnimation(0, "Fly", true);
            }
            // else
            // {
            //    // skeletonAnime.AnimationState.SetAnimation(0, "Move_1", false);
            // }
        }
        else if (startMove2)
        {
            // transform.position = Vector2.MoveTowards(transform.position, posList[1].position, dis / speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, posList[1].position) <= 0.01f)
            {
                Graphs.BattleSceneGraphWorker.instance.playerPos = 1;
                stopMoveEffect.gameObject.SetActive(true);
                skeletonAnime.skeleton.SetToSetupPose();
                skeletonAnime.state.ClearTracks();
                startMove2 = false;
                dis = 0;
                box.enabled = true;
                skeletonAnime.AnimationState.SetAnimation(0, "Fly", true);
            }
            // else
            // {
            //    // skeletonAnime.AnimationState.SetAnimation(0, "Move_1", false);
            // }
        }
        else if (startMove3)
        {
            // transform.position = Vector2.MoveTowards(transform.position, posList[2].position, dis / speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, posList[2].position) <= 0.01f)
            {
                Graphs.BattleSceneGraphWorker.instance.playerPos = 2;
                stopMoveEffect.gameObject.SetActive(true);
                skeletonAnime.skeleton.SetToSetupPose();
                skeletonAnime.state.ClearTracks();
                startMove3 = false;
                dis = 0;
                box.enabled = true;
                skeletonAnime.AnimationState.SetAnimation(0, "Fly", true);
            }
            // else
            // {
            //    // skeletonAnime.AnimationState.SetAnimation(0, "Move_1", false);
            // }
        }
        else if (startMove4)
        {
            // transform.position = Vector2.MoveTowards(transform.position, posList[3].position, dis / speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, posList[3].position) <= 0.01f)
            {
                Graphs.BattleSceneGraphWorker.instance.playerPos = 3;
                stopMoveEffect.gameObject.SetActive(true);
                skeletonAnime.skeleton.SetToSetupPose();
                skeletonAnime.state.ClearTracks();
                startMove4 = false;
                dis = 0;
                box.enabled = true;
                skeletonAnime.AnimationState.SetAnimation(0, "Fly", true);
            }
            // else
            // {
            //    // skeletonAnime.AnimationState.SetAnimation(0, "Move_1", false);
            // }
        }
    }
    //下面为四个移动位置的逻辑
    public void Move1()
    {
        dis = Vector2.Distance(transform.position, posList[0].position);
        startMove1 = true;
        transform.DOMove(posList[0].position,0.1f);
        box.enabled = false;
    }
    public void Move2()
    {
        dis = Vector2.Distance(transform.position, posList[1].position);
        startMove2 = true;
        transform.DOMove(posList[1].position,0.1f);
        box.enabled = false;
    }
    public void Move3()
    {
        dis = Vector2.Distance(transform.position, posList[2].position);
        startMove3 = true;
        transform.DOMove(posList[2].position,0.1f);
        box.enabled = false;
    }
    public void Move4()
    {
        dis = Vector2.Distance(transform.position, posList[3].position);
        startMove4 = true;
        transform.DOMove(posList[3].position,0.1f);
        box.enabled = false;
    }
    public void BeAttack(int enemyAtk)
    {
        damaged = true;
        // Handheld.Vibrate();
        if (Camera.main.GetComponent<CameraShake>().shake <= 0)
        {
            Camera.main.GetComponent<CameraShake>().shakeAmount = 0.2f;
            Camera.main.GetComponent<CameraShake>().shake = 0.3f;
        }
        GameObject.Find("Main2DCamera").GetComponent<Camera2DShake>().enabled = true;
        if (curHP > enemyAtk)
        {
            curHP -= enemyAtk;
        }
        else
        {
            if (isDead == false)
                Dead();
        }
        Graphs.BattleUIGraphWorker.instance.CallLua("ChangeLife");
    }
    void Dead()
    {
        Graphs.BattleUIGraphWorker.instance.SetAmojiAndTalk(7);
        isDead = true;
        curHP = 0;
        box.enabled = false;
        Graphs.BattleSceneGraphWorker.instance.gameEnd = true;
        Graphs.BattleSceneGraphWorker.instance.Failed();
    }
    //攻击动画以及屏幕抖动
    public void AtkAnima()
    {
        // Handheld.Vibrate();
        if (Camera.main.GetComponent<CameraShake>().shake <= 0)
        {
            Camera.main.GetComponent<CameraShake>().shakeAmount = 0.1f;
            Camera.main.GetComponent<CameraShake>().shake = 0.1f;
        }
        GameObject.Find("Main2DCamera").GetComponent<Camera2DShake>().enabled = true;

        skeletonAnime.skeleton.SetToSetupPose();
        skeletonAnime.state.ClearTracks();
        skeletonAnime.AnimationState.SetAnimation(0, atkAniList[Random.Range(0, 3)], false);

        skeletonAnime.AnimationState.AddAnimation(0, "Fly", true, 0);
        // atkEffect.Play();

    }
    //攻击BOSS时候的组合攻击
    public void AtkCombo()
    {
        // Handheld.Vibrate();
        if (Camera.main.GetComponent<CameraShake>().shake <= 0)
        {
            Camera.main.GetComponent<CameraShake>().shakeAmount = 0.1f;
            Camera.main.GetComponent<CameraShake>().shake = 0.1f;
        }
        GameObject.Find("Main2DCamera").GetComponent<Camera2DShake>().enabled = true;

        skeletonAnime.skeleton.SetToSetupPose();
        skeletonAnime.state.ClearTracks();
        skeletonAnime.AnimationState.SetAnimation(0, atkAniList[1], false);

        skeletonAnime.AnimationState.AddAnimation(0, "Fly", true, 0);
        // atkEffect.Play();
    }
    //攻击BOSS时候的组合攻击2
    public void AtkCombo2()
    {
        // Handheld.Vibrate();
        if (Camera.main.GetComponent<CameraShake>().shake <= 0)
        {
            Camera.main.GetComponent<CameraShake>().shakeAmount = 0.1f;
            Camera.main.GetComponent<CameraShake>().shake = 0.1f;
        }
        GameObject.Find("Main2DCamera").GetComponent<Camera2DShake>().enabled = true;

        skeletonAnime.skeleton.SetToSetupPose();
        skeletonAnime.state.ClearTracks();
        skeletonAnime.AnimationState.SetAnimation(0, atkAniList[2], false);

        skeletonAnime.AnimationState.AddAnimation(0, "Fly", true, 0);
        // atkEffect.Play();
    }
    
    //使用技能
    public void UseSkill()
    {

        if (skill.gameObject.activeSelf == false && skillIsCD == false && isDead == false)
        {
            Graphs.BattleUIGraphWorker.instance.SetAmojiAndTalk(5);
            //skill_Image.color = Color.white;
            // Debug.LogError("skill");
            skillIsCD = true;
            useSkill = true;
            skill.gameObject.SetActive(true);
            StartCoroutine(StartCD());
        }
    }
    //技能CD，现在为4秒
    IEnumerator StartCD()
    {
        yield return new WaitForSeconds(4f);
        skill.gameObject.SetActive(false);
        skillIsCD = false;
        useSkill = false;
        // skill_Image.color = Color.clear;
    }
}
