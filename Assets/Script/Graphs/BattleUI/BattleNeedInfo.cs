﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleNeedInfo
{
    public int stageID;
    public int charaterID;
    public int charaterFeel;
    public int maxHP;
    public int curHP;
    public int bgmID;
}
