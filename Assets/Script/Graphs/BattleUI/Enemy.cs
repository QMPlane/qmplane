﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using org.jiira.protobuf;
using DG.Tweening;
public class Enemy : MonoBehaviour
{
    // Start is called before the first frame update
    float life;
    float maxLife;
    private float speed = 6;
    float fixSpeed;
    int atk;
    //分数
    int score;
    //击坠数
    int shootDown;
    public TextMesh buildTime;
    bool beAttack = false;
    SkeletonAnimation skeletonAnime;
    BoxCollider2D myCollider;
    ParticleSystem atkEffect;
    GameObject kuang;
    public void Awake()
    {
        skeletonAnime = GetComponentInChildren<SkeletonAnimation>();
        buildTime = GetComponentInChildren<TextMesh>();
        myCollider = GetComponent<BoxCollider2D>();
        atkEffect = transform.GetChild(2).GetComponent<ParticleSystem>();
        kuang = transform.GetChild(3).gameObject;
    }
    //初始化敌人数据
    public void Init(STEnemy thisEnemy)
    {
        life = thisEnemy.Life;
        maxLife = thisEnemy.Life;
        fixSpeed = thisEnemy.Speed;
        atk = thisEnemy.Damage;
        score = thisEnemy.Score;
        shootDown = thisEnemy.Crashed;
        if (life >= 2)
        {
            kuang.SetActive(true);
            SetLife(life);
        }
    }
    public void BeAttack(int atk)
    {
        atkEffect.Play();
        life -= atk;
        if (life <= 0)
        {
            Death();
        }
        // Debug.Log(Graphs.BattleSceneGraphWorker.instance.score);
        // Debug.Log(Graphs.BattleSceneGraphWorker.instance.shootDown);
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (beAttack == false)
        {
            transform.Translate(Vector2.left * Time.fixedDeltaTime * speed * fixSpeed);
        }
    }
    void Death()
    {
        kuang.SetActive(false);
        if (Graphs.BattleUIGraphWorker.instance.isCloseUp == true)
        {
            Graphs.BattleUIGraphWorker.instance.isCloseUp = false;
            Graphs.BattleUIGraphWorker.instance.CloseDown();
            // StartCoroutine(WaitClose());
        }
        if (Mathf.Ceil(maxLife / Player.instance.atk) >= 3)
        {
            skeletonAnime.AnimationState.SetAnimation(0, "Attacked_2", false);
            Graphs.BattleUIGraphWorker.instance.SetAmojiAndTalk(3);
        }
        else
        {
            skeletonAnime.AnimationState.SetAnimation(0, "Attacked_1", false);
        }
        Graphs.BattleSceneGraphWorker.instance.score += score;
        Graphs.BattleSceneGraphWorker.instance.shootDown += shootDown;
        if (Graphs.BattleSceneGraphWorker.instance.shootDown == 5 || Graphs.BattleSceneGraphWorker.instance.shootDown == 30 || Graphs.BattleSceneGraphWorker.instance.shootDown == 60)
        {
            Graphs.BattleUIGraphWorker.instance.SetAmojiAndTalk(1);
        }
        else if (Graphs.BattleSceneGraphWorker.instance.shootDown == 15 || Graphs.BattleSceneGraphWorker.instance.shootDown == 45 || Graphs.BattleSceneGraphWorker.instance.shootDown == 85)
        {
            Graphs.BattleUIGraphWorker.instance.SetAmojiAndTalk(2);
        }
        Graphs.BattleUIGraphWorker.instance.CallLua("ChangeShootDown");
        myCollider.enabled = false;
        Destroy(this.gameObject, 1f);
    }
    //碰到左侧墙壁死亡
    void WallDeath()
    {
        kuang.SetActive(false);
        myCollider.enabled = false;
        Destroy(this.gameObject, 1f);
    }
    //触碰器检测逻辑
    private void OnTriggerEnter2D(Collider2D other)
    {
        //Fire是左侧墙壁
        if (other.tag == "Fire" && Player.instance != null)
        {
            if (Player.instance.isDead == false)
            {
                Graphs.BattleUIGraphWorker.instance.SetAmojiAndTalk(4);
                Player.instance.BeAttack(atk);
                
            }
            WallDeath();
            // Debug.Log("wall");
        }
        else if (other.tag == "Player")
        {
            beAttack = true;
            if (Mathf.Ceil(maxLife / Player.instance.atk) >= 3)
            {
                Graphs.BattleUIGraphWorker.instance.CloseUp();
                Camera.main.GetComponent<CameraShake>().shakeAmount = 0.5f;
                Camera.main.GetComponent<CameraShake>().shake = 0.1f;

                GameObject.Find("Main2DCamera").GetComponent<Camera2DShake>().enabled = true;
                StartCoroutine(BeAttack2());
                Graphs.BattleUIGraphWorker.instance.isCloseUp = true;


            }
            else if (Mathf.Ceil(maxLife / Player.instance.atk) == 2)
            {
                StartCoroutine(BeAttack1());
            }
            else
            {
                Player.instance.AtkAnima();
                BeAttack(Player.instance.atk);
            }

            //  Debug.Log("player");
        }
        //被技能攻击到
        else if (other.name == "skill")
        {
            // Debug.LogError("skillKill");
            StartCoroutine(SkillDead());
        }
    }
    // public void SetBuildTime(string time)
    // {
    //     buildTime.text = time;
    // }

    //BOSS怪特殊显示逻辑
    public void SetLife(float life)
    {
        if (life > 0)
        {
            buildTime.text = life.ToString();
        }
        else
        {
            buildTime.text = "KO";
        }
    }
    //需要两次攻击的敌人（以后根据需求更改）
    IEnumerator BeAttack1()
    {
        BeAttack(Player.instance.atk);
        Player.instance.AtkAnima();
        SetLife(life);
        yield return new WaitForSeconds(0.3f);
        BeAttack(Player.instance.atk);
        Player.instance.AtkAnima();
        SetLife(life);
    }
    //需要三次攻击的敌人（以后根据需求更改）
    IEnumerator BeAttack2()
    {
        BeAttack(Player.instance.atk);
        Player.instance.AtkCombo();
        SetLife(life);
        buildTime.transform.DOScale(new Vector3(1.5f,1.5f,1.5f),0.2f).From();
        yield return new WaitForSeconds(0.3f);
        BeAttack(Player.instance.atk);
        Player.instance.AtkCombo2();
        SetLife(life);
        buildTime.transform.DOScale(new Vector3(1.5f,1.5f,1.5f),0.2f).From();
        yield return new WaitForSeconds(0.3f);
        BeAttack(Player.instance.atk);
        Player.instance.AtkCombo();
        SetLife(life);
        buildTime.transform.DOScale(new Vector3(1.5f,1.5f,1.5f),0.2f).From();
    }

    //被技能打死
    IEnumerator SkillDead()
    {
        kuang.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        atkEffect.Play();
        skeletonAnime.AnimationState.SetAnimation(0, "Attacked_1", false);
        Graphs.BattleSceneGraphWorker.instance.score += score;
        Graphs.BattleSceneGraphWorker.instance.shootDown += shootDown;
        Graphs.BattleUIGraphWorker.instance.CallLua("ChangeShootDown");
        myCollider.enabled = false;
        Destroy(this.gameObject, 1f);
    }
    
}