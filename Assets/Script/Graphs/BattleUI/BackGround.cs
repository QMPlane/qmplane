﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BackGround : MonoBehaviour {
    /// <summary>
    /// 背景图移动速度
    /// </summary>
    public float speed = 0.1f;
    /// <summary>
    /// 背景材质球
    /// </summary>
    Material material;
    float y;
    public float Y
    {
        get { return y; }
        set { y = value; }
    }
    void Start () {
        material = GetComponent<Image>().material;
        
    }

    void Update()
    {
        Y += Time.deltaTime * speed;
        //给shader里对应的字段赋值（下面任意一个方法都可以）
        //material.SetTextureOffset("_MainTex",new Vector2 (0,Y));
        material.mainTextureOffset = new Vector2(Y, 0);
    }
     void OnDisable() {
        material.mainTextureOffset = new Vector2(0,0);
    }
}

