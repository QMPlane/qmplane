﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePool
{
    private GameObject _prefab;
    private GameObject _selfPool;
    private List<GameObject> activeList;
    private List<GameObject> inactiveList;
    private float useInterval = 10;
    
    public void Init(GameObject prefab, Transform parent, int preloadCount,bool autoDestroy)
    {
        activeList = new List<GameObject>(preloadCount);
        inactiveList = new List<GameObject>(preloadCount);
        _prefab = prefab;
        _selfPool = new GameObject(prefab.name + "Pool");
        _selfPool.transform.SetParent(parent);
        Preload(preloadCount);

    }
    //从队列里拿出可用的预设
    public void Spawn()
    {
        GameObject go = null;
        if (activeList.Count > 0)
        {
            go = activeList[0];
            activeList.Remove(go); 
                   
        }
        else
        {
            go = SpawnNew();
            inactiveList.Add(go);
        }
    }
    private void Preload(int count)
    {
        GameObject temp = null;
        for (int i = 0; i < count; i++)
        {
            temp = SpawnNew();
            inactiveList.Add(temp);
            temp.SetActive(false);
        }
    }
    //生成实例
    public GameObject SpawnNew()
    {
        return GameObject.Instantiate(_prefab, _selfPool.transform);
    }
}
