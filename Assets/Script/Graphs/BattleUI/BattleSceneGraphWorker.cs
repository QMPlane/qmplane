﻿using Sacu.Factory.Worker;
using Sacu.Utils;
using Google.Producer.Events;
using org.jiira.protobuf;
using Sacu.Collection;

using UnityEngine.UI;
using UnityEngine;

using System.Collections;
using Datas;
using System.Collections.Generic;
using System;

namespace Graphs
{
    public class BattleSceneGraphWorker : SAGraphWorker
    {
        public static BattleSceneGraphWorker instance;


        STLevels level;

        public AudioSource musicSource;
        //进入战斗场景前的音乐
        public string lastAudioClip;
        public List<string[]> stageInfo = new List<string[]>();
        public Dictionary<int, MapModel.MapData> map = new Dictionary<int, MapModel.MapData>();
        // Start is called before the first frame update
        int currentStep = 0;
        public static float timer;
        Vector3[] posList = new Vector3[4];
        public Dictionary<int, int> passiveSkillList = new Dictionary<int, int>();
        public int currentStage;
        public int activitySkill;
        //主角ID
        public int charaterID;
        // public int maxHP;
        // public int curHP;
        // public int playerATK;
        //游戏结束判断
        public bool gameEnd;
        //是否为胜利
        public bool isWin;
        //击坠数
        public int shootDown;
        //获得分数
        public int score;
        //第一次进入游戏时候的血量
        public int firstCurrentHP;
        //角色当前所在行数0,1,2,3
        public int playerPos;
        override protected void init()
        {
            base.init();
            map = MapModel.Instance.stageData;
            instance = this;
            shootDown = 0;
            score = 0;
            timer = 0;
            // Debug.Log(posList.Length);
            //初始化主角四个移动位置
            posList[0] = new Vector3(10, 4.5f, 0);
            posList[1] = new Vector3(10, 2, 0);
            posList[2] = new Vector3(10, -1, 0);
            posList[3] = new Vector3(10, -4, 0);
            musicSource = Camera.main.GetComponent<AudioSource>();
            lastAudioClip = musicSource.clip.name;
            //Debug.LogError(currentStage);

            //Debug.LogError(stageInfo.Count);
        }
        override protected void onRegister()
        {
            base.onRegister();
            addEventDispatcherWithHandle(CommandCollection.Sock + ProtoTypeEnum.SBaseReward, getRewardHandler);
            // addEventDispatcherWithHandle(CommandCollection.Sock + ProtoTypeEnum.SSingleUpdate, getWin);
        }
        override protected void onStart(System.Object args)
        {
            base.onStart(args);
            //打开该模块时候获取到关卡所需要的数据
            BattleNeedInfo info = args as BattleNeedInfo;
            currentStage = info.stageID;
            charaterID = info.charaterID;
            GetSkillBuff();
            Graphs.BattleUIGraphWorker.instance.GetMyTalk();
            Graphs.BattleUIGraphWorker.instance.FindEmojiPlayer();
            Player.instance.maxHP = info.maxHP;
            Player.instance.curHP = info.curHP;
            firstCurrentHP = info.curHP;
            Init();
            ReStart();
        }

        //posList[int.Parse(stageInfo[currentStep][2])].position
        private void Update()
        {
            //暂停功能
            if (Input.GetKeyDown(KeyCode.P))
            {
                BattleUIGraphWorker.instance.CallLua("TimeOut");
            }
        }
        void FixedUpdate()
        {
            //重新开始功能
            if (Input.GetKeyDown(KeyCode.R))
            {
                ReStart();
            }
            timer += Time.fixedDeltaTime;
            //生成敌人逻辑
            if (currentStep < stageInfo.Count)
            {
                if (timer >= float.Parse(stageInfo[currentStep][1]) / 1000 && gameEnd == false)
                {
                    // Debug.LogError(currentStep);
                    // Debug.LogError("敌人   "+stageInfo[currentStep][0]);
                    // Debug.LogError("时间   "+stageInfo[currentStep][1]);
                    // Debug.LogError("位置   "+int.Parse(stageInfo[currentStep][2]) );

                    GameObject go = SACache.getResWithName<GameObject>("Model/BattlePrefabs/Enemy/Enemy_" + stageInfo[currentStep][0], "prefab");

                    GameObject enemy = Instantiate(go, posList[int.Parse(stageInfo[currentStep][2]) - 1], Quaternion.identity, GameObject.Find("EnemyList").transform) as GameObject;
                    enemy.AddComponent<Enemy>().Init(STEnemy.getMap()[int.Parse(stageInfo[currentStep][0])]);
                    StartCoroutine("HighLight", int.Parse(stageInfo[currentStep][2]));
                    // enemy.GetComponent("Enemy").SendMessage("SetBuildTime",(currentStep) + " _ " + stageInfo[currentStep][1]);

                    currentStep += 1;
                }
            }
            else
            {
                //胜利判断逻辑
                //Debug.Log("PlayerHP             "  + Player.instance.curHP);
                if (currentStep >= stageInfo.Count && Player.instance.curHP > 0 && gameEnd == false && musicSource.isPlaying == false)
                {
                    if (GameObject.FindGameObjectsWithTag("Enemy").Length <= 0)
                    {
                        Graphs.BattleUIGraphWorker.instance.SetAmojiAndTalk(6);
                        gameEnd = true;
                        isWin = true;
                        Win();
                    }
                }
            }

        }
        override protected void onRemove()
        {
            base.onRemove();
            removeEventDispatcher(CommandCollection.Sock + ProtoTypeEnum.SBaseReward);
            // removeEventDispatcher(CommandCollection.Sock + ProtoTypeEnum.SSingleUpdate);
        }
        //左侧坐标高亮
        IEnumerator HighLight(int pos)
        {
            //Debug.LogError(pos);
            BattleUIGraphWorker.instance.CallLua("ShowPosHighLight", pos);
            //callLuaFun("ShowPosHighLight",pos);
            yield return new WaitForSeconds(2);
            BattleUIGraphWorker.instance.CallLua("ClosePosHighLight", pos);
            //callLuaFun("ClosePosHighLight",pos);
        }


        //解析关卡ID，敌人，坐标
        public void Init()
        {
            stageInfo.Clear();
            // Debug.Log(STLevels.getMap().Count + "      +++++++++++++");
            level = STLevels.getMap()[currentStage];
            // Debug.Log(level.Name + "      -------------");
            // Debug.Log(level.SOF[0] + "      =================");
            //foreach(string a in level.SOF){
            for (int i = 0; i < level.SOF.Length; ++i)
            {
                string a = level.SOF[i];
                stageInfo.Add(a.Split(':'));
                // Debug.Log(stageInfo[i][1] + "      -------------------");
                // Debug.Log(stageInfo[i][2] + "      -------------------");
            }
        }
        //暂停音乐
        public void PauseMusic()
        {
            musicSource.Pause();
        }
        //继续音乐
        public void ContinueMusic()
        {
            musicSource.UnPause();
        }
        //重新开始
        public void ReStart()
        {
            Graphs.BattleUIGraphWorker.instance.PlayStartAnima();
            Graphs.BattleUIGraphWorker.instance.CloseText();
            //Graphs.BattleUIGraphWorker.instance.SetAmojiAndTalk(0);
            musicSource.clip = SACache.getAudioClipWithName("Sound/LevelBGM/" + STLevels.getMap()[currentStage].Bgm);
            musicSource.loop = false;
            musicSource.Play();
            shootDown = 0;
            score = 0;
            gameEnd = false;
            isWin = false;
            Player.instance.isDead = false;
            Player.instance.skill.gameObject.SetActive(false);
            Player.instance.skillIsCD = false;
            Player.instance.useSkill = false;

            GameObject[] enemyList = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (GameObject go in enemyList)
            {
                Destroy(go);
            }
            timer = 0;
            Player.instance.gameObject.SetActive(true);
            Player.instance.RePos();
            Player.instance.box.enabled = true;
            Player.instance.curHP = firstCurrentHP;
            playerPos = 0;
            BattleUIGraphWorker.instance.CallLua("ChangeLife");
            BattleUIGraphWorker.instance.CallLua("ChangeShootDown");
            //BattleUIGraphWorker.instance.CallLua("Move2");
            currentStep = 0;

        }
        //根据好感度获取加成（需要修改为策划表改版后的）
        public void GetSkillBuff()
        {
            passiveSkillList.Clear();
            //服务器取消息
            SGetCharacterList.Builder CharacterData = (SGetCharacterList.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SGetCharacterList);
            IList<int> CharacterIDList = CharacterData.IdsList;
            IList<int> CharacterFeelList = CharacterData.FeelsList;

            for (int i = 0; i < CharacterIDList.Count; i++)
            {
                passiveSkillList.Add(CharacterIDList[i], CharacterFeelList[i]);
            }
            if (passiveSkillList.ContainsKey(charaterID))
            {
                // Debug.LogError(charaterID + "计算主角好感buff");
                //等待策划修改
                if (passiveSkillList[charaterID] > 100)
                {
                    Player.instance.maxHP += 10;
                }
                if (passiveSkillList[charaterID] > 150)
                {
                    Player.instance.maxHP += 10;
                }
            }

        }
        public void GetActiveSkill(int characterid)
        {
            //  STCharacter.getMap()[characterid].Skill1
        }
        public void Win()
        {

            Debug.LogError("发送胜利接口");
            CBaseReward.Builder cBaseReward = (CBaseReward.Builder)CommandCollection.getDataModel(ProtoTypeEnum.CBaseReward);
            cBaseReward.Level = Graphs.MapUIGraphWorker.Instance.nextCell;
            cBaseReward.Score = score;
            cBaseReward.IsWin = true;
            cBaseReward.Type = STLevels.getMap()[currentStage].Type;
            cBaseReward.ShootDown = shootDown;
            cBaseReward.Power = Player.instance.curHP;
            //战斗胜利接口
            Datas.SocketDataWorker wokerStageReward = (Datas.SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            wokerStageReward.sendMessage(ProtoTypeEnum.CBaseReward, cBaseReward.Build().ToByteArray());
        }
        public void Failed()
        {

            Debug.LogError("发送失败接口");
            CBaseReward.Builder cBaseReward = (CBaseReward.Builder)CommandCollection.getDataModel(ProtoTypeEnum.CBaseReward);
            cBaseReward.Level = Graphs.MapUIGraphWorker.Instance.nextCell;
            cBaseReward.Score = score;
            cBaseReward.IsWin = false;
            cBaseReward.Type = STLevels.getMap()[currentStage].Type;
            cBaseReward.ShootDown = shootDown;
            cBaseReward.Power = Player.instance.curHP;
            // cBaseReward.Level = 6202;
            // cBaseReward.Score = 10;
            // cBaseReward.IsWin = false;
            // cBaseReward.Type = 1;
            // cBaseReward.ShootDown = 1;
            // cBaseReward.Power = 200;
            //战斗失败接口
            Datas.SocketDataWorker wokerStageReward = (Datas.SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            wokerStageReward.sendMessage(ProtoTypeEnum.CBaseReward, cBaseReward.Build().ToByteArray());
        }
        private void getRewardHandler(FactoryEvent action)
        {
            Debug.LogError("监听奖励" + currentStage);
            SASocketDataDAO sdd = (SASocketDataDAO)action.Body;
            SBaseReward.Builder reward = (SBaseReward.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SBaseReward, sdd.bytes);
            Debug.LogError("道具" + reward.RewardID);
            if (reward.IsWin == false)
            {
                Debug.LogError("失败" + currentStage);
                StartCoroutine(WaitFailed());
                StartCoroutine(Graphs.BattleUIGraphWorker.instance.FailedShowText());

            }
            else
            {
                MapModel.Instance.stageData[currentStage].clear = true;
                for (int i = 0; i < map[currentStage].openStage.Count; i++)
                {
                    map[map[currentStage].openStage[i]].isLock = false;
                    GameObject.Find(map[currentStage].openStage[i].ToString()).transform.GetChild(1).gameObject.SetActive(false);
                }
                Debug.LogError("胜利" + currentStage);
                StartCoroutine(WaitWin());
                StartCoroutine(Graphs.BattleUIGraphWorker.instance.WinShowText());
            }

        }
        // public void getWin(FactoryEvent action){
        //       Updates sdd = (Updates)action.Body;
        //     Debug.LogError(sdd.ID + "      update");
        // }
        //延迟弹出失败界面逻辑
        IEnumerator WaitFailed()
        {
            yield return new WaitForSeconds(2);
            dispatchEvent("isFailed");
            yield return new WaitForSeconds(1);
            Player.instance.gameObject.SetActive(false);
            Graphs.BattleUIGraphWorker.instance.PlayOverAnima();
            Graphs.BattleUIGraphWorker.instance.CallLua("ShowFailedBtn");
        }
        //延迟弹出胜利界面逻辑
        IEnumerator WaitWin()
        {
            yield return new WaitForSeconds(2);
            dispatchEvent("isWin");
            yield return new WaitForSeconds(1);
            Player.instance.gameObject.SetActive(false);
            Graphs.BattleUIGraphWorker.instance.PlayOverAnima();
        }
        //退出战斗时音乐变回主界面设定音乐
        public void ReturnMusic()
        {

            musicSource.clip = SACache.getAudioClipWithName("Sound/Bgm/" + lastAudioClip.Replace("cn_", ""));
            // Debug.LogError("Sound/Bgm/" + lastAudioClip.Replace("cn_",""));
            musicSource.loop = true;
            musicSource.Play();
        }
    }
}
