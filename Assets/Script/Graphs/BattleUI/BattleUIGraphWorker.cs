﻿using Sacu.Factory.Worker;
using Sacu.Utils;
using Google.Producer.Events;
using org.jiira.protobuf;
using Sacu.Collection;

using UnityEngine.UI;
using UnityEngine;

using System.Collections;
using Datas;
using System.Collections.Generic;
using System;
using Spine.Unity;
namespace Graphs
{
    public class BattleUIGraphWorker : SAGraphWorker
    {
        public static BattleUIGraphWorker instance;
        public SkeletonGraphic winSpine;
        public SkeletonGraphic failedSpine;

        //bg为镜头放大时候下方建筑放大用
        GameObject bg;
        //是否为攻击BOSS时候镜头放大状态
        public bool isCloseUp = false;
        Vector3 bigCloseUp = new Vector3(1.4f, 1.4f, 1.4f);
        //镜头初始坐标
        Vector3 orginPos = new Vector3(0, 0, -10);
        // Vector3 cameraCloseUp = new Vector3(-1, 1, -10);

        //打BOSS时候镜头放大需要调整位置的4个坐标
        Vector3[] cameraPosList = new Vector3[4];
        float timer;
        //当前相机size大小，并非scale
        float curCameraSize;
        List<string> myTalkList = new List<string>();
        //左上角主角表情动画
        SkeletonGraphic emojiPlayer;
        Text talkText;
        Text talkTextShadow;
        public Text winScoreText;
        public GameObject winScroeObj;

        public Text winShootDownText;
        public Text failedShootDownText;
        //入场动画
        Animator uiAnimator;
        public void CallLua(string fun, int a)
        {
            callLuaFun(fun, a);
        }
        public void CallLua(string fun)
        {
            callLuaFun(fun);
        }
        public void WinAnima()
        {
            winSpine.AnimationState.SetAnimation(0, "Vectory_start", false);
            winSpine.AnimationState.AddAnimation(0, "Vectory_continue", true, 0);
        }
        public void FailedAnima()
        {
            failedSpine.AnimationState.SetAnimation(0, "Failed_start", false);
            failedSpine.AnimationState.AddAnimation(0, "Failed_continue", true, 0);
        }
        override protected void init()
        {
            base.init();
            instance = this;
            cameraPosList[0] = new Vector3(-1, 1, -10);
            cameraPosList[1] = new Vector3(-1, 1, -10);
            cameraPosList[2] = new Vector3(-1, 0, -10);
            cameraPosList[3] = new Vector3(-1, -0.5f, -10);

            //Debug.LogError(stageInfo.Count);
        }
        override protected void onRegister()
        {
            base.onRegister();

        }
        override protected void onRemove()
        {
            base.onRemove();

        }
        override protected void onStart(System.Object args)
        {
            base.onStart(args);
            winSpine = transform.Find("WinPanel/WinSpine").GetComponent<SkeletonGraphic>();
            failedSpine = transform.Find("FailedPanel/FailedSpine").GetComponent<SkeletonGraphic>();
            bg = transform.Find("bg/moveBuildding").gameObject;
            talkText = transform.Find("Top/talk/talkText").GetComponent<Text>();
            talkTextShadow = transform.Find("Top/talk/talkShadow").GetComponent<Text>();

            winScoreText = transform.Find("WinPanel/score/scoreText").GetComponent<Text>();
            winScroeObj = transform.Find("WinPanel/score").gameObject;
            winShootDownText = transform.Find("WinPanel/shootDownText").GetComponent<Text>();
            failedShootDownText = transform.Find("FailedPanel/shootDownText").GetComponent<Text>();
            uiAnimator = GetComponent<Animator>();
            timer = 0;
            curCameraSize = Camera.main.orthographicSize;
        }
        public void PlayStartAnima(){
            uiAnimator.SetInteger("AnimaInt",1);
           // StartCoroutine(WaitS());
        }
        public void PlayTalkAnima(){
            uiAnimator.SetInteger("AnimaInt",2);
            
        }
        public void PlayOverAnima(){
            uiAnimator.SetInteger("AnimaInt",3);
            
        }
        IEnumerator WaitS(){
            yield return new WaitForSeconds(0.1f);
            uiAnimator.SetInteger("AnimaInt",0);
        }
        public void FindEmojiPlayer()
        {
            emojiPlayer = transform.Find("Top/Icon/Emoji_" + Graphs.BattleSceneGraphWorker.instance.charaterID).GetComponent<SkeletonGraphic>();
            // Debug.LogError(emojiPlayer.name);
        }
        //左上角主角表情spine
        public void SetAmojiAndTalk(int id)
        {
            //0是入场
            //1击败5个/30个敌人/60个
            //2击败15个/45个/85个
            //3击败高等级敌人
            //4被击
            //5大招
            //6胜利
            //7失败
            PlayTalkAnima();
            switch (id)
            {
                case 0:
                    talkText.text = myTalkList[id];
                    talkTextShadow.text = myTalkList[id];
                    emojiPlayer.AnimationState.SetAnimation(0, "talk", false);
                    emojiPlayer.AnimationState.AddAnimation(0, "idel", true, 0);
                    StartCoroutine(WaitS());
                    break;
                case 1:
                    talkText.text = myTalkList[id];
                    talkTextShadow.text = myTalkList[id];
                    emojiPlayer.AnimationState.SetAnimation(0, "talk", false);
                    emojiPlayer.AnimationState.AddAnimation(0, "idel", true, 0);
                    StartCoroutine(WaitS());
                    break;
                case 2:
                    talkText.text = myTalkList[id];
                    talkTextShadow.text = myTalkList[id];
                    emojiPlayer.AnimationState.SetAnimation(0, "talk", false);
                    emojiPlayer.AnimationState.AddAnimation(0, "idel", true, 0);
                   StartCoroutine(WaitS());
                    break;
                case 3:
                    talkText.text = myTalkList[id];
                    talkTextShadow.text = myTalkList[id];
                    emojiPlayer.AnimationState.SetAnimation(0, "talk", false);
                    emojiPlayer.AnimationState.AddAnimation(0, "idel", true, 0);
                   StartCoroutine(WaitS());
                    break;
                case 4:
                    talkText.text = myTalkList[id];
                    talkTextShadow.text = myTalkList[id];
                    emojiPlayer.AnimationState.SetAnimation(0, "injured", false);
                    emojiPlayer.AnimationState.AddAnimation(0, "idel", true, 0);
                    StartCoroutine(WaitS());
                    break;
                case 5:
                    talkText.text = myTalkList[id];
                    talkTextShadow.text = myTalkList[id];
                    emojiPlayer.AnimationState.SetAnimation(0, "rage", false);
                    emojiPlayer.AnimationState.AddAnimation(0, "idel", true, 0);
                   StartCoroutine(WaitS());
                    break;
                case 6:
                    talkText.text = myTalkList[id];
                    talkTextShadow.text = myTalkList[id];
                    emojiPlayer.AnimationState.SetAnimation(0, "talk", false);
                    emojiPlayer.AnimationState.AddAnimation(0, "idel", true, 0);
                    StartCoroutine(WaitS());
                    break;
                case 7:
                    // Debug.LogError("7");
                    // Debug.LogError(myTalkList[id]);
                    talkText.text = myTalkList[id];
                    talkTextShadow.text = myTalkList[id];
                    emojiPlayer.AnimationState.SetAnimation(0, "sad", false);
                    emojiPlayer.AnimationState.AddAnimation(0, "idel", true, 0);
                    StartCoroutine(WaitS());
                    break;
                default:
                    break;
            }
        }
        //镜头拉近
        public void CloseUp()
        {
            bg.transform.localScale = bigCloseUp;
            bg.transform.localPosition = new Vector3(0, -1360, 0);
            Camera.main.orthographicSize = curCameraSize - 2;
            Camera.main.transform.position = cameraPosList[Graphs.BattleSceneGraphWorker.instance.playerPos];

        }
        //镜头复位
        public void CloseDown()
        {
            bg.transform.localScale = Vector3.one;
            Camera.main.orthographicSize = curCameraSize;
            Camera.main.transform.position = orginPos;
            bg.transform.localPosition = new Vector3(0, -960, 0);
        }
        //左上角主角头像谈话（读表）
        public void GetMyTalk()
        {
            myTalkList.Clear();
            if (Graphs.BattleSceneGraphWorker.instance.charaterID == 1001)
            {
                for (int i = 0; i < 8; i++)
                {
                    myTalkList.Add(STBattleTalk.getMap()[14000 + i].Talk);
                }
            }
            else if (Graphs.BattleSceneGraphWorker.instance.charaterID == 1002)
            {
                for (int i = 0; i < 8; i++)
                {
                    myTalkList.Add(STBattleTalk.getMap()[14008 + i].Talk);
                }
            }
            else if (Graphs.BattleSceneGraphWorker.instance.charaterID == 1003)
            {
                for (int i = 0; i < 8; i++)
                {
                    myTalkList.Add(STBattleTalk.getMap()[14016 + i].Talk);
                }
            }
            else if (Graphs.BattleSceneGraphWorker.instance.charaterID == 1004)
            {
                for (int i = 0; i < 8; i++)
                {
                    myTalkList.Add(STBattleTalk.getMap()[14024 + i].Talk);
                }
            }
            else if (Graphs.BattleSceneGraphWorker.instance.charaterID == 1005)
            {
                for (int i = 0; i < 8; i++)
                {
                    myTalkList.Add(STBattleTalk.getMap()[14032 + i].Talk);
                }
            }
            else if (Graphs.BattleSceneGraphWorker.instance.charaterID == 1006)
            {
                for (int i = 0; i < 8; i++)
                {
                    myTalkList.Add(STBattleTalk.getMap()[14040 + i].Talk);
                }
            }
            else
            {
                Debug.LogError("不存在该角色");
            }
            // foreach(string a in myTalkList){
            //     Debug.LogError(a);
            // }
        }
        public void CloseText(){
            winShootDownText.gameObject.SetActive(false);
            failedShootDownText.gameObject.SetActive(false);
        }
        //延迟显示胜利界面击坠和分数
        public IEnumerator WinShowText()
        {
            yield return new WaitForSeconds(6);
            winShootDownText.gameObject.SetActive(true);
            winShootDownText.text = Graphs.BattleSceneGraphWorker.instance.shootDown.ToString();
            yield return new WaitForSeconds(1);
            winScroeObj.SetActive(true);
            winScoreText.text = Graphs.BattleSceneGraphWorker.instance.score.ToString();
        }
        public IEnumerator FailedShowText()
        {
            yield return new WaitForSeconds(5);
            failedShootDownText.gameObject.SetActive(true);
            failedShootDownText.text = Graphs.BattleSceneGraphWorker.instance.shootDown.ToString();
        }
        // public void Update()
        // {
        //     if (isCloseUp == true)
        //     {
        //         timer += Time.deltaTime * 5;
        //     }
        //     else
        //     {
        //         timer -= Time.deltaTime * 10;
        //     }
        //     if (timer > 1)
        //     {
        //         timer = 1;
        //     }
        //     else if (timer < 0)
        //     {
        //         timer = 0;
        //     }
        //     CloseUp(timer);
        // }
    }
}


