﻿using Sacu.Factory.Worker;
using Sacu.Utils;
using Google.Producer.Events;
using org.jiira.protobuf;
using Datas;
using System.Diagnostics;

namespace Graphs
{
    public class ErrorCodeGraphWorker : SAGraphWorker
    {
        override protected void init()
        {
            base.init();

        }
        override protected void onRegister()
        {
            base.onRegister();
            addEventDispatcherWithHandle(CommandCollection.Sock + ProtoTypeEnum.SError, errorCodeHandler);

        }

        override protected void onRemove()
        {
            base.onRemove();
            removeEventDispatcher(CommandCollection.Sock + ProtoTypeEnum.SUserData);
        }
        private void errorCodeHandler(FactoryEvent action)
        {
            SASocketDataDAO sdd = (SASocketDataDAO)action.Body;
            SError.Builder error = (SError.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SError, sdd.bytes);
            ErrorCodeEnum erro = (ErrorCodeEnum)error.Code;
            SAUtils.Console("错误码：" + error.Code);//枚举error的枚举数字
            SAUtils.Console("错误码：" + (ErrorCodeEnum)error.Code);
            switch (erro)
            {
                case ErrorCodeEnum.SocketDisconnectError:
                case ErrorCodeEnum.UserNameOrPassWordError:
                case ErrorCodeEnum.AccountNotFoundError:
                case ErrorCodeEnum.NickNameExistError:
                case ErrorCodeEnum.AccountIsFoundError:  
                case ErrorCodeEnum.AccountCreateError: 
                case ErrorCodeEnum.AccountError:
                case ErrorCodeEnum.RepeatLoginError:
                case ErrorCodeEnum.AccountOfflineError:
                case ErrorCodeEnum.BalanceIsNotEnoughError:
                case ErrorCodeEnum.DataBaseError:
                case ErrorCodeEnum.DataTableError:
                case ErrorCodeEnum.OutOfIndexError:
                case ErrorCodeEnum.CharacterDataError:
                case ErrorCodeEnum.CharacterNotFoundError:
                case ErrorCodeEnum.LackOfPartError:
                case ErrorCodeEnum.PartToMysqlError:
                case ErrorCodeEnum.LackOfKeyError:
                case ErrorCodeEnum.BuyError:
                case ErrorCodeEnum.ReceiveError:
                case ErrorCodeEnum.LackOfItemError:
                case ErrorCodeEnum.IdentityError:
                case ErrorCodeEnum.OnlineTimeOut:
                case ErrorCodeEnum.HookError:
                    Sacu.Utils.SAManager.Instance.sendMessageToFactory("QNSActiveUIFactory", ActionCollection.SError + ActionCollection.Open, error.Code);//将枚举的数字传过去
                    break;
                default:
                    break;
            }
        }
    }
}
