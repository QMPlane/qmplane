﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Xml;
using System;
using System.IO;
using System.Diagnostics;
using System.Net;
using Utils.Thread.LoadFile;
using System.Threading;
using UnityEngine.UI;

using System.ComponentModel;

using org.jiira.protobuf;

using Sacu.Utils;
using Sacu.Collection;
using Google.Producer.Events;
using Sacu.Factory.Worker;
using Utils;

using UnityEngine.Networking;
using System.Security.Cryptography;
using XLua;

namespace Graphs
{
    public class AssetBundleRemoteGraphWorker : SAGraphWorker
    {
        
        private const string MD5_NAME = "VersionMD5.xml";
        //数据缓存
        protected Dictionary<string, string> version;
        protected Dictionary<string, string> newVersion;
        protected long time;
        private string localFilePath;

        private XmlDocument convertXML;
        private Dictionary<string, string>.Enumerator resListEN;
        //消息对象
        protected FactoryEvent factoryEvent;
        protected ProgressData progressData;

        //private long total;//总
        //private long current;//当前
        //private byte[] fileBytes;

        /// <summary>
        /// UI
        /// </summary>

        // 进度条
        protected Slider slider;
        protected Slider slider2;

        private AssetBundle[] abs;
        private int abs_position;

        private Text LoadingDesc;

        private const float MaxSlider = 100;

        private Thread td;
        private MD5CryptoServiceProvider md5Generator;
        /// <summary>
        /// 加载类前期跳过lua初始化,等程序加载完毕再进行lua初始化
        /// </summary>
        override protected void callLuaFun(string funName, System.Object value = null)
        {
            
        }
        protected override void init()
        {
            Application.runInBackground = false;
            md5Generator = new MD5CryptoServiceProvider();
            children = transform.GetComponentsInChildren<Transform>();
            len = children.Length;
            //这里初始化Lua
            originName = patchName.Replace(SAAppConfig.PrefabUIDir, "");
            luaName = displayName.Substring(displayName.LastIndexOf(".") + 1);
            setActive(false);//初始化之后
            HideVec3 = SAManager.Instance.HideVec3;
            IsNew = false;
            if (_start)
            {
                IsMainStart = true;
            }
            //ui对象引用
            slider = getComponentForGameObjectName<Slider>("ContentRoot/SliderBar");
            slider.minValue = 0;
            slider.maxValue = MaxSlider;

            slider2 = getComponentForGameObjectName<Slider>("ContentRoot/SliderBar2");
            slider2.minValue = 0;
            slider2.maxValue = MaxSlider;

            progressData = new ProgressData();
            factoryEvent = new FactoryEvent(SAACollection.PROGRESS, progressData);
            progressData.current = progressData.total = 1;

            LoadingDesc = getComponentForGameObjectName<Text>("ContentRoot/LoadingDesc");
        }
        protected override void onStart(System.Object args)
        {
            base.onStart(args);
            if (true || !PlayerPrefs.HasKey("resources_version") || PlayerPrefs.GetInt("resources_version") != SAGameVersion.Instance.ioc_version)
            {//更新资源
                refreshLocalVersionInfo();
            } else
            {
                dispatchEvent(SAACollection.REFRESH + ActionCollection.LocalFile + SAACollection.COMPLETE);//更新完成
            }
                
            //initSocket();//临时直接初始化socket
        }
        protected override void onRegister()
        {
            base.onRegister();
        }

        protected override void onRemove()
        {
            base.onRemove();
            //removeEvent();
        }
        override protected void luaValidate(string type)
        {

        }
        /// <summary>
        /// 更新本地版本信息缓存
        /// </summary>
        protected void refreshLocalVersionInfo()
        {
            //获取当前(旧的)版本
            version = getXmlToDictionary(SALang.getLocalXMLWithName(MD5_NAME).DocumentElement.ChildNodes);
            //加载远程(最新)版本信息
            addEventDispatcherWithHandle(SAACollection.REFRESH + ActionCollection.LocalFile + SAACollection.COMPLETE, updateLocalFileComplete);//资源加载完成
            //加载
            if (SAAppConfig.UIRelease)
            {
                addEventDispatcherWithHandle(SAACollection.REFRESH + ActionCollection.LocalFile, updateLocalFile);//资源加载
                StartCoroutine(OnUpdateResource());
            }
            else {
                dispatchEvent(SAACollection.REFRESH + ActionCollection.LocalFile + SAACollection.COMPLETE);//更新完成
            }
        }
        private Dictionary<string, string> getXmlToDictionary(XmlNodeList nodeList)
        {
            Dictionary<string, string> temp = new Dictionary<string, string>();
            XmlNode node;
            for (int i = 0; i < nodeList.Count; i++)
            {
                node = nodeList[i];
                if (node.Name.Equals("File"))
                {
                    temp.Add(node.Attributes.GetNamedItem("FileName").Value, node.Attributes.GetNamedItem("MD5").Value);
                }
            }
            return temp;
        }
        IEnumerator OnUpdateResource()
        {
            string random = SAAppConfig.ConfigRelease ? "?" + DateTime.Now.ToString("yyyymmddhhmmss") : "";
            string versionFile = SAGameVersion.asset_path + SAAppConfig.LanguagePlatform + "/" + MD5_NAME + random;
            UnityWebRequest www = UnityWebRequest.Get(versionFile);
            progressData.info = "更新资源文件...";
            progressData.name = "Resources";
            dispatchEvent(SAACollection.REFRESH + ActionCollection.LocalFile);
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                SAUtils.Console(www.error + ":" + versionFile);
                yield break;
            }
            convertXML = SALang.stringConvertXML(www.downloadHandler.text);//等待写入
            newVersion = getXmlToDictionary(convertXML.DocumentElement.ChildNodes);

            resListEN = newVersion.GetEnumerator();
            progressData.total = newVersion.Count;
            dispatchEvent(SAACollection.REFRESH + ActionCollection.LocalFile);

            addEventDispatcherWithHandle(SAACollection.REFRESH + ActionCollection.LocalFileComplement, downloadComplete);//资源加载完毕
            td = new Thread(download);
            td.Start();
        }

        private void download()
        {
            if (resListEN.MoveNext())
            {
                string fileKey = resListEN.Current.Key;
                localFilePath = Path.Combine(SAAppConfig.RemotePath, fileKey);
                bool isExist = File.Exists(localFilePath);//判断本地文件


                progressData.info = "";
                int idx = fileKey.LastIndexOf(".");
                if (idx != -1)
                {
                    progressData.name = fileKey.Substring(0, idx);
                }
                idx = progressData.name.LastIndexOf(".");
                if (idx != -1)
                {
                    progressData.name = progressData.name.Substring(idx + 4);
                }
                ++progressData.current;
                dispatchEvent(SAACollection.REFRESH + ActionCollection.LocalFile);
                if (isExist)
                {
                    FileStream file = new FileStream(localFilePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                    byte[] hash = md5Generator.ComputeHash(file);
                    string strMD5 = BitConverter.ToString(hash);
                    isExist = strMD5.Equals(resListEN.Current.Value);
                    file.Close();
                    //isExist = version.ContainsKey(fileKey) && version[fileKey].Equals(resListEN.Current.Value);//貌似没意义了
                    if (!isExist)
                    {
                        File.Delete(localFilePath);
                    }
                }
                if (!isExist)//需要下载
                {
                    //loadFileWithName(fileKey);
                    string remoteFilePath = SAGameVersion.asset_path + fileKey;

                    WebClient www = new WebClient();
                    www.DownloadProgressChanged += new DownloadProgressChangedEventHandler(downProgress);
                    www.DownloadFileCompleted += new AsyncCompletedEventHandler(downComplement);
                    SAUtils.Console(remoteFilePath);
                    string fileName = Path.Combine(SAAppConfig.RemotePath, resListEN.Current.Key);
                    SALang.MakDirValid(fileName.Substring(0, fileName.LastIndexOf('/')));
                    www.DownloadFileAsync(new Uri(remoteFilePath), fileName);
                }
                else
                {
                    download();
                }
            } else
            {
                progressData.info = "更新完毕";
                dispatchEvent(SAACollection.REFRESH + ActionCollection.LocalFileComplement);
                //downloadComplete();
            }
        }
        private void downProgress(object sender, DownloadProgressChangedEventArgs e)
        {
            progressData.sc = e.ProgressPercentage;
            progressData.info = slider2.value + "%";
            dispatchEvent(SAACollection.REFRESH + ActionCollection.LocalFile);
        }
        private void downComplement(object sender, AsyncCompletedEventArgs e)
        {
            progressData.info = "100%";
            dispatchEvent(SAACollection.REFRESH + ActionCollection.LocalFile);
            download();
        }
        private void updateLocalFile(FactoryEvent e)
        {
            slider2.value = progressData.sc;
            //有UI后在这里控制显示进度、内容等
            LoadingDesc.text = progressData.name + "(" + progressData.info + "), 总进度 : " + progressData.current + "/" + progressData.total;
            slider.value = progressData.current;
            slider.maxValue =  progressData.total;
        }
        private void downloadComplete(FactoryEvent e)
        {
            SAUtils.Console("可以进入游戏..."); 
            removeEventDispatcher(SAACollection.REFRESH + ActionCollection.LocalFileComplement);
            StopAllCoroutines();
            SALang.MakDirValid(SAAppConfig.RemotePath);

            convertXML.Save(Path.Combine(SAAppConfig.RemotePath, MD5_NAME));//写入本地最新XML
            version = newVersion;
            //资源缓存
            SACache.LoadLuaPackage(SAAppConfig.RemotePath, SAAppConfig.LanguagePlatform + "/" + SAAppConfig.LuaPathName);//lua 缓存
            //unity资源
            abs = new AssetBundle[(version.Count - 3) / 2 + 2];//Platform Platform.manifest lua.bytes other.manifest
            abs_position = 0;
            //shader
            loadAssetBundleManifest(SAAppConfig.LanguagePlatform + "s");
            //资源
            loadAssetBundleManifest(SAAppConfig.LanguagePlatform);
            for (int i = 0; i < abs.Length; ++i)
            {
                if (null != abs[i])
                {
                    abs[i].Unload(false);
                    abs[i] = null;
                }
            }
            SACache.getResDictionary(SAACollection.AtlasStr).Clear();

            abs_position = 0;
            removeEventDispatcher(SAACollection.REFRESH + ActionCollection.LocalFile);//资源加载
            dispatchEvent(SAACollection.REFRESH + ActionCollection.LocalFile + SAACollection.COMPLETE);//更新完成
        }
        private void loadAssetBundleManifest(string dir)
        {
            string _dir = Path.Combine(SAAppConfig.RemotePath, dir) + "/" + dir;
            if (File.Exists(_dir))
            {
                AssetBundle ab = AssetBundle.LoadFromFile(_dir);//加载依赖文件
                AssetBundleManifest abm = ab.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
                abs[abs_position++] = ab;
                dependLoad(abm, dir, abm.GetAllAssetBundles());
            }
        }

        /**
         * 如果依赖环形 则会死循环(理论不会出现)
         */
        private void dependLoad(AssetBundleManifest abm, string dir, string[] depends)
        {
            for (int i = 0; i < depends.Length; ++i)
            {
                string[] cdepends = abm.GetAllDependencies(depends[i]);
                for (int j = 0; j < cdepends.Length; ++j)
                {
                    dependLoad(abm, dir, cdepends);
                }
                addDependToCache(depends[i], dir);
            }
        }

        private void addDependToCache(string dependName, string dir)
        {
            int dot = dependName.LastIndexOf('.');
            string contrast = dependName.Substring(0, dot);//缓存名称
            string cacheName = dependName.Substring(0, dependName.IndexOf('.'));//缓存集合
            
            Dictionary<string, System.Object> assetCache = SACache.getResDictionary(cacheName);
            if (null != assetCache && !assetCache.ContainsKey(contrast))
            {
                AssetBundle ab = AssetBundle.LoadFromFile(Path.Combine(SAAppConfig.RemotePath, dir + "/" +dependName));//加载依赖文件
                if (null != ab)
                {
                    dependName = SAAppConfig.LocalDevDir.ToLower() + dependName.Substring(0, dot).Replace(".", "/") + dependName.Substring(dot);
                    abs[abs_position++] = ab;
                    dot = contrast.IndexOf(".") + 1;
                    string tempContrast = contrast.Substring(dot);
                    if (SAAppConfig.CacheModel ||
                        (cacheName.Equals(SAACollection.UIStr) && tempContrast.Length >= SAACollection.ComminLen && tempContrast.Substring(0, SAACollection.ComminLen).Equals(SAACollection.CommonStr)))
                    {
                        assetCache[contrast] = ab.LoadAsset(dependName);
                    }
                    else if (cacheName.Equals(SAACollection.ShaderStr))
                    {
                        string[] names = ab.GetAllAssetNames();
                        string tempKey;
                        string tempName;
                        int tempKeyBegin;
                        for (int i = 0; i < names.Length; ++i)
                        {
                            tempName = names[i];
                            tempKeyBegin = tempName.LastIndexOf("/") + 1;
                            tempKey = tempName.Substring(tempKeyBegin, tempName.Length - tempKeyBegin - SAACollection.MatSuffixLen);
                            assetCache[tempKey] = ab.LoadAsset(tempName);
                        }
                    }
                } else
                {
                    SAUtils.Console("无效加载资源:" + dependName);
                }
            }
        }
        
        void OnApplicationPause(bool pause)
        { 
            //SAUtils.Console(pause?"停止":"运行");
            if(pause){
                time= DateTime.Now.Ticks;
            }
            else{
                time = (DateTime.Now.Ticks - time)/10000;
                //SAUtils.Console("经过"+time+"毫秒");
            }
        }
        
        /// <summary>
        /// 资源加载完成
        /// </summary>
        /// <param name="e"></param>
        private void updateLocalFileComplete(FactoryEvent e)
        {
            removeEventDispatcher(SAACollection.REFRESH + ActionCollection.LocalFile + SAACollection.COMPLETE);//资源加载完成
            //SAManager.Instance.startFactory("ConsoleUIFactory");
            //初始化 AssetBundleRemoteLua
            string localFilePath = Path.Combine(SAAppConfig.LuaRelease?SAAppConfig.RemoteLuaDir : SAAppConfig.DevLuaDir, originName + ".lua");
            bool isExist = File.Exists(localFilePath);//判断本地文件
            if (isExist)
            {
                luaEnv = new LuaEnv();
                luaEnv.AddLoader(LuaFile);
                luaEnv.DoString("require '" + localFilePath + "'");
                //SAUtils.Console(localFilePath);
                //################以下按照流程顺序补齐lua操作
                base.callLuaFun(SAACollection.luaNew, this);//初始化
                base.callLuaFun(SAACollection.luaRegister);//注册
                base.callLuaFun(SAACollection.luaStart);//启动
                //内部自动处理加载关闭以及接下来的执行任务
                base.callLuaFun(SAACollection.luaRegisterComplete);//启动完毕
            }
            SAUtils.Console("AssetBundleFactory complete");
            Sacu.Utils.SAManager.Instance.startFactory("QNSActiveUIFactory");
           /* AudioClip iconAtlasStarFeeter = (AudioClip)SACache.getAudioClipWithName("music/11007");
            SAUtils.Console(iconAtlasStarFeeter.name);*/
            /*UnityEngine.U2D.SpriteAtlas iconAtlasStarFeeter = (UnityEngine.U2D.SpriteAtlas)SACache.getResWithName("Textures/Daily/Music/Bug1Atlas", "spriteatlas");
            Sprite b = iconAtlasStarFeeter.GetSprite("board_2");
            SAUtils.Console(b.name);*/
        }
    }
}
