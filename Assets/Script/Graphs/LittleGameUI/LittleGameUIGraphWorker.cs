﻿using Sacu.Factory.Worker;
using Sacu.Utils;
using Google.Producer.Events;
using org.jiira.protobuf;
using Sacu.Collection;

using UnityEngine.UI;
using UnityEngine;

using System.Collections;
using Datas;
using System.Collections.Generic;
using Spine.Unity;

namespace Graphs
{
    public class LittleGameUIGraphWorker : SAGraphWorker
    {
        public static LittleGameUIGraphWorker instance;
        public SkeletonGraphic failedSpine;

        STLevels level;
        string lastAudioClip;
        AudioSource musicSource;
        public int currentStage;

        public int charaterID;
        // public int maxHP;
        // public int curHP;
        // public int playerATK;
        public bool gameEnd;
        public int score = 0;
        public float timer = 0;
        private Transform parent;
        //水管1坐标位置
        Vector2[] pipe1PosList = new Vector2[3];
        //水管2坐标位置
        Vector2[] pipe2PosList = new Vector2[4];
        override protected void init()
        {
            base.init();
            instance = this;
            score = 0;
            timer = 0;

            musicSource = Camera.main.GetComponent<AudioSource>();
            musicSource.Play();
            //初始化坐标位置
            pipe1PosList[0] = new Vector2(0, 330);
            pipe1PosList[1] = new Vector2(0, 20);
            pipe1PosList[2] = new Vector2(0, -260);
            pipe2PosList[0] = new Vector2(0, 520);
            pipe2PosList[1] = new Vector2(0, 210);
            pipe2PosList[2] = new Vector2(0, -110);
            pipe2PosList[3] = new Vector2(0, -440);
            lastAudioClip = musicSource.clip.name;
        }
        override protected void onRegister()
        {
            base.onRegister();
            addEventDispatcherWithHandle(CommandCollection.Sock + ProtoTypeEnum.SBaseReward, getRewardHandler);
            // addEventDispatcherWithHandle(CommandCollection.Sock + ProtoTypeEnum.SSingleUpdate, getWin);
        }
        override protected void onStart(System.Object args)
        {
            base.onStart(args);
            BattleNeedInfo info = args as BattleNeedInfo;
            currentStage = info.stageID;
            charaterID = info.charaterID;

            MiniPlayer.instance.maxHP = info.maxHP;
            MiniPlayer.instance.curHP = info.curHP;

            failedSpine = transform.Find("FailedPanel/FailedSpine").GetComponent<SkeletonGraphic>();
            parent = transform.Find("pipePos").transform;

            ReStart();
        }

        //posList[int.Parse(stageInfo[currentStep][2])].position
        void FixedUpdate()
        {
            if (gameEnd == false)
            {
                timer += Time.deltaTime;
                //Debug.LogError(1.5 - TestMini.instance.score / 50.0f);
                if (timer > (1.5 - score / 50.0f))
                {
                    int a = Random.Range(1, 4);
                    GameObject go = Instantiate(SACache.getResWithName<GameObject>("Model/BattlePrefabs/LittleGame/Pipe" + a, "prefab"), Vector3.zero, Quaternion.identity, parent);

                    if (a == 1)
                    {
                        go.transform.localPosition = pipe1PosList[Random.Range(0, 3)];
                    }
                    else if (a == 2)
                    {
                        go.transform.localPosition = pipe2PosList[Random.Range(0, 4)];
                    }
                    else
                    {
                        go.transform.localPosition = Vector2.zero;
                    }
                    Destroy(go, 4f);
                    //Debug.LogError(go.transform.position);
                    //go.transform.SetParent(parent);
                    timer = 0;
                }
            }
        }
        override protected void onRemove()
        {
            base.onRemove();
            removeEventDispatcher(CommandCollection.Sock + ProtoTypeEnum.SBaseReward);
            // removeEventDispatcher(CommandCollection.Sock + ProtoTypeEnum.SSingleUpdate);
        }

        public void ReStart()
        {
            musicSource.clip = SACache.getAudioClipWithName("Sound/LevelBGM/" + STLevels.getMap()[currentStage].Bgm);
            musicSource.loop = false;
            musicSource.Play();
            gameEnd = false;
            MiniPlayer.instance.isDead = false;
            GameObject[] enemyList = GameObject.FindGameObjectsWithTag("Enemy");
            if (enemyList.Length > 0)
            {
                foreach (GameObject go in enemyList)
                {
                    Destroy(go);
                }
            }
            timer = 0;
            score = 0;
            MiniPlayer.instance.gameObject.SetActive(true);
            MiniPlayer.instance.gameObject.transform.position = MiniPlayer.instance.posList[0].position;
            MiniPlayer.instance.curHP = 200;
            CallLua("ChangeLife");
            CallLua("ChangeScore");
            CallLua("Move1");


        }


        // public void Win()
        // {
        //     Debug.LogError("发送胜利接口");
        //     CBaseReward.Builder cBaseReward = (CBaseReward.Builder)CommandCollection.getDataModel(ProtoTypeEnum.CBaseReward);
        //     cBaseReward.Level = Graphs.MapUIGraphWorker.Instance.nextCell;
        //     cBaseReward.Score = score;
        //     cBaseReward.IsWin = true;
        //     cBaseReward.Type = STLevels.getMap()[currentStage].Type;
        //     cBaseReward.ShootDown = 0;
        //     cBaseReward.Power = MiniPlayer.instance.curHP;
        //     //战斗胜利接口
        //     Datas.SocketDataWorker wokerStageReward = (Datas.SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
        //     wokerStageReward.sendMessage(ProtoTypeEnum.CBaseReward, cBaseReward.Build().ToByteArray());
        // }
        public void Failed()
        {
            Debug.LogError("发送失败接口");
            CBaseReward.Builder cBaseReward = (CBaseReward.Builder)CommandCollection.getDataModel(ProtoTypeEnum.CBaseReward);
            cBaseReward.Level = Graphs.MapUIGraphWorker.Instance.nextCell;
            cBaseReward.Score = score;
            cBaseReward.IsWin = false;
            cBaseReward.Type = STLevels.getMap()[currentStage].Type;
            cBaseReward.ShootDown = 0;
            cBaseReward.Power = MiniPlayer.instance.curHP;
            // cBaseReward.Level = 6202;
            // cBaseReward.Score = 10;
            // cBaseReward.IsWin = false;
            // cBaseReward.Type = 1;
            // cBaseReward.ShootDown = 1;
            // cBaseReward.Power = 200;
            //战斗失败接口
            Datas.SocketDataWorker wokerStageReward = (Datas.SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            wokerStageReward.sendMessage(ProtoTypeEnum.CBaseReward, cBaseReward.Build().ToByteArray());
        }
        private void getRewardHandler(FactoryEvent action)
        {
            Debug.LogError("监听奖励" + currentStage);
            SASocketDataDAO sdd = (SASocketDataDAO)action.Body;
            SBaseReward.Builder reward = (SBaseReward.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SBaseReward, sdd.bytes);
            Debug.LogError("道具" + reward.RewardID);
            if (reward.IsWin == false)
            {
                Debug.LogError("失败" + currentStage);
                StartCoroutine(WaitFailed());

            }
            else
            {
                // MapModel.Instance.stageData[currentStage].clear = true;
                // for (int i = 0; i < map[currentStage].openStage.Count; i++)
                // {
                //     map[map[currentStage].openStage[i]].isLock = false;
                //     GameObject.Find(map[currentStage].openStage[i].ToString()).transform.GetChild(1).gameObject.SetActive(false);
                // }
                // Debug.LogError("胜利" + currentStage);
                // dispatchEvent("isWin");
            }

        }
        // public void getWin(FactoryEvent action){
        //       Updates sdd = (Updates)action.Body;
        //     Debug.LogError(sdd.ID + "      update");
        // }
        IEnumerator WaitFailed()
        {
            yield return new WaitForSeconds(3);
            GameObject[] enemyList = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (GameObject go in enemyList)
            {
                Destroy(go);
            }
            dispatchEvent("isFailed");
        }
        public void CallLua(string fun)
        {
            callLuaFun(fun);
        }
        //播放小游戏失败界面spine动画
        public void FailedAnima()
        {
            failedSpine.AnimationState.SetAnimation(0, "Start", false);
            failedSpine.AnimationState.AddAnimation(0, "Continu", true, 0);
        }
        public void ReturnMusic()
        {

            musicSource.clip = SACache.getAudioClipWithName("Sound/Bgm/" + lastAudioClip.Replace("cn_", ""));
            // Debug.LogError("Sound/Bgm/" + lastAudioClip.Replace("cn_",""));
            musicSource.loop = true;
            musicSource.Play();
        }
    }
}
