﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sacu.Utils;
public class TestMini : MonoBehaviour
{
    public static TestMini instance;
    public int score = 0;
    public float timer = 0;
    private Transform parent;
    Vector2[] pipe1PosList = new Vector2[3];
    Vector2[] pipe2PosList = new Vector2[4];
    
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        parent = GameObject.Find("pipePos").transform;
        pipe1PosList[0] = new Vector2(0, 330);
        pipe1PosList[1] = new Vector2(0, 20);
        pipe1PosList[2] = new Vector2(0, -260);
        pipe2PosList[0] = new Vector2(0, 520);
        pipe2PosList[1] = new Vector2(0, 210);
        pipe2PosList[2] = new Vector2(0, -110);
        pipe2PosList[3] = new Vector2(0, -440);
    }
    
    // Update is called once per frame
    void Update()
    {
        
        timer += Time.deltaTime;
        //Debug.LogError(1.5 - TestMini.instance.score / 50.0f);
        if(timer > (1.5 - TestMini.instance.score / 50.0f)){
            int a = Random.Range(1,4);
            GameObject go =   Instantiate(SACache.getResWithName<GameObject>("Model/BattlePrefabs/LittleGame/Pipe" + a , "prefab"),Vector3.zero,Quaternion.identity,parent);
            
            if(a == 1){
                go.transform.localPosition = pipe1PosList[Random.Range(0,3)];
            }else if(a == 2){
                go.transform.localPosition = pipe2PosList[Random.Range(0,4)];
            }else{
                go.transform.localPosition = Vector2.zero;
            }
            Destroy(go,4f);
            //Debug.LogError(go.transform.position);
            //go.transform.SetParent(parent);
            timer= 0;
        }
    }
}
