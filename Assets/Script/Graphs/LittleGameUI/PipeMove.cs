﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeMove : MonoBehaviour
{
    private float speed = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
        transform.Translate(Vector2.left *Time.deltaTime * (speed + Graphs.LittleGameUIGraphWorker.instance.score / 20.0f));
        //Debug.LogError(TestMini.instance.score / 50.0f);
    }
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Player"){
            // Debug.LogError("加分");
            Graphs.LittleGameUIGraphWorker.instance.score += 1;
            Graphs.LittleGameUIGraphWorker.instance.CallLua("ChangeScore");
        }
    }
}
