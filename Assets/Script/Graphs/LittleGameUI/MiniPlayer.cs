﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using DG.Tweening;
public class MiniPlayer : MonoBehaviour
{
    // private static Player instance;
    // public static Player Instance{
    //     get{
    //         if(instance == null){
    //             instance = new Player();
    //         }
    //         return instance;
    //     }
    // }
    public static MiniPlayer instance;
    public Transform[] posList;
    public int maxHP;
    public int curHP;
    bool startMove1 = false;
    bool startMove2 = false;
    bool startMove3 = false;
    bool startMove4 = false;
    float dis = 0;
    float speed = 0.1f;
    public int atk = 1;
    public bool isDead = false;
    public ParticleSystem stopMoveEffect;
    SkeletonGraphic skeletonAnime;

    void Awake()
    {
        instance = this;
        skeletonAnime = GetComponentInChildren<SkeletonGraphic>();

    }
    // Start is called before the first frame update
    void Start()
    {
        // skeletonAnime.AnimationState.SetAnimation(0, "Move_1", false);
        // skeletonAnime.AnimationState.AddAnimation(0, "Fly", true,0);
        // maxHP = Graphs.BattleSceneGraphWorker.instance.maxHP;
        // curHP = Graphs.BattleSceneGraphWorker.instance.curHP;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (startMove1)
        {

            // transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, posList[0].position.y, 0), dis / speed * Time.deltaTime);
            if (Vector3.Distance(transform.position, new Vector3(transform.position.x, posList[0].position.y, 0)) <= 0.01f)
            {

                stopMoveEffect.gameObject.SetActive(true);
                startMove1 = false;
                dis = 0;
                // Debug.LogError("over1");
                skeletonAnime.AnimationState.SetAnimation(0, "Fly", true);
            }
            // else
            // {
            //     skeletonAnime.AnimationState.SetAnimation(0, "Move_1", false);
            // }
        }
        else if (startMove2)
        {
            // transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, posList[1].position.y, 0), dis / speed * Time.deltaTime);
            if (Vector3.Distance(transform.position, new Vector3(transform.position.x, posList[1].position.y, 0)) <= 0.01f)
            {
                // Debug.LogError("Move2");
                stopMoveEffect.gameObject.SetActive(true);
                startMove2 = false;
                dis = 0;
                // Debug.LogError("over2");
                skeletonAnime.AnimationState.SetAnimation(0, "Fly", true);
            }
            // else
            // {
            //     skeletonAnime.AnimationState.SetAnimation(0, "Move_1", false);
            // }
        }
        else if (startMove3)
        {
            // transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, posList[2].position.y, 0), dis / speed * Time.deltaTime);
            if (Vector3.Distance(transform.position, new Vector3(transform.position.x, posList[2].position.y, 0)) <= 0.01f)
            {

                stopMoveEffect.gameObject.SetActive(true);
                startMove3 = false;
                dis = 0;
                // Debug.LogError("over3");
                skeletonAnime.AnimationState.SetAnimation(0, "Fly", true);
            }
            // else
            // {
            //     skeletonAnime.AnimationState.SetAnimation(0, "Move_1", false);
            // }
        }
        else if (startMove4)
        {
            // transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x, posList[3].position.y, 0), dis / speed * Time.deltaTime);
            if (Vector3.Distance(transform.position, new Vector3(transform.position.x, posList[3].position.y, 0)) <= 0.01f)
            {

                stopMoveEffect.gameObject.SetActive(true);
                startMove4 = false;
                dis = 0;
                // Debug.LogError("over4");
                skeletonAnime.AnimationState.SetAnimation(0, "Fly", true);
            }
            // else
            // {
            //     skeletonAnime.AnimationState.SetAnimation(0, "Move_1", false);
            // }
        }
    }
    public void Move1()
    {
        dis = Vector3.Distance(transform.position, posList[0].position);

        startMove1 = true;
        transform.DOMove(new Vector3(transform.position.x,posList[0].position.y,0), 0.1f);
    }
    public void Move2()
    {
        dis = Vector3.Distance(transform.position, posList[1].position);

        startMove2 = true;
        transform.DOMove(new Vector3(transform.position.x,posList[1].position.y,0), 0.1f);
    }
    public void Move3()
    {
        dis = Vector3.Distance(transform.position, posList[2].position);

        startMove3 = true;
        transform.DOMove(new Vector3(transform.position.x,posList[2].position.y,0), 0.1f);
    }
    public void Move4()
    {
        dis = Vector3.Distance(transform.position, posList[3].position);

        startMove4 = true;
        transform.DOMove(new Vector3(transform.position.x,posList[3].position.y,0), 0.1f);
    }
    public void BeAttack(int enemyAtk)
    {

        if (curHP > enemyAtk)
        {
            curHP -= enemyAtk;
        }
        else
        {
            if (isDead == false)
                Dead();
        }
        Graphs.LittleGameUIGraphWorker.instance.CallLua("ChangeLife");
    }
    void Dead()
    {
        isDead = true;
        curHP = 0;
        gameObject.SetActive(false);
        Graphs.LittleGameUIGraphWorker.instance.gameEnd = true;
        Graphs.LittleGameUIGraphWorker.instance.Failed();
        Graphs.LittleGameUIGraphWorker.instance.CallLua("ChangeLife");

    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        // Debug.LogError(other.gameObject.name);
        if (other.gameObject.tag == "Fire")
        {
            this.gameObject.SetActive(false);
            //    Debug.LogError("Dead");
            Dead();
        }
    }

}
