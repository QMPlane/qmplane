﻿using Sacu.Factory.Worker;
using Sacu.Utils;
using Google.Producer.Events;
using org.jiira.protobuf;
using Datas;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;


using System.Collections.Generic;
using System;
using DG.Tweening;
using System.Collections;

namespace Graphs
{
    public class QNSActiveUIGraphWorker : SAGraphWorker
    {
        
        private SAManager sm;
        public GameObject QNS;
        public GameObject Frame;
        public GameObject BGIma;
        private GameObject Frame_Error;
        private GameObject FrameItem;
        private SpriteAtlas ItemAtlas;
        //弹窗的几种类型1.QNS
        //            2.道具弹窗直接显示道具的图标和数量  点击确定后弹窗消失
        //            3.升级之后的弹窗  显示属性
        //            4.音乐的弹窗
        //            5.是否进入？

        public int QNSId;//QNS的ID
        private int ItemId;//Item的ID
        private int ItemCount;//Item的数量
        override protected void init()
        {
            base.init();
            addEventDispatcherWithHandle(ActionCollection.QNSActive + ActionCollection.Open, GetID);
            addEventDispatcherWithHandle(ActionCollection.ItemActive + ActionCollection.Open, GetUp);
            addEventDispatcherWithHandle(ActionCollection.FeelTxt + ActionCollection.Open, ActiveFrameTxt);
            addEventDispatcherWithHandle(ActionCollection.QNSGets + ActionCollection.Open, GetStr);
            addEventDispatcherWithHandle(ActionCollection.ShootDownDont + ActionCollection.Open, GetStr);
            addEventDispatcherWithHandle(ActionCollection.SError + ActionCollection.Open, ActiveError);
            ItemAtlas = SACache.getSpriteAtlasWithName("Atlas/ItemAtlas");
            sm = SAManager.Instance;
            QNS = getGameObjectByTypeName("Panel", "Image").gameObject;
            Frame = getGameObjectByTypeName("Frame", "Image").gameObject;
            BGIma = getGameObjectByTypeName("BGIma", "Image").gameObject;
            FrameItem = getGameObjectByTypeName("Frame_Item", "Image").gameObject;
            Frame_Error = getGameObjectForName("Frame_Error");
        }
        override protected void onRegister()
        {
            base.onRegister();
        }

        override protected void onRemove()
        {
            base.onRemove();
            removeEventDispatcher(ActionCollection.QNSActive + ActionCollection.Open);
            removeEventDispatcher(ActionCollection.ItemActive + ActionCollection.Open);
        }
        //先从池子里拿如果没有的话就克隆   有的话直接拿
        private void ActiveError(FactoryEvent action)
        {
            
            int num = Convert.ToInt32(action.Body);
            Text ErrorTxt = (Text)getGameObjectByTypeName("Frame_Error/Text", "Text");
            ErrorTxt.text = CommandCollection.ECM[num];
            //开启协程
            StartCoroutine("cueTextFade");
        }
        IEnumerator cueTextFade()
        {
            //初始化文字，透明度为0
            //GameObject go = GamePoolS.instance.GetObjectFromPool(Frame_Error,gameObject);
            GameObject go = Instantiate<GameObject>(Frame_Error, transform);
            go.SetActive(true);
            Text ErrorTxt = go.transform.Find("Text").GetComponent<Text>();
            ErrorTxt.color = new Color(ErrorTxt.color.r, ErrorTxt.color.g, ErrorTxt.color.b, 0);
            //文字渐现
            ErrorTxt.DOFade(1f, 0.5f);
            //文字停顿1秒
            yield return new WaitForSeconds(1f);
            //文字渐隐
            ErrorTxt.DOFade(0f, 0.5f);
            //渐隐结束后销毁物体
            yield return new WaitForSeconds(0.5f);
            go.SetActive(false);
        }
        private void GetStr(FactoryEvent action)
        {

            string str = Convert.ToString(action.Body);
            gameObject.transform.SetAsLastSibling();
            BGIma.SetActive(true);
            FrameItem.SetActive(true);
            FrameItem.transform.DOScale(new Vector3(1, 1, 1), 0.5f);
            FrameItem.transform.Find("CancleBtn").GetComponent<Button>().onClick.AddListener(Close);
            FrameItem.transform.Find("CloseBtn").GetComponent<Button>().onClick.AddListener(Close);
            Image item = (Image)getGameObjectByTypeName("Frame_Item/Item1", "Image");
            item.gameObject.SetActive(false);
            Text FeelTxt = (Text)getGameObjectByTypeName("Frame_Item/FeelTxt", "Text");
            Text Txt_ = (Text)getGameObjectByTypeName("Frame_Item/Text", "Text");
            Txt_.gameObject.SetActive(false);
            FeelTxt.text = str;
            FeelTxt.gameObject.SetActive(true);
        }
        //击杀荣誉榜如果未满足条件
        public void ActiveShootDownFrameTxt(FactoryEvent action)
        {
            gameObject.transform.SetAsLastSibling();
            BGIma.SetActive(true);
            FrameItem.SetActive(true);
            FrameItem.transform.DOScale(new Vector3(1, 1, 1), 0.5f);
            FrameItem.transform.Find("CancleBtn").GetComponent<Button>().onClick.AddListener(Close);
            FrameItem.transform.Find("CloseBtn").GetComponent<Button>().onClick.AddListener(Close);
            Image item = (Image)getGameObjectByTypeName("Frame_Item/Item1", "Image");
            item.gameObject.SetActive(false);
            Text FeelTxt = (Text)getGameObjectByTypeName("Frame_Item/FeelTxt", "Text");
            Text Txt_ = (Text)getGameObjectByTypeName("Frame_Item/Text", "Text");
            Txt_.gameObject.SetActive(false);
            FeelTxt.text = "您还未满足领取条件";
            FeelTxt.gameObject.SetActive(true);
        }
        //ins或者击杀荣誉榜如果领取过显示这个弹窗
        public void ActiveQNSFrameTxt(FactoryEvent action)
        {
            gameObject.transform.SetAsLastSibling();
            BGIma.SetActive(true);
            FrameItem.SetActive(true);
            FrameItem.transform.DOScale(new Vector3(1, 1, 1), 0.5f);
            FrameItem.transform.Find("CancleBtn").GetComponent<Button>().onClick.AddListener(Close);
            FrameItem.transform.Find("CloseBtn").GetComponent<Button>().onClick.AddListener(Close);
            Image item = (Image)getGameObjectByTypeName("Frame_Item/Item1", "Image");
            item.gameObject.SetActive(false);
            Text FeelTxt = (Text)getGameObjectByTypeName("Frame_Item/FeelTxt", "Text");
            Text Txt_ = (Text)getGameObjectByTypeName("Frame_Item/Text", "Text");
            Txt_.gameObject.SetActive(false);
            FeelTxt.text = "您已经领取过奖励了，不要贪心哦";
            FeelTxt.gameObject.SetActive(true);
        }
        //抽奖的道具数不足用这个方法
        public void ActiveFrameTxt(FactoryEvent action)
        {
            gameObject.transform.SetAsLastSibling();
            BGIma.SetActive(true);
            FrameItem.SetActive(true);
            FrameItem.transform.DOScale(new Vector3(1, 1, 1), 0.5f);
            FrameItem.transform.Find("CancleBtn").GetComponent<Button>().onClick.AddListener(Close);
            FrameItem.transform.Find("CloseBtn").GetComponent<Button>().onClick.AddListener(Close);
            Image item = (Image)getGameObjectByTypeName("Frame_Item/Item1", "Image");
            item.gameObject.SetActive(false);
            Text FeelTxt = (Text)getGameObjectByTypeName("Frame_Item/FeelTxt", "Text");
            Text Txt_ = (Text)getGameObjectByTypeName("Frame_Item/Text", "Text");
            Txt_.gameObject.SetActive(false);
            FeelTxt.text = "您的齿轮数量不足！";
            FeelTxt.gameObject.SetActive(true);
        }
        #region 抽奖和ins的领取道具都是这个
        private void GetUp(FactoryEvent action)
        {
            gameObject.transform.SetAsLastSibling();

            List<int> up = new List<int>();
            up = (List<int>)action.Body;
            ItemId = up[0];
            ItemCount = up[1];
            if (ItemId > 1000 && ItemId < 1099)
            {
                Debug.Log("好感度提升");
                ActiveFrameItem_Feel();
            }
            else
            {
                ActiveFrameItem();
            }
        }
        public void ActiveFrameItem()
        {
            Text FeelTxt = (Text)getGameObjectByTypeName("Frame_Item/FeelTxt", "Text");
            FeelTxt.gameObject.SetActive(false);
            BGIma.SetActive(true);
            FrameItem.SetActive(true);
            FrameItem.transform.DOScale(new Vector3(1, 1, 1), 0.5f);

            FrameItem.transform.Find("CancleBtn").GetComponent<Button>().onClick.AddListener(Close);
            FrameItem.transform.Find("CloseBtn").GetComponent<Button>().onClick.AddListener(Close);
            Image item = (Image)getGameObjectByTypeName("Frame_Item/Item1", "Image");
            Text itemTxt = (Text)getGameObjectByTypeName("Frame_Item/Item1/Text", "Text");
            item.gameObject.SetActive(true);
            itemTxt.text = Convert.ToString(ItemCount);
            item.sprite = ItemAtlas.GetSprite("cn_" + Convert.ToString(ItemId));
            item.gameObject.SetActive(true);
        }
        public void ActiveFrameItem_Feel()
        {
            BGIma.SetActive(true);
            FrameItem.SetActive(true);
            FrameItem.transform.DOScale(new Vector3(1, 1, 1), 0.5f);
            FrameItem.transform.Find("CancleBtn").GetComponent<Button>().onClick.AddListener(Close);
            FrameItem.transform.Find("CloseBtn").GetComponent<Button>().onClick.AddListener(Close);
            Image item = (Image)getGameObjectByTypeName("Frame_Item/Item1", "Image");
            Text FeelTxt = (Text)getGameObjectByTypeName("Frame_Item/FeelTxt", "Text");
            item.gameObject.SetActive(false);
            List<STCharacter> StaticChaList = STCharacter.getList();
            string CharName;
            for (int i = 0; i < StaticChaList.Count; i++)
            {
                if (StaticChaList[i].Id == ItemId)
                {
                    CharName = StaticChaList[i].Name;
                    FeelTxt.text = "恭喜您的" + CharName + "机师的好感度提升" + Convert.ToString(ItemCount) + "点";
                    break;
                }
            }
        }
        #endregion
        private void Close()
        {
            FrameItem.transform.DOScale(new Vector3(0.1f, 0.1f, 0.1f), 0.5f);
            BGIma.SetActive(false);
            FrameItem.SetActive(false);
            
        }
        #region QNS的回调及其弹窗方法
        private void GetID(FactoryEvent action)
        {
            gameObject.transform.SetAsLastSibling();
            STInstagram up = (STInstagram)action.Body;
            QNSId = up.Id;
            ActiveFrame();
        }

        /// <summary>
        /// QNS的弹窗
        /// </summary>
        public void ActiveFrame()
        {
            Frame.SetActive(true);
            BGIma.SetActive(true);
            //BGIma.GetComponent<Image>().color = new Color(97f, 16f, 14f, 187f);
            Frame.transform.DOScale(new Vector3(1, 1, 1), 0.5f);

            Frame.transform.Find("CancleBtn").GetComponent<Button>().onClick.AddListener(ActiveQNS);
            Frame.transform.Find("CloseBtn").GetComponent<Button>().onClick.AddListener(ActiveQNS);
        }
        /// <summary>
        /// 按钮的回调函数将QNS的界面显示出来
        /// </summary>
        public void ActiveQNS()
        {
            Frame.SetActive(false);
            QNS.SetActive(true);
            QNS.transform.DOScale(new Vector3(1, 1, 1), 0.5f);
            callLuaFun("activeQNS", QNSId);
        } 
        #endregion
        public List<string> GetList(string[] s)
        {
            List<string> TxtList = new List<string>();//将静态表里的数据分割用到的表
            for (int i = 0; i < s.Length; i++)
            {
                TxtList.Add(s[i]);
            }
            return TxtList;
        }
        public string[] GetSplitStr(string s)
        {

            string[] sArray = s.Split('&');

            return sArray;
        }
    }
}

