﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sacu.Utils
{
    public class StaticTool : MonoBehaviour
    {
        private static StaticTool _instance;
        public static StaticTool Instance
        {
            get
            {
                if (null == _instance)
                {
                    _instance = new StaticTool();
                }
                return _instance;
            }
        }
        public void AddSC(GameObject go)
        {
            if (go.GetComponent<Test>() == null)
            {
                go.AddComponent<Test>();
            }
        }
        public void DeleteSC(GameObject go)
        {
            if (go.GetComponent<Test>() == null)
            {
                return;
            }
            UnityEngine.Object ob = (UnityEngine.Object)go.GetComponent<Test>();
            Destroy(ob);
        }
    }
}
