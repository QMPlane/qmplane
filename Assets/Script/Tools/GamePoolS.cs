﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePoolS : MonoBehaviour
{
    private Dictionary<string, List<GameObject>> pool;
    public static GamePoolS instance;

    void Awake()
    {
        instance = this;
    }
    void Start()
    {
        //string为预设体的name，list存入预设体生成的gameobject
        pool = new Dictionary<string, List<GameObject>>();
    }

    //取对象  定义取对象，需要什么参数，对象声明的位置和角度
    public GameObject GetObjectFromPool(GameObject objName,GameObject prefab)
    {
        //声明一个gameobject
        GameObject go;
        //判断是否包含对应的list，并且list有值，不为空
        if (pool.ContainsKey(objName.name) && pool[objName.name].Count > 0)
        {
            //取出对应的第0个元素，赋值给go
            go = pool[objName.name][0];
            //移除list中赋值给go的第0个元素
            pool[objName.name].RemoveAt(0);
            //激活该对象
            go.SetActive(true);
        }
        //不是上面的情况都无法从池子中取出gameobject 只能克隆
        else
        {
            //将克隆的对象作为预设体放在resources文件夹中
            //resources.load加载出来的对象为object类型，需要里氏转换成gameobject类型
            go = Instantiate(objName, prefab.transform);
        }
        
        //最后返回go
        return go;
    }

    //存对象
    //需要一个gameobject类型的对象镀锡做参数，不需要返回值
    public void PushobjectTopool(GameObject go)
    {
        //对象和预制体的名字不一致，采用字符串切割，将对象的名字切割成两部分，第0部分为预设体的名字。
        string prefabName = go.name.Split('(')[0];
        if (pool.ContainsKey(prefabName))
        {
            pool[prefabName].Add(go);
        }
        //如果没有对应的list
        else
        {
            //新建list并把go添加进去
            pool[prefabName] = new List<GameObject>() { go };
        }
        //存到池子中的物体隐藏
        go.SetActive(false);
    }

}
