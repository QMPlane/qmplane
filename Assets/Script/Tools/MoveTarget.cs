﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using Sacu.Utils;
using UnityEngine.UI;


    public class MoveTarget : MonoBehaviour
    {

    public float num;
    public bool IsBool = false;
    public static MoveTarget _instance;
    public GameObject a;
    public GameObject b;
    public SpriteAtlas iconAtlasBG;
    public string SpriteName;
    private void Awake()
    {
        _instance = this;
    }
    void Start()
    {
        iconAtlasBG = SACache.getSpriteAtlasWithName("Atlas/BGAtlas");
        num = 0f;
    }
    void Update()
    {
       if (IsBool)
       {
            num += 0.03f;
            a.GetComponent<Image>().material.SetFloat("_Alpha", num);
       }
       if (num > 1f)
       {
            SpriteName = a.GetComponent<Image>().sprite.name;
            string s = SpriteName.Replace("(Clone)","");
            IsBool = false;
            a.SetActive(false);
            b.GetComponent<Image>().sprite = iconAtlasBG.GetSprite(s);
            a.GetComponent<Image>().material.SetFloat("_Alpha", 0);
            num = 0f;
       }
    }
}

