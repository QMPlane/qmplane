﻿////
////  DoFadeTest.cs
////  Project: GUITest
////
////  Created by zhiheng.shao
////  Copyright  2016年 zhiheng.shao. All rights reserved.
////
////  Description

using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using DG.Tweening.RickExtension;

public class Test : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        
        this.GetComponent<Image>().DOFades(0, 2).SetLoops(-1, LoopType.Yoyo);
        //.GetComponent<Renderer>().material.DOFade(1, 1).SetLoops(-1, LoopType.Yoyo);
    }
}

namespace DG.Tweening.RickExtension
{
    public static class DOTweenExteion
    {
        public static Tweener DOFades(this Image image, float endValue, float duration)
        {
            Debug.Log("CustomDoFade");
            return DOTween.To(image.AlphaGetter, image.AlphaSetter, endValue, duration);
        }
        private static float AlphaGetter(this Image image)
        {
            return image.color.a;
        }

        private static void AlphaSetter(this Image image, float alpha)
        {
            Color oldColor = image.color;
            oldColor.a = alpha;
            image.color = oldColor;
        }
    }
}