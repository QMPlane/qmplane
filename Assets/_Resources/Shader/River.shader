﻿Shader "Custom/River"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Speed ("Speed", float) = 1.0
        _uv1 ("uv1", float) = 0.5
        _uv2 ("uv2", float) = 0.5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "Queue" = "Transparent"}
        LOD 100
        
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            float _Speed;
            float _uv1;
            float _uv2;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }
            /**
            让纹理动起来
            UV : 
            */
            fixed4 frag (v2f i) : SV_Target
            {
                float timer = _Time.y * _Speed;

                /**
                按照_MainTex纹理和Shader中_MainTex设置的比例设置到区域
                tex2D(纹理图片,对应uv坐标的比例点)
                */
                // sample the texture
                //fixed4 col = tex2D(_MainTex, i.uv);

                float2 tempUV = i.uv;
                tempUV.x -= timer;

                fixed4 col = tex2D(_MainTex, tempUV);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
