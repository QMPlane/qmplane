﻿Shader "Custom/Struct"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color ("Color", range(0, 1)) = 1
    }
    /**
    和shader1.0渲染管线相同

    语义:
    POSITION : 获取模型顶点信息
    NORMAL : 获取法线信息
    TEXCOORD(n) : 可用于从顶点着色器传递到片段着色器的精度值传递对象,n代表维度，一般用来获取UV信息
    COLOR : 可用户从顶点着色器到片段着色器的颜色值传递,值类型为 float4
    TANGENT : 获取切线信息
    SV_POSITION : 表示经过MVP矩阵已经转化到屏幕坐标的位置。
    SV_Target : 表示要输出到的目标相机 render target

    顶点着色器从FBX获取信息

    MVP
    M:物体坐标系（相对坐标系）转换到世界坐标系
    V:世界坐标系转化到相机坐标系
    P:相机坐标系转化到屏幕坐标系
    */

    SubShader
    {
        // No culling or depth
        //Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert//定义一个顶点着色器 入口函数
            #pragma fragment frag//定义一个片段着色器 入口函数

            #include "UnityCG.cginc"

            //顶点着色器输入值结构体,通过meshrender获得结构体参数
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            //顶点你着色器的输出值结构体,片段着色器的输入值结构体
            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            float _Color;


            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                // just invert the colors
                //col.rgb = 1 - col.rgb;
                return fixed4(0.5 * _Color, 0, 1, 1) + col * (1 - _Color);
            }
            ENDCG
        }
    }
}
