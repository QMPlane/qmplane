﻿Shader "Custom/AStruct"
{
    Properties
    {
        _MainTex ("Texture1", 2D) = "white" {}
    }
     
    SubShader{
        Blend SrcAlpha OneMinusSrcAlpha
        Pass
        {
            SetTexture[_MainTex]{
                combine Texture * Primary
			}
        }
    }
}
