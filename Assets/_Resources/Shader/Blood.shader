﻿Shader "Custom/Blood"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Value ("Value", float) = 2.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            float _Value;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 tmpUV = i.uv;
                float tmpLength;

                if(tmpUV.x < 0.5){//左边
                    tmpLength = length(tmpUV - float2(-0.5, 0.5));
				} else {//右边
                    tmpLength = length(float2(1.5, 0.5) - tmpUV);
				}

                tmpLength *= _Value;

                // sample the texture

                float _OffsetX1 = sin(_Time.y * 200)/200;
                //float _OffsetX2 = sin(_Time.y * 150) / 400;
                //fixed4 scol = tex2D(_MainTex, i.uv + float2(0, _OffsetX2));
                float2 uv = i.uv + float2(0, _OffsetX1);
                fixed4 col = tex2D(_MainTex, uv);
                col = lerp(col, fixed4(1, 0, 0, 1), 1 - clamp(0, 1, tmpLength))
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
