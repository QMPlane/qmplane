﻿Shader "Custom/TrembleX"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
		_OffsetX1("Offset X1", Range(-0.5, 0.5)) = 0.0
	    _OffsetX2("Offset X2", Range(-0.5, 0.5)) = 0.0
    }
    /**
    顶点着色器
        1.计算顶点位置
        2.矩阵转换
    片段着色器
        1.纹理寻址
        2.灯光计算


    */
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag
            #include "UnityCG.cginc"

            sampler2D _MainTex;
            float _OffsetX1;
            float _OffsetX2;

            fixed4 frag(v2f_img i) : SV_Target
            {
                _OffsetX1 = sin(_Time.y * 100) / 100;
                //_OffsetX2 = sin(_Time.y * 50) / 70;
                fixed4 col = fixed4(1, 1, 1, 1);
                //fixed4 scol = tex2D(_MainTex, i.uv + float2(_OffsetX2, 0));//闪屏
                float2 uv = i.uv + float2(_OffsetX1, 0);
                col.rgb = tex2D(_MainTex, uv).rgb;
                return col * 0.9;// + scol * 0.5;
            }
            ENDCG
        }
    }
}
