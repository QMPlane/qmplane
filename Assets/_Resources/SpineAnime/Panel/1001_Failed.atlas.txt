
1001_Failed.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
Anger
  rotate: true
  xy: 1615, 242
  size: 73, 71
  orig: 73, 71
  offset: 0, 0
  index: -1
Effect_link
  rotate: true
  xy: 2, 343
  size: 679, 562
  orig: 679, 562
  offset: 0, 0
  index: -1
Effect_spot
  rotate: false
  xy: 1279, 676
  size: 346, 346
  orig: 346, 346
  offset: 0, 0
  index: -1
Effect_triange
  rotate: true
  xy: 522, 48
  size: 12, 10
  orig: 12, 10
  offset: 0, 0
  index: -1
F_BG
  rotate: false
  xy: 600, 215
  size: 30, 30
  orig: 30, 30
  offset: 0, 0
  index: -1
F_word_1
  rotate: true
  xy: 1392, 307
  size: 237, 158
  orig: 237, 158
  offset: 0, 0
  index: -1
F_word_2
  rotate: false
  xy: 321, 2
  size: 369, 32
  orig: 369, 32
  offset: 0, 0
  index: -1
F_word_3
  rotate: true
  xy: 1150, 155
  size: 376, 81
  orig: 376, 81
  offset: 0, 0
  index: -1
F_word_4
  rotate: true
  xy: 568, 98
  size: 243, 30
  orig: 243, 30
  offset: 0, 0
  index: -1
F_word_6
  rotate: true
  xy: 522, 62
  size: 279, 44
  orig: 279, 44
  offset: 0, 0
  index: -1
R-ear-droop-r
  rotate: false
  xy: 637, 102
  size: 348, 424
  orig: 348, 424
  offset: 0, 0
  index: -1
R-ear-droopl-l-1
  rotate: false
  xy: 1292, 546
  size: 172, 85
  orig: 172, 85
  offset: 0, 0
  index: -1
R-ear-droopl-l-3
  rotate: true
  xy: 1552, 399
  size: 235, 179
  orig: 235, 179
  offset: 0, 0
  index: -1
R-ear-droopl-l-4
  rotate: true
  xy: 987, 133
  size: 394, 161
  orig: 394, 161
  offset: 0, 0
  index: -1
R-eye-smile
  rotate: true
  xy: 600, 247
  size: 94, 32
  orig: 94, 32
  offset: 0, 0
  index: -1
R-face-failed
  rotate: true
  xy: 566, 528
  size: 494, 354
  orig: 494, 354
  offset: 0, 0
  index: -1
R-mouth-smile
  rotate: true
  xy: 566, 343
  size: 183, 69
  orig: 183, 69
  offset: 0, 0
  index: -1
R_s_e
  rotate: true
  xy: 644, 53
  size: 47, 44
  orig: 47, 44
  offset: 0, 0
  index: -1
R_small
  rotate: false
  xy: 1263, 3
  size: 169, 156
  orig: 169, 156
  offset: 0, 0
  index: -1
R_small_e_L
  rotate: false
  xy: 922, 529
  size: 109, 96
  orig: 109, 96
  offset: 0, 0
  index: -1
R_small_e_r
  rotate: false
  xy: 1597, 44
  size: 62, 171
  orig: 62, 171
  offset: 0, 0
  index: -1
S-Body-2
  rotate: false
  xy: 1131, 9
  size: 130, 122
  orig: 141, 125
  offset: 0, 0
  index: -1
S-Leg-h-l-1
  rotate: false
  xy: 1630, 337
  size: 57, 60
  orig: 57, 60
  offset: 0, 0
  index: -1
S-Leg-l-2
  rotate: true
  xy: 1033, 533
  size: 92, 117
  orig: 92, 118
  offset: 0, 0
  index: -1
S-Tie
  rotate: false
  xy: 568, 36
  size: 74, 60
  orig: 80, 64
  offset: 0, 0
  index: -1
S-crown
  rotate: false
  xy: 1434, 15
  size: 161, 181
  orig: 161, 181
  offset: 0, 0
  index: -1
S-dress-4
  rotate: false
  xy: 1392, 198
  size: 173, 107
  orig: 173, 107
  offset: 0, 0
  index: -1
S-eye-2
  rotate: false
  xy: 1279, 633
  size: 132, 41
  orig: 132, 41
  offset: 0, 0
  index: -1
S-eye-3
  rotate: true
  xy: 1086, 5
  size: 126, 43
  orig: 126, 43
  offset: 0, 0
  index: -1
S-hair-headwear-l
  rotate: false
  xy: 922, 627
  size: 127, 395
  orig: 127, 395
  offset: 0, 0
  index: -1
S-hair-headwear-r
  rotate: true
  xy: 692, 2
  size: 98, 392
  orig: 98, 392
  offset: 0, 0
  index: -1
S-ht-s-l
  rotate: false
  xy: 1233, 161
  size: 157, 374
  orig: 157, 374
  offset: 0, 0
  index: -1
S-ht-s-r
  rotate: false
  xy: 1051, 629
  size: 226, 393
  orig: 230, 396
  offset: 4, 0
  index: -1
S-xiongbu
  rotate: true
  xy: 1237, 538
  size: 89, 53
  orig: 89, 53
  offset: 0, 0
  index: -1
S_FrontHair_2
  rotate: true
  xy: 2, 30
  size: 311, 211
  orig: 311, 211
  offset: 0, 0
  index: -1
S_Hair_L_2
  rotate: true
  xy: 1552, 317
  size: 80, 76
  orig: 80, 76
  offset: 0, 0
  index: -1
Sweat
  rotate: true
  xy: 1152, 537
  size: 90, 83
  orig: 90, 83
  offset: 0, 0
  index: -1
Tear
  rotate: true
  xy: 2, 5
  size: 23, 18
  orig: 23, 18
  offset: 0, 0
  index: -1
V_word_10
  rotate: true
  xy: 1567, 217
  size: 98, 46
  orig: 98, 46
  offset: 0, 0
  index: -1
V_word_2
  rotate: false
  xy: 173, 12
  size: 146, 16
  orig: 146, 16
  offset: 0, 0
  index: -1
V_word_6
  rotate: false
  xy: 1413, 636
  size: 191, 38
  orig: 191, 38
  offset: 0, 0
  index: -1
V_word_8.5
  rotate: false
  xy: 215, 36
  size: 305, 305
  orig: 305, 305
  offset: 0, 0
  index: -1
V_word_9
  rotate: false
  xy: 1466, 550
  size: 84, 84
  orig: 84, 84
  offset: 0, 0
  index: -1
眉毛
  rotate: false
  xy: 22, 10
  size: 149, 18
  orig: 149, 18
  offset: 0, 0
  index: -1
