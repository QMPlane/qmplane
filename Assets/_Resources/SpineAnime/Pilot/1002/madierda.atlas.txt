
madierda.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
Larm
  rotate: true
  xy: 306, 2
  size: 41, 61
  orig: 41, 61
  offset: 0, 0
  index: -1
Lfoot
  rotate: false
  xy: 643, 435
  size: 36, 75
  orig: 36, 77
  offset: 0, 0
  index: -1
Lhand
  rotate: false
  xy: 681, 437
  size: 54, 73
  orig: 54, 73
  offset: 0, 0
  index: -1
Lknife
  rotate: true
  xy: 2, 13
  size: 238, 220
  orig: 238, 220
  offset: 0, 0
  index: -1
Lleg
  rotate: true
  xy: 224, 2
  size: 41, 80
  orig: 41, 82
  offset: 0, 2
  index: -1
Lstreamer
  rotate: false
  xy: 598, 412
  size: 43, 98
  orig: 43, 98
  offset: 0, 0
  index: -1
Rarm
  rotate: false
  xy: 686, 364
  size: 39, 71
  orig: 39, 72
  offset: 0, 1
  index: -1
Rfoot
  rotate: true
  xy: 369, 4
  size: 39, 85
  orig: 39, 85
  offset: 0, 0
  index: -1
Rhand
  rotate: false
  xy: 692, 292
  size: 52, 70
  orig: 52, 70
  offset: 0, 0
  index: -1
Rknife
  rotate: true
  xy: 124, 278
  size: 232, 226
  orig: 232, 226
  offset: 0, 0
  index: -1
Rleg
  rotate: false
  xy: 638, 332
  size: 46, 78
  orig: 46, 78
  offset: 0, 0
  index: -1
Rstreamer
  rotate: false
  xy: 543, 408
  size: 53, 102
  orig: 53, 102
  offset: 0, 0
  index: -1
adorn01
  rotate: false
  xy: 124, 264
  size: 42, 12
  orig: 42, 12
  offset: 0, 0
  index: -1
body
  rotate: false
  xy: 526, 15
  size: 139, 144
  orig: 139, 144
  offset: 0, 0
  index: -1
chest
  rotate: false
  xy: 352, 280
  size: 59, 32
  orig: 59, 32
  offset: 0, 0
  index: -1
face
  rotate: true
  xy: 531, 180
  size: 132, 81
  orig: 132, 81
  offset: 0, 0
  index: -1
hair01
  rotate: false
  xy: 224, 45
  size: 221, 231
  orig: 221, 231
  offset: 0, 0
  index: -1
hair02
  rotate: false
  xy: 2, 253
  size: 120, 257
  orig: 120, 257
  offset: 0, 0
  index: -1
hair03
  rotate: true
  xy: 614, 232
  size: 85, 76
  orig: 85, 76
  offset: 0, 0
  index: -1
hair04
  rotate: true
  xy: 447, 161
  size: 151, 82
  orig: 151, 82
  offset: 0, 0
  index: -1
head
  rotate: false
  xy: 352, 314
  size: 189, 196
  orig: 189, 196
  offset: 0, 0
  index: -1
knife03
  rotate: true
  xy: 2, 3
  size: 8, 57
  orig: 8, 57
  offset: 0, 0
  index: -1
neck
  rotate: false
  xy: 614, 166
  size: 83, 64
  orig: 83, 64
  offset: 0, 0
  index: -1
skirt
  rotate: true
  xy: 456, 10
  size: 149, 68
  orig: 149, 68
  offset: 0, 0
  index: -1
skirt01
  rotate: false
  xy: 543, 319
  size: 46, 87
  orig: 46, 87
  offset: 0, 0
  index: -1
skirt02
  rotate: false
  xy: 591, 319
  size: 45, 87
  orig: 45, 87
  offset: 0, 0
  index: -1
zhuangshi
  rotate: false
  xy: 692, 233
  size: 46, 57
  orig: 46, 57
  offset: 0, 0
  index: -1
